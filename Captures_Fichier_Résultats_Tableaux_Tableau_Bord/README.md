# README #

### Le résultat et le tableau de bord sur PowerBi essentiellement et les fichier qui permettent de les réstitués

Vous trouverez ici quelques captures d'écran du tableau de bord obtenu.

Les fichiers avec l'extention .pbix vont aider à générer le tableau avec les données.

Sinon, on peut toujours reconstruire la tableau, en suivant les instructions suivante :

1. Tout d'abord ouvrir Power Bi et cliquer sur ajouter de nouvelles sources
2. On charge le fichier .xls contenant nos résultats (dw).
3. Ensuite nous avons juste a cliquer sur les champs qu'on souhaite analyser et glisser un attribut et l'utiliser comme filtre en cliquant sur l'onglet en bas à gauche de la palette.