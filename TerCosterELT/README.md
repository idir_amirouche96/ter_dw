# README #

### Manuelle d'utilisation des sources:

Le dossier contient les sources du projet et les résultats des executions en format excel.

**Très important: les idenfiant Mysql utilisés ici sont:**

**user=root_
mdp=_	!c'est le champs vide, en effet étant donné que les mots de passes sont chiffrés, je l'ai laissé à vide**

**Les fichiers Excel générer lors de l'execution sont stockés dans un répertoire de mon pc, 
/cbma/data/TOI....../.DIM...xls, il faut modifier le chemin ou le programme risque de léver une exception.**

1. La classe TRANSFORMATION_CHARGEMENT_DW.java permet de transformer une base de données du service informatique et de générer les tables de dimensions. Pour utiliser cette classe correctement, pensez à modifier les variables et les noms des tables et bases de données. Vous trouverez un exemple dans les vidéos du dossier "Videos et aides talend".

2. La seconde classe TRANS_CHARG_DW_VERSION_BIO.java effectue les mêmes opérations avec quelques méthodes qui different. Par exemple, la table "modules" du service de bio ne comporte pas de champs "grade" et "prime", d'où l'utilisation d'un deuxième job.

3. La classe FUSION_FACT_TABLES.java permet de fusionner les tables de dimensions d'une base de donnée (résultat des deux classes précédente) avec la table de notre entrepot de données. Même remarque sur le nom des bases de données utilisés. Ici, nous avons utilisés l'opération "insert ou update" pour ne pas perdre l'information.

4. Enfin, la derniere classe du projet SQL_TO_XSL.java, permet de générer un fichier xls avec les données du DW.
