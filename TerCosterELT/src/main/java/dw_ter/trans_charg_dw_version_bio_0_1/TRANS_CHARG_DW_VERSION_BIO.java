

package dw_ter.trans_charg_dw_version_bio_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;

@SuppressWarnings("unused")
/**
 * Job: TRANS_CHARG_DW_VERSION_BIO Purpose: TRANSFORMATION_CHARGEMENT_DW<br>
 * Description: Dans ce job, nous allons charger la base de données du service informatique, joindre les tables, supprimer les attributs non essentiels à notre analyse et enfin les fusionner avec notre DW <br>
 * @author belaidcherfa2012@hotmail.com
 * @version 7.0.1.20180411_1414
 * @status 
 */
public class TRANS_CHARG_DW_VERSION_BIO implements TalendJob {

	protected static void logIgnoredError(String message, Throwable cause) {
		System.err.println(message);
		if (cause != null) {
			cause.printStackTrace();
		}

	}

	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}

	private final static String defaultCharset = java.nio.charset.Charset
			.defaultCharset().name();

	private final static String utf8Charset = "UTF-8";

	// contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String, String> propertyTypes = new java.util.HashMap<>();

		public PropertiesWithType(java.util.Properties properties) {
			super(properties);
		}

		public PropertiesWithType() {
			super();
		}

		public void setContextType(String key, String type) {
			propertyTypes.put(key, type);
		}

		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}

	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();

	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties) {
			super(properties);
		}

		public ContextProperties() {
			super();
		}

		public void synchronizeContext() {

		}

	}

	private ContextProperties context = new ContextProperties();

	public ContextProperties getContext() {
		return this.context;
	}

	private final String jobVersion = "0.1";
	private final String jobName = "TRANS_CHARG_DW_VERSION_BIO";
	private final String projectName = "DW_TER";
	public Integer errorCode = null;
	private String currentComponent = "";

	private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
	private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();

	private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
	private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
	public final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();

	private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";

	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(
			java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources
				.entrySet()) {
			talendDataSources.put(
					dataSourceEntry.getKey(),
					new routines.system.TalendDataSource(dataSourceEntry
							.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap
				.put(KEY_DB_DATASOURCES_RAW,
						new java.util.HashMap<String, javax.sql.DataSource>(
								dataSources));
	}

	LogCatcherUtils talendLogs_LOGS = new LogCatcherUtils();
	StatCatcherUtils talendStats_STATS = new StatCatcherUtils(
			"_ra9vEGNxEei_x97qqSR7kA", "0.1");
	MetterCatcherUtils talendMeter_METTER = new MetterCatcherUtils(
			"_ra9vEGNxEei_x97qqSR7kA", "0.1");

	private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
	private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(
			new java.io.BufferedOutputStream(baos));

	public String getExceptionStackTrace() {
		if ("failure".equals(this.getStatus())) {
			errorMessagePS.flush();
			return baos.toString();
		}
		return null;
	}

	private Exception exception;

	public Exception getException() {
		if ("failure".equals(this.getStatus())) {
			return this.exception;
		}
		return null;
	}

	private class TalendException extends Exception {

		private static final long serialVersionUID = 1L;

		private java.util.Map<String, Object> globalMap = null;
		private Exception e = null;
		private String currentComponent = null;
		private String virtualComponentName = null;

		public void setVirtualComponentName(String virtualComponentName) {
			this.virtualComponentName = virtualComponentName;
		}

		private TalendException(Exception e, String errorComponent,
				final java.util.Map<String, Object> globalMap) {
			this.currentComponent = errorComponent;
			this.globalMap = globalMap;
			this.e = e;
		}

		public Exception getException() {
			return this.e;
		}

		public String getCurrentComponent() {
			return this.currentComponent;
		}

		public String getExceptionCauseMessage(Exception e) {
			Throwable cause = e;
			String message = null;
			int i = 10;
			while (null != cause && 0 < i--) {
				message = cause.getMessage();
				if (null == message) {
					cause = cause.getCause();
				} else {
					break;
				}
			}
			if (null == message) {
				message = e.getClass().getName();
			}
			return message;
		}

		@Override
		public void printStackTrace() {
			if (!(e instanceof TalendException || e instanceof TDieException)) {
				if (virtualComponentName != null
						&& currentComponent.indexOf(virtualComponentName + "_") == 0) {
					globalMap.put(virtualComponentName + "_ERROR_MESSAGE",
							getExceptionCauseMessage(e));
				}
				globalMap.put(currentComponent + "_ERROR_MESSAGE",
						getExceptionCauseMessage(e));
				System.err.println("Exception in component " + currentComponent
						+ " (" + jobName + ")");
			}
			if (!(e instanceof TDieException)) {
				if (e instanceof TalendException) {
					e.printStackTrace();
				} else {
					e.printStackTrace();
					e.printStackTrace(errorMessagePS);
					TRANS_CHARG_DW_VERSION_BIO.this.exception = e;
				}
			}
			if (!(e instanceof TalendException)) {
				try {
					for (java.lang.reflect.Method m : this.getClass()
							.getEnclosingClass().getMethods()) {
						if (m.getName().compareTo(currentComponent + "_error") == 0) {
							m.invoke(TRANS_CHARG_DW_VERSION_BIO.this,
									new Object[] { e, currentComponent,
											globalMap });
							break;
						}
					}

					if (!(e instanceof TDieException)) {
						talendLogs_LOGS.addMessage("Java Exception",
								currentComponent, 6, e.getClass().getName()
										+ ":" + e.getMessage(), 1);
						talendLogs_LOGSProcess(globalMap);
					}
				} catch (TalendException e) {
					// do nothing

				} catch (Exception e) {
					this.e.printStackTrace();
				}
			}
		}
	}

	public void tDBInput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileOutputExcel_1_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tUniqRow_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_1_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_5_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_5_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_2_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_5_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_8_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileOutputExcel_3_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_8_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_8_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_6_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_6_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_7_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_7_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_8_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_8_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_3_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_8_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tUniqRow_1_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_8_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_9_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_9_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_5_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_9_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_10_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_10_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_11_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_11_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_12_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_12_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_13_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_13_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_14_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_14_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_6_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_14_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_4_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_14_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileOutputExcel_4_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_14_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_15_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_15_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_16_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_16_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_19_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_19_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tMap_7_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_19_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBOutput_6_error(Exception exception, String errorComponent,
			final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_19_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileOutputExcel_5_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_19_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tFileOutputExcel_2_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tFileOutputExcel_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row10_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row12_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_3_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row11_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_4_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row7_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_5_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row2_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_6_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row3_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_7_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row30_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_8_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row8_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_9_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row6_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_10_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row5_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_11_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row15_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_12_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row14_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_13_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row18_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_15_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAdvancedHash_row19_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_16_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAggregateRow_1_AGGOUT_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		tAggregateRow_1_AGGIN_error(exception, errorComponent, globalMap);

	}

	public void tAggregateRow_1_AGGIN_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_5_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tAggregateRow_2_AGGOUT_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		tAggregateRow_2_AGGIN_error(exception, errorComponent, globalMap);

	}

	public void tAggregateRow_2_AGGIN_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		tDBInput_9_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendStats_STATS_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendStats_CONSOLE_error(exception, errorComponent, globalMap);

	}

	public void talendStats_CONSOLE_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendStats_STATS_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendLogs_LOGS_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendLogs_CONSOLE_error(exception, errorComponent, globalMap);

	}

	public void talendLogs_CONSOLE_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendLogs_LOGS_onSubJobError(exception, errorComponent, globalMap);
	}

	public void talendMeter_METTER_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		talendMeter_CONSOLE_error(exception, errorComponent, globalMap);

	}

	public void talendMeter_CONSOLE_error(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		end_Hash.put(errorComponent, System.currentTimeMillis());

		status = "failure";

		talendMeter_METTER_onSubJobError(exception, errorComponent, globalMap);
	}

	public void tDBInput_1_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_2_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_3_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_4_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_5_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_6_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_7_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_8_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_9_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_10_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_11_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_12_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_13_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_14_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_15_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_16_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tDBInput_19_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void tFileOutputExcel_2_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendStats_STATS_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendLogs_LOGS_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public void talendMeter_METTER_onSubJobError(Exception exception,
			String errorComponent, final java.util.Map<String, Object> globalMap)
			throws TalendException {

		resumeUtil.addLog("SYSTEM_LOG", "NODE:" + errorComponent, "", Thread
				.currentThread().getId() + "", "FATAL", "",
				exception.getMessage(),
				ResumeUtil.getExceptionStackTrace(exception), "");

	}

	public static class row34Struct implements
			routines.system.IPersistableRow<row34Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public Integer SK_DIM_TEMPS;

		public Integer getSK_DIM_TEMPS() {
			return this.SK_DIM_TEMPS;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime
						* result
						+ ((this.SK_DIM_TEMPS == null) ? 0 : this.SK_DIM_TEMPS
								.hashCode());

				result = prime * result + (int) this.anneedebut;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row34Struct other = (row34Struct) obj;

			if (this.SK_DIM_TEMPS == null) {
				if (other.SK_DIM_TEMPS != null)
					return false;

			} else if (!this.SK_DIM_TEMPS.equals(other.SK_DIM_TEMPS))

				return false;

			if (this.anneedebut != other.anneedebut)
				return false;

			return true;
		}

		public void copyDataTo(row34Struct other) {

			other.SK_DIM_TEMPS = this.SK_DIM_TEMPS;
			other.anneedebut = this.anneedebut;

		}

		public void copyKeysDataTo(row34Struct other) {

			other.SK_DIM_TEMPS = this.SK_DIM_TEMPS;
			other.anneedebut = this.anneedebut;

		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.SK_DIM_TEMPS = readInteger(dis);

					this.anneedebut = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.SK_DIM_TEMPS, dos);

				// int

				dos.writeInt(this.anneedebut);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("SK_DIM_TEMPS=" + String.valueOf(SK_DIM_TEMPS));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row34Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.SK_DIM_TEMPS,
					other.SK_DIM_TEMPS);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.anneedebut,
					other.anneedebut);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class ENSEIGNANT_DPTStruct implements
			routines.system.IPersistableRow<ENSEIGNANT_DPTStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codedept;

		public int getCodedept() {
			return this.codedept;
		}

		public int DIRECTEUR_DPT;

		public int getDIRECTEUR_DPT() {
			return this.DIRECTEUR_DPT;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public String NOM_PRENOM_ENS;

		public String getNOM_PRENOM_ENS() {
			return this.NOM_PRENOM_ENS;
		}

		public int codegrade;

		public int getCodegrade() {
			return this.codegrade;
		}

		public String codecourt;

		public String getCodecourt() {
			return this.codecourt;
		}

		public String nomlong;

		public String getNomlong() {
			return this.nomlong;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public int heures;

		public int getHeures() {
			return this.heures;
		}

		public int ANNNE_ENS;

		public int getANNNE_ENS() {
			return this.ANNNE_ENS;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codedept;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final ENSEIGNANT_DPTStruct other = (ENSEIGNANT_DPTStruct) obj;

			if (this.codedept != other.codedept)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(ENSEIGNANT_DPTStruct other) {

			other.codedept = this.codedept;
			other.DIRECTEUR_DPT = this.DIRECTEUR_DPT;
			other.enseignantID = this.enseignantID;
			other.NOM_PRENOM_ENS = this.NOM_PRENOM_ENS;
			other.codegrade = this.codegrade;
			other.codecourt = this.codecourt;
			other.nomlong = this.nomlong;
			other.type = this.type;
			other.heures = this.heures;
			other.ANNNE_ENS = this.ANNNE_ENS;

		}

		public void copyKeysDataTo(ENSEIGNANT_DPTStruct other) {

			other.codedept = this.codedept;
			other.enseignantID = this.enseignantID;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codedept = dis.readInt();

					this.DIRECTEUR_DPT = dis.readInt();

					this.enseignantID = dis.readInt();

					this.NOM_PRENOM_ENS = readString(dis);

					this.codegrade = dis.readInt();

					this.codecourt = readString(dis);

					this.nomlong = readString(dis);

					this.type = readString(dis);

					this.heures = dis.readInt();

					this.ANNNE_ENS = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codedept);

				// int

				dos.writeInt(this.DIRECTEUR_DPT);

				// int

				dos.writeInt(this.enseignantID);

				// String

				writeString(this.NOM_PRENOM_ENS, dos);

				// int

				dos.writeInt(this.codegrade);

				// String

				writeString(this.codecourt, dos);

				// String

				writeString(this.nomlong, dos);

				// String

				writeString(this.type, dos);

				// int

				dos.writeInt(this.heures);

				// int

				dos.writeInt(this.ANNNE_ENS);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codedept=" + String.valueOf(codedept));
			sb.append(",DIRECTEUR_DPT=" + String.valueOf(DIRECTEUR_DPT));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",NOM_PRENOM_ENS=" + NOM_PRENOM_ENS);
			sb.append(",codegrade=" + String.valueOf(codegrade));
			sb.append(",codecourt=" + codecourt);
			sb.append(",nomlong=" + nomlong);
			sb.append(",type=" + type);
			sb.append(",heures=" + String.valueOf(heures));
			sb.append(",ANNNE_ENS=" + String.valueOf(ANNNE_ENS));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(ENSEIGNANT_DPTStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codedept, other.codedept);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class ENS_DPT_BDStruct implements
			routines.system.IPersistableRow<ENS_DPT_BDStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codedept;

		public int getCodedept() {
			return this.codedept;
		}

		public int DIRECTEUR_DPT;

		public int getDIRECTEUR_DPT() {
			return this.DIRECTEUR_DPT;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public String NOM_PRENOM_ENS;

		public String getNOM_PRENOM_ENS() {
			return this.NOM_PRENOM_ENS;
		}

		public int codegrade;

		public int getCodegrade() {
			return this.codegrade;
		}

		public String codecourt;

		public String getCodecourt() {
			return this.codecourt;
		}

		public String nomlong;

		public String getNomlong() {
			return this.nomlong;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public int heures;

		public int getHeures() {
			return this.heures;
		}

		public int ANNNE_ENS;

		public int getANNNE_ENS() {
			return this.ANNNE_ENS;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codedept;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final ENS_DPT_BDStruct other = (ENS_DPT_BDStruct) obj;

			if (this.codedept != other.codedept)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(ENS_DPT_BDStruct other) {

			other.codedept = this.codedept;
			other.DIRECTEUR_DPT = this.DIRECTEUR_DPT;
			other.enseignantID = this.enseignantID;
			other.NOM_PRENOM_ENS = this.NOM_PRENOM_ENS;
			other.codegrade = this.codegrade;
			other.codecourt = this.codecourt;
			other.nomlong = this.nomlong;
			other.type = this.type;
			other.heures = this.heures;
			other.ANNNE_ENS = this.ANNNE_ENS;

		}

		public void copyKeysDataTo(ENS_DPT_BDStruct other) {

			other.codedept = this.codedept;
			other.enseignantID = this.enseignantID;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codedept = dis.readInt();

					this.DIRECTEUR_DPT = dis.readInt();

					this.enseignantID = dis.readInt();

					this.NOM_PRENOM_ENS = readString(dis);

					this.codegrade = dis.readInt();

					this.codecourt = readString(dis);

					this.nomlong = readString(dis);

					this.type = readString(dis);

					this.heures = dis.readInt();

					this.ANNNE_ENS = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codedept);

				// int

				dos.writeInt(this.DIRECTEUR_DPT);

				// int

				dos.writeInt(this.enseignantID);

				// String

				writeString(this.NOM_PRENOM_ENS, dos);

				// int

				dos.writeInt(this.codegrade);

				// String

				writeString(this.codecourt, dos);

				// String

				writeString(this.nomlong, dos);

				// String

				writeString(this.type, dos);

				// int

				dos.writeInt(this.heures);

				// int

				dos.writeInt(this.ANNNE_ENS);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codedept=" + String.valueOf(codedept));
			sb.append(",DIRECTEUR_DPT=" + String.valueOf(DIRECTEUR_DPT));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",NOM_PRENOM_ENS=" + NOM_PRENOM_ENS);
			sb.append(",codegrade=" + String.valueOf(codegrade));
			sb.append(",codecourt=" + codecourt);
			sb.append(",nomlong=" + nomlong);
			sb.append(",type=" + type);
			sb.append(",heures=" + String.valueOf(heures));
			sb.append(",ANNNE_ENS=" + String.valueOf(ANNNE_ENS));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(ENS_DPT_BDStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codedept, other.codedept);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class dim_tempsStruct implements
			routines.system.IPersistableRow<dim_tempsStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public Integer SK_DIM_TEMPS;

		public Integer getSK_DIM_TEMPS() {
			return this.SK_DIM_TEMPS;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime
						* result
						+ ((this.SK_DIM_TEMPS == null) ? 0 : this.SK_DIM_TEMPS
								.hashCode());

				result = prime * result + (int) this.anneedebut;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final dim_tempsStruct other = (dim_tempsStruct) obj;

			if (this.SK_DIM_TEMPS == null) {
				if (other.SK_DIM_TEMPS != null)
					return false;

			} else if (!this.SK_DIM_TEMPS.equals(other.SK_DIM_TEMPS))

				return false;

			if (this.anneedebut != other.anneedebut)
				return false;

			return true;
		}

		public void copyDataTo(dim_tempsStruct other) {

			other.SK_DIM_TEMPS = this.SK_DIM_TEMPS;
			other.anneedebut = this.anneedebut;

		}

		public void copyKeysDataTo(dim_tempsStruct other) {

			other.SK_DIM_TEMPS = this.SK_DIM_TEMPS;
			other.anneedebut = this.anneedebut;

		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.SK_DIM_TEMPS = readInteger(dis);

					this.anneedebut = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// Integer

				writeInteger(this.SK_DIM_TEMPS, dos);

				// int

				dos.writeInt(this.anneedebut);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("SK_DIM_TEMPS=" + String.valueOf(SK_DIM_TEMPS));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(dim_tempsStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.SK_DIM_TEMPS,
					other.SK_DIM_TEMPS);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.anneedebut,
					other.anneedebut);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row9Struct implements
			routines.system.IPersistableRow<row9Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codedept;

		public int getCodedept() {
			return this.codedept;
		}

		public String nom;

		public String getNom() {
			return this.nom;
		}

		public int directeur;

		public int getDirecteur() {
			return this.directeur;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codedept = dis.readInt();

					this.nom = readString(dis);

					this.directeur = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codedept);

				// String

				writeString(this.nom, dos);

				// int

				dos.writeInt(this.directeur);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codedept=" + String.valueOf(codedept));
			sb.append(",nom=" + nom);
			sb.append(",directeur=" + String.valueOf(directeur));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row9Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class after_tDBInput_1Struct implements
			routines.system.IPersistableRow<after_tDBInput_1Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codedept;

		public int getCodedept() {
			return this.codedept;
		}

		public String nom;

		public String getNom() {
			return this.nom;
		}

		public int directeur;

		public int getDirecteur() {
			return this.directeur;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codedept = dis.readInt();

					this.nom = readString(dis);

					this.directeur = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codedept);

				// String

				writeString(this.nom, dos);

				// int

				dos.writeInt(this.directeur);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codedept=" + String.valueOf(codedept));
			sb.append(",nom=" + nom);
			sb.append(",directeur=" + String.valueOf(directeur));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(after_tDBInput_1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_1Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_1_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				tDBInput_2Process(globalMap);
				tDBInput_8Process(globalMap);

				row9Struct row9 = new row9Struct();
				ENSEIGNANT_DPTStruct ENSEIGNANT_DPT = new ENSEIGNANT_DPTStruct();
				ENS_DPT_BDStruct ENS_DPT_BD = new ENS_DPT_BDStruct();
				dim_tempsStruct dim_temps = new dim_tempsStruct();
				row34Struct row34 = new row34Struct();

				/**
				 * [tFileOutputExcel_1 begin ] start
				 */

				ok_Hash.put("tFileOutputExcel_1", false);
				start_Hash
						.put("tFileOutputExcel_1", System.currentTimeMillis());

				currentComponent = "tFileOutputExcel_1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("ENSEIGNANT_DPT"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tFileOutputExcel_1 = 0;

				class BytesLimit65535_tFileOutputExcel_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tFileOutputExcel_1().limitLog4jByte();

				int columnIndex_tFileOutputExcel_1 = 0;

				String fileName_tFileOutputExcel_1 = "/home/cbma/Data/TOS_DI-20180411_1414-V7.0.1/workspace/DIM_ENS_DPT.xls";
				int nb_line_tFileOutputExcel_1 = 0;
				org.talend.ExcelTool xlsxTool_tFileOutputExcel_1 = new org.talend.ExcelTool();
				xlsxTool_tFileOutputExcel_1.setSheet("Sheet1");
				xlsxTool_tFileOutputExcel_1.setAppend(false, false);
				xlsxTool_tFileOutputExcel_1.setRecalculateFormula(false);
				xlsxTool_tFileOutputExcel_1.setXY(false, 0, 0, false);

				xlsxTool_tFileOutputExcel_1
						.prepareXlsxFile(fileName_tFileOutputExcel_1);

				xlsxTool_tFileOutputExcel_1.setFont("");

				if (xlsxTool_tFileOutputExcel_1.getStartRow() == 0) {

					xlsxTool_tFileOutputExcel_1.addRow();

					xlsxTool_tFileOutputExcel_1.addCellValue("codedept");

					xlsxTool_tFileOutputExcel_1.addCellValue("DIRECTEUR_DPT");

					xlsxTool_tFileOutputExcel_1.addCellValue("enseignantID");

					xlsxTool_tFileOutputExcel_1.addCellValue("NOM_PRENOM_ENS");

					xlsxTool_tFileOutputExcel_1.addCellValue("codegrade");

					xlsxTool_tFileOutputExcel_1.addCellValue("codecourt");

					xlsxTool_tFileOutputExcel_1.addCellValue("nomlong");

					xlsxTool_tFileOutputExcel_1.addCellValue("type");

					xlsxTool_tFileOutputExcel_1.addCellValue("heures");

					xlsxTool_tFileOutputExcel_1.addCellValue("ANNNE_ENS");

					nb_line_tFileOutputExcel_1++;

				}

				/**
				 * [tFileOutputExcel_1 begin ] stop
				 */

				/**
				 * [tDBOutput_1 begin ] start
				 */

				ok_Hash.put("tDBOutput_1", false);
				start_Hash.put("tDBOutput_1", System.currentTimeMillis());

				currentComponent = "tDBOutput_1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection(
								"ENS_DPT_BD" + iterateId, 0, 0);

					}
				}

				int tos_count_tDBOutput_1 = 0;

				class BytesLimit65535_tDBOutput_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBOutput_1().limitLog4jByte();

				int nb_line_tDBOutput_1 = 0;
				int nb_line_update_tDBOutput_1 = 0;
				int nb_line_inserted_tDBOutput_1 = 0;
				int nb_line_deleted_tDBOutput_1 = 0;
				int nb_line_rejected_tDBOutput_1 = 0;

				int deletedCount_tDBOutput_1 = 0;
				int updatedCount_tDBOutput_1 = 0;
				int insertedCount_tDBOutput_1 = 0;

				int rejectedCount_tDBOutput_1 = 0;

				String tableName_tDBOutput_1 = "DIM_ENS_DPT";
				boolean whetherReject_tDBOutput_1 = false;

				java.util.Calendar calendar_tDBOutput_1 = java.util.Calendar
						.getInstance();
				calendar_tDBOutput_1.set(1, 0, 1, 0, 0, 0);
				long year1_tDBOutput_1 = calendar_tDBOutput_1.getTime()
						.getTime();
				calendar_tDBOutput_1.set(10000, 0, 1, 0, 0, 0);
				long year10000_tDBOutput_1 = calendar_tDBOutput_1.getTime()
						.getTime();
				long date_tDBOutput_1;

				java.sql.Connection conn_tDBOutput_1 = null;
				String dbProperties_tDBOutput_1 = "noDatetimeStringSync=true";
				String url_tDBOutput_1 = null;
				if (dbProperties_tDBOutput_1 == null
						|| dbProperties_tDBOutput_1.trim().length() == 0) {
					url_tDBOutput_1 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ "rewriteBatchedStatements=true";
				} else {
					String properties_tDBOutput_1 = "noDatetimeStringSync=true";
					if (!properties_tDBOutput_1
							.contains("rewriteBatchedStatements")) {
						properties_tDBOutput_1 += "&rewriteBatchedStatements=true";
					}

					url_tDBOutput_1 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ properties_tDBOutput_1;
				}
				String driverClass_tDBOutput_1 = "org.gjt.mm.mysql.Driver";

				String dbUser_tDBOutput_1 = "root";

				final String decryptedPassword_tDBOutput_1 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBOutput_1 = decryptedPassword_tDBOutput_1;
				java.lang.Class.forName(driverClass_tDBOutput_1);

				conn_tDBOutput_1 = java.sql.DriverManager.getConnection(
						url_tDBOutput_1, dbUser_tDBOutput_1, dbPwd_tDBOutput_1);

				resourceMap.put("conn_tDBOutput_1", conn_tDBOutput_1);
				conn_tDBOutput_1.setAutoCommit(false);
				int commitEvery_tDBOutput_1 = 10000;
				int commitCounter_tDBOutput_1 = 0;

				int count_tDBOutput_1 = 0;

				java.sql.Statement stmtDrop_tDBOutput_1 = conn_tDBOutput_1
						.createStatement();
				stmtDrop_tDBOutput_1.execute("DROP TABLE `"
						+ tableName_tDBOutput_1 + "`");
				stmtDrop_tDBOutput_1.close();
				java.sql.Statement stmtCreate_tDBOutput_1 = conn_tDBOutput_1
						.createStatement();
				stmtCreate_tDBOutput_1
						.execute("CREATE TABLE `"
								+ tableName_tDBOutput_1
								+ "`(`codedept` INT(10)  default 0  not null ,`DIRECTEUR_DPT` INT(10)  default 0  not null ,`enseignantID` INT(10)  default 0  not null ,`NOM_PRENOM_ENS` VARCHAR(100)   not null ,`codegrade` INT(10)  default 0  not null ,`codecourt` VARCHAR(10)   not null ,`nomlong` VARCHAR(100)   not null ,`type` VARCHAR(32)   not null ,`heures` INT(10)  default 0  not null ,`ANNNE_ENS` INT(10)  default 0  not null ,primary key(`codedept`,`enseignantID`))");
				stmtCreate_tDBOutput_1.close();

				String insert_tDBOutput_1 = "INSERT INTO `"
						+ "DIM_ENS_DPT"
						+ "` (`codedept`,`DIRECTEUR_DPT`,`enseignantID`,`NOM_PRENOM_ENS`,`codegrade`,`codecourt`,`nomlong`,`type`,`heures`,`ANNNE_ENS`) VALUES (?,?,?,?,?,?,?,?,?,?)";
				int batchSize_tDBOutput_1 = 100;
				int batchSizeCounter_tDBOutput_1 = 0;

				java.sql.PreparedStatement pstmt_tDBOutput_1 = conn_tDBOutput_1
						.prepareStatement(insert_tDBOutput_1);

				/**
				 * [tDBOutput_1 begin ] stop
				 */

				/**
				 * [tDBOutput_2 begin ] start
				 */

				ok_Hash.put("tDBOutput_2", false);
				start_Hash.put("tDBOutput_2", System.currentTimeMillis());

				currentComponent = "tDBOutput_2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row34" + iterateId, 0,
								0);

					}
				}

				int tos_count_tDBOutput_2 = 0;

				class BytesLimit65535_tDBOutput_2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBOutput_2().limitLog4jByte();

				int nb_line_tDBOutput_2 = 0;
				int nb_line_update_tDBOutput_2 = 0;
				int nb_line_inserted_tDBOutput_2 = 0;
				int nb_line_deleted_tDBOutput_2 = 0;
				int nb_line_rejected_tDBOutput_2 = 0;

				int deletedCount_tDBOutput_2 = 0;
				int updatedCount_tDBOutput_2 = 0;
				int insertedCount_tDBOutput_2 = 0;

				int rejectedCount_tDBOutput_2 = 0;

				String tableName_tDBOutput_2 = "DIM_TEMPS";
				boolean whetherReject_tDBOutput_2 = false;

				java.util.Calendar calendar_tDBOutput_2 = java.util.Calendar
						.getInstance();
				calendar_tDBOutput_2.set(1, 0, 1, 0, 0, 0);
				long year1_tDBOutput_2 = calendar_tDBOutput_2.getTime()
						.getTime();
				calendar_tDBOutput_2.set(10000, 0, 1, 0, 0, 0);
				long year10000_tDBOutput_2 = calendar_tDBOutput_2.getTime()
						.getTime();
				long date_tDBOutput_2;

				java.sql.Connection conn_tDBOutput_2 = null;
				String dbProperties_tDBOutput_2 = "noDatetimeStringSync=true";
				String url_tDBOutput_2 = null;
				if (dbProperties_tDBOutput_2 == null
						|| dbProperties_tDBOutput_2.trim().length() == 0) {
					url_tDBOutput_2 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ "rewriteBatchedStatements=true";
				} else {
					String properties_tDBOutput_2 = "noDatetimeStringSync=true";
					if (!properties_tDBOutput_2
							.contains("rewriteBatchedStatements")) {
						properties_tDBOutput_2 += "&rewriteBatchedStatements=true";
					}

					url_tDBOutput_2 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ properties_tDBOutput_2;
				}
				String driverClass_tDBOutput_2 = "org.gjt.mm.mysql.Driver";

				String dbUser_tDBOutput_2 = "root";

				final String decryptedPassword_tDBOutput_2 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBOutput_2 = decryptedPassword_tDBOutput_2;
				java.lang.Class.forName(driverClass_tDBOutput_2);

				conn_tDBOutput_2 = java.sql.DriverManager.getConnection(
						url_tDBOutput_2, dbUser_tDBOutput_2, dbPwd_tDBOutput_2);

				resourceMap.put("conn_tDBOutput_2", conn_tDBOutput_2);
				conn_tDBOutput_2.setAutoCommit(false);
				int commitEvery_tDBOutput_2 = 10000;
				int commitCounter_tDBOutput_2 = 0;

				int count_tDBOutput_2 = 0;

				java.sql.Statement stmtDrop_tDBOutput_2 = conn_tDBOutput_2
						.createStatement();
				stmtDrop_tDBOutput_2.execute("DROP TABLE `"
						+ tableName_tDBOutput_2 + "`");
				stmtDrop_tDBOutput_2.close();
				java.sql.Statement stmtCreate_tDBOutput_2 = conn_tDBOutput_2
						.createStatement();
				stmtCreate_tDBOutput_2
						.execute("CREATE TABLE `"
								+ tableName_tDBOutput_2
								+ "`(`SK_DIM_TEMPS` INT(0)  ,`anneedebut` INT(10)  default 0  not null ,primary key(`SK_DIM_TEMPS`,`anneedebut`))");
				stmtCreate_tDBOutput_2.close();

				String insert_tDBOutput_2 = "INSERT INTO `" + "DIM_TEMPS"
						+ "` (`SK_DIM_TEMPS`,`anneedebut`) VALUES (?,?)";
				int batchSize_tDBOutput_2 = 100;
				int batchSizeCounter_tDBOutput_2 = 0;

				java.sql.PreparedStatement pstmt_tDBOutput_2 = conn_tDBOutput_2
						.prepareStatement(insert_tDBOutput_2);

				/**
				 * [tDBOutput_2 begin ] stop
				 */

				/**
				 * [tUniqRow_2 begin ] start
				 */

				ok_Hash.put("tUniqRow_2", false);
				start_Hash.put("tUniqRow_2", System.currentTimeMillis());

				currentComponent = "tUniqRow_2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("dim_temps" + iterateId,
								0, 0);

					}
				}

				int tos_count_tUniqRow_2 = 0;

				class BytesLimit65535_tUniqRow_2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tUniqRow_2().limitLog4jByte();

				class KeyStruct_tUniqRow_2 {

					private static final int DEFAULT_HASHCODE = 1;
					private static final int PRIME = 31;
					private int hashCode = DEFAULT_HASHCODE;
					public boolean hashCodeDirty = true;

					int anneedebut;

					@Override
					public int hashCode() {
						if (this.hashCodeDirty) {
							final int prime = PRIME;
							int result = DEFAULT_HASHCODE;

							result = prime * result + (int) this.anneedebut;

							this.hashCode = result;
							this.hashCodeDirty = false;
						}
						return this.hashCode;
					}

					@Override
					public boolean equals(Object obj) {
						if (this == obj)
							return true;
						if (obj == null)
							return false;
						if (getClass() != obj.getClass())
							return false;
						final KeyStruct_tUniqRow_2 other = (KeyStruct_tUniqRow_2) obj;

						if (this.anneedebut != other.anneedebut)
							return false;

						return true;
					}

				}

				int nb_uniques_tUniqRow_2 = 0;
				int nb_duplicates_tUniqRow_2 = 0;
				KeyStruct_tUniqRow_2 finder_tUniqRow_2 = new KeyStruct_tUniqRow_2();
				java.util.Set<KeyStruct_tUniqRow_2> keystUniqRow_2 = new java.util.HashSet<KeyStruct_tUniqRow_2>();

				/**
				 * [tUniqRow_2 begin ] stop
				 */

				/**
				 * [tMap_1 begin ] start
				 */

				ok_Hash.put("tMap_1", false);
				start_Hash.put("tMap_1", System.currentTimeMillis());

				currentComponent = "tMap_1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row9" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_1 = 0;

				class BytesLimit65535_tMap_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tMap_1().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row10Struct> tHash_Lookup_row10 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row10Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row10Struct>) globalMap
						.get("tHash_Lookup_row10"));

				tHash_Lookup_row10.initGet();

				row10Struct row10HashKey = new row10Struct();
				row10Struct row10Default = new row10Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row11Struct> tHash_Lookup_row11 = null;

				row11Struct row11HashKey = new row11Struct();
				row11Struct row11Default = new row11Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row12Struct> tHash_Lookup_row12 = null;

				row12Struct row12HashKey = new row12Struct();
				row12Struct row12Default = new row12Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row30Struct> tHash_Lookup_row30 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row30Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row30Struct>) globalMap
						.get("tHash_Lookup_row30"));

				tHash_Lookup_row30.initGet();

				row30Struct row30HashKey = new row30Struct();
				row30Struct row30Default = new row30Struct();
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_1__Struct {
				}
				Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				ENSEIGNANT_DPTStruct ENSEIGNANT_DPT_tmp = new ENSEIGNANT_DPTStruct();
				ENS_DPT_BDStruct ENS_DPT_BD_tmp = new ENS_DPT_BDStruct();
				dim_tempsStruct dim_temps_tmp = new dim_tempsStruct();
				// ###############################

				/**
				 * [tMap_1 begin ] stop
				 */

				/**
				 * [tDBInput_1 begin ] start
				 */

				ok_Hash.put("tDBInput_1", false);
				start_Hash.put("tDBInput_1", System.currentTimeMillis());

				currentComponent = "tDBInput_1";

				int tos_count_tDBInput_1 = 0;

				class BytesLimit65535_tDBInput_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_1().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_1 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_1.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_1 = calendar_tDBInput_1.getTime();
				int nb_line_tDBInput_1 = 0;
				java.sql.Connection conn_tDBInput_1 = null;
				String driverClass_tDBInput_1 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_1);
				String dbUser_tDBInput_1 = "root";

				final String decryptedPassword_tDBInput_1 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_1 = decryptedPassword_tDBInput_1;

				String url_tDBInput_1 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_1 = java.sql.DriverManager.getConnection(
						url_tDBInput_1, dbUser_tDBInput_1, dbPwd_tDBInput_1);

				java.sql.Statement stmt_tDBInput_1 = conn_tDBInput_1
						.createStatement();

				String dbquery_tDBInput_1 = "SELECT \n  `departements`.`codedept`, \n  `departements`.`nom`, \n  `departements`.`directeur`\nFROM `departements`";

				globalMap.put("tDBInput_1_QUERY", dbquery_tDBInput_1);
				java.sql.ResultSet rs_tDBInput_1 = null;

				try {
					rs_tDBInput_1 = stmt_tDBInput_1
							.executeQuery(dbquery_tDBInput_1);
					java.sql.ResultSetMetaData rsmd_tDBInput_1 = rs_tDBInput_1
							.getMetaData();
					int colQtyInRs_tDBInput_1 = rsmd_tDBInput_1
							.getColumnCount();

					String tmpContent_tDBInput_1 = null;

					while (rs_tDBInput_1.next()) {
						nb_line_tDBInput_1++;

						if (colQtyInRs_tDBInput_1 < 1) {
							row9.codedept = 0;
						} else {

							if (rs_tDBInput_1.getObject(1) != null) {
								row9.codedept = rs_tDBInput_1.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_1 < 2) {
							row9.nom = null;
						} else {

							row9.nom = routines.system.JDBCUtil.getString(
									rs_tDBInput_1, 2, false);
						}
						if (colQtyInRs_tDBInput_1 < 3) {
							row9.directeur = 0;
						} else {

							if (rs_tDBInput_1.getObject(3) != null) {
								row9.directeur = rs_tDBInput_1.getInt(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_1 begin ] stop
						 */

						/**
						 * [tDBInput_1 main ] start
						 */

						currentComponent = "tDBInput_1";

						tos_count_tDBInput_1++;

						/**
						 * [tDBInput_1 main ] stop
						 */

						/**
						 * [tDBInput_1 process_data_begin ] start
						 */

						currentComponent = "tDBInput_1";

						/**
						 * [tDBInput_1 process_data_begin ] stop
						 */

						/**
						 * [tMap_1 main ] start
						 */

						currentComponent = "tMap_1";

						// row9
						// row9

						if (execStat) {
							runStat.updateStatOnConnection("row9" + iterateId,
									1, 1);
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_1 = false;
						boolean mainRowRejected_tMap_1 = false;

						// /////////////////////////////////////////////
						// Starting Lookup Table "row10"
						// /////////////////////////////////////////////

						boolean forceLooprow10 = false;

						row10Struct row10ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_1) { // G_TM_M_020

							tHash_Lookup_row10.lookup(row10HashKey);

							if (!tHash_Lookup_row10.hasNext()) { // G_TM_M_090

								forceLooprow10 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						else { // G 20 - G 21
							forceLooprow10 = true;
						} // G 21

						row10Struct row10 = null;

						while ((tHash_Lookup_row10 != null && tHash_Lookup_row10
								.hasNext()) || forceLooprow10) { // G_TM_M_043

							// CALL close loop of lookup 'row10'

							row10Struct fromLookup_row10 = null;
							row10 = row10Default;

							if (!forceLooprow10) { // G 46

								fromLookup_row10 = tHash_Lookup_row10.next();

								if (fromLookup_row10 != null) {
									row10 = fromLookup_row10;
								}

							} // G 46

							forceLooprow10 = false;

							// /////////////////////////////////////////////
							// Starting Lookup Table "row11"
							// /////////////////////////////////////////////

							boolean forceLooprow11 = false;

							row11Struct row11ObjectFromLookup = null;

							if (!rejectedInnerJoin_tMap_1) { // G_TM_M_020

								hasCasePrimitiveKeyWithNull_tMap_1 = false;

								Object exprKeyValue_row11__codegrade = row10.codegrade;
								if (exprKeyValue_row11__codegrade == null) {
									hasCasePrimitiveKeyWithNull_tMap_1 = true;
								} else {
									row11HashKey.codegrade = (int) (Integer) exprKeyValue_row11__codegrade;
								}

								row11HashKey.hashCodeDirty = true;

								if (!hasCasePrimitiveKeyWithNull_tMap_1) { // G_TM_M_091

									tDBInput_4Process(globalMap);

									tHash_Lookup_row11 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row11Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row11Struct>) globalMap
											.get("tHash_Lookup_row11"));

									tHash_Lookup_row11.initGet();

									tHash_Lookup_row11.lookup(row11HashKey);

								} // G_TM_M_091

								if (hasCasePrimitiveKeyWithNull_tMap_1
										|| !tHash_Lookup_row11.hasNext()) { // G_TM_M_090

									rejectedInnerJoin_tMap_1 = true;

								} // G_TM_M_090

							} // G_TM_M_020

							if (tHash_Lookup_row11 != null
									&& tHash_Lookup_row11
											.getCount(row11HashKey) > 1) { // G
																			// 071

								// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row11' and it contains more one result from keys :  row11.codegrade = '"
								// + row11HashKey.codegrade + "'");
							} // G 071

							row11Struct row11 = null;

							row11Struct fromLookup_row11 = null;
							row11 = row11Default;

							if (tHash_Lookup_row11 != null
									&& tHash_Lookup_row11.hasNext()) { // G 099

								fromLookup_row11 = tHash_Lookup_row11.next();

							} // G 099

							if (fromLookup_row11 != null) {
								row11 = fromLookup_row11;
							}

							// /////////////////////////////////////////////
							// Starting Lookup Table "row12"
							// /////////////////////////////////////////////

							boolean forceLooprow12 = false;

							row12Struct row12ObjectFromLookup = null;

							if (!rejectedInnerJoin_tMap_1) { // G_TM_M_020

								hasCasePrimitiveKeyWithNull_tMap_1 = false;

								Object exprKeyValue_row12__codedept = row9.codedept;
								if (exprKeyValue_row12__codedept == null) {
									hasCasePrimitiveKeyWithNull_tMap_1 = true;
								} else {
									row12HashKey.codedept = (int) (Integer) exprKeyValue_row12__codedept;
								}

								Object exprKeyValue_row12__codeens = row10.enseignantID;
								if (exprKeyValue_row12__codeens == null) {
									hasCasePrimitiveKeyWithNull_tMap_1 = true;
								} else {
									row12HashKey.codeens = (int) (Integer) exprKeyValue_row12__codeens;
								}

								row12HashKey.hashCodeDirty = true;

								if (!hasCasePrimitiveKeyWithNull_tMap_1) { // G_TM_M_091

									tDBInput_3Process(globalMap);

									tHash_Lookup_row12 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row12Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row12Struct>) globalMap
											.get("tHash_Lookup_row12"));

									tHash_Lookup_row12.initGet();

									tHash_Lookup_row12.lookup(row12HashKey);

								} // G_TM_M_091

							} // G_TM_M_020

							if (tHash_Lookup_row12 != null
									&& tHash_Lookup_row12
											.getCount(row12HashKey) > 1) { // G
																			// 071

								// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row12' and it contains more one result from keys :  row12.codedept = '"
								// + row12HashKey.codedept +
								// "', row12.codeens = '" + row12HashKey.codeens
								// + "'");
							} // G 071

							row12Struct row12 = null;

							row12Struct fromLookup_row12 = null;
							row12 = row12Default;

							if (tHash_Lookup_row12 != null
									&& tHash_Lookup_row12.hasNext()) { // G 099

								fromLookup_row12 = tHash_Lookup_row12.next();

							} // G 099

							if (fromLookup_row12 != null) {
								row12 = fromLookup_row12;
							}

							// /////////////////////////////////////////////
							// Starting Lookup Table "row30"
							// /////////////////////////////////////////////

							boolean forceLooprow30 = false;

							row30Struct row30ObjectFromLookup = null;

							if (!rejectedInnerJoin_tMap_1) { // G_TM_M_020

								tHash_Lookup_row30.lookup(row30HashKey);

								if (!tHash_Lookup_row30.hasNext()) { // G_TM_M_090

									forceLooprow30 = true;

								} // G_TM_M_090

							} // G_TM_M_020

							else { // G 20 - G 21
								forceLooprow30 = true;
							} // G 21

							row30Struct row30 = null;

							while ((tHash_Lookup_row30 != null && tHash_Lookup_row30
									.hasNext()) || forceLooprow30) { // G_TM_M_043

								// CALL close loop of lookup 'row30'

								row30Struct fromLookup_row30 = null;
								row30 = row30Default;

								if (!forceLooprow30) { // G 46

									fromLookup_row30 = tHash_Lookup_row30
											.next();

									if (fromLookup_row30 != null) {
										row30 = fromLookup_row30;
									}

								} // G 46

								forceLooprow30 = false;

								// ###############################
								{ // start of Var scope

									// ###############################
									// # Vars tables

									Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
									// ###############################
									// # Output tables

									ENSEIGNANT_DPT = null;
									ENS_DPT_BD = null;
									dim_temps = null;

									if (!rejectedInnerJoin_tMap_1) {

										// # Output table : 'ENSEIGNANT_DPT'
										ENSEIGNANT_DPT_tmp.codedept = row9.codedept;
										ENSEIGNANT_DPT_tmp.DIRECTEUR_DPT = row9.directeur;
										ENSEIGNANT_DPT_tmp.enseignantID = row10.enseignantID;
										ENSEIGNANT_DPT_tmp.NOM_PRENOM_ENS = row10.nom
												.concat(" ").concat(
														row10.prenom);
										ENSEIGNANT_DPT_tmp.codegrade = row11.codegrade;
										ENSEIGNANT_DPT_tmp.codecourt = row11.codecourt;
										ENSEIGNANT_DPT_tmp.nomlong = row11.nomlong;
										ENSEIGNANT_DPT_tmp.type = row11.type;
										ENSEIGNANT_DPT_tmp.heures = row11.heures;
										ENSEIGNANT_DPT_tmp.ANNNE_ENS = row30.anneedebut;
										ENSEIGNANT_DPT = ENSEIGNANT_DPT_tmp;

										// # Output table : 'ENS_DPT_BD'
										ENS_DPT_BD_tmp.codedept = row9.codedept;
										ENS_DPT_BD_tmp.DIRECTEUR_DPT = row9.directeur;
										ENS_DPT_BD_tmp.enseignantID = row10.enseignantID;
										ENS_DPT_BD_tmp.NOM_PRENOM_ENS = row10.nom
												.concat(" ").concat(
														row10.prenom);
										ENS_DPT_BD_tmp.codegrade = row11.codegrade;
										ENS_DPT_BD_tmp.codecourt = row11.codecourt;
										ENS_DPT_BD_tmp.nomlong = row11.nomlong;
										ENS_DPT_BD_tmp.type = row11.type;
										ENS_DPT_BD_tmp.heures = row11.heures;
										ENS_DPT_BD_tmp.ANNNE_ENS = row30.anneedebut;
										ENS_DPT_BD = ENS_DPT_BD_tmp;

										// # Output table : 'dim_temps'
										dim_temps_tmp.SK_DIM_TEMPS = Numeric
												.sequence("s1", 1, 1);
										dim_temps_tmp.anneedebut = row30.anneedebut;
										dim_temps = dim_temps_tmp;
									} // closing inner join bracket (2)
										// ###############################

								} // end of Var scope

								rejectedInnerJoin_tMap_1 = false;

								tos_count_tMap_1++;

								/**
								 * [tMap_1 main ] stop
								 */

								/**
								 * [tMap_1 process_data_begin ] start
								 */

								currentComponent = "tMap_1";

								/**
								 * [tMap_1 process_data_begin ] stop
								 */
								// Start of branch "ENSEIGNANT_DPT"
								if (ENSEIGNANT_DPT != null) {

									/**
									 * [tFileOutputExcel_1 main ] start
									 */

									currentComponent = "tFileOutputExcel_1";

									// ENSEIGNANT_DPT
									// ENSEIGNANT_DPT

									if (execStat) {
										runStat.updateStatOnConnection(
												"ENSEIGNANT_DPT" + iterateId,
												1, 1);
									}

									xlsxTool_tFileOutputExcel_1.addRow();

									xlsxTool_tFileOutputExcel_1
											.addCellValue(Double.parseDouble(String
													.valueOf(ENSEIGNANT_DPT.codedept)));

									xlsxTool_tFileOutputExcel_1
											.addCellValue(Double.parseDouble(String
													.valueOf(ENSEIGNANT_DPT.DIRECTEUR_DPT)));

									xlsxTool_tFileOutputExcel_1
											.addCellValue(Double.parseDouble(String
													.valueOf(ENSEIGNANT_DPT.enseignantID)));

									if (ENSEIGNANT_DPT.NOM_PRENOM_ENS != null) {

										xlsxTool_tFileOutputExcel_1
												.addCellValue(String
														.valueOf(ENSEIGNANT_DPT.NOM_PRENOM_ENS));
									} else {
										xlsxTool_tFileOutputExcel_1
												.addCellNullValue();
									}

									xlsxTool_tFileOutputExcel_1
											.addCellValue(Double.parseDouble(String
													.valueOf(ENSEIGNANT_DPT.codegrade)));

									if (ENSEIGNANT_DPT.codecourt != null) {

										xlsxTool_tFileOutputExcel_1
												.addCellValue(String
														.valueOf(ENSEIGNANT_DPT.codecourt));
									} else {
										xlsxTool_tFileOutputExcel_1
												.addCellNullValue();
									}

									if (ENSEIGNANT_DPT.nomlong != null) {

										xlsxTool_tFileOutputExcel_1
												.addCellValue(String
														.valueOf(ENSEIGNANT_DPT.nomlong));
									} else {
										xlsxTool_tFileOutputExcel_1
												.addCellNullValue();
									}

									if (ENSEIGNANT_DPT.type != null) {

										xlsxTool_tFileOutputExcel_1
												.addCellValue(String
														.valueOf(ENSEIGNANT_DPT.type));
									} else {
										xlsxTool_tFileOutputExcel_1
												.addCellNullValue();
									}

									xlsxTool_tFileOutputExcel_1
											.addCellValue(Double.parseDouble(String
													.valueOf(ENSEIGNANT_DPT.heures)));

									xlsxTool_tFileOutputExcel_1
											.addCellValue(Double.parseDouble(String
													.valueOf(ENSEIGNANT_DPT.ANNNE_ENS)));
									nb_line_tFileOutputExcel_1++;

									tos_count_tFileOutputExcel_1++;

									/**
									 * [tFileOutputExcel_1 main ] stop
									 */

									/**
									 * [tFileOutputExcel_1 process_data_begin ]
									 * start
									 */

									currentComponent = "tFileOutputExcel_1";

									/**
									 * [tFileOutputExcel_1 process_data_begin ]
									 * stop
									 */

									/**
									 * [tFileOutputExcel_1 process_data_end ]
									 * start
									 */

									currentComponent = "tFileOutputExcel_1";

									/**
									 * [tFileOutputExcel_1 process_data_end ]
									 * stop
									 */

								} // End of branch "ENSEIGNANT_DPT"

								// Start of branch "ENS_DPT_BD"
								if (ENS_DPT_BD != null) {

									/**
									 * [tDBOutput_1 main ] start
									 */

									currentComponent = "tDBOutput_1";

									// ENS_DPT_BD
									// ENS_DPT_BD

									if (execStat) {
										runStat.updateStatOnConnection(
												"ENS_DPT_BD" + iterateId, 1, 1);
									}

									whetherReject_tDBOutput_1 = false;
									pstmt_tDBOutput_1.setInt(1,
											ENS_DPT_BD.codedept);

									pstmt_tDBOutput_1.setInt(2,
											ENS_DPT_BD.DIRECTEUR_DPT);

									pstmt_tDBOutput_1.setInt(3,
											ENS_DPT_BD.enseignantID);

									if (ENS_DPT_BD.NOM_PRENOM_ENS == null) {
										pstmt_tDBOutput_1.setNull(4,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_1.setString(4,
												ENS_DPT_BD.NOM_PRENOM_ENS);
									}

									pstmt_tDBOutput_1.setInt(5,
											ENS_DPT_BD.codegrade);

									if (ENS_DPT_BD.codecourt == null) {
										pstmt_tDBOutput_1.setNull(6,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_1.setString(6,
												ENS_DPT_BD.codecourt);
									}

									if (ENS_DPT_BD.nomlong == null) {
										pstmt_tDBOutput_1.setNull(7,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_1.setString(7,
												ENS_DPT_BD.nomlong);
									}

									if (ENS_DPT_BD.type == null) {
										pstmt_tDBOutput_1.setNull(8,
												java.sql.Types.VARCHAR);
									} else {
										pstmt_tDBOutput_1.setString(8,
												ENS_DPT_BD.type);
									}

									pstmt_tDBOutput_1.setInt(9,
											ENS_DPT_BD.heures);

									pstmt_tDBOutput_1.setInt(10,
											ENS_DPT_BD.ANNNE_ENS);

									pstmt_tDBOutput_1.addBatch();
									nb_line_tDBOutput_1++;

									batchSizeCounter_tDBOutput_1++;
									if (batchSize_tDBOutput_1 <= batchSizeCounter_tDBOutput_1) {
										try {
											int countSum_tDBOutput_1 = 0;
											for (int countEach_tDBOutput_1 : pstmt_tDBOutput_1
													.executeBatch()) {
												countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0
														: 1);
											}
											insertedCount_tDBOutput_1 += countSum_tDBOutput_1;
										} catch (java.sql.BatchUpdateException e) {
											int countSum_tDBOutput_1 = 0;
											for (int countEach_tDBOutput_1 : e
													.getUpdateCounts()) {
												countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0
														: countEach_tDBOutput_1);
											}
											insertedCount_tDBOutput_1 += countSum_tDBOutput_1;
											System.err.println(e.getMessage());
										}

										batchSizeCounter_tDBOutput_1 = 0;
									}
									commitCounter_tDBOutput_1++;

									if (commitEvery_tDBOutput_1 <= commitCounter_tDBOutput_1) {

										try {
											int countSum_tDBOutput_1 = 0;
											for (int countEach_tDBOutput_1 : pstmt_tDBOutput_1
													.executeBatch()) {
												countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0
														: 1);
											}
											insertedCount_tDBOutput_1 += countSum_tDBOutput_1;
										} catch (java.sql.BatchUpdateException e) {
											int countSum_tDBOutput_1 = 0;
											for (int countEach_tDBOutput_1 : e
													.getUpdateCounts()) {
												countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0
														: countEach_tDBOutput_1);
											}
											insertedCount_tDBOutput_1 += countSum_tDBOutput_1;
											System.err.println(e.getMessage());

										}
										conn_tDBOutput_1.commit();
										commitCounter_tDBOutput_1 = 0;

									}

									tos_count_tDBOutput_1++;

									/**
									 * [tDBOutput_1 main ] stop
									 */

									/**
									 * [tDBOutput_1 process_data_begin ] start
									 */

									currentComponent = "tDBOutput_1";

									/**
									 * [tDBOutput_1 process_data_begin ] stop
									 */

									/**
									 * [tDBOutput_1 process_data_end ] start
									 */

									currentComponent = "tDBOutput_1";

									/**
									 * [tDBOutput_1 process_data_end ] stop
									 */

								} // End of branch "ENS_DPT_BD"

								// Start of branch "dim_temps"
								if (dim_temps != null) {

									/**
									 * [tUniqRow_2 main ] start
									 */

									currentComponent = "tUniqRow_2";

									// dim_temps
									// dim_temps

									if (execStat) {
										runStat.updateStatOnConnection(
												"dim_temps" + iterateId, 1, 1);
									}

									row34 = null;
									finder_tUniqRow_2.anneedebut = dim_temps.anneedebut;
									finder_tUniqRow_2.hashCodeDirty = true;
									if (!keystUniqRow_2
											.contains(finder_tUniqRow_2)) {
										KeyStruct_tUniqRow_2 new_tUniqRow_2 = new KeyStruct_tUniqRow_2();

										new_tUniqRow_2.anneedebut = dim_temps.anneedebut;

										keystUniqRow_2.add(new_tUniqRow_2);
										if (row34 == null) {

											row34 = new row34Struct();
										}
										row34.SK_DIM_TEMPS = dim_temps.SK_DIM_TEMPS;
										row34.anneedebut = dim_temps.anneedebut;
										nb_uniques_tUniqRow_2++;
									} else {
										nb_duplicates_tUniqRow_2++;
									}

									tos_count_tUniqRow_2++;

									/**
									 * [tUniqRow_2 main ] stop
									 */

									/**
									 * [tUniqRow_2 process_data_begin ] start
									 */

									currentComponent = "tUniqRow_2";

									/**
									 * [tUniqRow_2 process_data_begin ] stop
									 */
									// Start of branch "row34"
									if (row34 != null) {

										/**
										 * [tDBOutput_2 main ] start
										 */

										currentComponent = "tDBOutput_2";

										// row34
										// row34

										if (execStat) {
											runStat.updateStatOnConnection(
													"row34" + iterateId, 1, 1);
										}

										whetherReject_tDBOutput_2 = false;
										if (row34.SK_DIM_TEMPS == null) {
											pstmt_tDBOutput_2.setNull(1,
													java.sql.Types.INTEGER);
										} else {
											pstmt_tDBOutput_2.setInt(1,
													row34.SK_DIM_TEMPS);
										}

										pstmt_tDBOutput_2.setInt(2,
												row34.anneedebut);

										pstmt_tDBOutput_2.addBatch();
										nb_line_tDBOutput_2++;

										batchSizeCounter_tDBOutput_2++;
										if (batchSize_tDBOutput_2 <= batchSizeCounter_tDBOutput_2) {
											try {
												int countSum_tDBOutput_2 = 0;
												for (int countEach_tDBOutput_2 : pstmt_tDBOutput_2
														.executeBatch()) {
													countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0
															: 1);
												}
												insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
											} catch (java.sql.BatchUpdateException e) {
												int countSum_tDBOutput_2 = 0;
												for (int countEach_tDBOutput_2 : e
														.getUpdateCounts()) {
													countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0
															: countEach_tDBOutput_2);
												}
												insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
												System.err.println(e
														.getMessage());
											}

											batchSizeCounter_tDBOutput_2 = 0;
										}
										commitCounter_tDBOutput_2++;

										if (commitEvery_tDBOutput_2 <= commitCounter_tDBOutput_2) {

											try {
												int countSum_tDBOutput_2 = 0;
												for (int countEach_tDBOutput_2 : pstmt_tDBOutput_2
														.executeBatch()) {
													countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0
															: 1);
												}
												insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
											} catch (java.sql.BatchUpdateException e) {
												int countSum_tDBOutput_2 = 0;
												for (int countEach_tDBOutput_2 : e
														.getUpdateCounts()) {
													countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0
															: countEach_tDBOutput_2);
												}
												insertedCount_tDBOutput_2 += countSum_tDBOutput_2;
												System.err.println(e
														.getMessage());

											}
											conn_tDBOutput_2.commit();
											commitCounter_tDBOutput_2 = 0;

										}

										tos_count_tDBOutput_2++;

										/**
										 * [tDBOutput_2 main ] stop
										 */

										/**
										 * [tDBOutput_2 process_data_begin ]
										 * start
										 */

										currentComponent = "tDBOutput_2";

										/**
										 * [tDBOutput_2 process_data_begin ]
										 * stop
										 */

										/**
										 * [tDBOutput_2 process_data_end ] start
										 */

										currentComponent = "tDBOutput_2";

										/**
										 * [tDBOutput_2 process_data_end ] stop
										 */

									} // End of branch "row34"

									/**
									 * [tUniqRow_2 process_data_end ] start
									 */

									currentComponent = "tUniqRow_2";

									/**
									 * [tUniqRow_2 process_data_end ] stop
									 */

								} // End of branch "dim_temps"

							} // close loop of lookup 'row30' // G_TM_M_043

						} // close loop of lookup 'row10' // G_TM_M_043

						/**
						 * [tMap_1 process_data_end ] start
						 */

						currentComponent = "tMap_1";

						/**
						 * [tMap_1 process_data_end ] stop
						 */

						/**
						 * [tDBInput_1 process_data_end ] start
						 */

						currentComponent = "tDBInput_1";

						/**
						 * [tDBInput_1 process_data_end ] stop
						 */

						/**
						 * [tDBInput_1 end ] start
						 */

						currentComponent = "tDBInput_1";

					}
				} finally {
					if (rs_tDBInput_1 != null) {
						rs_tDBInput_1.close();
					}
					stmt_tDBInput_1.close();
					if (conn_tDBInput_1 != null && !conn_tDBInput_1.isClosed()) {

						conn_tDBInput_1.close();

					}

				}

				globalMap.put("tDBInput_1_NB_LINE", nb_line_tDBInput_1);

				ok_Hash.put("tDBInput_1", true);
				end_Hash.put("tDBInput_1", System.currentTimeMillis());

				/**
				 * [tDBInput_1 end ] stop
				 */

				/**
				 * [tMap_1 end ] start
				 */

				currentComponent = "tMap_1";

				// ###############################
				// # Lookup hashes releasing
				if (tHash_Lookup_row10 != null) {
					tHash_Lookup_row10.endGet();
				}
				globalMap.remove("tHash_Lookup_row10");

				if (tHash_Lookup_row11 != null) {
					tHash_Lookup_row11.endGet();
				}
				globalMap.remove("tHash_Lookup_row11");

				if (tHash_Lookup_row12 != null) {
					tHash_Lookup_row12.endGet();
				}
				globalMap.remove("tHash_Lookup_row12");

				if (tHash_Lookup_row30 != null) {
					tHash_Lookup_row30.endGet();
				}
				globalMap.remove("tHash_Lookup_row30");

				// ###############################

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row9" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tMap_1", true);
				end_Hash.put("tMap_1", System.currentTimeMillis());

				/**
				 * [tMap_1 end ] stop
				 */

				/**
				 * [tFileOutputExcel_1 end ] start
				 */

				currentComponent = "tFileOutputExcel_1";

				xlsxTool_tFileOutputExcel_1.writeExcel(
						fileName_tFileOutputExcel_1, true);

				nb_line_tFileOutputExcel_1 = nb_line_tFileOutputExcel_1 - 1;

				globalMap.put("tFileOutputExcel_1_NB_LINE",
						nb_line_tFileOutputExcel_1);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("ENSEIGNANT_DPT"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tFileOutputExcel_1", true);
				end_Hash.put("tFileOutputExcel_1", System.currentTimeMillis());

				/**
				 * [tFileOutputExcel_1 end ] stop
				 */

				/**
				 * [tDBOutput_1 end ] start
				 */

				currentComponent = "tDBOutput_1";

				try {
					if (batchSizeCounter_tDBOutput_1 != 0) {
						int countSum_tDBOutput_1 = 0;

						for (int countEach_tDBOutput_1 : pstmt_tDBOutput_1
								.executeBatch()) {
							countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0
									: 1);
						}

						insertedCount_tDBOutput_1 += countSum_tDBOutput_1;

					}

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_1 = 0;
					for (int countEach_tDBOutput_1 : e.getUpdateCounts()) {
						countSum_tDBOutput_1 += (countEach_tDBOutput_1 < 0 ? 0
								: countEach_tDBOutput_1);
					}

					insertedCount_tDBOutput_1 += countSum_tDBOutput_1;

					globalMap.put(currentComponent + "_ERROR_MESSAGE",
							e.getMessage());
					System.err.println(e.getMessage());

				}
				batchSizeCounter_tDBOutput_1 = 0;

				if (pstmt_tDBOutput_1 != null) {

					pstmt_tDBOutput_1.close();

				}

				if (commitCounter_tDBOutput_1 > 0) {

					conn_tDBOutput_1.commit();

				}

				conn_tDBOutput_1.close();

				resourceMap.put("finish_tDBOutput_1", true);

				nb_line_deleted_tDBOutput_1 = nb_line_deleted_tDBOutput_1
						+ deletedCount_tDBOutput_1;
				nb_line_update_tDBOutput_1 = nb_line_update_tDBOutput_1
						+ updatedCount_tDBOutput_1;
				nb_line_inserted_tDBOutput_1 = nb_line_inserted_tDBOutput_1
						+ insertedCount_tDBOutput_1;
				nb_line_rejected_tDBOutput_1 = nb_line_rejected_tDBOutput_1
						+ rejectedCount_tDBOutput_1;

				globalMap.put("tDBOutput_1_NB_LINE", nb_line_tDBOutput_1);
				globalMap.put("tDBOutput_1_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_1);
				globalMap.put("tDBOutput_1_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_1);
				globalMap.put("tDBOutput_1_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_1);
				globalMap.put("tDBOutput_1_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_1);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection(
								"ENS_DPT_BD" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tDBOutput_1", true);
				end_Hash.put("tDBOutput_1", System.currentTimeMillis());

				/**
				 * [tDBOutput_1 end ] stop
				 */

				/**
				 * [tUniqRow_2 end ] start
				 */

				currentComponent = "tUniqRow_2";

				globalMap.put("tUniqRow_2_NB_UNIQUES", nb_uniques_tUniqRow_2);
				globalMap.put("tUniqRow_2_NB_DUPLICATES",
						nb_duplicates_tUniqRow_2);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("dim_temps" + iterateId,
								2, 0);
					}
				}

				ok_Hash.put("tUniqRow_2", true);
				end_Hash.put("tUniqRow_2", System.currentTimeMillis());

				/**
				 * [tUniqRow_2 end ] stop
				 */

				/**
				 * [tDBOutput_2 end ] start
				 */

				currentComponent = "tDBOutput_2";

				try {
					if (batchSizeCounter_tDBOutput_2 != 0) {
						int countSum_tDBOutput_2 = 0;

						for (int countEach_tDBOutput_2 : pstmt_tDBOutput_2
								.executeBatch()) {
							countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0
									: 1);
						}

						insertedCount_tDBOutput_2 += countSum_tDBOutput_2;

					}

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_2 = 0;
					for (int countEach_tDBOutput_2 : e.getUpdateCounts()) {
						countSum_tDBOutput_2 += (countEach_tDBOutput_2 < 0 ? 0
								: countEach_tDBOutput_2);
					}

					insertedCount_tDBOutput_2 += countSum_tDBOutput_2;

					globalMap.put(currentComponent + "_ERROR_MESSAGE",
							e.getMessage());
					System.err.println(e.getMessage());

				}
				batchSizeCounter_tDBOutput_2 = 0;

				if (pstmt_tDBOutput_2 != null) {

					pstmt_tDBOutput_2.close();

				}

				if (commitCounter_tDBOutput_2 > 0) {

					conn_tDBOutput_2.commit();

				}

				conn_tDBOutput_2.close();

				resourceMap.put("finish_tDBOutput_2", true);

				nb_line_deleted_tDBOutput_2 = nb_line_deleted_tDBOutput_2
						+ deletedCount_tDBOutput_2;
				nb_line_update_tDBOutput_2 = nb_line_update_tDBOutput_2
						+ updatedCount_tDBOutput_2;
				nb_line_inserted_tDBOutput_2 = nb_line_inserted_tDBOutput_2
						+ insertedCount_tDBOutput_2;
				nb_line_rejected_tDBOutput_2 = nb_line_rejected_tDBOutput_2
						+ rejectedCount_tDBOutput_2;

				globalMap.put("tDBOutput_2_NB_LINE", nb_line_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_2);
				globalMap.put("tDBOutput_2_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_2);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row34" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tDBOutput_2", true);
				end_Hash.put("tDBOutput_2", System.currentTimeMillis());

				/**
				 * [tDBOutput_2 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			// free memory for "tMap_1"
			globalMap.remove("tHash_Lookup_row10");

			// free memory for "tMap_1"
			globalMap.remove("tHash_Lookup_row12");

			// free memory for "tMap_1"
			globalMap.remove("tHash_Lookup_row11");

			// free memory for "tMap_1"
			globalMap.remove("tHash_Lookup_row30");

			try {

				/**
				 * [tDBInput_1 finally ] start
				 */

				currentComponent = "tDBInput_1";

				/**
				 * [tDBInput_1 finally ] stop
				 */

				/**
				 * [tMap_1 finally ] start
				 */

				currentComponent = "tMap_1";

				/**
				 * [tMap_1 finally ] stop
				 */

				/**
				 * [tFileOutputExcel_1 finally ] start
				 */

				currentComponent = "tFileOutputExcel_1";

				/**
				 * [tFileOutputExcel_1 finally ] stop
				 */

				/**
				 * [tDBOutput_1 finally ] start
				 */

				currentComponent = "tDBOutput_1";

				if (resourceMap.get("finish_tDBOutput_1") == null) {
					if (resourceMap.get("conn_tDBOutput_1") != null) {
						try {

							java.sql.Connection ctn_tDBOutput_1 = (java.sql.Connection) resourceMap
									.get("conn_tDBOutput_1");

							ctn_tDBOutput_1.close();

						} catch (java.sql.SQLException sqlEx_tDBOutput_1) {
							String errorMessage_tDBOutput_1 = "failed to close the connection in tDBOutput_1 :"
									+ sqlEx_tDBOutput_1.getMessage();

							System.err.println(errorMessage_tDBOutput_1);
						}
					}
				}

				/**
				 * [tDBOutput_1 finally ] stop
				 */

				/**
				 * [tUniqRow_2 finally ] start
				 */

				currentComponent = "tUniqRow_2";

				/**
				 * [tUniqRow_2 finally ] stop
				 */

				/**
				 * [tDBOutput_2 finally ] start
				 */

				currentComponent = "tDBOutput_2";

				if (resourceMap.get("finish_tDBOutput_2") == null) {
					if (resourceMap.get("conn_tDBOutput_2") != null) {
						try {

							java.sql.Connection ctn_tDBOutput_2 = (java.sql.Connection) resourceMap
									.get("conn_tDBOutput_2");

							ctn_tDBOutput_2.close();

						} catch (java.sql.SQLException sqlEx_tDBOutput_2) {
							String errorMessage_tDBOutput_2 = "failed to close the connection in tDBOutput_2 :"
									+ sqlEx_tDBOutput_2.getMessage();

							System.err.println(errorMessage_tDBOutput_2);
						}
					}
				}

				/**
				 * [tDBOutput_2 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_1_SUBPROCESS_STATE", 1);
	}

	public static class row10Struct implements
			routines.system.IPersistableRow<row10Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public String nom;

		public String getNom() {
			return this.nom;
		}

		public String prenom;

		public String getPrenom() {
			return this.prenom;
		}

		public int codegrade;

		public int getCodegrade() {
			return this.codegrade;
		}

		public int annee;

		public int getAnnee() {
			return this.annee;
		}

		public String password;

		public String getPassword() {
			return this.password;
		}

		public String email;

		public String getEmail() {
			return this.email;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.enseignantID = dis.readInt();

					this.nom = readString(dis);

					this.prenom = readString(dis);

					this.codegrade = dis.readInt();

					this.annee = dis.readInt();

					this.password = readString(dis);

					this.email = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.enseignantID);

				// String

				writeString(this.nom, dos);

				// String

				writeString(this.prenom, dos);

				// int

				dos.writeInt(this.codegrade);

				// int

				dos.writeInt(this.annee);

				// String

				writeString(this.password, dos);

				// String

				writeString(this.email, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("enseignantID=" + String.valueOf(enseignantID));
			sb.append(",nom=" + nom);
			sb.append(",prenom=" + prenom);
			sb.append(",codegrade=" + String.valueOf(codegrade));
			sb.append(",annee=" + String.valueOf(annee));
			sb.append(",password=" + password);
			sb.append(",email=" + email);
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row10Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_2Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row10Struct row10 = new row10Struct();

				/**
				 * [tAdvancedHash_row10 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row10", false);
				start_Hash.put("tAdvancedHash_row10",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row10";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row10" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row10 = 0;

				class BytesLimit65535_tAdvancedHash_row10 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row10().limitLog4jByte();

				// connection name:row10
				// source node:tDBInput_2 - inputs:(after_tDBInput_1)
				// outputs:(row10,row10) | target node:tAdvancedHash_row10 -
				// inputs:(row10) outputs:()
				// linked node: tMap_1 - inputs:(row9,row10,row12,row11,row30)
				// outputs:(ENSEIGNANT_DPT,ENS_DPT_BD,dim_temps)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row10 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.ALL_ROWS;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row10Struct> tHash_Lookup_row10 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row10Struct> getLookup(matchingModeEnum_row10);

				globalMap.put("tHash_Lookup_row10", tHash_Lookup_row10);

				/**
				 * [tAdvancedHash_row10 begin ] stop
				 */

				/**
				 * [tDBInput_2 begin ] start
				 */

				ok_Hash.put("tDBInput_2", false);
				start_Hash.put("tDBInput_2", System.currentTimeMillis());

				currentComponent = "tDBInput_2";

				int tos_count_tDBInput_2 = 0;

				class BytesLimit65535_tDBInput_2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_2().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_2 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_2.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_2 = calendar_tDBInput_2.getTime();
				int nb_line_tDBInput_2 = 0;
				java.sql.Connection conn_tDBInput_2 = null;
				String driverClass_tDBInput_2 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_2);
				String dbUser_tDBInput_2 = "root";

				final String decryptedPassword_tDBInput_2 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_2 = decryptedPassword_tDBInput_2;

				String url_tDBInput_2 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_2 = java.sql.DriverManager.getConnection(
						url_tDBInput_2, dbUser_tDBInput_2, dbPwd_tDBInput_2);

				java.sql.Statement stmt_tDBInput_2 = conn_tDBInput_2
						.createStatement();

				String dbquery_tDBInput_2 = "SELECT \n  `enseignants`.`enseignantID`, \n  `enseignants`.`nom`, \n  `enseignants`.`prenom`, \n  `enseignants`.`codegrade`"
						+ ", \n  `enseignants`.`annee`, \n  `enseignants`.`password`, \n  `enseignants`.`email`\nFROM `enseignants`";

				globalMap.put("tDBInput_2_QUERY", dbquery_tDBInput_2);
				java.sql.ResultSet rs_tDBInput_2 = null;

				try {
					rs_tDBInput_2 = stmt_tDBInput_2
							.executeQuery(dbquery_tDBInput_2);
					java.sql.ResultSetMetaData rsmd_tDBInput_2 = rs_tDBInput_2
							.getMetaData();
					int colQtyInRs_tDBInput_2 = rsmd_tDBInput_2
							.getColumnCount();

					String tmpContent_tDBInput_2 = null;

					while (rs_tDBInput_2.next()) {
						nb_line_tDBInput_2++;

						if (colQtyInRs_tDBInput_2 < 1) {
							row10.enseignantID = 0;
						} else {

							if (rs_tDBInput_2.getObject(1) != null) {
								row10.enseignantID = rs_tDBInput_2.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_2 < 2) {
							row10.nom = null;
						} else {

							row10.nom = routines.system.JDBCUtil.getString(
									rs_tDBInput_2, 2, false);
						}
						if (colQtyInRs_tDBInput_2 < 3) {
							row10.prenom = null;
						} else {

							row10.prenom = routines.system.JDBCUtil.getString(
									rs_tDBInput_2, 3, false);
						}
						if (colQtyInRs_tDBInput_2 < 4) {
							row10.codegrade = 0;
						} else {

							if (rs_tDBInput_2.getObject(4) != null) {
								row10.codegrade = rs_tDBInput_2.getInt(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_2 < 5) {
							row10.annee = 0;
						} else {

							if (rs_tDBInput_2.getObject(5) != null) {
								row10.annee = rs_tDBInput_2.getInt(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_2 < 6) {
							row10.password = null;
						} else {

							row10.password = routines.system.JDBCUtil
									.getString(rs_tDBInput_2, 6, false);
						}
						if (colQtyInRs_tDBInput_2 < 7) {
							row10.email = null;
						} else {

							row10.email = routines.system.JDBCUtil.getString(
									rs_tDBInput_2, 7, false);
						}

						/**
						 * [tDBInput_2 begin ] stop
						 */

						/**
						 * [tDBInput_2 main ] start
						 */

						currentComponent = "tDBInput_2";

						tos_count_tDBInput_2++;

						/**
						 * [tDBInput_2 main ] stop
						 */

						/**
						 * [tDBInput_2 process_data_begin ] start
						 */

						currentComponent = "tDBInput_2";

						/**
						 * [tDBInput_2 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row10 main ] start
						 */

						currentComponent = "tAdvancedHash_row10";

						// row10
						// row10

						if (execStat) {
							runStat.updateStatOnConnection("row10" + iterateId,
									1, 1);
						}

						row10Struct row10_HashRow = new row10Struct();

						row10_HashRow.enseignantID = row10.enseignantID;

						row10_HashRow.nom = row10.nom;

						row10_HashRow.prenom = row10.prenom;

						row10_HashRow.codegrade = row10.codegrade;

						row10_HashRow.annee = row10.annee;

						row10_HashRow.password = row10.password;

						row10_HashRow.email = row10.email;

						tHash_Lookup_row10.put(row10_HashRow);

						tos_count_tAdvancedHash_row10++;

						/**
						 * [tAdvancedHash_row10 main ] stop
						 */

						/**
						 * [tAdvancedHash_row10 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row10";

						/**
						 * [tAdvancedHash_row10 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row10 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row10";

						/**
						 * [tAdvancedHash_row10 process_data_end ] stop
						 */

						/**
						 * [tDBInput_2 process_data_end ] start
						 */

						currentComponent = "tDBInput_2";

						/**
						 * [tDBInput_2 process_data_end ] stop
						 */

						/**
						 * [tDBInput_2 end ] start
						 */

						currentComponent = "tDBInput_2";

					}
				} finally {
					if (rs_tDBInput_2 != null) {
						rs_tDBInput_2.close();
					}
					stmt_tDBInput_2.close();
					if (conn_tDBInput_2 != null && !conn_tDBInput_2.isClosed()) {

						conn_tDBInput_2.close();

					}

				}

				globalMap.put("tDBInput_2_NB_LINE", nb_line_tDBInput_2);

				ok_Hash.put("tDBInput_2", true);
				end_Hash.put("tDBInput_2", System.currentTimeMillis());

				/**
				 * [tDBInput_2 end ] stop
				 */

				/**
				 * [tAdvancedHash_row10 end ] start
				 */

				currentComponent = "tAdvancedHash_row10";

				tHash_Lookup_row10.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row10" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row10", true);
				end_Hash.put("tAdvancedHash_row10", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row10 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_2 finally ] start
				 */

				currentComponent = "tDBInput_2";

				/**
				 * [tDBInput_2 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row10 finally ] start
				 */

				currentComponent = "tAdvancedHash_row10";

				/**
				 * [tAdvancedHash_row10 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_2_SUBPROCESS_STATE", 1);
	}

	public static class row12Struct implements
			routines.system.IPersistableComparableLookupRow<row12Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codedept;

		public int getCodedept() {
			return this.codedept;
		}

		public int codeens;

		public int getCodeens() {
			return this.codeens;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codedept;

				result = prime * result + (int) this.codeens;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row12Struct other = (row12Struct) obj;

			if (this.codedept != other.codedept)
				return false;

			if (this.codeens != other.codeens)
				return false;

			return true;
		}

		public void copyDataTo(row12Struct other) {

			other.codedept = this.codedept;
			other.codeens = this.codeens;

		}

		public void copyKeysDataTo(row12Struct other) {

			other.codedept = this.codedept;
			other.codeens = this.codeens;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codedept = dis.readInt();

					this.codeens = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codedept);

				// int

				dos.writeInt(this.codeens);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

			}

			finally {
			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

			} finally {
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codedept=" + String.valueOf(codedept));
			sb.append(",codeens=" + String.valueOf(codeens));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row12Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codedept, other.codedept);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.codeens, other.codeens);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_3Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_3_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row12Struct row12 = new row12Struct();

				/**
				 * [tAdvancedHash_row12 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row12", false);
				start_Hash.put("tAdvancedHash_row12",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row12";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row12" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row12 = 0;

				class BytesLimit65535_tAdvancedHash_row12 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row12().limitLog4jByte();

				// connection name:row12
				// source node:tDBInput_3 - inputs:() outputs:(row12,row12) |
				// target node:tAdvancedHash_row12 - inputs:(row12) outputs:()
				// linked node: tMap_1 - inputs:(row9,row10,row12,row11,row30)
				// outputs:(ENSEIGNANT_DPT,ENS_DPT_BD,dim_temps)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row12 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row12Struct> tHash_Lookup_row12 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row12Struct> getLookup(matchingModeEnum_row12);

				globalMap.put("tHash_Lookup_row12", tHash_Lookup_row12);

				/**
				 * [tAdvancedHash_row12 begin ] stop
				 */

				/**
				 * [tDBInput_3 begin ] start
				 */

				ok_Hash.put("tDBInput_3", false);
				start_Hash.put("tDBInput_3", System.currentTimeMillis());

				currentComponent = "tDBInput_3";

				int tos_count_tDBInput_3 = 0;

				class BytesLimit65535_tDBInput_3 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_3().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_3 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_3.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_3 = calendar_tDBInput_3.getTime();
				int nb_line_tDBInput_3 = 0;
				java.sql.Connection conn_tDBInput_3 = null;
				String driverClass_tDBInput_3 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_3);
				String dbUser_tDBInput_3 = "root";

				final String decryptedPassword_tDBInput_3 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_3 = decryptedPassword_tDBInput_3;

				String url_tDBInput_3 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_3 = java.sql.DriverManager.getConnection(
						url_tDBInput_3, dbUser_tDBInput_3, dbPwd_tDBInput_3);

				java.sql.Statement stmt_tDBInput_3 = conn_tDBInput_3
						.createStatement();

				String dbquery_tDBInput_3 = "SELECT \n  `membresdep`.`codedept`, \n  `membresdep`.`codeens`\nFROM `membresdep`";

				globalMap.put("tDBInput_3_QUERY", dbquery_tDBInput_3);
				java.sql.ResultSet rs_tDBInput_3 = null;

				try {
					rs_tDBInput_3 = stmt_tDBInput_3
							.executeQuery(dbquery_tDBInput_3);
					java.sql.ResultSetMetaData rsmd_tDBInput_3 = rs_tDBInput_3
							.getMetaData();
					int colQtyInRs_tDBInput_3 = rsmd_tDBInput_3
							.getColumnCount();

					String tmpContent_tDBInput_3 = null;

					while (rs_tDBInput_3.next()) {
						nb_line_tDBInput_3++;

						if (colQtyInRs_tDBInput_3 < 1) {
							row12.codedept = 0;
						} else {

							if (rs_tDBInput_3.getObject(1) != null) {
								row12.codedept = rs_tDBInput_3.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_3 < 2) {
							row12.codeens = 0;
						} else {

							if (rs_tDBInput_3.getObject(2) != null) {
								row12.codeens = rs_tDBInput_3.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_3 begin ] stop
						 */

						/**
						 * [tDBInput_3 main ] start
						 */

						currentComponent = "tDBInput_3";

						tos_count_tDBInput_3++;

						/**
						 * [tDBInput_3 main ] stop
						 */

						/**
						 * [tDBInput_3 process_data_begin ] start
						 */

						currentComponent = "tDBInput_3";

						/**
						 * [tDBInput_3 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row12 main ] start
						 */

						currentComponent = "tAdvancedHash_row12";

						// row12
						// row12

						if (execStat) {
							runStat.updateStatOnConnection("row12" + iterateId,
									1, 1);
						}

						row12Struct row12_HashRow = new row12Struct();

						row12_HashRow.codedept = row12.codedept;

						row12_HashRow.codeens = row12.codeens;

						tHash_Lookup_row12.put(row12_HashRow);

						tos_count_tAdvancedHash_row12++;

						/**
						 * [tAdvancedHash_row12 main ] stop
						 */

						/**
						 * [tAdvancedHash_row12 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row12";

						/**
						 * [tAdvancedHash_row12 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row12 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row12";

						/**
						 * [tAdvancedHash_row12 process_data_end ] stop
						 */

						/**
						 * [tDBInput_3 process_data_end ] start
						 */

						currentComponent = "tDBInput_3";

						/**
						 * [tDBInput_3 process_data_end ] stop
						 */

						/**
						 * [tDBInput_3 end ] start
						 */

						currentComponent = "tDBInput_3";

					}
				} finally {
					if (rs_tDBInput_3 != null) {
						rs_tDBInput_3.close();
					}
					stmt_tDBInput_3.close();
					if (conn_tDBInput_3 != null && !conn_tDBInput_3.isClosed()) {

						conn_tDBInput_3.close();

					}

				}

				globalMap.put("tDBInput_3_NB_LINE", nb_line_tDBInput_3);

				ok_Hash.put("tDBInput_3", true);
				end_Hash.put("tDBInput_3", System.currentTimeMillis());

				/**
				 * [tDBInput_3 end ] stop
				 */

				/**
				 * [tAdvancedHash_row12 end ] start
				 */

				currentComponent = "tAdvancedHash_row12";

				tHash_Lookup_row12.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row12" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row12", true);
				end_Hash.put("tAdvancedHash_row12", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row12 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_3 finally ] start
				 */

				currentComponent = "tDBInput_3";

				/**
				 * [tDBInput_3 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row12 finally ] start
				 */

				currentComponent = "tAdvancedHash_row12";

				/**
				 * [tAdvancedHash_row12 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_3_SUBPROCESS_STATE", 1);
	}

	public static class row11Struct implements
			routines.system.IPersistableComparableLookupRow<row11Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codegrade;

		public int getCodegrade() {
			return this.codegrade;
		}

		public String codecourt;

		public String getCodecourt() {
			return this.codecourt;
		}

		public String nomlong;

		public String getNomlong() {
			return this.nomlong;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public int heures;

		public int getHeures() {
			return this.heures;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codegrade;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row11Struct other = (row11Struct) obj;

			if (this.codegrade != other.codegrade)
				return false;

			return true;
		}

		public void copyDataTo(row11Struct other) {

			other.codegrade = this.codegrade;
			other.codecourt = this.codecourt;
			other.nomlong = this.nomlong;
			other.type = this.type;
			other.heures = this.heures;

		}

		public void copyKeysDataTo(row11Struct other) {

			other.codegrade = this.codegrade;

		}

		private String readString(DataInputStream dis, ObjectInputStream ois)
				throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				byte[] byteArray = new byte[length];
				dis.read(byteArray);
				strReturn = new String(byteArray, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, DataOutputStream dos,
				ObjectOutputStream oos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codegrade = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codegrade);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.codecourt = readString(dis, ois);

				this.nomlong = readString(dis, ois);

				this.type = readString(dis, ois);

				this.heures = dis.readInt();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				writeString(this.codecourt, dos, oos);

				writeString(this.nomlong, dos, oos);

				writeString(this.type, dos, oos);

				dos.writeInt(this.heures);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codegrade=" + String.valueOf(codegrade));
			sb.append(",codecourt=" + codecourt);
			sb.append(",nomlong=" + nomlong);
			sb.append(",type=" + type);
			sb.append(",heures=" + String.valueOf(heures));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row11Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codegrade, other.codegrade);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_4Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_4_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row11Struct row11 = new row11Struct();

				/**
				 * [tAdvancedHash_row11 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row11", false);
				start_Hash.put("tAdvancedHash_row11",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row11";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row11" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row11 = 0;

				class BytesLimit65535_tAdvancedHash_row11 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row11().limitLog4jByte();

				// connection name:row11
				// source node:tDBInput_4 - inputs:() outputs:(row11,row11) |
				// target node:tAdvancedHash_row11 - inputs:(row11) outputs:()
				// linked node: tMap_1 - inputs:(row9,row10,row12,row11,row30)
				// outputs:(ENSEIGNANT_DPT,ENS_DPT_BD,dim_temps)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row11 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row11Struct> tHash_Lookup_row11 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row11Struct> getLookup(matchingModeEnum_row11);

				globalMap.put("tHash_Lookup_row11", tHash_Lookup_row11);

				/**
				 * [tAdvancedHash_row11 begin ] stop
				 */

				/**
				 * [tDBInput_4 begin ] start
				 */

				ok_Hash.put("tDBInput_4", false);
				start_Hash.put("tDBInput_4", System.currentTimeMillis());

				currentComponent = "tDBInput_4";

				int tos_count_tDBInput_4 = 0;

				class BytesLimit65535_tDBInput_4 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_4().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_4 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_4.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_4 = calendar_tDBInput_4.getTime();
				int nb_line_tDBInput_4 = 0;
				java.sql.Connection conn_tDBInput_4 = null;
				String driverClass_tDBInput_4 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_4);
				String dbUser_tDBInput_4 = "root";

				final String decryptedPassword_tDBInput_4 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_4 = decryptedPassword_tDBInput_4;

				String url_tDBInput_4 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_4 = java.sql.DriverManager.getConnection(
						url_tDBInput_4, dbUser_tDBInput_4, dbPwd_tDBInput_4);

				java.sql.Statement stmt_tDBInput_4 = conn_tDBInput_4
						.createStatement();

				String dbquery_tDBInput_4 = "SELECT \n  `grades`.`codegrade`, \n  `grades`.`codecourt`, \n  `grades`.`nomlong`, \n  `grades`.`type`, \n  `grades`.`heures"
						+ "`\nFROM `grades`";

				globalMap.put("tDBInput_4_QUERY", dbquery_tDBInput_4);
				java.sql.ResultSet rs_tDBInput_4 = null;

				try {
					rs_tDBInput_4 = stmt_tDBInput_4
							.executeQuery(dbquery_tDBInput_4);
					java.sql.ResultSetMetaData rsmd_tDBInput_4 = rs_tDBInput_4
							.getMetaData();
					int colQtyInRs_tDBInput_4 = rsmd_tDBInput_4
							.getColumnCount();

					String tmpContent_tDBInput_4 = null;

					while (rs_tDBInput_4.next()) {
						nb_line_tDBInput_4++;

						if (colQtyInRs_tDBInput_4 < 1) {
							row11.codegrade = 0;
						} else {

							if (rs_tDBInput_4.getObject(1) != null) {
								row11.codegrade = rs_tDBInput_4.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_4 < 2) {
							row11.codecourt = null;
						} else {

							row11.codecourt = routines.system.JDBCUtil
									.getString(rs_tDBInput_4, 2, false);
						}
						if (colQtyInRs_tDBInput_4 < 3) {
							row11.nomlong = null;
						} else {

							row11.nomlong = routines.system.JDBCUtil.getString(
									rs_tDBInput_4, 3, false);
						}
						if (colQtyInRs_tDBInput_4 < 4) {
							row11.type = null;
						} else {

							row11.type = routines.system.JDBCUtil.getString(
									rs_tDBInput_4, 4, false);
						}
						if (colQtyInRs_tDBInput_4 < 5) {
							row11.heures = 0;
						} else {

							if (rs_tDBInput_4.getObject(5) != null) {
								row11.heures = rs_tDBInput_4.getInt(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_4 begin ] stop
						 */

						/**
						 * [tDBInput_4 main ] start
						 */

						currentComponent = "tDBInput_4";

						tos_count_tDBInput_4++;

						/**
						 * [tDBInput_4 main ] stop
						 */

						/**
						 * [tDBInput_4 process_data_begin ] start
						 */

						currentComponent = "tDBInput_4";

						/**
						 * [tDBInput_4 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row11 main ] start
						 */

						currentComponent = "tAdvancedHash_row11";

						// row11
						// row11

						if (execStat) {
							runStat.updateStatOnConnection("row11" + iterateId,
									1, 1);
						}

						row11Struct row11_HashRow = new row11Struct();

						row11_HashRow.codegrade = row11.codegrade;

						row11_HashRow.codecourt = row11.codecourt;

						row11_HashRow.nomlong = row11.nomlong;

						row11_HashRow.type = row11.type;

						row11_HashRow.heures = row11.heures;

						tHash_Lookup_row11.put(row11_HashRow);

						tos_count_tAdvancedHash_row11++;

						/**
						 * [tAdvancedHash_row11 main ] stop
						 */

						/**
						 * [tAdvancedHash_row11 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row11";

						/**
						 * [tAdvancedHash_row11 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row11 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row11";

						/**
						 * [tAdvancedHash_row11 process_data_end ] stop
						 */

						/**
						 * [tDBInput_4 process_data_end ] start
						 */

						currentComponent = "tDBInput_4";

						/**
						 * [tDBInput_4 process_data_end ] stop
						 */

						/**
						 * [tDBInput_4 end ] start
						 */

						currentComponent = "tDBInput_4";

					}
				} finally {
					if (rs_tDBInput_4 != null) {
						rs_tDBInput_4.close();
					}
					stmt_tDBInput_4.close();
					if (conn_tDBInput_4 != null && !conn_tDBInput_4.isClosed()) {

						conn_tDBInput_4.close();

					}

				}

				globalMap.put("tDBInput_4_NB_LINE", nb_line_tDBInput_4);

				ok_Hash.put("tDBInput_4", true);
				end_Hash.put("tDBInput_4", System.currentTimeMillis());

				/**
				 * [tDBInput_4 end ] stop
				 */

				/**
				 * [tAdvancedHash_row11 end ] start
				 */

				currentComponent = "tAdvancedHash_row11";

				tHash_Lookup_row11.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row11" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row11", true);
				end_Hash.put("tAdvancedHash_row11", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row11 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_4 finally ] start
				 */

				currentComponent = "tDBInput_4";

				/**
				 * [tDBInput_4 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row11 finally ] start
				 */

				currentComponent = "tAdvancedHash_row11";

				/**
				 * [tAdvancedHash_row11 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_4_SUBPROCESS_STATE", 1);
	}

	public static class row7Struct implements
			routines.system.IPersistableComparableLookupRow<row7Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public float CM_A_FAIRE;

		public float getCM_A_FAIRE() {
			return this.CM_A_FAIRE;
		}

		public float TD_A_FAIRE;

		public float getTD_A_FAIRE() {
			return this.TD_A_FAIRE;
		}

		public float TP_A_FAIRE;

		public float getTP_A_FAIRE() {
			return this.TP_A_FAIRE;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row7Struct other = (row7Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(row7Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.CM_A_FAIRE = this.CM_A_FAIRE;
			other.TD_A_FAIRE = this.TD_A_FAIRE;
			other.TP_A_FAIRE = this.TP_A_FAIRE;

		}

		public void copyKeysDataTo(row7Struct other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.CM_A_FAIRE = dis.readFloat();

				this.TD_A_FAIRE = dis.readFloat();

				this.TP_A_FAIRE = dis.readFloat();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				dos.writeFloat(this.CM_A_FAIRE);

				dos.writeFloat(this.TD_A_FAIRE);

				dos.writeFloat(this.TP_A_FAIRE);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",CM_A_FAIRE=" + String.valueOf(CM_A_FAIRE));
			sb.append(",TD_A_FAIRE=" + String.valueOf(TD_A_FAIRE));
			sb.append(",TP_A_FAIRE=" + String.valueOf(TP_A_FAIRE));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row7Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class OnRowsEndStructtAggregateRow_1 implements
			routines.system.IPersistableRow<OnRowsEndStructtAggregateRow_1> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public float CM_A_FAIRE;

		public float getCM_A_FAIRE() {
			return this.CM_A_FAIRE;
		}

		public float TD_A_FAIRE;

		public float getTD_A_FAIRE() {
			return this.TD_A_FAIRE;
		}

		public float TP_A_FAIRE;

		public float getTP_A_FAIRE() {
			return this.TP_A_FAIRE;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final OnRowsEndStructtAggregateRow_1 other = (OnRowsEndStructtAggregateRow_1) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(OnRowsEndStructtAggregateRow_1 other) {

			other.codemodsemestre = this.codemodsemestre;
			other.CM_A_FAIRE = this.CM_A_FAIRE;
			other.TD_A_FAIRE = this.TD_A_FAIRE;
			other.TP_A_FAIRE = this.TP_A_FAIRE;

		}

		public void copyKeysDataTo(OnRowsEndStructtAggregateRow_1 other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.CM_A_FAIRE = dis.readFloat();

					this.TD_A_FAIRE = dis.readFloat();

					this.TP_A_FAIRE = dis.readFloat();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// float

				dos.writeFloat(this.CM_A_FAIRE);

				// float

				dos.writeFloat(this.TD_A_FAIRE);

				// float

				dos.writeFloat(this.TP_A_FAIRE);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",CM_A_FAIRE=" + String.valueOf(CM_A_FAIRE));
			sb.append(",TD_A_FAIRE=" + String.valueOf(TD_A_FAIRE));
			sb.append(",TP_A_FAIRE=" + String.valueOf(TP_A_FAIRE));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(OnRowsEndStructtAggregateRow_1 other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class out1Struct implements
			routines.system.IPersistableRow<out1Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public double CM_A_FAIRE;

		public double getCM_A_FAIRE() {
			return this.CM_A_FAIRE;
		}

		public double TD_A_FAIRE;

		public double getTD_A_FAIRE() {
			return this.TD_A_FAIRE;
		}

		public double TP_A_FAIRE;

		public double getTP_A_FAIRE() {
			return this.TP_A_FAIRE;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final out1Struct other = (out1Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(out1Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.CM_A_FAIRE = this.CM_A_FAIRE;
			other.TD_A_FAIRE = this.TD_A_FAIRE;
			other.TP_A_FAIRE = this.TP_A_FAIRE;

		}

		public void copyKeysDataTo(out1Struct other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.CM_A_FAIRE = dis.readDouble();

					this.TD_A_FAIRE = dis.readDouble();

					this.TP_A_FAIRE = dis.readDouble();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// double

				dos.writeDouble(this.CM_A_FAIRE);

				// double

				dos.writeDouble(this.TD_A_FAIRE);

				// double

				dos.writeDouble(this.TP_A_FAIRE);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",CM_A_FAIRE=" + String.valueOf(CM_A_FAIRE));
			sb.append(",TD_A_FAIRE=" + String.valueOf(TD_A_FAIRE));
			sb.append(",TP_A_FAIRE=" + String.valueOf(TP_A_FAIRE));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(out1Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row1Struct implements
			routines.system.IPersistableRow<row1Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public double heuresCM;

		public double getHeuresCM() {
			return this.heuresCM;
		}

		public int nombregroupes;

		public int getNombregroupes() {
			return this.nombregroupes;
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.heuresCM = dis.readDouble();

					this.nombregroupes = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// double

				dos.writeDouble(this.heuresCM);

				// int

				dos.writeInt(this.nombregroupes);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",heuresCM=" + String.valueOf(heuresCM));
			sb.append(",nombregroupes=" + String.valueOf(nombregroupes));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row1Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class after_tDBInput_5Struct implements
			routines.system.IPersistableRow<after_tDBInput_5Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public double heuresCM;

		public double getHeuresCM() {
			return this.heuresCM;
		}

		public int nombregroupes;

		public int getNombregroupes() {
			return this.nombregroupes;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final after_tDBInput_5Struct other = (after_tDBInput_5Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(after_tDBInput_5Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.heuresCM = this.heuresCM;
			other.nombregroupes = this.nombregroupes;

		}

		public void copyKeysDataTo(after_tDBInput_5Struct other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.heuresCM = dis.readDouble();

					this.nombregroupes = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// double

				dos.writeDouble(this.heuresCM);

				// int

				dos.writeInt(this.nombregroupes);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",heuresCM=" + String.valueOf(heuresCM));
			sb.append(",nombregroupes=" + String.valueOf(nombregroupes));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(after_tDBInput_5Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_5Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_5_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				tDBInput_6Process(globalMap);
				tDBInput_7Process(globalMap);

				row1Struct row1 = new row1Struct();
				out1Struct out1 = new out1Struct();
				row7Struct row7 = new row7Struct();

				/**
				 * [tAggregateRow_1_AGGOUT begin ] start
				 */

				ok_Hash.put("tAggregateRow_1_AGGOUT", false);
				start_Hash.put("tAggregateRow_1_AGGOUT",
						System.currentTimeMillis());

				currentVirtualComponent = "tAggregateRow_1";

				currentComponent = "tAggregateRow_1_AGGOUT";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("out1" + iterateId, 0, 0);

					}
				}

				int tos_count_tAggregateRow_1_AGGOUT = 0;

				class BytesLimit65535_tAggregateRow_1_AGGOUT {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAggregateRow_1_AGGOUT().limitLog4jByte();

				// ------------ Seems it is not used

				java.util.Map hashAggreg_tAggregateRow_1 = new java.util.HashMap();

				// ------------

				class UtilClass_tAggregateRow_1 { // G_OutBegin_AggR_144

					public double sd(Double[] data) {
						final int n = data.length;
						if (n < 2) {
							return Double.NaN;
						}
						double d1 = 0d;
						double d2 = 0d;

						for (int i = 0; i < data.length; i++) {
							d1 += (data[i] * data[i]);
							d2 += data[i];
						}

						return Math.sqrt((n * d1 - d2 * d2) / n / (n - 1));
					}

					public void checkedIADD(byte a, byte b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						byte r = (byte) (a + b);
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'short/Short'", "'byte/Byte'"));
						}
					}

					public void checkedIADD(short a, short b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						short r = (short) (a + b);
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'int/Integer'", "'short/Short'"));
						}
					}

					public void checkedIADD(int a, int b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						int r = a + b;
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'long/Long'", "'int/Integer'"));
						}
					}

					public void checkedIADD(long a, long b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						long r = a + b;
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'long/Long'"));
						}
					}

					public void checkedIADD(float a, float b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkUlp) {
							float minAddedValue = Math.ulp(a);
							if (minAddedValue > Math.abs(b)) {
								throw new RuntimeException(
										buildPrecisionMessage(
												String.valueOf(a),
												String.valueOf(b),
												"'double' or 'BigDecimal'",
												"'float/Float'"));
							}
						}

						if (checkTypeOverFlow
								&& ((double) a + (double) b > (double) Float.MAX_VALUE)
								|| ((double) a + (double) b < (double) -Float.MAX_VALUE)) {
							throw new RuntimeException(
									buildOverflowMessage(String.valueOf(a),
											String.valueOf(b),
											"'double' or 'BigDecimal'",
											"'float/Float'"));
						}
					}

					public void checkedIADD(double a, double b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkUlp) {
							double minAddedValue = Math.ulp(a);
							if (minAddedValue > Math.abs(b)) {
								throw new RuntimeException(
										buildPrecisionMessage(
												String.valueOf(a),
												String.valueOf(a),
												"'BigDecimal'",
												"'double/Double'"));
							}
						}

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, byte b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, short b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, int b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, float b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkUlp) {
							double minAddedValue = Math.ulp(a);
							if (minAddedValue > Math.abs(b)) {
								throw new RuntimeException(
										buildPrecisionMessage(
												String.valueOf(a),
												String.valueOf(a),
												"'BigDecimal'",
												"'double/Double'"));
							}
						}

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					private String buildOverflowMessage(String a, String b,
							String advicedTypes, String originalType) {
						return "Type overflow when adding "
								+ b
								+ " to "
								+ a
								+ ", to resolve this problem, increase the precision by using "
								+ advicedTypes + " type in place of "
								+ originalType + ".";
					}

					private String buildPrecisionMessage(String a, String b,
							String advicedTypes, String originalType) {
						return "The double precision is unsufficient to add the value "
								+ b
								+ " to "
								+ a
								+ ", to resolve this problem, increase the precision by using "
								+ advicedTypes
								+ " type in place of "
								+ originalType + ".";
					}

				} // G_OutBegin_AggR_144

				UtilClass_tAggregateRow_1 utilClass_tAggregateRow_1 = new UtilClass_tAggregateRow_1();

				class AggOperationStruct_tAggregateRow_1 { // G_OutBegin_AggR_100

					private static final int DEFAULT_HASHCODE = 1;
					private static final int PRIME = 31;
					private int hashCode = DEFAULT_HASHCODE;
					public boolean hashCodeDirty = true;

					int codemodsemestre;
					BigDecimal CM_A_FAIRE_sum;
					BigDecimal TD_A_FAIRE_sum;
					BigDecimal TP_A_FAIRE_sum;

					@Override
					public int hashCode() {
						if (this.hashCodeDirty) {
							final int prime = PRIME;
							int result = DEFAULT_HASHCODE;

							result = prime * result
									+ (int) this.codemodsemestre;

							this.hashCode = result;
							this.hashCodeDirty = false;
						}
						return this.hashCode;
					}

					@Override
					public boolean equals(Object obj) {
						if (this == obj)
							return true;
						if (obj == null)
							return false;
						if (getClass() != obj.getClass())
							return false;
						final AggOperationStruct_tAggregateRow_1 other = (AggOperationStruct_tAggregateRow_1) obj;

						if (this.codemodsemestre != other.codemodsemestre)
							return false;

						return true;
					}

				} // G_OutBegin_AggR_100

				AggOperationStruct_tAggregateRow_1 operation_result_tAggregateRow_1 = null;
				AggOperationStruct_tAggregateRow_1 operation_finder_tAggregateRow_1 = new AggOperationStruct_tAggregateRow_1();
				java.util.Map<AggOperationStruct_tAggregateRow_1, AggOperationStruct_tAggregateRow_1> hash_tAggregateRow_1 = new java.util.HashMap<AggOperationStruct_tAggregateRow_1, AggOperationStruct_tAggregateRow_1>();

				/**
				 * [tAggregateRow_1_AGGOUT begin ] stop
				 */

				/**
				 * [tMap_2 begin ] start
				 */

				ok_Hash.put("tMap_2", false);
				start_Hash.put("tMap_2", System.currentTimeMillis());

				currentComponent = "tMap_2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row1" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_2 = 0;

				class BytesLimit65535_tMap_2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tMap_2().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row2Struct> tHash_Lookup_row2 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row2Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row2Struct>) globalMap
						.get("tHash_Lookup_row2"));

				row2Struct row2HashKey = new row2Struct();
				row2Struct row2Default = new row2Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row3Struct> tHash_Lookup_row3 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row3Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row3Struct>) globalMap
						.get("tHash_Lookup_row3"));

				row3Struct row3HashKey = new row3Struct();
				row3Struct row3Default = new row3Struct();
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_2__Struct {
				}
				Var__tMap_2__Struct Var__tMap_2 = new Var__tMap_2__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				out1Struct out1_tmp = new out1Struct();
				// ###############################

				/**
				 * [tMap_2 begin ] stop
				 */

				/**
				 * [tDBInput_5 begin ] start
				 */

				ok_Hash.put("tDBInput_5", false);
				start_Hash.put("tDBInput_5", System.currentTimeMillis());

				currentComponent = "tDBInput_5";

				int tos_count_tDBInput_5 = 0;

				class BytesLimit65535_tDBInput_5 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_5().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_5 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_5.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_5 = calendar_tDBInput_5.getTime();
				int nb_line_tDBInput_5 = 0;
				java.sql.Connection conn_tDBInput_5 = null;
				String driverClass_tDBInput_5 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_5);
				String dbUser_tDBInput_5 = "root";

				final String decryptedPassword_tDBInput_5 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_5 = decryptedPassword_tDBInput_5;

				String url_tDBInput_5 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_5 = java.sql.DriverManager.getConnection(
						url_tDBInput_5, dbUser_tDBInput_5, dbPwd_tDBInput_5);

				java.sql.Statement stmt_tDBInput_5 = conn_tDBInput_5
						.createStatement();

				String dbquery_tDBInput_5 = "SELECT \n  `horairesCM`.`codemodsemestre`, \n  `horairesCM`.`heuresCM`, \n  `horairesCM`.`nombregroupes`\nFROM `horairesCM`"
						+ "";

				globalMap.put("tDBInput_5_QUERY", dbquery_tDBInput_5);
				java.sql.ResultSet rs_tDBInput_5 = null;

				try {
					rs_tDBInput_5 = stmt_tDBInput_5
							.executeQuery(dbquery_tDBInput_5);
					java.sql.ResultSetMetaData rsmd_tDBInput_5 = rs_tDBInput_5
							.getMetaData();
					int colQtyInRs_tDBInput_5 = rsmd_tDBInput_5
							.getColumnCount();

					String tmpContent_tDBInput_5 = null;

					while (rs_tDBInput_5.next()) {
						nb_line_tDBInput_5++;

						if (colQtyInRs_tDBInput_5 < 1) {
							row1.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_5.getObject(1) != null) {
								row1.codemodsemestre = rs_tDBInput_5.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_5 < 2) {
							row1.heuresCM = 0;
						} else {

							if (rs_tDBInput_5.getObject(2) != null) {
								row1.heuresCM = rs_tDBInput_5.getDouble(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_5 < 3) {
							row1.nombregroupes = 0;
						} else {

							if (rs_tDBInput_5.getObject(3) != null) {
								row1.nombregroupes = rs_tDBInput_5.getInt(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_5 begin ] stop
						 */

						/**
						 * [tDBInput_5 main ] start
						 */

						currentComponent = "tDBInput_5";

						tos_count_tDBInput_5++;

						/**
						 * [tDBInput_5 main ] stop
						 */

						/**
						 * [tDBInput_5 process_data_begin ] start
						 */

						currentComponent = "tDBInput_5";

						/**
						 * [tDBInput_5 process_data_begin ] stop
						 */

						/**
						 * [tMap_2 main ] start
						 */

						currentComponent = "tMap_2";

						// row1
						// row1

						if (execStat) {
							runStat.updateStatOnConnection("row1" + iterateId,
									1, 1);
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_2 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_2 = false;
						boolean mainRowRejected_tMap_2 = false;

						// /////////////////////////////////////////////
						// Starting Lookup Table "row2"
						// /////////////////////////////////////////////

						boolean forceLooprow2 = false;

						row2Struct row2ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_2) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_2 = false;

							Object exprKeyValue_row2__codemodsemestre = row1.codemodsemestre;
							if (exprKeyValue_row2__codemodsemestre == null) {
								hasCasePrimitiveKeyWithNull_tMap_2 = true;
							} else {
								row2HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row2__codemodsemestre;
							}

							row2HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_2) { // G_TM_M_091

								tHash_Lookup_row2.lookup(row2HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_2
									|| !tHash_Lookup_row2.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_2 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row2 != null
								&& tHash_Lookup_row2.getCount(row2HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row2' and it contains more one result from keys :  row2.codemodsemestre = '"
							// + row2HashKey.codemodsemestre + "'");
						} // G 071

						row2Struct row2 = null;

						row2Struct fromLookup_row2 = null;
						row2 = row2Default;

						if (tHash_Lookup_row2 != null
								&& tHash_Lookup_row2.hasNext()) { // G 099

							fromLookup_row2 = tHash_Lookup_row2.next();

						} // G 099

						if (fromLookup_row2 != null) {
							row2 = fromLookup_row2;
						}

						// /////////////////////////////////////////////
						// Starting Lookup Table "row3"
						// /////////////////////////////////////////////

						boolean forceLooprow3 = false;

						row3Struct row3ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_2) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_2 = false;

							Object exprKeyValue_row3__codemodsemestre = row1.codemodsemestre;
							if (exprKeyValue_row3__codemodsemestre == null) {
								hasCasePrimitiveKeyWithNull_tMap_2 = true;
							} else {
								row3HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row3__codemodsemestre;
							}

							row3HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_2) { // G_TM_M_091

								tHash_Lookup_row3.lookup(row3HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_2
									|| !tHash_Lookup_row3.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_2 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row3 != null
								&& tHash_Lookup_row3.getCount(row3HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row3' and it contains more one result from keys :  row3.codemodsemestre = '"
							// + row3HashKey.codemodsemestre + "'");
						} // G 071

						row3Struct row3 = null;

						row3Struct fromLookup_row3 = null;
						row3 = row3Default;

						if (tHash_Lookup_row3 != null
								&& tHash_Lookup_row3.hasNext()) { // G 099

							fromLookup_row3 = tHash_Lookup_row3.next();

						} // G 099

						if (fromLookup_row3 != null) {
							row3 = fromLookup_row3;
						}

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_2__Struct Var = Var__tMap_2;// ###############################
							// ###############################
							// # Output tables

							out1 = null;

							if (!rejectedInnerJoin_tMap_2) {

								// # Output table : 'out1'
								out1_tmp.codemodsemestre = row1.codemodsemestre;
								out1_tmp.CM_A_FAIRE = row1.heuresCM
										* row1.nombregroupes;
								out1_tmp.TD_A_FAIRE = row2.heuresTD
										* row2.nombregroupes;
								out1_tmp.TP_A_FAIRE = row3.heuresTP
										* row3.nombregroupes;
								out1 = out1_tmp;
							} // closing inner join bracket (2)
								// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_2 = false;

						tos_count_tMap_2++;

						/**
						 * [tMap_2 main ] stop
						 */

						/**
						 * [tMap_2 process_data_begin ] start
						 */

						currentComponent = "tMap_2";

						/**
						 * [tMap_2 process_data_begin ] stop
						 */
						// Start of branch "out1"
						if (out1 != null) {

							/**
							 * [tAggregateRow_1_AGGOUT main ] start
							 */

							currentVirtualComponent = "tAggregateRow_1";

							currentComponent = "tAggregateRow_1_AGGOUT";

							// out1
							// out1

							if (execStat) {
								runStat.updateStatOnConnection("out1"
										+ iterateId, 1, 1);
							}

							operation_finder_tAggregateRow_1.codemodsemestre = out1.codemodsemestre;

							operation_finder_tAggregateRow_1.hashCodeDirty = true;

							operation_result_tAggregateRow_1 = hash_tAggregateRow_1
									.get(operation_finder_tAggregateRow_1);

							if (operation_result_tAggregateRow_1 == null) { // G_OutMain_AggR_001

								operation_result_tAggregateRow_1 = new AggOperationStruct_tAggregateRow_1();

								operation_result_tAggregateRow_1.codemodsemestre = operation_finder_tAggregateRow_1.codemodsemestre;

								hash_tAggregateRow_1.put(
										operation_result_tAggregateRow_1,
										operation_result_tAggregateRow_1);

							} // G_OutMain_AggR_001

							if (operation_result_tAggregateRow_1.CM_A_FAIRE_sum == null) {
								operation_result_tAggregateRow_1.CM_A_FAIRE_sum = new BigDecimal(
										0).setScale(2);
							}
							operation_result_tAggregateRow_1.CM_A_FAIRE_sum = operation_result_tAggregateRow_1.CM_A_FAIRE_sum
									.add(new BigDecimal(String
											.valueOf(out1.CM_A_FAIRE)));

							if (operation_result_tAggregateRow_1.TD_A_FAIRE_sum == null) {
								operation_result_tAggregateRow_1.TD_A_FAIRE_sum = new BigDecimal(
										0).setScale(2);
							}
							operation_result_tAggregateRow_1.TD_A_FAIRE_sum = operation_result_tAggregateRow_1.TD_A_FAIRE_sum
									.add(new BigDecimal(String
											.valueOf(out1.TD_A_FAIRE)));

							if (operation_result_tAggregateRow_1.TP_A_FAIRE_sum == null) {
								operation_result_tAggregateRow_1.TP_A_FAIRE_sum = new BigDecimal(
										0).setScale(2);
							}
							operation_result_tAggregateRow_1.TP_A_FAIRE_sum = operation_result_tAggregateRow_1.TP_A_FAIRE_sum
									.add(new BigDecimal(String
											.valueOf(out1.TP_A_FAIRE)));

							tos_count_tAggregateRow_1_AGGOUT++;

							/**
							 * [tAggregateRow_1_AGGOUT main ] stop
							 */

							/**
							 * [tAggregateRow_1_AGGOUT process_data_begin ]
							 * start
							 */

							currentVirtualComponent = "tAggregateRow_1";

							currentComponent = "tAggregateRow_1_AGGOUT";

							/**
							 * [tAggregateRow_1_AGGOUT process_data_begin ] stop
							 */

							/**
							 * [tAggregateRow_1_AGGOUT process_data_end ] start
							 */

							currentVirtualComponent = "tAggregateRow_1";

							currentComponent = "tAggregateRow_1_AGGOUT";

							/**
							 * [tAggregateRow_1_AGGOUT process_data_end ] stop
							 */

						} // End of branch "out1"

						/**
						 * [tMap_2 process_data_end ] start
						 */

						currentComponent = "tMap_2";

						/**
						 * [tMap_2 process_data_end ] stop
						 */

						/**
						 * [tDBInput_5 process_data_end ] start
						 */

						currentComponent = "tDBInput_5";

						/**
						 * [tDBInput_5 process_data_end ] stop
						 */

						/**
						 * [tDBInput_5 end ] start
						 */

						currentComponent = "tDBInput_5";

					}
				} finally {
					if (rs_tDBInput_5 != null) {
						rs_tDBInput_5.close();
					}
					stmt_tDBInput_5.close();
					if (conn_tDBInput_5 != null && !conn_tDBInput_5.isClosed()) {

						conn_tDBInput_5.close();

					}

				}

				globalMap.put("tDBInput_5_NB_LINE", nb_line_tDBInput_5);

				ok_Hash.put("tDBInput_5", true);
				end_Hash.put("tDBInput_5", System.currentTimeMillis());

				/**
				 * [tDBInput_5 end ] stop
				 */

				/**
				 * [tMap_2 end ] start
				 */

				currentComponent = "tMap_2";

				// ###############################
				// # Lookup hashes releasing
				if (tHash_Lookup_row2 != null) {
					tHash_Lookup_row2.endGet();
				}
				globalMap.remove("tHash_Lookup_row2");

				if (tHash_Lookup_row3 != null) {
					tHash_Lookup_row3.endGet();
				}
				globalMap.remove("tHash_Lookup_row3");

				// ###############################

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row1" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tMap_2", true);
				end_Hash.put("tMap_2", System.currentTimeMillis());

				/**
				 * [tMap_2 end ] stop
				 */

				/**
				 * [tAggregateRow_1_AGGOUT end ] start
				 */

				currentVirtualComponent = "tAggregateRow_1";

				currentComponent = "tAggregateRow_1_AGGOUT";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("out1" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAggregateRow_1_AGGOUT", true);
				end_Hash.put("tAggregateRow_1_AGGOUT",
						System.currentTimeMillis());

				/**
				 * [tAggregateRow_1_AGGOUT end ] stop
				 */

				/**
				 * [tAdvancedHash_row7 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row7", false);
				start_Hash
						.put("tAdvancedHash_row7", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row7";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row7" + iterateId, 0, 0);

					}
				}

				int tos_count_tAdvancedHash_row7 = 0;

				class BytesLimit65535_tAdvancedHash_row7 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row7().limitLog4jByte();

				// connection name:row7
				// source node:tAggregateRow_1_AGGIN - inputs:(OnRowsEnd)
				// outputs:(row7,row7) | target node:tAdvancedHash_row7 -
				// inputs:(row7) outputs:()
				// linked node: tMap_4 - inputs:(row7,MODULE,row8)
				// outputs:(MODULES_SEMESTRE,DIM_SEMESTRE_MOD)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row7 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row7Struct> tHash_Lookup_row7 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row7Struct> getLookup(matchingModeEnum_row7);

				globalMap.put("tHash_Lookup_row7", tHash_Lookup_row7);

				/**
				 * [tAdvancedHash_row7 begin ] stop
				 */

				/**
				 * [tAggregateRow_1_AGGIN begin ] start
				 */

				ok_Hash.put("tAggregateRow_1_AGGIN", false);
				start_Hash.put("tAggregateRow_1_AGGIN",
						System.currentTimeMillis());

				currentVirtualComponent = "tAggregateRow_1";

				currentComponent = "tAggregateRow_1_AGGIN";

				int tos_count_tAggregateRow_1_AGGIN = 0;

				class BytesLimit65535_tAggregateRow_1_AGGIN {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAggregateRow_1_AGGIN().limitLog4jByte();

				java.util.Collection<AggOperationStruct_tAggregateRow_1> values_tAggregateRow_1 = hash_tAggregateRow_1
						.values();

				globalMap.put("tAggregateRow_1_NB_LINE",
						values_tAggregateRow_1.size());

				for (AggOperationStruct_tAggregateRow_1 aggregated_row_tAggregateRow_1 : values_tAggregateRow_1) { // G_AggR_600

					/**
					 * [tAggregateRow_1_AGGIN begin ] stop
					 */

					/**
					 * [tAggregateRow_1_AGGIN main ] start
					 */

					currentVirtualComponent = "tAggregateRow_1";

					currentComponent = "tAggregateRow_1_AGGIN";

					row7.codemodsemestre = aggregated_row_tAggregateRow_1.codemodsemestre;

					if (aggregated_row_tAggregateRow_1.CM_A_FAIRE_sum != null) {
						row7.CM_A_FAIRE = aggregated_row_tAggregateRow_1.CM_A_FAIRE_sum
								.floatValue();

					} else {

						row7.CM_A_FAIRE = 0;

					}

					if (aggregated_row_tAggregateRow_1.TD_A_FAIRE_sum != null) {
						row7.TD_A_FAIRE = aggregated_row_tAggregateRow_1.TD_A_FAIRE_sum
								.floatValue();

					} else {

						row7.TD_A_FAIRE = 0;

					}

					if (aggregated_row_tAggregateRow_1.TP_A_FAIRE_sum != null) {
						row7.TP_A_FAIRE = aggregated_row_tAggregateRow_1.TP_A_FAIRE_sum
								.floatValue();

					} else {

						row7.TP_A_FAIRE = 0;

					}

					tos_count_tAggregateRow_1_AGGIN++;

					/**
					 * [tAggregateRow_1_AGGIN main ] stop
					 */

					/**
					 * [tAggregateRow_1_AGGIN process_data_begin ] start
					 */

					currentVirtualComponent = "tAggregateRow_1";

					currentComponent = "tAggregateRow_1_AGGIN";

					/**
					 * [tAggregateRow_1_AGGIN process_data_begin ] stop
					 */

					/**
					 * [tAdvancedHash_row7 main ] start
					 */

					currentComponent = "tAdvancedHash_row7";

					// row7
					// row7

					if (execStat) {
						runStat.updateStatOnConnection("row7" + iterateId, 1, 1);
					}

					row7Struct row7_HashRow = new row7Struct();

					row7_HashRow.codemodsemestre = row7.codemodsemestre;

					row7_HashRow.CM_A_FAIRE = row7.CM_A_FAIRE;

					row7_HashRow.TD_A_FAIRE = row7.TD_A_FAIRE;

					row7_HashRow.TP_A_FAIRE = row7.TP_A_FAIRE;

					tHash_Lookup_row7.put(row7_HashRow);

					tos_count_tAdvancedHash_row7++;

					/**
					 * [tAdvancedHash_row7 main ] stop
					 */

					/**
					 * [tAdvancedHash_row7 process_data_begin ] start
					 */

					currentComponent = "tAdvancedHash_row7";

					/**
					 * [tAdvancedHash_row7 process_data_begin ] stop
					 */

					/**
					 * [tAdvancedHash_row7 process_data_end ] start
					 */

					currentComponent = "tAdvancedHash_row7";

					/**
					 * [tAdvancedHash_row7 process_data_end ] stop
					 */

					/**
					 * [tAggregateRow_1_AGGIN process_data_end ] start
					 */

					currentVirtualComponent = "tAggregateRow_1";

					currentComponent = "tAggregateRow_1_AGGIN";

					/**
					 * [tAggregateRow_1_AGGIN process_data_end ] stop
					 */

					/**
					 * [tAggregateRow_1_AGGIN end ] start
					 */

					currentVirtualComponent = "tAggregateRow_1";

					currentComponent = "tAggregateRow_1_AGGIN";

				} // G_AggR_600

				ok_Hash.put("tAggregateRow_1_AGGIN", true);
				end_Hash.put("tAggregateRow_1_AGGIN",
						System.currentTimeMillis());

				/**
				 * [tAggregateRow_1_AGGIN end ] stop
				 */

				/**
				 * [tAdvancedHash_row7 end ] start
				 */

				currentComponent = "tAdvancedHash_row7";

				tHash_Lookup_row7.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row7" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAdvancedHash_row7", true);
				end_Hash.put("tAdvancedHash_row7", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row7 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			// free memory for "tAggregateRow_1_AGGIN"
			globalMap.remove("tAggregateRow_1");

			// free memory for "tMap_2"
			globalMap.remove("tHash_Lookup_row2");

			// free memory for "tMap_2"
			globalMap.remove("tHash_Lookup_row3");

			try {

				/**
				 * [tDBInput_5 finally ] start
				 */

				currentComponent = "tDBInput_5";

				/**
				 * [tDBInput_5 finally ] stop
				 */

				/**
				 * [tMap_2 finally ] start
				 */

				currentComponent = "tMap_2";

				/**
				 * [tMap_2 finally ] stop
				 */

				/**
				 * [tAggregateRow_1_AGGOUT finally ] start
				 */

				currentVirtualComponent = "tAggregateRow_1";

				currentComponent = "tAggregateRow_1_AGGOUT";

				/**
				 * [tAggregateRow_1_AGGOUT finally ] stop
				 */

				/**
				 * [tAggregateRow_1_AGGIN finally ] start
				 */

				currentVirtualComponent = "tAggregateRow_1";

				currentComponent = "tAggregateRow_1_AGGIN";

				/**
				 * [tAggregateRow_1_AGGIN finally ] stop
				 */

				/**
				 * [tAdvancedHash_row7 finally ] start
				 */

				currentComponent = "tAdvancedHash_row7";

				/**
				 * [tAdvancedHash_row7 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_5_SUBPROCESS_STATE", 1);
	}

	public static class row2Struct implements
			routines.system.IPersistableComparableLookupRow<row2Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public double heuresTD;

		public double getHeuresTD() {
			return this.heuresTD;
		}

		public int nombregroupes;

		public int getNombregroupes() {
			return this.nombregroupes;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row2Struct other = (row2Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(row2Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.heuresTD = this.heuresTD;
			other.nombregroupes = this.nombregroupes;

		}

		public void copyKeysDataTo(row2Struct other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.heuresTD = dis.readDouble();

				this.nombregroupes = dis.readInt();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				dos.writeDouble(this.heuresTD);

				dos.writeInt(this.nombregroupes);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",heuresTD=" + String.valueOf(heuresTD));
			sb.append(",nombregroupes=" + String.valueOf(nombregroupes));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row2Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_6Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_6_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row2Struct row2 = new row2Struct();

				/**
				 * [tAdvancedHash_row2 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row2", false);
				start_Hash
						.put("tAdvancedHash_row2", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row2";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row2" + iterateId, 0, 0);

					}
				}

				int tos_count_tAdvancedHash_row2 = 0;

				class BytesLimit65535_tAdvancedHash_row2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row2().limitLog4jByte();

				// connection name:row2
				// source node:tDBInput_6 - inputs:(after_tDBInput_5)
				// outputs:(row2,row2) | target node:tAdvancedHash_row2 -
				// inputs:(row2) outputs:()
				// linked node: tMap_2 - inputs:(row1,row2,row3) outputs:(out1)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row2 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row2Struct> tHash_Lookup_row2 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row2Struct> getLookup(matchingModeEnum_row2);

				globalMap.put("tHash_Lookup_row2", tHash_Lookup_row2);

				/**
				 * [tAdvancedHash_row2 begin ] stop
				 */

				/**
				 * [tDBInput_6 begin ] start
				 */

				ok_Hash.put("tDBInput_6", false);
				start_Hash.put("tDBInput_6", System.currentTimeMillis());

				currentComponent = "tDBInput_6";

				int tos_count_tDBInput_6 = 0;

				class BytesLimit65535_tDBInput_6 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_6().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_6 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_6.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_6 = calendar_tDBInput_6.getTime();
				int nb_line_tDBInput_6 = 0;
				java.sql.Connection conn_tDBInput_6 = null;
				String driverClass_tDBInput_6 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_6);
				String dbUser_tDBInput_6 = "root";

				final String decryptedPassword_tDBInput_6 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_6 = decryptedPassword_tDBInput_6;

				String url_tDBInput_6 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_6 = java.sql.DriverManager.getConnection(
						url_tDBInput_6, dbUser_tDBInput_6, dbPwd_tDBInput_6);

				java.sql.Statement stmt_tDBInput_6 = conn_tDBInput_6
						.createStatement();

				String dbquery_tDBInput_6 = "SELECT \n  `horairesTD`.`codemodsemestre`, \n  `horairesTD`.`heuresTD`, \n  `horairesTD`.`nombregroupes`\nFROM `horairesTD`"
						+ "";

				globalMap.put("tDBInput_6_QUERY", dbquery_tDBInput_6);
				java.sql.ResultSet rs_tDBInput_6 = null;

				try {
					rs_tDBInput_6 = stmt_tDBInput_6
							.executeQuery(dbquery_tDBInput_6);
					java.sql.ResultSetMetaData rsmd_tDBInput_6 = rs_tDBInput_6
							.getMetaData();
					int colQtyInRs_tDBInput_6 = rsmd_tDBInput_6
							.getColumnCount();

					String tmpContent_tDBInput_6 = null;

					while (rs_tDBInput_6.next()) {
						nb_line_tDBInput_6++;

						if (colQtyInRs_tDBInput_6 < 1) {
							row2.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_6.getObject(1) != null) {
								row2.codemodsemestre = rs_tDBInput_6.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_6 < 2) {
							row2.heuresTD = 0;
						} else {

							if (rs_tDBInput_6.getObject(2) != null) {
								row2.heuresTD = rs_tDBInput_6.getDouble(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_6 < 3) {
							row2.nombregroupes = 0;
						} else {

							if (rs_tDBInput_6.getObject(3) != null) {
								row2.nombregroupes = rs_tDBInput_6.getInt(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_6 begin ] stop
						 */

						/**
						 * [tDBInput_6 main ] start
						 */

						currentComponent = "tDBInput_6";

						tos_count_tDBInput_6++;

						/**
						 * [tDBInput_6 main ] stop
						 */

						/**
						 * [tDBInput_6 process_data_begin ] start
						 */

						currentComponent = "tDBInput_6";

						/**
						 * [tDBInput_6 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row2 main ] start
						 */

						currentComponent = "tAdvancedHash_row2";

						// row2
						// row2

						if (execStat) {
							runStat.updateStatOnConnection("row2" + iterateId,
									1, 1);
						}

						row2Struct row2_HashRow = new row2Struct();

						row2_HashRow.codemodsemestre = row2.codemodsemestre;

						row2_HashRow.heuresTD = row2.heuresTD;

						row2_HashRow.nombregroupes = row2.nombregroupes;

						tHash_Lookup_row2.put(row2_HashRow);

						tos_count_tAdvancedHash_row2++;

						/**
						 * [tAdvancedHash_row2 main ] stop
						 */

						/**
						 * [tAdvancedHash_row2 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row2";

						/**
						 * [tAdvancedHash_row2 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row2 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row2";

						/**
						 * [tAdvancedHash_row2 process_data_end ] stop
						 */

						/**
						 * [tDBInput_6 process_data_end ] start
						 */

						currentComponent = "tDBInput_6";

						/**
						 * [tDBInput_6 process_data_end ] stop
						 */

						/**
						 * [tDBInput_6 end ] start
						 */

						currentComponent = "tDBInput_6";

					}
				} finally {
					if (rs_tDBInput_6 != null) {
						rs_tDBInput_6.close();
					}
					stmt_tDBInput_6.close();
					if (conn_tDBInput_6 != null && !conn_tDBInput_6.isClosed()) {

						conn_tDBInput_6.close();

					}

				}

				globalMap.put("tDBInput_6_NB_LINE", nb_line_tDBInput_6);

				ok_Hash.put("tDBInput_6", true);
				end_Hash.put("tDBInput_6", System.currentTimeMillis());

				/**
				 * [tDBInput_6 end ] stop
				 */

				/**
				 * [tAdvancedHash_row2 end ] start
				 */

				currentComponent = "tAdvancedHash_row2";

				tHash_Lookup_row2.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row2" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAdvancedHash_row2", true);
				end_Hash.put("tAdvancedHash_row2", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row2 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_6 finally ] start
				 */

				currentComponent = "tDBInput_6";

				/**
				 * [tDBInput_6 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row2 finally ] start
				 */

				currentComponent = "tAdvancedHash_row2";

				/**
				 * [tAdvancedHash_row2 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_6_SUBPROCESS_STATE", 1);
	}

	public static class row3Struct implements
			routines.system.IPersistableComparableLookupRow<row3Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public double heuresTP;

		public double getHeuresTP() {
			return this.heuresTP;
		}

		public int nombregroupes;

		public int getNombregroupes() {
			return this.nombregroupes;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row3Struct other = (row3Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(row3Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.heuresTP = this.heuresTP;
			other.nombregroupes = this.nombregroupes;

		}

		public void copyKeysDataTo(row3Struct other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.heuresTP = dis.readDouble();

				this.nombregroupes = dis.readInt();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				dos.writeDouble(this.heuresTP);

				dos.writeInt(this.nombregroupes);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",heuresTP=" + String.valueOf(heuresTP));
			sb.append(",nombregroupes=" + String.valueOf(nombregroupes));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row3Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_7Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_7_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row3Struct row3 = new row3Struct();

				/**
				 * [tAdvancedHash_row3 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row3", false);
				start_Hash
						.put("tAdvancedHash_row3", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row3" + iterateId, 0, 0);

					}
				}

				int tos_count_tAdvancedHash_row3 = 0;

				class BytesLimit65535_tAdvancedHash_row3 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row3().limitLog4jByte();

				// connection name:row3
				// source node:tDBInput_7 - inputs:(after_tDBInput_5)
				// outputs:(row3,row3) | target node:tAdvancedHash_row3 -
				// inputs:(row3) outputs:()
				// linked node: tMap_2 - inputs:(row1,row2,row3) outputs:(out1)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row3 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row3Struct> tHash_Lookup_row3 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row3Struct> getLookup(matchingModeEnum_row3);

				globalMap.put("tHash_Lookup_row3", tHash_Lookup_row3);

				/**
				 * [tAdvancedHash_row3 begin ] stop
				 */

				/**
				 * [tDBInput_7 begin ] start
				 */

				ok_Hash.put("tDBInput_7", false);
				start_Hash.put("tDBInput_7", System.currentTimeMillis());

				currentComponent = "tDBInput_7";

				int tos_count_tDBInput_7 = 0;

				class BytesLimit65535_tDBInput_7 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_7().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_7 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_7.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_7 = calendar_tDBInput_7.getTime();
				int nb_line_tDBInput_7 = 0;
				java.sql.Connection conn_tDBInput_7 = null;
				String driverClass_tDBInput_7 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_7);
				String dbUser_tDBInput_7 = "root";

				final String decryptedPassword_tDBInput_7 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_7 = decryptedPassword_tDBInput_7;

				String url_tDBInput_7 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_7 = java.sql.DriverManager.getConnection(
						url_tDBInput_7, dbUser_tDBInput_7, dbPwd_tDBInput_7);

				java.sql.Statement stmt_tDBInput_7 = conn_tDBInput_7
						.createStatement();

				String dbquery_tDBInput_7 = "SELECT \n  `horairesTP`.`codemodsemestre`, \n  `horairesTP`.`heuresTP`, \n  `horairesTP`.`nombregroupes`\nFROM `horairesTP`"
						+ "";

				globalMap.put("tDBInput_7_QUERY", dbquery_tDBInput_7);
				java.sql.ResultSet rs_tDBInput_7 = null;

				try {
					rs_tDBInput_7 = stmt_tDBInput_7
							.executeQuery(dbquery_tDBInput_7);
					java.sql.ResultSetMetaData rsmd_tDBInput_7 = rs_tDBInput_7
							.getMetaData();
					int colQtyInRs_tDBInput_7 = rsmd_tDBInput_7
							.getColumnCount();

					String tmpContent_tDBInput_7 = null;

					while (rs_tDBInput_7.next()) {
						nb_line_tDBInput_7++;

						if (colQtyInRs_tDBInput_7 < 1) {
							row3.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_7.getObject(1) != null) {
								row3.codemodsemestre = rs_tDBInput_7.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_7 < 2) {
							row3.heuresTP = 0;
						} else {

							if (rs_tDBInput_7.getObject(2) != null) {
								row3.heuresTP = rs_tDBInput_7.getDouble(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_7 < 3) {
							row3.nombregroupes = 0;
						} else {

							if (rs_tDBInput_7.getObject(3) != null) {
								row3.nombregroupes = rs_tDBInput_7.getInt(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_7 begin ] stop
						 */

						/**
						 * [tDBInput_7 main ] start
						 */

						currentComponent = "tDBInput_7";

						tos_count_tDBInput_7++;

						/**
						 * [tDBInput_7 main ] stop
						 */

						/**
						 * [tDBInput_7 process_data_begin ] start
						 */

						currentComponent = "tDBInput_7";

						/**
						 * [tDBInput_7 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row3 main ] start
						 */

						currentComponent = "tAdvancedHash_row3";

						// row3
						// row3

						if (execStat) {
							runStat.updateStatOnConnection("row3" + iterateId,
									1, 1);
						}

						row3Struct row3_HashRow = new row3Struct();

						row3_HashRow.codemodsemestre = row3.codemodsemestre;

						row3_HashRow.heuresTP = row3.heuresTP;

						row3_HashRow.nombregroupes = row3.nombregroupes;

						tHash_Lookup_row3.put(row3_HashRow);

						tos_count_tAdvancedHash_row3++;

						/**
						 * [tAdvancedHash_row3 main ] stop
						 */

						/**
						 * [tAdvancedHash_row3 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row3";

						/**
						 * [tAdvancedHash_row3 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row3 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row3";

						/**
						 * [tAdvancedHash_row3 process_data_end ] stop
						 */

						/**
						 * [tDBInput_7 process_data_end ] start
						 */

						currentComponent = "tDBInput_7";

						/**
						 * [tDBInput_7 process_data_end ] stop
						 */

						/**
						 * [tDBInput_7 end ] start
						 */

						currentComponent = "tDBInput_7";

					}
				} finally {
					if (rs_tDBInput_7 != null) {
						rs_tDBInput_7.close();
					}
					stmt_tDBInput_7.close();
					if (conn_tDBInput_7 != null && !conn_tDBInput_7.isClosed()) {

						conn_tDBInput_7.close();

					}

				}

				globalMap.put("tDBInput_7_NB_LINE", nb_line_tDBInput_7);

				ok_Hash.put("tDBInput_7", true);
				end_Hash.put("tDBInput_7", System.currentTimeMillis());

				/**
				 * [tDBInput_7 end ] stop
				 */

				/**
				 * [tAdvancedHash_row3 end ] start
				 */

				currentComponent = "tAdvancedHash_row3";

				tHash_Lookup_row3.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row3" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAdvancedHash_row3", true);
				end_Hash.put("tAdvancedHash_row3", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row3 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_7 finally ] start
				 */

				currentComponent = "tDBInput_7";

				/**
				 * [tDBInput_7 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row3 finally ] start
				 */

				currentComponent = "tAdvancedHash_row3";

				/**
				 * [tAdvancedHash_row3 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_7_SUBPROCESS_STATE", 1);
	}

	public static class row30Struct implements
			routines.system.IPersistableRow<row30Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.anneedebut = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.anneedebut);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("anneedebut=" + String.valueOf(anneedebut));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row30Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class MODULES_SEMESTREStruct implements
			routines.system.IPersistableRow<MODULES_SEMESTREStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public String codeprefixe;

		public String getCodeprefixe() {
			return this.codeprefixe;
		}

		public String intitule;

		public String getIntitule() {
			return this.intitule;
		}

		public float CM_A_FAIRE;

		public float getCM_A_FAIRE() {
			return this.CM_A_FAIRE;
		}

		public float TD_A_FAIRE;

		public float getTD_A_FAIRE() {
			return this.TD_A_FAIRE;
		}

		public float TP_A_FAIRE;

		public float getTP_A_FAIRE() {
			return this.TP_A_FAIRE;
		}

		public double HEURES_A_FAIRE;

		public double getHEURES_A_FAIRE() {
			return this.HEURES_A_FAIRE;
		}

		public double CM_FAITES;

		public double getCM_FAITES() {
			return this.CM_FAITES;
		}

		public double TD_FAITES;

		public double getTD_FAITES() {
			return this.TD_FAITES;
		}

		public double TP_FAITES;

		public double getTP_FAITES() {
			return this.TP_FAITES;
		}

		public double HEURES_FAITES;

		public double getHEURES_FAITES() {
			return this.HEURES_FAITES;
		}

		public double BILAN_CM;

		public double getBILAN_CM() {
			return this.BILAN_CM;
		}

		public double BILAN_TD;

		public double getBILAN_TD() {
			return this.BILAN_TD;
		}

		public double BILAN_TP;

		public double getBILAN_TP() {
			return this.BILAN_TP;
		}

		public double BILAN_UE;

		public double getBILAN_UE() {
			return this.BILAN_UE;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		public int ID_RESP_MOD;

		public int getID_RESP_MOD() {
			return this.ID_RESP_MOD;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codesemestre;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.anneedebut;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final MODULES_SEMESTREStruct other = (MODULES_SEMESTREStruct) obj;

			if (this.codesemestre != other.codesemestre)
				return false;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.anneedebut != other.anneedebut)
				return false;

			return true;
		}

		public void copyDataTo(MODULES_SEMESTREStruct other) {

			other.codesemestre = this.codesemestre;
			other.codemodsemestre = this.codemodsemestre;
			other.codeprefixe = this.codeprefixe;
			other.intitule = this.intitule;
			other.CM_A_FAIRE = this.CM_A_FAIRE;
			other.TD_A_FAIRE = this.TD_A_FAIRE;
			other.TP_A_FAIRE = this.TP_A_FAIRE;
			other.HEURES_A_FAIRE = this.HEURES_A_FAIRE;
			other.CM_FAITES = this.CM_FAITES;
			other.TD_FAITES = this.TD_FAITES;
			other.TP_FAITES = this.TP_FAITES;
			other.HEURES_FAITES = this.HEURES_FAITES;
			other.BILAN_CM = this.BILAN_CM;
			other.BILAN_TD = this.BILAN_TD;
			other.BILAN_TP = this.BILAN_TP;
			other.BILAN_UE = this.BILAN_UE;
			other.anneedebut = this.anneedebut;
			other.ID_RESP_MOD = this.ID_RESP_MOD;

		}

		public void copyKeysDataTo(MODULES_SEMESTREStruct other) {

			other.codesemestre = this.codesemestre;
			other.codemodsemestre = this.codemodsemestre;
			other.anneedebut = this.anneedebut;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codesemestre = dis.readInt();

					this.codemodsemestre = dis.readInt();

					this.codeprefixe = readString(dis);

					this.intitule = readString(dis);

					this.CM_A_FAIRE = dis.readFloat();

					this.TD_A_FAIRE = dis.readFloat();

					this.TP_A_FAIRE = dis.readFloat();

					this.HEURES_A_FAIRE = dis.readDouble();

					this.CM_FAITES = dis.readDouble();

					this.TD_FAITES = dis.readDouble();

					this.TP_FAITES = dis.readDouble();

					this.HEURES_FAITES = dis.readDouble();

					this.BILAN_CM = dis.readDouble();

					this.BILAN_TD = dis.readDouble();

					this.BILAN_TP = dis.readDouble();

					this.BILAN_UE = dis.readDouble();

					this.anneedebut = dis.readInt();

					this.ID_RESP_MOD = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.codemodsemestre);

				// String

				writeString(this.codeprefixe, dos);

				// String

				writeString(this.intitule, dos);

				// float

				dos.writeFloat(this.CM_A_FAIRE);

				// float

				dos.writeFloat(this.TD_A_FAIRE);

				// float

				dos.writeFloat(this.TP_A_FAIRE);

				// double

				dos.writeDouble(this.HEURES_A_FAIRE);

				// double

				dos.writeDouble(this.CM_FAITES);

				// double

				dos.writeDouble(this.TD_FAITES);

				// double

				dos.writeDouble(this.TP_FAITES);

				// double

				dos.writeDouble(this.HEURES_FAITES);

				// double

				dos.writeDouble(this.BILAN_CM);

				// double

				dos.writeDouble(this.BILAN_TD);

				// double

				dos.writeDouble(this.BILAN_TP);

				// double

				dos.writeDouble(this.BILAN_UE);

				// int

				dos.writeInt(this.anneedebut);

				// int

				dos.writeInt(this.ID_RESP_MOD);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codesemestre=" + String.valueOf(codesemestre));
			sb.append(",codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",codeprefixe=" + codeprefixe);
			sb.append(",intitule=" + intitule);
			sb.append(",CM_A_FAIRE=" + String.valueOf(CM_A_FAIRE));
			sb.append(",TD_A_FAIRE=" + String.valueOf(TD_A_FAIRE));
			sb.append(",TP_A_FAIRE=" + String.valueOf(TP_A_FAIRE));
			sb.append(",HEURES_A_FAIRE=" + String.valueOf(HEURES_A_FAIRE));
			sb.append(",CM_FAITES=" + String.valueOf(CM_FAITES));
			sb.append(",TD_FAITES=" + String.valueOf(TD_FAITES));
			sb.append(",TP_FAITES=" + String.valueOf(TP_FAITES));
			sb.append(",HEURES_FAITES=" + String.valueOf(HEURES_FAITES));
			sb.append(",BILAN_CM=" + String.valueOf(BILAN_CM));
			sb.append(",BILAN_TD=" + String.valueOf(BILAN_TD));
			sb.append(",BILAN_TP=" + String.valueOf(BILAN_TP));
			sb.append(",BILAN_UE=" + String.valueOf(BILAN_UE));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append(",ID_RESP_MOD=" + String.valueOf(ID_RESP_MOD));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(MODULES_SEMESTREStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codesemestre,
					other.codesemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.anneedebut,
					other.anneedebut);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class DIM_SEMESTRE_MODStruct implements
			routines.system.IPersistableRow<DIM_SEMESTRE_MODStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public String codeprefixe;

		public String getCodeprefixe() {
			return this.codeprefixe;
		}

		public String intitule;

		public String getIntitule() {
			return this.intitule;
		}

		public float CM_A_FAIRE;

		public float getCM_A_FAIRE() {
			return this.CM_A_FAIRE;
		}

		public float TD_A_FAIRE;

		public float getTD_A_FAIRE() {
			return this.TD_A_FAIRE;
		}

		public float TP_A_FAIRE;

		public float getTP_A_FAIRE() {
			return this.TP_A_FAIRE;
		}

		public double HEURES_A_FAIRE;

		public double getHEURES_A_FAIRE() {
			return this.HEURES_A_FAIRE;
		}

		public double CM_FAITES;

		public double getCM_FAITES() {
			return this.CM_FAITES;
		}

		public double TD_FAITES;

		public double getTD_FAITES() {
			return this.TD_FAITES;
		}

		public double TP_FAITES;

		public double getTP_FAITES() {
			return this.TP_FAITES;
		}

		public double HEURES_FAITES;

		public double getHEURES_FAITES() {
			return this.HEURES_FAITES;
		}

		public double BILAN_CM;

		public double getBILAN_CM() {
			return this.BILAN_CM;
		}

		public double BILAN_TD;

		public double getBILAN_TD() {
			return this.BILAN_TD;
		}

		public double BILAN_TP;

		public double getBILAN_TP() {
			return this.BILAN_TP;
		}

		public double BILAN_UE;

		public double getBILAN_UE() {
			return this.BILAN_UE;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		public int ID_RESP_MOD;

		public int getID_RESP_MOD() {
			return this.ID_RESP_MOD;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codesemestre;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.anneedebut;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final DIM_SEMESTRE_MODStruct other = (DIM_SEMESTRE_MODStruct) obj;

			if (this.codesemestre != other.codesemestre)
				return false;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.anneedebut != other.anneedebut)
				return false;

			return true;
		}

		public void copyDataTo(DIM_SEMESTRE_MODStruct other) {

			other.codesemestre = this.codesemestre;
			other.codemodsemestre = this.codemodsemestre;
			other.codeprefixe = this.codeprefixe;
			other.intitule = this.intitule;
			other.CM_A_FAIRE = this.CM_A_FAIRE;
			other.TD_A_FAIRE = this.TD_A_FAIRE;
			other.TP_A_FAIRE = this.TP_A_FAIRE;
			other.HEURES_A_FAIRE = this.HEURES_A_FAIRE;
			other.CM_FAITES = this.CM_FAITES;
			other.TD_FAITES = this.TD_FAITES;
			other.TP_FAITES = this.TP_FAITES;
			other.HEURES_FAITES = this.HEURES_FAITES;
			other.BILAN_CM = this.BILAN_CM;
			other.BILAN_TD = this.BILAN_TD;
			other.BILAN_TP = this.BILAN_TP;
			other.BILAN_UE = this.BILAN_UE;
			other.anneedebut = this.anneedebut;
			other.ID_RESP_MOD = this.ID_RESP_MOD;

		}

		public void copyKeysDataTo(DIM_SEMESTRE_MODStruct other) {

			other.codesemestre = this.codesemestre;
			other.codemodsemestre = this.codemodsemestre;
			other.anneedebut = this.anneedebut;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codesemestre = dis.readInt();

					this.codemodsemestre = dis.readInt();

					this.codeprefixe = readString(dis);

					this.intitule = readString(dis);

					this.CM_A_FAIRE = dis.readFloat();

					this.TD_A_FAIRE = dis.readFloat();

					this.TP_A_FAIRE = dis.readFloat();

					this.HEURES_A_FAIRE = dis.readDouble();

					this.CM_FAITES = dis.readDouble();

					this.TD_FAITES = dis.readDouble();

					this.TP_FAITES = dis.readDouble();

					this.HEURES_FAITES = dis.readDouble();

					this.BILAN_CM = dis.readDouble();

					this.BILAN_TD = dis.readDouble();

					this.BILAN_TP = dis.readDouble();

					this.BILAN_UE = dis.readDouble();

					this.anneedebut = dis.readInt();

					this.ID_RESP_MOD = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.codemodsemestre);

				// String

				writeString(this.codeprefixe, dos);

				// String

				writeString(this.intitule, dos);

				// float

				dos.writeFloat(this.CM_A_FAIRE);

				// float

				dos.writeFloat(this.TD_A_FAIRE);

				// float

				dos.writeFloat(this.TP_A_FAIRE);

				// double

				dos.writeDouble(this.HEURES_A_FAIRE);

				// double

				dos.writeDouble(this.CM_FAITES);

				// double

				dos.writeDouble(this.TD_FAITES);

				// double

				dos.writeDouble(this.TP_FAITES);

				// double

				dos.writeDouble(this.HEURES_FAITES);

				// double

				dos.writeDouble(this.BILAN_CM);

				// double

				dos.writeDouble(this.BILAN_TD);

				// double

				dos.writeDouble(this.BILAN_TP);

				// double

				dos.writeDouble(this.BILAN_UE);

				// int

				dos.writeInt(this.anneedebut);

				// int

				dos.writeInt(this.ID_RESP_MOD);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codesemestre=" + String.valueOf(codesemestre));
			sb.append(",codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",codeprefixe=" + codeprefixe);
			sb.append(",intitule=" + intitule);
			sb.append(",CM_A_FAIRE=" + String.valueOf(CM_A_FAIRE));
			sb.append(",TD_A_FAIRE=" + String.valueOf(TD_A_FAIRE));
			sb.append(",TP_A_FAIRE=" + String.valueOf(TP_A_FAIRE));
			sb.append(",HEURES_A_FAIRE=" + String.valueOf(HEURES_A_FAIRE));
			sb.append(",CM_FAITES=" + String.valueOf(CM_FAITES));
			sb.append(",TD_FAITES=" + String.valueOf(TD_FAITES));
			sb.append(",TP_FAITES=" + String.valueOf(TP_FAITES));
			sb.append(",HEURES_FAITES=" + String.valueOf(HEURES_FAITES));
			sb.append(",BILAN_CM=" + String.valueOf(BILAN_CM));
			sb.append(",BILAN_TD=" + String.valueOf(BILAN_TD));
			sb.append(",BILAN_TP=" + String.valueOf(BILAN_TP));
			sb.append(",BILAN_UE=" + String.valueOf(BILAN_UE));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append(",ID_RESP_MOD=" + String.valueOf(ID_RESP_MOD));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(DIM_SEMESTRE_MODStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codesemestre,
					other.codesemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.anneedebut,
					other.anneedebut);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class MODULEStruct implements
			routines.system.IPersistableRow<MODULEStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codemod;

		public int getCodemod() {
			return this.codemod;
		}

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public String codeprefixe;

		public String getCodeprefixe() {
			return this.codeprefixe;
		}

		public String codesuffixe;

		public String getCodesuffixe() {
			return this.codesuffixe;
		}

		public String intitule;

		public String getIntitule() {
			return this.intitule;
		}

		public int responsable;

		public int getResponsable() {
			return this.responsable;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemod = dis.readInt();

					this.codesemestre = dis.readInt();

					this.codemodsemestre = dis.readInt();

					this.codeprefixe = readString(dis);

					this.codesuffixe = readString(dis);

					this.intitule = readString(dis);

					this.responsable = dis.readInt();

					this.anneedebut = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemod);

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.codemodsemestre);

				// String

				writeString(this.codeprefixe, dos);

				// String

				writeString(this.codesuffixe, dos);

				// String

				writeString(this.intitule, dos);

				// int

				dos.writeInt(this.responsable);

				// int

				dos.writeInt(this.anneedebut);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemod=" + String.valueOf(codemod));
			sb.append(",codesemestre=" + String.valueOf(codesemestre));
			sb.append(",codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",codeprefixe=" + codeprefixe);
			sb.append(",codesuffixe=" + codesuffixe);
			sb.append(",intitule=" + intitule);
			sb.append(",responsable=" + String.valueOf(responsable));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(MODULEStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class ANNE_MODULEStruct implements
			routines.system.IPersistableRow<ANNE_MODULEStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.anneedebut;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final ANNE_MODULEStruct other = (ANNE_MODULEStruct) obj;

			if (this.anneedebut != other.anneedebut)
				return false;

			return true;
		}

		public void copyDataTo(ANNE_MODULEStruct other) {

			other.anneedebut = this.anneedebut;

		}

		public void copyKeysDataTo(ANNE_MODULEStruct other) {

			other.anneedebut = this.anneedebut;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.anneedebut = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.anneedebut);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("anneedebut=" + String.valueOf(anneedebut));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(ANNE_MODULEStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.anneedebut,
					other.anneedebut);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row13Struct implements
			routines.system.IPersistableRow<row13Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codemod;

		public int getCodemod() {
			return this.codemod;
		}

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public boolean optionnel;

		public boolean getOptionnel() {
			return this.optionnel;
		}

		public boolean alternatif;

		public boolean getAlternatif() {
			return this.alternatif;
		}

		public Integer codealternatif;

		public Integer getCodealternatif() {
			return this.codealternatif;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemod = dis.readInt();

					this.codesemestre = dis.readInt();

					this.codemodsemestre = dis.readInt();

					this.optionnel = dis.readBoolean();

					this.alternatif = dis.readBoolean();

					this.codealternatif = readInteger(dis);

					this.verrou = dis.readBoolean();

					this.verrouDPT = dis.readBoolean();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemod);

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.codemodsemestre);

				// boolean

				dos.writeBoolean(this.optionnel);

				// boolean

				dos.writeBoolean(this.alternatif);

				// Integer

				writeInteger(this.codealternatif, dos);

				// boolean

				dos.writeBoolean(this.verrou);

				// boolean

				dos.writeBoolean(this.verrouDPT);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemod=" + String.valueOf(codemod));
			sb.append(",codesemestre=" + String.valueOf(codesemestre));
			sb.append(",codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",optionnel=" + String.valueOf(optionnel));
			sb.append(",alternatif=" + String.valueOf(alternatif));
			sb.append(",codealternatif=" + String.valueOf(codealternatif));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row13Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class after_tDBInput_8Struct implements
			routines.system.IPersistableRow<after_tDBInput_8Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemod;

		public int getCodemod() {
			return this.codemod;
		}

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public boolean optionnel;

		public boolean getOptionnel() {
			return this.optionnel;
		}

		public boolean alternatif;

		public boolean getAlternatif() {
			return this.alternatif;
		}

		public Integer codealternatif;

		public Integer getCodealternatif() {
			return this.codealternatif;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemod;

				result = prime * result + (int) this.codesemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final after_tDBInput_8Struct other = (after_tDBInput_8Struct) obj;

			if (this.codemod != other.codemod)
				return false;

			if (this.codesemestre != other.codesemestre)
				return false;

			return true;
		}

		public void copyDataTo(after_tDBInput_8Struct other) {

			other.codemod = this.codemod;
			other.codesemestre = this.codesemestre;
			other.codemodsemestre = this.codemodsemestre;
			other.optionnel = this.optionnel;
			other.alternatif = this.alternatif;
			other.codealternatif = this.codealternatif;
			other.verrou = this.verrou;
			other.verrouDPT = this.verrouDPT;

		}

		public void copyKeysDataTo(after_tDBInput_8Struct other) {

			other.codemod = this.codemod;
			other.codesemestre = this.codesemestre;

		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemod = dis.readInt();

					this.codesemestre = dis.readInt();

					this.codemodsemestre = dis.readInt();

					this.optionnel = dis.readBoolean();

					this.alternatif = dis.readBoolean();

					this.codealternatif = readInteger(dis);

					this.verrou = dis.readBoolean();

					this.verrouDPT = dis.readBoolean();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemod);

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.codemodsemestre);

				// boolean

				dos.writeBoolean(this.optionnel);

				// boolean

				dos.writeBoolean(this.alternatif);

				// Integer

				writeInteger(this.codealternatif, dos);

				// boolean

				dos.writeBoolean(this.verrou);

				// boolean

				dos.writeBoolean(this.verrouDPT);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemod=" + String.valueOf(codemod));
			sb.append(",codesemestre=" + String.valueOf(codesemestre));
			sb.append(",codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",optionnel=" + String.valueOf(optionnel));
			sb.append(",alternatif=" + String.valueOf(alternatif));
			sb.append(",codealternatif=" + String.valueOf(codealternatif));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(after_tDBInput_8Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemod, other.codemod);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.codesemestre,
					other.codesemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_8Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_8_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				tDBInput_5Process(globalMap);
				tDBInput_9Process(globalMap);
				tDBInput_12Process(globalMap);

				row13Struct row13 = new row13Struct();
				MODULEStruct MODULE = new MODULEStruct();
				MODULES_SEMESTREStruct MODULES_SEMESTRE = new MODULES_SEMESTREStruct();
				DIM_SEMESTRE_MODStruct DIM_SEMESTRE_MOD = new DIM_SEMESTRE_MODStruct();
				ANNE_MODULEStruct ANNE_MODULE = new ANNE_MODULEStruct();
				row30Struct row30 = new row30Struct();

				/**
				 * [tFileOutputExcel_3 begin ] start
				 */

				ok_Hash.put("tFileOutputExcel_3", false);
				start_Hash
						.put("tFileOutputExcel_3", System.currentTimeMillis());

				currentComponent = "tFileOutputExcel_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("MODULES_SEMESTRE"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tFileOutputExcel_3 = 0;

				class BytesLimit65535_tFileOutputExcel_3 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tFileOutputExcel_3().limitLog4jByte();

				int columnIndex_tFileOutputExcel_3 = 0;

				String fileName_tFileOutputExcel_3 = "/home/cbma/Data/TOS_DI-20180411_1414-V7.0.1/workspace/DIM_MODULES_PAR_SEMESTRE.xls";
				int nb_line_tFileOutputExcel_3 = 0;
				org.talend.ExcelTool xlsxTool_tFileOutputExcel_3 = new org.talend.ExcelTool();
				xlsxTool_tFileOutputExcel_3.setSheet("Sheet1");
				xlsxTool_tFileOutputExcel_3.setAppend(false, false);
				xlsxTool_tFileOutputExcel_3.setRecalculateFormula(false);
				xlsxTool_tFileOutputExcel_3.setXY(false, 0, 0, false);

				xlsxTool_tFileOutputExcel_3
						.prepareXlsxFile(fileName_tFileOutputExcel_3);

				xlsxTool_tFileOutputExcel_3.setFont("");

				if (xlsxTool_tFileOutputExcel_3.getStartRow() == 0) {

					xlsxTool_tFileOutputExcel_3.addRow();

					xlsxTool_tFileOutputExcel_3.addCellValue("codesemestre");

					xlsxTool_tFileOutputExcel_3.addCellValue("codemodsemestre");

					xlsxTool_tFileOutputExcel_3.addCellValue("codeprefixe");

					xlsxTool_tFileOutputExcel_3.addCellValue("intitule");

					xlsxTool_tFileOutputExcel_3.addCellValue("CM_A_FAIRE");

					xlsxTool_tFileOutputExcel_3.addCellValue("TD_A_FAIRE");

					xlsxTool_tFileOutputExcel_3.addCellValue("TP_A_FAIRE");

					xlsxTool_tFileOutputExcel_3.addCellValue("HEURES_A_FAIRE");

					xlsxTool_tFileOutputExcel_3.addCellValue("CM_FAITES");

					xlsxTool_tFileOutputExcel_3.addCellValue("TD_FAITES");

					xlsxTool_tFileOutputExcel_3.addCellValue("TP_FAITES");

					xlsxTool_tFileOutputExcel_3.addCellValue("HEURES_FAITES");

					xlsxTool_tFileOutputExcel_3.addCellValue("BILAN_CM");

					xlsxTool_tFileOutputExcel_3.addCellValue("BILAN_TD");

					xlsxTool_tFileOutputExcel_3.addCellValue("BILAN_TP");

					xlsxTool_tFileOutputExcel_3.addCellValue("BILAN_UE");

					xlsxTool_tFileOutputExcel_3.addCellValue("anneedebut");

					xlsxTool_tFileOutputExcel_3.addCellValue("ID_RESP_MOD");

					nb_line_tFileOutputExcel_3++;

				}

				/**
				 * [tFileOutputExcel_3 begin ] stop
				 */

				/**
				 * [tDBOutput_3 begin ] start
				 */

				ok_Hash.put("tDBOutput_3", false);
				start_Hash.put("tDBOutput_3", System.currentTimeMillis());

				currentComponent = "tDBOutput_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("DIM_SEMESTRE_MOD"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tDBOutput_3 = 0;

				class BytesLimit65535_tDBOutput_3 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBOutput_3().limitLog4jByte();

				int nb_line_tDBOutput_3 = 0;
				int nb_line_update_tDBOutput_3 = 0;
				int nb_line_inserted_tDBOutput_3 = 0;
				int nb_line_deleted_tDBOutput_3 = 0;
				int nb_line_rejected_tDBOutput_3 = 0;

				int deletedCount_tDBOutput_3 = 0;
				int updatedCount_tDBOutput_3 = 0;
				int insertedCount_tDBOutput_3 = 0;

				int rejectedCount_tDBOutput_3 = 0;

				String tableName_tDBOutput_3 = "DIM_MODULE";
				boolean whetherReject_tDBOutput_3 = false;

				java.util.Calendar calendar_tDBOutput_3 = java.util.Calendar
						.getInstance();
				calendar_tDBOutput_3.set(1, 0, 1, 0, 0, 0);
				long year1_tDBOutput_3 = calendar_tDBOutput_3.getTime()
						.getTime();
				calendar_tDBOutput_3.set(10000, 0, 1, 0, 0, 0);
				long year10000_tDBOutput_3 = calendar_tDBOutput_3.getTime()
						.getTime();
				long date_tDBOutput_3;

				java.sql.Connection conn_tDBOutput_3 = null;
				String dbProperties_tDBOutput_3 = "noDatetimeStringSync=true";
				String url_tDBOutput_3 = null;
				if (dbProperties_tDBOutput_3 == null
						|| dbProperties_tDBOutput_3.trim().length() == 0) {
					url_tDBOutput_3 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ "rewriteBatchedStatements=true";
				} else {
					String properties_tDBOutput_3 = "noDatetimeStringSync=true";
					if (!properties_tDBOutput_3
							.contains("rewriteBatchedStatements")) {
						properties_tDBOutput_3 += "&rewriteBatchedStatements=true";
					}

					url_tDBOutput_3 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ properties_tDBOutput_3;
				}
				String driverClass_tDBOutput_3 = "org.gjt.mm.mysql.Driver";

				String dbUser_tDBOutput_3 = "root";

				final String decryptedPassword_tDBOutput_3 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBOutput_3 = decryptedPassword_tDBOutput_3;
				java.lang.Class.forName(driverClass_tDBOutput_3);

				conn_tDBOutput_3 = java.sql.DriverManager.getConnection(
						url_tDBOutput_3, dbUser_tDBOutput_3, dbPwd_tDBOutput_3);

				resourceMap.put("conn_tDBOutput_3", conn_tDBOutput_3);
				conn_tDBOutput_3.setAutoCommit(false);
				int commitEvery_tDBOutput_3 = 10000;
				int commitCounter_tDBOutput_3 = 0;

				int count_tDBOutput_3 = 0;

				java.sql.Statement stmtDrop_tDBOutput_3 = conn_tDBOutput_3
						.createStatement();
				stmtDrop_tDBOutput_3.execute("DROP TABLE `"
						+ tableName_tDBOutput_3 + "`");
				stmtDrop_tDBOutput_3.close();
				java.sql.Statement stmtCreate_tDBOutput_3 = conn_tDBOutput_3
						.createStatement();
				stmtCreate_tDBOutput_3
						.execute("CREATE TABLE `"
								+ tableName_tDBOutput_3
								+ "`(`codesemestre` INT(10)  default 0  not null ,`codemodsemestre` INT(10)  default 0  not null ,`codeprefixe` VARCHAR(10)   not null ,`intitule` VARCHAR(100)   not null ,`CM_A_FAIRE` DECIMAL(11,2)  default 0.00  not null ,`TD_A_FAIRE` DECIMAL(11,2)  default 0.00  not null ,`TP_A_FAIRE` DECIMAL(11,2)  default 0.00  not null ,`HEURES_A_FAIRE` DOUBLE  not null ,`CM_FAITES` DECIMAL(11,2)  default 0.00  not null ,`TD_FAITES` DECIMAL(11,2)  default 0.00  not null ,`TP_FAITES` DECIMAL(11,2)  default 0.00  not null ,`HEURES_FAITES` DOUBLE  not null ,`BILAN_CM` DECIMAL(11,2)  default 0.00  not null ,`BILAN_TD` DECIMAL(11,2)  default 0.00  not null ,`BILAN_TP` DECIMAL(11,2)  default 0.00  not null ,`BILAN_UE` DOUBLE  not null ,`anneedebut` INT(10)  default 0  not null ,`ID_RESP_MOD` INT(10)  default 0  not null ,primary key(`codesemestre`,`codemodsemestre`,`anneedebut`))");
				stmtCreate_tDBOutput_3.close();

				String insert_tDBOutput_3 = "INSERT INTO `"
						+ "DIM_MODULE"
						+ "` (`codesemestre`,`codemodsemestre`,`codeprefixe`,`intitule`,`CM_A_FAIRE`,`TD_A_FAIRE`,`TP_A_FAIRE`,`HEURES_A_FAIRE`,`CM_FAITES`,`TD_FAITES`,`TP_FAITES`,`HEURES_FAITES`,`BILAN_CM`,`BILAN_TD`,`BILAN_TP`,`BILAN_UE`,`anneedebut`,`ID_RESP_MOD`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				int batchSize_tDBOutput_3 = 100;
				int batchSizeCounter_tDBOutput_3 = 0;

				java.sql.PreparedStatement pstmt_tDBOutput_3 = conn_tDBOutput_3
						.prepareStatement(insert_tDBOutput_3);

				/**
				 * [tDBOutput_3 begin ] stop
				 */

				/**
				 * [tMap_4 begin ] start
				 */

				ok_Hash.put("tMap_4", false);
				start_Hash.put("tMap_4", System.currentTimeMillis());

				currentComponent = "tMap_4";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("MODULE" + iterateId, 0,
								0);

					}
				}

				int tos_count_tMap_4 = 0;

				class BytesLimit65535_tMap_4 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tMap_4().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row7Struct> tHash_Lookup_row7 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row7Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row7Struct>) globalMap
						.get("tHash_Lookup_row7"));

				row7Struct row7HashKey = new row7Struct();
				row7Struct row7Default = new row7Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row8Struct> tHash_Lookup_row8 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row8Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row8Struct>) globalMap
						.get("tHash_Lookup_row8"));

				row8Struct row8HashKey = new row8Struct();
				row8Struct row8Default = new row8Struct();
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_4__Struct {
				}
				Var__tMap_4__Struct Var__tMap_4 = new Var__tMap_4__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				MODULES_SEMESTREStruct MODULES_SEMESTRE_tmp = new MODULES_SEMESTREStruct();
				DIM_SEMESTRE_MODStruct DIM_SEMESTRE_MOD_tmp = new DIM_SEMESTRE_MODStruct();
				// ###############################

				/**
				 * [tMap_4 begin ] stop
				 */

				/**
				 * [tAdvancedHash_row30 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row30", false);
				start_Hash.put("tAdvancedHash_row30",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row30";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row30" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row30 = 0;

				class BytesLimit65535_tAdvancedHash_row30 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row30().limitLog4jByte();

				// connection name:row30
				// source node:tUniqRow_1 - inputs:(ANNE_MODULE)
				// outputs:(row30,row30) | target node:tAdvancedHash_row30 -
				// inputs:(row30) outputs:()
				// linked node: tMap_1 - inputs:(row9,row10,row12,row11,row30)
				// outputs:(ENSEIGNANT_DPT,ENS_DPT_BD,dim_temps)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row30 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.ALL_ROWS;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row30Struct> tHash_Lookup_row30 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row30Struct> getLookup(matchingModeEnum_row30);

				globalMap.put("tHash_Lookup_row30", tHash_Lookup_row30);

				/**
				 * [tAdvancedHash_row30 begin ] stop
				 */

				/**
				 * [tUniqRow_1 begin ] start
				 */

				ok_Hash.put("tUniqRow_1", false);
				start_Hash.put("tUniqRow_1", System.currentTimeMillis());

				currentComponent = "tUniqRow_1";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("ANNE_MODULE"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tUniqRow_1 = 0;

				class BytesLimit65535_tUniqRow_1 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tUniqRow_1().limitLog4jByte();

				class KeyStruct_tUniqRow_1 {

					private static final int DEFAULT_HASHCODE = 1;
					private static final int PRIME = 31;
					private int hashCode = DEFAULT_HASHCODE;
					public boolean hashCodeDirty = true;

					int anneedebut;

					@Override
					public int hashCode() {
						if (this.hashCodeDirty) {
							final int prime = PRIME;
							int result = DEFAULT_HASHCODE;

							result = prime * result + (int) this.anneedebut;

							this.hashCode = result;
							this.hashCodeDirty = false;
						}
						return this.hashCode;
					}

					@Override
					public boolean equals(Object obj) {
						if (this == obj)
							return true;
						if (obj == null)
							return false;
						if (getClass() != obj.getClass())
							return false;
						final KeyStruct_tUniqRow_1 other = (KeyStruct_tUniqRow_1) obj;

						if (this.anneedebut != other.anneedebut)
							return false;

						return true;
					}

				}

				int nb_uniques_tUniqRow_1 = 0;
				int nb_duplicates_tUniqRow_1 = 0;
				KeyStruct_tUniqRow_1 finder_tUniqRow_1 = new KeyStruct_tUniqRow_1();
				java.util.Set<KeyStruct_tUniqRow_1> keystUniqRow_1 = new java.util.HashSet<KeyStruct_tUniqRow_1>();

				/**
				 * [tUniqRow_1 begin ] stop
				 */

				/**
				 * [tMap_3 begin ] start
				 */

				ok_Hash.put("tMap_3", false);
				start_Hash.put("tMap_3", System.currentTimeMillis());

				currentComponent = "tMap_3";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row13" + iterateId, 0,
								0);

					}
				}

				int tos_count_tMap_3 = 0;

				class BytesLimit65535_tMap_3 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tMap_3().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row15Struct> tHash_Lookup_row15 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row15Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row15Struct>) globalMap
						.get("tHash_Lookup_row15"));

				row15Struct row15HashKey = new row15Struct();
				row15Struct row15Default = new row15Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row14Struct> tHash_Lookup_row14 = null;

				row14Struct row14HashKey = new row14Struct();
				row14Struct row14Default = new row14Struct();
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_3__Struct {
				}
				Var__tMap_3__Struct Var__tMap_3 = new Var__tMap_3__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				MODULEStruct MODULE_tmp = new MODULEStruct();
				ANNE_MODULEStruct ANNE_MODULE_tmp = new ANNE_MODULEStruct();
				// ###############################

				/**
				 * [tMap_3 begin ] stop
				 */

				/**
				 * [tDBInput_8 begin ] start
				 */

				ok_Hash.put("tDBInput_8", false);
				start_Hash.put("tDBInput_8", System.currentTimeMillis());

				currentComponent = "tDBInput_8";

				int tos_count_tDBInput_8 = 0;

				class BytesLimit65535_tDBInput_8 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_8().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_8 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_8.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_8 = calendar_tDBInput_8.getTime();
				int nb_line_tDBInput_8 = 0;
				java.sql.Connection conn_tDBInput_8 = null;
				String driverClass_tDBInput_8 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_8);
				String dbUser_tDBInput_8 = "root";

				final String decryptedPassword_tDBInput_8 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_8 = decryptedPassword_tDBInput_8;

				String url_tDBInput_8 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_8 = java.sql.DriverManager.getConnection(
						url_tDBInput_8, dbUser_tDBInput_8, dbPwd_tDBInput_8);

				java.sql.Statement stmt_tDBInput_8 = conn_tDBInput_8
						.createStatement();

				String dbquery_tDBInput_8 = "SELECT \n  `menusemestre`.`codemod`, \n  `menusemestre`.`codesemestre`, \n  `menusemestre`.`codemodsemestre`, \n  `menuseme"
						+ "stre`.`optionnel`, \n  `menusemestre`.`alternatif`, \n  `menusemestre`.`codealternatif`, \n  `menusemestre`.`verrou`, \n  `m"
						+ "enusemestre`.`verrouDPT`\nFROM `menusemestre`";

				globalMap.put("tDBInput_8_QUERY", dbquery_tDBInput_8);
				java.sql.ResultSet rs_tDBInput_8 = null;

				try {
					rs_tDBInput_8 = stmt_tDBInput_8
							.executeQuery(dbquery_tDBInput_8);
					java.sql.ResultSetMetaData rsmd_tDBInput_8 = rs_tDBInput_8
							.getMetaData();
					int colQtyInRs_tDBInput_8 = rsmd_tDBInput_8
							.getColumnCount();

					String tmpContent_tDBInput_8 = null;

					while (rs_tDBInput_8.next()) {
						nb_line_tDBInput_8++;

						if (colQtyInRs_tDBInput_8 < 1) {
							row13.codemod = 0;
						} else {

							if (rs_tDBInput_8.getObject(1) != null) {
								row13.codemod = rs_tDBInput_8.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_8 < 2) {
							row13.codesemestre = 0;
						} else {

							if (rs_tDBInput_8.getObject(2) != null) {
								row13.codesemestre = rs_tDBInput_8.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_8 < 3) {
							row13.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_8.getObject(3) != null) {
								row13.codemodsemestre = rs_tDBInput_8.getInt(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_8 < 4) {
							row13.optionnel = false;
						} else {

							if (rs_tDBInput_8.getObject(4) != null) {
								row13.optionnel = rs_tDBInput_8.getBoolean(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_8 < 5) {
							row13.alternatif = false;
						} else {

							if (rs_tDBInput_8.getObject(5) != null) {
								row13.alternatif = rs_tDBInput_8.getBoolean(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_8 < 6) {
							row13.codealternatif = null;
						} else {

							if (rs_tDBInput_8.getObject(6) != null) {
								row13.codealternatif = rs_tDBInput_8.getInt(6);
							} else {
								row13.codealternatif = null;
							}
						}
						if (colQtyInRs_tDBInput_8 < 7) {
							row13.verrou = false;
						} else {

							if (rs_tDBInput_8.getObject(7) != null) {
								row13.verrou = rs_tDBInput_8.getBoolean(7);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_8 < 8) {
							row13.verrouDPT = false;
						} else {

							if (rs_tDBInput_8.getObject(8) != null) {
								row13.verrouDPT = rs_tDBInput_8.getBoolean(8);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_8 begin ] stop
						 */

						/**
						 * [tDBInput_8 main ] start
						 */

						currentComponent = "tDBInput_8";

						tos_count_tDBInput_8++;

						/**
						 * [tDBInput_8 main ] stop
						 */

						/**
						 * [tDBInput_8 process_data_begin ] start
						 */

						currentComponent = "tDBInput_8";

						/**
						 * [tDBInput_8 process_data_begin ] stop
						 */

						/**
						 * [tMap_3 main ] start
						 */

						currentComponent = "tMap_3";

						// row13
						// row13

						if (execStat) {
							runStat.updateStatOnConnection("row13" + iterateId,
									1, 1);
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_3 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_3 = false;
						boolean mainRowRejected_tMap_3 = false;

						// /////////////////////////////////////////////
						// Starting Lookup Table "row15"
						// /////////////////////////////////////////////

						boolean forceLooprow15 = false;

						row15Struct row15ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_3) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_3 = false;

							Object exprKeyValue_row15__codesemestre = row13.codesemestre;
							if (exprKeyValue_row15__codesemestre == null) {
								hasCasePrimitiveKeyWithNull_tMap_3 = true;
							} else {
								row15HashKey.codesemestre = (int) (Integer) exprKeyValue_row15__codesemestre;
							}

							row15HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_3) { // G_TM_M_091

								tHash_Lookup_row15.lookup(row15HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_3
									|| !tHash_Lookup_row15.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_3 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row15 != null
								&& tHash_Lookup_row15.getCount(row15HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row15' and it contains more one result from keys :  row15.codesemestre = '"
							// + row15HashKey.codesemestre + "'");
						} // G 071

						row15Struct row15 = null;

						row15Struct fromLookup_row15 = null;
						row15 = row15Default;

						if (tHash_Lookup_row15 != null
								&& tHash_Lookup_row15.hasNext()) { // G 099

							fromLookup_row15 = tHash_Lookup_row15.next();

						} // G 099

						if (fromLookup_row15 != null) {
							row15 = fromLookup_row15;
						}

						// /////////////////////////////////////////////
						// Starting Lookup Table "row14"
						// /////////////////////////////////////////////

						boolean forceLooprow14 = false;

						row14Struct row14ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_3) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_3 = false;

							Object exprKeyValue_row14__codemod = row13.codemod;
							if (exprKeyValue_row14__codemod == null) {
								hasCasePrimitiveKeyWithNull_tMap_3 = true;
							} else {
								row14HashKey.codemod = (int) (Integer) exprKeyValue_row14__codemod;
							}

							row14HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_3) { // G_TM_M_091

								tDBInput_13Process(globalMap);

								tHash_Lookup_row14 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row14Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row14Struct>) globalMap
										.get("tHash_Lookup_row14"));

								tHash_Lookup_row14.initGet();

								tHash_Lookup_row14.lookup(row14HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_3
									|| !tHash_Lookup_row14.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_3 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row14 != null
								&& tHash_Lookup_row14.getCount(row14HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row14' and it contains more one result from keys :  row14.codemod = '"
							// + row14HashKey.codemod + "'");
						} // G 071

						row14Struct row14 = null;

						row14Struct fromLookup_row14 = null;
						row14 = row14Default;

						if (tHash_Lookup_row14 != null
								&& tHash_Lookup_row14.hasNext()) { // G 099

							fromLookup_row14 = tHash_Lookup_row14.next();

						} // G 099

						if (fromLookup_row14 != null) {
							row14 = fromLookup_row14;
						}

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_3__Struct Var = Var__tMap_3;// ###############################
							// ###############################
							// # Output tables

							MODULE = null;
							ANNE_MODULE = null;

							if (!rejectedInnerJoin_tMap_3) {

								// # Output table : 'MODULE'
								MODULE_tmp.codemod = row13.codemod;
								MODULE_tmp.codesemestre = row13.codesemestre;
								MODULE_tmp.codemodsemestre = row13.codemodsemestre;
								MODULE_tmp.codeprefixe = row14.codeprefixe;
								MODULE_tmp.codesuffixe = row14.codesuffixe;
								MODULE_tmp.intitule = row14.intitule;
								MODULE_tmp.responsable = row14.responsable;
								MODULE_tmp.anneedebut = row15.anneedebut;
								MODULE = MODULE_tmp;

								// # Output table : 'ANNE_MODULE'
								ANNE_MODULE_tmp.anneedebut = row15.anneedebut;
								ANNE_MODULE = ANNE_MODULE_tmp;
							} // closing inner join bracket (2)
								// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_3 = false;

						tos_count_tMap_3++;

						/**
						 * [tMap_3 main ] stop
						 */

						/**
						 * [tMap_3 process_data_begin ] start
						 */

						currentComponent = "tMap_3";

						/**
						 * [tMap_3 process_data_begin ] stop
						 */
						// Start of branch "MODULE"
						if (MODULE != null) {

							/**
							 * [tMap_4 main ] start
							 */

							currentComponent = "tMap_4";

							// MODULE
							// MODULE

							if (execStat) {
								runStat.updateStatOnConnection("MODULE"
										+ iterateId, 1, 1);
							}

							boolean hasCasePrimitiveKeyWithNull_tMap_4 = false;

							// ###############################
							// # Input tables (lookups)
							boolean rejectedInnerJoin_tMap_4 = false;
							boolean mainRowRejected_tMap_4 = false;

							// /////////////////////////////////////////////
							// Starting Lookup Table "row7"
							// /////////////////////////////////////////////

							boolean forceLooprow7 = false;

							row7Struct row7ObjectFromLookup = null;

							if (!rejectedInnerJoin_tMap_4) { // G_TM_M_020

								hasCasePrimitiveKeyWithNull_tMap_4 = false;

								Object exprKeyValue_row7__codemodsemestre = MODULE.codemodsemestre;
								if (exprKeyValue_row7__codemodsemestre == null) {
									hasCasePrimitiveKeyWithNull_tMap_4 = true;
								} else {
									row7HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row7__codemodsemestre;
								}

								row7HashKey.hashCodeDirty = true;

								if (!hasCasePrimitiveKeyWithNull_tMap_4) { // G_TM_M_091

									tHash_Lookup_row7.lookup(row7HashKey);

								} // G_TM_M_091

								if (hasCasePrimitiveKeyWithNull_tMap_4
										|| !tHash_Lookup_row7.hasNext()) { // G_TM_M_090

									rejectedInnerJoin_tMap_4 = true;

								} // G_TM_M_090

							} // G_TM_M_020

							if (tHash_Lookup_row7 != null
									&& tHash_Lookup_row7.getCount(row7HashKey) > 1) { // G
																						// 071

								// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row7' and it contains more one result from keys :  row7.codemodsemestre = '"
								// + row7HashKey.codemodsemestre + "'");
							} // G 071

							row7Struct row7 = null;

							row7Struct fromLookup_row7 = null;
							row7 = row7Default;

							if (tHash_Lookup_row7 != null
									&& tHash_Lookup_row7.hasNext()) { // G 099

								fromLookup_row7 = tHash_Lookup_row7.next();

							} // G 099

							if (fromLookup_row7 != null) {
								row7 = fromLookup_row7;
							}

							// /////////////////////////////////////////////
							// Starting Lookup Table "row8"
							// /////////////////////////////////////////////

							boolean forceLooprow8 = false;

							row8Struct row8ObjectFromLookup = null;

							if (!rejectedInnerJoin_tMap_4) { // G_TM_M_020

								hasCasePrimitiveKeyWithNull_tMap_4 = false;

								Object exprKeyValue_row8__codemodsemestre = MODULE.codemodsemestre;
								if (exprKeyValue_row8__codemodsemestre == null) {
									hasCasePrimitiveKeyWithNull_tMap_4 = true;
								} else {
									row8HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row8__codemodsemestre;
								}

								row8HashKey.hashCodeDirty = true;

								if (!hasCasePrimitiveKeyWithNull_tMap_4) { // G_TM_M_091

									tHash_Lookup_row8.lookup(row8HashKey);

								} // G_TM_M_091

							} // G_TM_M_020

							if (tHash_Lookup_row8 != null
									&& tHash_Lookup_row8.getCount(row8HashKey) > 1) { // G
																						// 071

								// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row8' and it contains more one result from keys :  row8.codemodsemestre = '"
								// + row8HashKey.codemodsemestre + "'");
							} // G 071

							row8Struct row8 = null;

							row8Struct fromLookup_row8 = null;
							row8 = row8Default;

							if (tHash_Lookup_row8 != null
									&& tHash_Lookup_row8.hasNext()) { // G 099

								fromLookup_row8 = tHash_Lookup_row8.next();

							} // G 099

							if (fromLookup_row8 != null) {
								row8 = fromLookup_row8;
							}

							// ###############################
							{ // start of Var scope

								// ###############################
								// # Vars tables

								Var__tMap_4__Struct Var = Var__tMap_4;// ###############################
								// ###############################
								// # Output tables

								MODULES_SEMESTRE = null;
								DIM_SEMESTRE_MOD = null;

								if (!rejectedInnerJoin_tMap_4) {

									// # Output table : 'MODULES_SEMESTRE'
									MODULES_SEMESTRE_tmp.codesemestre = MODULE.codesemestre;
									MODULES_SEMESTRE_tmp.codemodsemestre = MODULE.codemodsemestre;
									MODULES_SEMESTRE_tmp.codeprefixe = MODULE.codeprefixe;
									MODULES_SEMESTRE_tmp.intitule = MODULE.intitule;
									MODULES_SEMESTRE_tmp.CM_A_FAIRE = row7.CM_A_FAIRE;
									MODULES_SEMESTRE_tmp.TD_A_FAIRE = row7.TD_A_FAIRE;
									MODULES_SEMESTRE_tmp.TP_A_FAIRE = row7.TP_A_FAIRE;
									MODULES_SEMESTRE_tmp.HEURES_A_FAIRE = row7.CM_A_FAIRE
											* 1.5
											+ row7.TD_A_FAIRE
											+ row7.TP_A_FAIRE;
									MODULES_SEMESTRE_tmp.CM_FAITES = row8.CM_FAITES;
									MODULES_SEMESTRE_tmp.TD_FAITES = row8.TD_FAITES;
									MODULES_SEMESTRE_tmp.TP_FAITES = row8.TP_FAITES;
									MODULES_SEMESTRE_tmp.HEURES_FAITES = row8.CM_FAITES
											* 1.5
											+ row8.TD_FAITES
											+ row8.TP_FAITES;
									MODULES_SEMESTRE_tmp.BILAN_CM = row8.CM_FAITES
											- row7.CM_A_FAIRE;
									MODULES_SEMESTRE_tmp.BILAN_TD = row8.TD_FAITES
											- row7.TD_A_FAIRE;
									MODULES_SEMESTRE_tmp.BILAN_TP = row8.TP_FAITES
											- row7.TP_A_FAIRE;
									MODULES_SEMESTRE_tmp.BILAN_UE = (row8.CM_FAITES
											* 1.5 + row8.TD_FAITES + row8.TP_FAITES)
											- (row7.CM_A_FAIRE * 1.5
													+ row7.TD_A_FAIRE + row7.TP_A_FAIRE);
									MODULES_SEMESTRE_tmp.anneedebut = MODULE.anneedebut;
									MODULES_SEMESTRE_tmp.ID_RESP_MOD = MODULE.responsable;
									MODULES_SEMESTRE = MODULES_SEMESTRE_tmp;

									// # Output table : 'DIM_SEMESTRE_MOD'
									DIM_SEMESTRE_MOD_tmp.codesemestre = MODULE.codesemestre;
									DIM_SEMESTRE_MOD_tmp.codemodsemestre = MODULE.codemodsemestre;
									DIM_SEMESTRE_MOD_tmp.codeprefixe = MODULE.codeprefixe;
									DIM_SEMESTRE_MOD_tmp.intitule = MODULE.intitule;
									DIM_SEMESTRE_MOD_tmp.CM_A_FAIRE = row7.CM_A_FAIRE;
									DIM_SEMESTRE_MOD_tmp.TD_A_FAIRE = row7.TD_A_FAIRE;
									DIM_SEMESTRE_MOD_tmp.TP_A_FAIRE = row7.TP_A_FAIRE;
									DIM_SEMESTRE_MOD_tmp.HEURES_A_FAIRE = row7.CM_A_FAIRE
											* 1.5
											+ row7.TD_A_FAIRE
											+ row7.TP_A_FAIRE;
									DIM_SEMESTRE_MOD_tmp.CM_FAITES = row8.CM_FAITES;
									DIM_SEMESTRE_MOD_tmp.TD_FAITES = row8.TD_FAITES;
									DIM_SEMESTRE_MOD_tmp.TP_FAITES = row8.TP_FAITES;
									DIM_SEMESTRE_MOD_tmp.HEURES_FAITES = row8.CM_FAITES
											* 1.5
											+ row8.TD_FAITES
											+ row8.TP_FAITES;
									DIM_SEMESTRE_MOD_tmp.BILAN_CM = row8.CM_FAITES
											- row7.CM_A_FAIRE;
									DIM_SEMESTRE_MOD_tmp.BILAN_TD = row8.TD_FAITES
											- row7.TD_A_FAIRE;
									DIM_SEMESTRE_MOD_tmp.BILAN_TP = row8.TP_FAITES
											- row7.TP_A_FAIRE;
									DIM_SEMESTRE_MOD_tmp.BILAN_UE = (row8.CM_FAITES
											* 1.5 + row8.TD_FAITES + row8.TP_FAITES)
											- (row7.CM_A_FAIRE * 1.5
													+ row7.TD_A_FAIRE + row7.TP_A_FAIRE);
									DIM_SEMESTRE_MOD_tmp.anneedebut = MODULE.anneedebut;
									DIM_SEMESTRE_MOD_tmp.ID_RESP_MOD = MODULE.responsable;
									DIM_SEMESTRE_MOD = DIM_SEMESTRE_MOD_tmp;
								} // closing inner join bracket (2)
									// ###############################

							} // end of Var scope

							rejectedInnerJoin_tMap_4 = false;

							tos_count_tMap_4++;

							/**
							 * [tMap_4 main ] stop
							 */

							/**
							 * [tMap_4 process_data_begin ] start
							 */

							currentComponent = "tMap_4";

							/**
							 * [tMap_4 process_data_begin ] stop
							 */
							// Start of branch "MODULES_SEMESTRE"
							if (MODULES_SEMESTRE != null) {

								/**
								 * [tFileOutputExcel_3 main ] start
								 */

								currentComponent = "tFileOutputExcel_3";

								// MODULES_SEMESTRE
								// MODULES_SEMESTRE

								if (execStat) {
									runStat.updateStatOnConnection(
											"MODULES_SEMESTRE" + iterateId, 1,
											1);
								}

								xlsxTool_tFileOutputExcel_3.addRow();

								xlsxTool_tFileOutputExcel_3
										.addCellValue(Double.parseDouble(String
												.valueOf(MODULES_SEMESTRE.codesemestre)));

								xlsxTool_tFileOutputExcel_3
										.addCellValue(Double.parseDouble(String
												.valueOf(MODULES_SEMESTRE.codemodsemestre)));

								if (MODULES_SEMESTRE.codeprefixe != null) {

									xlsxTool_tFileOutputExcel_3
											.addCellValue(String
													.valueOf(MODULES_SEMESTRE.codeprefixe));
								} else {
									xlsxTool_tFileOutputExcel_3
											.addCellNullValue();
								}

								if (MODULES_SEMESTRE.intitule != null) {

									xlsxTool_tFileOutputExcel_3
											.addCellValue(String
													.valueOf(MODULES_SEMESTRE.intitule));
								} else {
									xlsxTool_tFileOutputExcel_3
											.addCellNullValue();
								}

								xlsxTool_tFileOutputExcel_3
										.addCellValue(Double.parseDouble(String
												.valueOf(MODULES_SEMESTRE.CM_A_FAIRE)));

								xlsxTool_tFileOutputExcel_3
										.addCellValue(Double.parseDouble(String
												.valueOf(MODULES_SEMESTRE.TD_A_FAIRE)));

								xlsxTool_tFileOutputExcel_3
										.addCellValue(Double.parseDouble(String
												.valueOf(MODULES_SEMESTRE.TP_A_FAIRE)));

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.HEURES_A_FAIRE);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.CM_FAITES);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.TD_FAITES);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.TP_FAITES);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.HEURES_FAITES);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.BILAN_CM);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.BILAN_TD);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.BILAN_TP);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(MODULES_SEMESTRE.BILAN_UE);

								xlsxTool_tFileOutputExcel_3
										.addCellValue(Double.parseDouble(String
												.valueOf(MODULES_SEMESTRE.anneedebut)));

								xlsxTool_tFileOutputExcel_3
										.addCellValue(Double.parseDouble(String
												.valueOf(MODULES_SEMESTRE.ID_RESP_MOD)));
								nb_line_tFileOutputExcel_3++;

								tos_count_tFileOutputExcel_3++;

								/**
								 * [tFileOutputExcel_3 main ] stop
								 */

								/**
								 * [tFileOutputExcel_3 process_data_begin ]
								 * start
								 */

								currentComponent = "tFileOutputExcel_3";

								/**
								 * [tFileOutputExcel_3 process_data_begin ] stop
								 */

								/**
								 * [tFileOutputExcel_3 process_data_end ] start
								 */

								currentComponent = "tFileOutputExcel_3";

								/**
								 * [tFileOutputExcel_3 process_data_end ] stop
								 */

							} // End of branch "MODULES_SEMESTRE"

							// Start of branch "DIM_SEMESTRE_MOD"
							if (DIM_SEMESTRE_MOD != null) {

								/**
								 * [tDBOutput_3 main ] start
								 */

								currentComponent = "tDBOutput_3";

								// DIM_SEMESTRE_MOD
								// DIM_SEMESTRE_MOD

								if (execStat) {
									runStat.updateStatOnConnection(
											"DIM_SEMESTRE_MOD" + iterateId, 1,
											1);
								}

								whetherReject_tDBOutput_3 = false;
								pstmt_tDBOutput_3.setInt(1,
										DIM_SEMESTRE_MOD.codesemestre);

								pstmt_tDBOutput_3.setInt(2,
										DIM_SEMESTRE_MOD.codemodsemestre);

								if (DIM_SEMESTRE_MOD.codeprefixe == null) {
									pstmt_tDBOutput_3.setNull(3,
											java.sql.Types.VARCHAR);
								} else {
									pstmt_tDBOutput_3.setString(3,
											DIM_SEMESTRE_MOD.codeprefixe);
								}

								if (DIM_SEMESTRE_MOD.intitule == null) {
									pstmt_tDBOutput_3.setNull(4,
											java.sql.Types.VARCHAR);
								} else {
									pstmt_tDBOutput_3.setString(4,
											DIM_SEMESTRE_MOD.intitule);
								}

								pstmt_tDBOutput_3.setFloat(5,
										DIM_SEMESTRE_MOD.CM_A_FAIRE);

								pstmt_tDBOutput_3.setFloat(6,
										DIM_SEMESTRE_MOD.TD_A_FAIRE);

								pstmt_tDBOutput_3.setFloat(7,
										DIM_SEMESTRE_MOD.TP_A_FAIRE);

								pstmt_tDBOutput_3.setDouble(8,
										DIM_SEMESTRE_MOD.HEURES_A_FAIRE);

								pstmt_tDBOutput_3.setDouble(9,
										DIM_SEMESTRE_MOD.CM_FAITES);

								pstmt_tDBOutput_3.setDouble(10,
										DIM_SEMESTRE_MOD.TD_FAITES);

								pstmt_tDBOutput_3.setDouble(11,
										DIM_SEMESTRE_MOD.TP_FAITES);

								pstmt_tDBOutput_3.setDouble(12,
										DIM_SEMESTRE_MOD.HEURES_FAITES);

								pstmt_tDBOutput_3.setDouble(13,
										DIM_SEMESTRE_MOD.BILAN_CM);

								pstmt_tDBOutput_3.setDouble(14,
										DIM_SEMESTRE_MOD.BILAN_TD);

								pstmt_tDBOutput_3.setDouble(15,
										DIM_SEMESTRE_MOD.BILAN_TP);

								pstmt_tDBOutput_3.setDouble(16,
										DIM_SEMESTRE_MOD.BILAN_UE);

								pstmt_tDBOutput_3.setInt(17,
										DIM_SEMESTRE_MOD.anneedebut);

								pstmt_tDBOutput_3.setInt(18,
										DIM_SEMESTRE_MOD.ID_RESP_MOD);

								pstmt_tDBOutput_3.addBatch();
								nb_line_tDBOutput_3++;

								batchSizeCounter_tDBOutput_3++;
								if (batchSize_tDBOutput_3 <= batchSizeCounter_tDBOutput_3) {
									try {
										int countSum_tDBOutput_3 = 0;
										for (int countEach_tDBOutput_3 : pstmt_tDBOutput_3
												.executeBatch()) {
											countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0
													: 1);
										}
										insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
									} catch (java.sql.BatchUpdateException e) {
										int countSum_tDBOutput_3 = 0;
										for (int countEach_tDBOutput_3 : e
												.getUpdateCounts()) {
											countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0
													: countEach_tDBOutput_3);
										}
										insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
										System.err.println(e.getMessage());
									}

									batchSizeCounter_tDBOutput_3 = 0;
								}
								commitCounter_tDBOutput_3++;

								if (commitEvery_tDBOutput_3 <= commitCounter_tDBOutput_3) {

									try {
										int countSum_tDBOutput_3 = 0;
										for (int countEach_tDBOutput_3 : pstmt_tDBOutput_3
												.executeBatch()) {
											countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0
													: 1);
										}
										insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
									} catch (java.sql.BatchUpdateException e) {
										int countSum_tDBOutput_3 = 0;
										for (int countEach_tDBOutput_3 : e
												.getUpdateCounts()) {
											countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0
													: countEach_tDBOutput_3);
										}
										insertedCount_tDBOutput_3 += countSum_tDBOutput_3;
										System.err.println(e.getMessage());

									}
									conn_tDBOutput_3.commit();
									commitCounter_tDBOutput_3 = 0;

								}

								tos_count_tDBOutput_3++;

								/**
								 * [tDBOutput_3 main ] stop
								 */

								/**
								 * [tDBOutput_3 process_data_begin ] start
								 */

								currentComponent = "tDBOutput_3";

								/**
								 * [tDBOutput_3 process_data_begin ] stop
								 */

								/**
								 * [tDBOutput_3 process_data_end ] start
								 */

								currentComponent = "tDBOutput_3";

								/**
								 * [tDBOutput_3 process_data_end ] stop
								 */

							} // End of branch "DIM_SEMESTRE_MOD"

							/**
							 * [tMap_4 process_data_end ] start
							 */

							currentComponent = "tMap_4";

							/**
							 * [tMap_4 process_data_end ] stop
							 */

						} // End of branch "MODULE"

						// Start of branch "ANNE_MODULE"
						if (ANNE_MODULE != null) {

							/**
							 * [tUniqRow_1 main ] start
							 */

							currentComponent = "tUniqRow_1";

							// ANNE_MODULE
							// ANNE_MODULE

							if (execStat) {
								runStat.updateStatOnConnection("ANNE_MODULE"
										+ iterateId, 1, 1);
							}

							row30 = null;
							row30 = null;
							finder_tUniqRow_1.anneedebut = ANNE_MODULE.anneedebut;
							finder_tUniqRow_1.hashCodeDirty = true;
							if (!keystUniqRow_1.contains(finder_tUniqRow_1)) {
								KeyStruct_tUniqRow_1 new_tUniqRow_1 = new KeyStruct_tUniqRow_1();

								new_tUniqRow_1.anneedebut = ANNE_MODULE.anneedebut;

								keystUniqRow_1.add(new_tUniqRow_1);
								if (row30 == null) {

									row30 = new row30Struct();
								}
								row30.anneedebut = ANNE_MODULE.anneedebut;
								if (row30 == null) {

									row30 = new row30Struct();
								}
								row30.anneedebut = ANNE_MODULE.anneedebut;
								nb_uniques_tUniqRow_1++;
							} else {
								nb_duplicates_tUniqRow_1++;
							}

							tos_count_tUniqRow_1++;

							/**
							 * [tUniqRow_1 main ] stop
							 */

							/**
							 * [tUniqRow_1 process_data_begin ] start
							 */

							currentComponent = "tUniqRow_1";

							/**
							 * [tUniqRow_1 process_data_begin ] stop
							 */
							// Start of branch "row30"
							if (row30 != null) {

								/**
								 * [tAdvancedHash_row30 main ] start
								 */

								currentComponent = "tAdvancedHash_row30";

								// row30
								// row30

								if (execStat) {
									runStat.updateStatOnConnection("row30"
											+ iterateId, 1, 1);
								}

								row30Struct row30_HashRow = new row30Struct();

								row30_HashRow.anneedebut = row30.anneedebut;

								tHash_Lookup_row30.put(row30_HashRow);

								tos_count_tAdvancedHash_row30++;

								/**
								 * [tAdvancedHash_row30 main ] stop
								 */

								/**
								 * [tAdvancedHash_row30 process_data_begin ]
								 * start
								 */

								currentComponent = "tAdvancedHash_row30";

								/**
								 * [tAdvancedHash_row30 process_data_begin ]
								 * stop
								 */

								/**
								 * [tAdvancedHash_row30 process_data_end ] start
								 */

								currentComponent = "tAdvancedHash_row30";

								/**
								 * [tAdvancedHash_row30 process_data_end ] stop
								 */

							} // End of branch "row30"

							/**
							 * [tUniqRow_1 process_data_end ] start
							 */

							currentComponent = "tUniqRow_1";

							/**
							 * [tUniqRow_1 process_data_end ] stop
							 */

						} // End of branch "ANNE_MODULE"

						/**
						 * [tMap_3 process_data_end ] start
						 */

						currentComponent = "tMap_3";

						/**
						 * [tMap_3 process_data_end ] stop
						 */

						/**
						 * [tDBInput_8 process_data_end ] start
						 */

						currentComponent = "tDBInput_8";

						/**
						 * [tDBInput_8 process_data_end ] stop
						 */

						/**
						 * [tDBInput_8 end ] start
						 */

						currentComponent = "tDBInput_8";

					}
				} finally {
					if (rs_tDBInput_8 != null) {
						rs_tDBInput_8.close();
					}
					stmt_tDBInput_8.close();
					if (conn_tDBInput_8 != null && !conn_tDBInput_8.isClosed()) {

						conn_tDBInput_8.close();

					}

				}

				globalMap.put("tDBInput_8_NB_LINE", nb_line_tDBInput_8);

				ok_Hash.put("tDBInput_8", true);
				end_Hash.put("tDBInput_8", System.currentTimeMillis());

				/**
				 * [tDBInput_8 end ] stop
				 */

				/**
				 * [tMap_3 end ] start
				 */

				currentComponent = "tMap_3";

				// ###############################
				// # Lookup hashes releasing
				if (tHash_Lookup_row15 != null) {
					tHash_Lookup_row15.endGet();
				}
				globalMap.remove("tHash_Lookup_row15");

				if (tHash_Lookup_row14 != null) {
					tHash_Lookup_row14.endGet();
				}
				globalMap.remove("tHash_Lookup_row14");

				// ###############################

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row13" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tMap_3", true);
				end_Hash.put("tMap_3", System.currentTimeMillis());

				/**
				 * [tMap_3 end ] stop
				 */

				/**
				 * [tMap_4 end ] start
				 */

				currentComponent = "tMap_4";

				// ###############################
				// # Lookup hashes releasing
				if (tHash_Lookup_row7 != null) {
					tHash_Lookup_row7.endGet();
				}
				globalMap.remove("tHash_Lookup_row7");

				if (tHash_Lookup_row8 != null) {
					tHash_Lookup_row8.endGet();
				}
				globalMap.remove("tHash_Lookup_row8");

				// ###############################

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("MODULE" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tMap_4", true);
				end_Hash.put("tMap_4", System.currentTimeMillis());

				/**
				 * [tMap_4 end ] stop
				 */

				/**
				 * [tFileOutputExcel_3 end ] start
				 */

				currentComponent = "tFileOutputExcel_3";

				xlsxTool_tFileOutputExcel_3.writeExcel(
						fileName_tFileOutputExcel_3, true);

				nb_line_tFileOutputExcel_3 = nb_line_tFileOutputExcel_3 - 1;

				globalMap.put("tFileOutputExcel_3_NB_LINE",
						nb_line_tFileOutputExcel_3);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("MODULES_SEMESTRE"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tFileOutputExcel_3", true);
				end_Hash.put("tFileOutputExcel_3", System.currentTimeMillis());

				/**
				 * [tFileOutputExcel_3 end ] stop
				 */

				/**
				 * [tDBOutput_3 end ] start
				 */

				currentComponent = "tDBOutput_3";

				try {
					if (batchSizeCounter_tDBOutput_3 != 0) {
						int countSum_tDBOutput_3 = 0;

						for (int countEach_tDBOutput_3 : pstmt_tDBOutput_3
								.executeBatch()) {
							countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0
									: 1);
						}

						insertedCount_tDBOutput_3 += countSum_tDBOutput_3;

					}

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_3 = 0;
					for (int countEach_tDBOutput_3 : e.getUpdateCounts()) {
						countSum_tDBOutput_3 += (countEach_tDBOutput_3 < 0 ? 0
								: countEach_tDBOutput_3);
					}

					insertedCount_tDBOutput_3 += countSum_tDBOutput_3;

					globalMap.put(currentComponent + "_ERROR_MESSAGE",
							e.getMessage());
					System.err.println(e.getMessage());

				}
				batchSizeCounter_tDBOutput_3 = 0;

				if (pstmt_tDBOutput_3 != null) {

					pstmt_tDBOutput_3.close();

				}

				if (commitCounter_tDBOutput_3 > 0) {

					conn_tDBOutput_3.commit();

				}

				conn_tDBOutput_3.close();

				resourceMap.put("finish_tDBOutput_3", true);

				nb_line_deleted_tDBOutput_3 = nb_line_deleted_tDBOutput_3
						+ deletedCount_tDBOutput_3;
				nb_line_update_tDBOutput_3 = nb_line_update_tDBOutput_3
						+ updatedCount_tDBOutput_3;
				nb_line_inserted_tDBOutput_3 = nb_line_inserted_tDBOutput_3
						+ insertedCount_tDBOutput_3;
				nb_line_rejected_tDBOutput_3 = nb_line_rejected_tDBOutput_3
						+ rejectedCount_tDBOutput_3;

				globalMap.put("tDBOutput_3_NB_LINE", nb_line_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_3);
				globalMap.put("tDBOutput_3_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_3);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("DIM_SEMESTRE_MOD"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tDBOutput_3", true);
				end_Hash.put("tDBOutput_3", System.currentTimeMillis());

				/**
				 * [tDBOutput_3 end ] stop
				 */

				/**
				 * [tUniqRow_1 end ] start
				 */

				currentComponent = "tUniqRow_1";

				globalMap.put("tUniqRow_1_NB_UNIQUES", nb_uniques_tUniqRow_1);
				globalMap.put("tUniqRow_1_NB_DUPLICATES",
						nb_duplicates_tUniqRow_1);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("ANNE_MODULE"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tUniqRow_1", true);
				end_Hash.put("tUniqRow_1", System.currentTimeMillis());

				/**
				 * [tUniqRow_1 end ] stop
				 */

				/**
				 * [tAdvancedHash_row30 end ] start
				 */

				currentComponent = "tAdvancedHash_row30";

				tHash_Lookup_row30.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row30" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row30", true);
				end_Hash.put("tAdvancedHash_row30", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row30 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			// free memory for "tMap_4"
			globalMap.remove("tHash_Lookup_row7");

			// free memory for "tMap_4"
			globalMap.remove("tHash_Lookup_row8");

			// free memory for "tMap_3"
			globalMap.remove("tHash_Lookup_row15");

			// free memory for "tMap_3"
			globalMap.remove("tHash_Lookup_row14");

			try {

				/**
				 * [tDBInput_8 finally ] start
				 */

				currentComponent = "tDBInput_8";

				/**
				 * [tDBInput_8 finally ] stop
				 */

				/**
				 * [tMap_3 finally ] start
				 */

				currentComponent = "tMap_3";

				/**
				 * [tMap_3 finally ] stop
				 */

				/**
				 * [tMap_4 finally ] start
				 */

				currentComponent = "tMap_4";

				/**
				 * [tMap_4 finally ] stop
				 */

				/**
				 * [tFileOutputExcel_3 finally ] start
				 */

				currentComponent = "tFileOutputExcel_3";

				/**
				 * [tFileOutputExcel_3 finally ] stop
				 */

				/**
				 * [tDBOutput_3 finally ] start
				 */

				currentComponent = "tDBOutput_3";

				if (resourceMap.get("finish_tDBOutput_3") == null) {
					if (resourceMap.get("conn_tDBOutput_3") != null) {
						try {

							java.sql.Connection ctn_tDBOutput_3 = (java.sql.Connection) resourceMap
									.get("conn_tDBOutput_3");

							ctn_tDBOutput_3.close();

						} catch (java.sql.SQLException sqlEx_tDBOutput_3) {
							String errorMessage_tDBOutput_3 = "failed to close the connection in tDBOutput_3 :"
									+ sqlEx_tDBOutput_3.getMessage();

							System.err.println(errorMessage_tDBOutput_3);
						}
					}
				}

				/**
				 * [tDBOutput_3 finally ] stop
				 */

				/**
				 * [tUniqRow_1 finally ] start
				 */

				currentComponent = "tUniqRow_1";

				/**
				 * [tUniqRow_1 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row30 finally ] start
				 */

				currentComponent = "tAdvancedHash_row30";

				/**
				 * [tAdvancedHash_row30 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_8_SUBPROCESS_STATE", 1);
	}

	public static class row8Struct implements
			routines.system.IPersistableComparableLookupRow<row8Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public double CM_FAITES;

		public double getCM_FAITES() {
			return this.CM_FAITES;
		}

		public double TD_FAITES;

		public double getTD_FAITES() {
			return this.TD_FAITES;
		}

		public double TP_FAITES;

		public double getTP_FAITES() {
			return this.TP_FAITES;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row8Struct other = (row8Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(row8Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.CM_FAITES = this.CM_FAITES;
			other.TD_FAITES = this.TD_FAITES;
			other.TP_FAITES = this.TP_FAITES;

		}

		public void copyKeysDataTo(row8Struct other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.enseignantID = dis.readInt();

				this.CM_FAITES = dis.readDouble();

				this.TD_FAITES = dis.readDouble();

				this.TP_FAITES = dis.readDouble();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				dos.writeInt(this.enseignantID);

				dos.writeDouble(this.CM_FAITES);

				dos.writeDouble(this.TD_FAITES);

				dos.writeDouble(this.TP_FAITES);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",CM_FAITES=" + String.valueOf(CM_FAITES));
			sb.append(",TD_FAITES=" + String.valueOf(TD_FAITES));
			sb.append(",TP_FAITES=" + String.valueOf(TP_FAITES));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row8Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class OnRowsEndStructtAggregateRow_2 implements
			routines.system.IPersistableRow<OnRowsEndStructtAggregateRow_2> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public double CM_FAITES;

		public double getCM_FAITES() {
			return this.CM_FAITES;
		}

		public double TD_FAITES;

		public double getTD_FAITES() {
			return this.TD_FAITES;
		}

		public double TP_FAITES;

		public double getTP_FAITES() {
			return this.TP_FAITES;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final OnRowsEndStructtAggregateRow_2 other = (OnRowsEndStructtAggregateRow_2) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			return true;
		}

		public void copyDataTo(OnRowsEndStructtAggregateRow_2 other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.CM_FAITES = this.CM_FAITES;
			other.TD_FAITES = this.TD_FAITES;
			other.TP_FAITES = this.TP_FAITES;

		}

		public void copyKeysDataTo(OnRowsEndStructtAggregateRow_2 other) {

			other.codemodsemestre = this.codemodsemestre;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

					this.CM_FAITES = dis.readDouble();

					this.TD_FAITES = dis.readDouble();

					this.TP_FAITES = dis.readDouble();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

				// double

				dos.writeDouble(this.CM_FAITES);

				// double

				dos.writeDouble(this.TD_FAITES);

				// double

				dos.writeDouble(this.TP_FAITES);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",CM_FAITES=" + String.valueOf(CM_FAITES));
			sb.append(",TD_FAITES=" + String.valueOf(TD_FAITES));
			sb.append(",TP_FAITES=" + String.valueOf(TP_FAITES));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(OnRowsEndStructtAggregateRow_2 other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class HEURES_FAITESStruct implements
			routines.system.IPersistableRow<HEURES_FAITESStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public double CM_FAITES;

		public double getCM_FAITES() {
			return this.CM_FAITES;
		}

		public double TD_FAITES;

		public double getTD_FAITES() {
			return this.TD_FAITES;
		}

		public double TP_FAITES;

		public double getTP_FAITES() {
			return this.TP_FAITES;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final HEURES_FAITESStruct other = (HEURES_FAITESStruct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(HEURES_FAITESStruct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.CM_FAITES = this.CM_FAITES;
			other.TD_FAITES = this.TD_FAITES;
			other.TP_FAITES = this.TP_FAITES;

		}

		public void copyKeysDataTo(HEURES_FAITESStruct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

					this.CM_FAITES = dis.readDouble();

					this.TD_FAITES = dis.readDouble();

					this.TP_FAITES = dis.readDouble();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

				// double

				dos.writeDouble(this.CM_FAITES);

				// double

				dos.writeDouble(this.TD_FAITES);

				// double

				dos.writeDouble(this.TP_FAITES);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",CM_FAITES=" + String.valueOf(CM_FAITES));
			sb.append(",TD_FAITES=" + String.valueOf(TD_FAITES));
			sb.append(",TP_FAITES=" + String.valueOf(TP_FAITES));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(HEURES_FAITESStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row4Struct implements
			routines.system.IPersistableRow<row4Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public double heuresCM;

		public double getHeuresCM() {
			return this.heuresCM;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		public boolean paye;

		public boolean getPaye() {
			return this.paye;
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

					this.heuresCM = dis.readDouble();

					this.verrou = dis.readBoolean();

					this.verrouDPT = dis.readBoolean();

					this.paye = dis.readBoolean();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

				// double

				dos.writeDouble(this.heuresCM);

				// boolean

				dos.writeBoolean(this.verrou);

				// boolean

				dos.writeBoolean(this.verrouDPT);

				// boolean

				dos.writeBoolean(this.paye);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresCM=" + String.valueOf(heuresCM));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append(",paye=" + String.valueOf(paye));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row4Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class after_tDBInput_9Struct implements
			routines.system.IPersistableRow<after_tDBInput_9Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public double heuresCM;

		public double getHeuresCM() {
			return this.heuresCM;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		public boolean paye;

		public boolean getPaye() {
			return this.paye;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final after_tDBInput_9Struct other = (after_tDBInput_9Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(after_tDBInput_9Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.heuresCM = this.heuresCM;
			other.verrou = this.verrou;
			other.verrouDPT = this.verrouDPT;
			other.paye = this.paye;

		}

		public void copyKeysDataTo(after_tDBInput_9Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

					this.heuresCM = dis.readDouble();

					this.verrou = dis.readBoolean();

					this.verrouDPT = dis.readBoolean();

					this.paye = dis.readBoolean();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

				// double

				dos.writeDouble(this.heuresCM);

				// boolean

				dos.writeBoolean(this.verrou);

				// boolean

				dos.writeBoolean(this.verrouDPT);

				// boolean

				dos.writeBoolean(this.paye);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresCM=" + String.valueOf(heuresCM));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append(",paye=" + String.valueOf(paye));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(after_tDBInput_9Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_9Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_9_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				tDBInput_10Process(globalMap);
				tDBInput_11Process(globalMap);

				row4Struct row4 = new row4Struct();
				HEURES_FAITESStruct HEURES_FAITES = new HEURES_FAITESStruct();
				row8Struct row8 = new row8Struct();

				/**
				 * [tAggregateRow_2_AGGOUT begin ] start
				 */

				ok_Hash.put("tAggregateRow_2_AGGOUT", false);
				start_Hash.put("tAggregateRow_2_AGGOUT",
						System.currentTimeMillis());

				currentVirtualComponent = "tAggregateRow_2";

				currentComponent = "tAggregateRow_2_AGGOUT";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("HEURES_FAITES"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tAggregateRow_2_AGGOUT = 0;

				class BytesLimit65535_tAggregateRow_2_AGGOUT {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAggregateRow_2_AGGOUT().limitLog4jByte();

				// ------------ Seems it is not used

				java.util.Map hashAggreg_tAggregateRow_2 = new java.util.HashMap();

				// ------------

				class UtilClass_tAggregateRow_2 { // G_OutBegin_AggR_144

					public double sd(Double[] data) {
						final int n = data.length;
						if (n < 2) {
							return Double.NaN;
						}
						double d1 = 0d;
						double d2 = 0d;

						for (int i = 0; i < data.length; i++) {
							d1 += (data[i] * data[i]);
							d2 += data[i];
						}

						return Math.sqrt((n * d1 - d2 * d2) / n / (n - 1));
					}

					public void checkedIADD(byte a, byte b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						byte r = (byte) (a + b);
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'short/Short'", "'byte/Byte'"));
						}
					}

					public void checkedIADD(short a, short b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						short r = (short) (a + b);
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'int/Integer'", "'short/Short'"));
						}
					}

					public void checkedIADD(int a, int b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						int r = a + b;
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'long/Long'", "'int/Integer'"));
						}
					}

					public void checkedIADD(long a, long b,
							boolean checkTypeOverFlow, boolean checkUlp) {
						long r = a + b;
						if (checkTypeOverFlow && ((a ^ r) & (b ^ r)) < 0) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'long/Long'"));
						}
					}

					public void checkedIADD(float a, float b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkUlp) {
							float minAddedValue = Math.ulp(a);
							if (minAddedValue > Math.abs(b)) {
								throw new RuntimeException(
										buildPrecisionMessage(
												String.valueOf(a),
												String.valueOf(b),
												"'double' or 'BigDecimal'",
												"'float/Float'"));
							}
						}

						if (checkTypeOverFlow
								&& ((double) a + (double) b > (double) Float.MAX_VALUE)
								|| ((double) a + (double) b < (double) -Float.MAX_VALUE)) {
							throw new RuntimeException(
									buildOverflowMessage(String.valueOf(a),
											String.valueOf(b),
											"'double' or 'BigDecimal'",
											"'float/Float'"));
						}
					}

					public void checkedIADD(double a, double b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkUlp) {
							double minAddedValue = Math.ulp(a);
							if (minAddedValue > Math.abs(b)) {
								throw new RuntimeException(
										buildPrecisionMessage(
												String.valueOf(a),
												String.valueOf(a),
												"'BigDecimal'",
												"'double/Double'"));
							}
						}

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, byte b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, short b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, int b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					public void checkedIADD(double a, float b,
							boolean checkTypeOverFlow, boolean checkUlp) {

						if (checkUlp) {
							double minAddedValue = Math.ulp(a);
							if (minAddedValue > Math.abs(b)) {
								throw new RuntimeException(
										buildPrecisionMessage(
												String.valueOf(a),
												String.valueOf(a),
												"'BigDecimal'",
												"'double/Double'"));
							}
						}

						if (checkTypeOverFlow
								&& (a + b > (double) Double.MAX_VALUE)
								|| (a + b < -Double.MAX_VALUE)) {
							throw new RuntimeException(buildOverflowMessage(
									String.valueOf(a), String.valueOf(b),
									"'BigDecimal'", "'double/Double'"));
						}
					}

					private String buildOverflowMessage(String a, String b,
							String advicedTypes, String originalType) {
						return "Type overflow when adding "
								+ b
								+ " to "
								+ a
								+ ", to resolve this problem, increase the precision by using "
								+ advicedTypes + " type in place of "
								+ originalType + ".";
					}

					private String buildPrecisionMessage(String a, String b,
							String advicedTypes, String originalType) {
						return "The double precision is unsufficient to add the value "
								+ b
								+ " to "
								+ a
								+ ", to resolve this problem, increase the precision by using "
								+ advicedTypes
								+ " type in place of "
								+ originalType + ".";
					}

				} // G_OutBegin_AggR_144

				UtilClass_tAggregateRow_2 utilClass_tAggregateRow_2 = new UtilClass_tAggregateRow_2();

				class AggOperationStruct_tAggregateRow_2 { // G_OutBegin_AggR_100

					private static final int DEFAULT_HASHCODE = 1;
					private static final int PRIME = 31;
					private int hashCode = DEFAULT_HASHCODE;
					public boolean hashCodeDirty = true;

					int codemodsemestre;
					BigDecimal CM_FAITES_sum;
					BigDecimal TD_FAITES_sum;
					BigDecimal TP_FAITES_sum;

					@Override
					public int hashCode() {
						if (this.hashCodeDirty) {
							final int prime = PRIME;
							int result = DEFAULT_HASHCODE;

							result = prime * result
									+ (int) this.codemodsemestre;

							this.hashCode = result;
							this.hashCodeDirty = false;
						}
						return this.hashCode;
					}

					@Override
					public boolean equals(Object obj) {
						if (this == obj)
							return true;
						if (obj == null)
							return false;
						if (getClass() != obj.getClass())
							return false;
						final AggOperationStruct_tAggregateRow_2 other = (AggOperationStruct_tAggregateRow_2) obj;

						if (this.codemodsemestre != other.codemodsemestre)
							return false;

						return true;
					}

				} // G_OutBegin_AggR_100

				AggOperationStruct_tAggregateRow_2 operation_result_tAggregateRow_2 = null;
				AggOperationStruct_tAggregateRow_2 operation_finder_tAggregateRow_2 = new AggOperationStruct_tAggregateRow_2();
				java.util.Map<AggOperationStruct_tAggregateRow_2, AggOperationStruct_tAggregateRow_2> hash_tAggregateRow_2 = new java.util.HashMap<AggOperationStruct_tAggregateRow_2, AggOperationStruct_tAggregateRow_2>();

				/**
				 * [tAggregateRow_2_AGGOUT begin ] stop
				 */

				/**
				 * [tMap_5 begin ] start
				 */

				ok_Hash.put("tMap_5", false);
				start_Hash.put("tMap_5", System.currentTimeMillis());

				currentComponent = "tMap_5";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row4" + iterateId, 0, 0);

					}
				}

				int tos_count_tMap_5 = 0;

				class BytesLimit65535_tMap_5 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tMap_5().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct> tHash_Lookup_row5 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct>) globalMap
						.get("tHash_Lookup_row5"));

				row5Struct row5HashKey = new row5Struct();
				row5Struct row5Default = new row5Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row6Struct> tHash_Lookup_row6 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row6Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row6Struct>) globalMap
						.get("tHash_Lookup_row6"));

				row6Struct row6HashKey = new row6Struct();
				row6Struct row6Default = new row6Struct();
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_5__Struct {
				}
				Var__tMap_5__Struct Var__tMap_5 = new Var__tMap_5__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				HEURES_FAITESStruct HEURES_FAITES_tmp = new HEURES_FAITESStruct();
				// ###############################

				/**
				 * [tMap_5 begin ] stop
				 */

				/**
				 * [tDBInput_9 begin ] start
				 */

				ok_Hash.put("tDBInput_9", false);
				start_Hash.put("tDBInput_9", System.currentTimeMillis());

				currentComponent = "tDBInput_9";

				int tos_count_tDBInput_9 = 0;

				class BytesLimit65535_tDBInput_9 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_9().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_9 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_9.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_9 = calendar_tDBInput_9.getTime();
				int nb_line_tDBInput_9 = 0;
				java.sql.Connection conn_tDBInput_9 = null;
				String driverClass_tDBInput_9 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_9);
				String dbUser_tDBInput_9 = "root";

				final String decryptedPassword_tDBInput_9 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_9 = decryptedPassword_tDBInput_9;

				String url_tDBInput_9 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_9 = java.sql.DriverManager.getConnection(
						url_tDBInput_9, dbUser_tDBInput_9, dbPwd_tDBInput_9);

				java.sql.Statement stmt_tDBInput_9 = conn_tDBInput_9
						.createStatement();

				String dbquery_tDBInput_9 = "SELECT \n  `preserviceCM`.`codemodsemestre`, \n  `preserviceCM`.`enseignantID`, \n  `preserviceCM`.`heuresCM`, \n  `preserv"
						+ "iceCM`.`verrou`, \n  `preserviceCM`.`verrouDPT`, \n  `preserviceCM`.`paye`\nFROM `preserviceCM`";

				globalMap.put("tDBInput_9_QUERY", dbquery_tDBInput_9);
				java.sql.ResultSet rs_tDBInput_9 = null;

				try {
					rs_tDBInput_9 = stmt_tDBInput_9
							.executeQuery(dbquery_tDBInput_9);
					java.sql.ResultSetMetaData rsmd_tDBInput_9 = rs_tDBInput_9
							.getMetaData();
					int colQtyInRs_tDBInput_9 = rsmd_tDBInput_9
							.getColumnCount();

					String tmpContent_tDBInput_9 = null;

					while (rs_tDBInput_9.next()) {
						nb_line_tDBInput_9++;

						if (colQtyInRs_tDBInput_9 < 1) {
							row4.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_9.getObject(1) != null) {
								row4.codemodsemestre = rs_tDBInput_9.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_9 < 2) {
							row4.enseignantID = 0;
						} else {

							if (rs_tDBInput_9.getObject(2) != null) {
								row4.enseignantID = rs_tDBInput_9.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_9 < 3) {
							row4.heuresCM = 0;
						} else {

							if (rs_tDBInput_9.getObject(3) != null) {
								row4.heuresCM = rs_tDBInput_9.getDouble(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_9 < 4) {
							row4.verrou = false;
						} else {

							if (rs_tDBInput_9.getObject(4) != null) {
								row4.verrou = rs_tDBInput_9.getBoolean(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_9 < 5) {
							row4.verrouDPT = false;
						} else {

							if (rs_tDBInput_9.getObject(5) != null) {
								row4.verrouDPT = rs_tDBInput_9.getBoolean(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_9 < 6) {
							row4.paye = false;
						} else {

							if (rs_tDBInput_9.getObject(6) != null) {
								row4.paye = rs_tDBInput_9.getBoolean(6);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_9 begin ] stop
						 */

						/**
						 * [tDBInput_9 main ] start
						 */

						currentComponent = "tDBInput_9";

						tos_count_tDBInput_9++;

						/**
						 * [tDBInput_9 main ] stop
						 */

						/**
						 * [tDBInput_9 process_data_begin ] start
						 */

						currentComponent = "tDBInput_9";

						/**
						 * [tDBInput_9 process_data_begin ] stop
						 */

						/**
						 * [tMap_5 main ] start
						 */

						currentComponent = "tMap_5";

						// row4
						// row4

						if (execStat) {
							runStat.updateStatOnConnection("row4" + iterateId,
									1, 1);
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_5 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_5 = false;
						boolean mainRowRejected_tMap_5 = false;

						// /////////////////////////////////////////////
						// Starting Lookup Table "row5"
						// /////////////////////////////////////////////

						boolean forceLooprow5 = false;

						row5Struct row5ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_5) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_5 = false;

							Object exprKeyValue_row5__codemodsemestre = row4.codemodsemestre;
							if (exprKeyValue_row5__codemodsemestre == null) {
								hasCasePrimitiveKeyWithNull_tMap_5 = true;
							} else {
								row5HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row5__codemodsemestre;
							}

							Object exprKeyValue_row5__enseignantID = row4.enseignantID;
							if (exprKeyValue_row5__enseignantID == null) {
								hasCasePrimitiveKeyWithNull_tMap_5 = true;
							} else {
								row5HashKey.enseignantID = (int) (Integer) exprKeyValue_row5__enseignantID;
							}

							row5HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_5) { // G_TM_M_091

								tHash_Lookup_row5.lookup(row5HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_5
									|| !tHash_Lookup_row5.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_5 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row5 != null
								&& tHash_Lookup_row5.getCount(row5HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row5' and it contains more one result from keys :  row5.codemodsemestre = '"
							// + row5HashKey.codemodsemestre +
							// "', row5.enseignantID = '" +
							// row5HashKey.enseignantID + "'");
						} // G 071

						row5Struct row5 = null;

						row5Struct fromLookup_row5 = null;
						row5 = row5Default;

						if (tHash_Lookup_row5 != null
								&& tHash_Lookup_row5.hasNext()) { // G 099

							fromLookup_row5 = tHash_Lookup_row5.next();

						} // G 099

						if (fromLookup_row5 != null) {
							row5 = fromLookup_row5;
						}

						// /////////////////////////////////////////////
						// Starting Lookup Table "row6"
						// /////////////////////////////////////////////

						boolean forceLooprow6 = false;

						row6Struct row6ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_5) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_5 = false;

							Object exprKeyValue_row6__codemodsemestre = row4.codemodsemestre;
							if (exprKeyValue_row6__codemodsemestre == null) {
								hasCasePrimitiveKeyWithNull_tMap_5 = true;
							} else {
								row6HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row6__codemodsemestre;
							}

							Object exprKeyValue_row6__enseignantID = row4.enseignantID;
							if (exprKeyValue_row6__enseignantID == null) {
								hasCasePrimitiveKeyWithNull_tMap_5 = true;
							} else {
								row6HashKey.enseignantID = (int) (Integer) exprKeyValue_row6__enseignantID;
							}

							row6HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_5) { // G_TM_M_091

								tHash_Lookup_row6.lookup(row6HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_5
									|| !tHash_Lookup_row6.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_5 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row6 != null
								&& tHash_Lookup_row6.getCount(row6HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row6' and it contains more one result from keys :  row6.codemodsemestre = '"
							// + row6HashKey.codemodsemestre +
							// "', row6.enseignantID = '" +
							// row6HashKey.enseignantID + "'");
						} // G 071

						row6Struct row6 = null;

						row6Struct fromLookup_row6 = null;
						row6 = row6Default;

						if (tHash_Lookup_row6 != null
								&& tHash_Lookup_row6.hasNext()) { // G 099

							fromLookup_row6 = tHash_Lookup_row6.next();

						} // G 099

						if (fromLookup_row6 != null) {
							row6 = fromLookup_row6;
						}

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_5__Struct Var = Var__tMap_5;// ###############################
							// ###############################
							// # Output tables

							HEURES_FAITES = null;

							if (!rejectedInnerJoin_tMap_5) {

								// # Output table : 'HEURES_FAITES'
								HEURES_FAITES_tmp.codemodsemestre = row4.codemodsemestre;
								HEURES_FAITES_tmp.enseignantID = row4.enseignantID;
								HEURES_FAITES_tmp.CM_FAITES = row4.heuresCM;
								HEURES_FAITES_tmp.TD_FAITES = row5.heuresTD;
								HEURES_FAITES_tmp.TP_FAITES = row6.heuresTP;
								HEURES_FAITES = HEURES_FAITES_tmp;
							} // closing inner join bracket (2)
								// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_5 = false;

						tos_count_tMap_5++;

						/**
						 * [tMap_5 main ] stop
						 */

						/**
						 * [tMap_5 process_data_begin ] start
						 */

						currentComponent = "tMap_5";

						/**
						 * [tMap_5 process_data_begin ] stop
						 */
						// Start of branch "HEURES_FAITES"
						if (HEURES_FAITES != null) {

							/**
							 * [tAggregateRow_2_AGGOUT main ] start
							 */

							currentVirtualComponent = "tAggregateRow_2";

							currentComponent = "tAggregateRow_2_AGGOUT";

							// HEURES_FAITES
							// HEURES_FAITES

							if (execStat) {
								runStat.updateStatOnConnection("HEURES_FAITES"
										+ iterateId, 1, 1);
							}

							operation_finder_tAggregateRow_2.codemodsemestre = HEURES_FAITES.codemodsemestre;

							operation_finder_tAggregateRow_2.hashCodeDirty = true;

							operation_result_tAggregateRow_2 = hash_tAggregateRow_2
									.get(operation_finder_tAggregateRow_2);

							if (operation_result_tAggregateRow_2 == null) { // G_OutMain_AggR_001

								operation_result_tAggregateRow_2 = new AggOperationStruct_tAggregateRow_2();

								operation_result_tAggregateRow_2.codemodsemestre = operation_finder_tAggregateRow_2.codemodsemestre;

								hash_tAggregateRow_2.put(
										operation_result_tAggregateRow_2,
										operation_result_tAggregateRow_2);

							} // G_OutMain_AggR_001

							if (operation_result_tAggregateRow_2.CM_FAITES_sum == null) {
								operation_result_tAggregateRow_2.CM_FAITES_sum = new BigDecimal(
										0).setScale(2);
							}
							operation_result_tAggregateRow_2.CM_FAITES_sum = operation_result_tAggregateRow_2.CM_FAITES_sum
									.add(new BigDecimal(String
											.valueOf(HEURES_FAITES.CM_FAITES)));

							if (operation_result_tAggregateRow_2.TD_FAITES_sum == null) {
								operation_result_tAggregateRow_2.TD_FAITES_sum = new BigDecimal(
										0).setScale(2);
							}
							operation_result_tAggregateRow_2.TD_FAITES_sum = operation_result_tAggregateRow_2.TD_FAITES_sum
									.add(new BigDecimal(String
											.valueOf(HEURES_FAITES.TD_FAITES)));

							if (operation_result_tAggregateRow_2.TP_FAITES_sum == null) {
								operation_result_tAggregateRow_2.TP_FAITES_sum = new BigDecimal(
										0).setScale(2);
							}
							operation_result_tAggregateRow_2.TP_FAITES_sum = operation_result_tAggregateRow_2.TP_FAITES_sum
									.add(new BigDecimal(String
											.valueOf(HEURES_FAITES.TP_FAITES)));

							tos_count_tAggregateRow_2_AGGOUT++;

							/**
							 * [tAggregateRow_2_AGGOUT main ] stop
							 */

							/**
							 * [tAggregateRow_2_AGGOUT process_data_begin ]
							 * start
							 */

							currentVirtualComponent = "tAggregateRow_2";

							currentComponent = "tAggregateRow_2_AGGOUT";

							/**
							 * [tAggregateRow_2_AGGOUT process_data_begin ] stop
							 */

							/**
							 * [tAggregateRow_2_AGGOUT process_data_end ] start
							 */

							currentVirtualComponent = "tAggregateRow_2";

							currentComponent = "tAggregateRow_2_AGGOUT";

							/**
							 * [tAggregateRow_2_AGGOUT process_data_end ] stop
							 */

						} // End of branch "HEURES_FAITES"

						/**
						 * [tMap_5 process_data_end ] start
						 */

						currentComponent = "tMap_5";

						/**
						 * [tMap_5 process_data_end ] stop
						 */

						/**
						 * [tDBInput_9 process_data_end ] start
						 */

						currentComponent = "tDBInput_9";

						/**
						 * [tDBInput_9 process_data_end ] stop
						 */

						/**
						 * [tDBInput_9 end ] start
						 */

						currentComponent = "tDBInput_9";

					}
				} finally {
					if (rs_tDBInput_9 != null) {
						rs_tDBInput_9.close();
					}
					stmt_tDBInput_9.close();
					if (conn_tDBInput_9 != null && !conn_tDBInput_9.isClosed()) {

						conn_tDBInput_9.close();

					}

				}

				globalMap.put("tDBInput_9_NB_LINE", nb_line_tDBInput_9);

				ok_Hash.put("tDBInput_9", true);
				end_Hash.put("tDBInput_9", System.currentTimeMillis());

				/**
				 * [tDBInput_9 end ] stop
				 */

				/**
				 * [tMap_5 end ] start
				 */

				currentComponent = "tMap_5";

				// ###############################
				// # Lookup hashes releasing
				if (tHash_Lookup_row5 != null) {
					tHash_Lookup_row5.endGet();
				}
				globalMap.remove("tHash_Lookup_row5");

				if (tHash_Lookup_row6 != null) {
					tHash_Lookup_row6.endGet();
				}
				globalMap.remove("tHash_Lookup_row6");

				// ###############################

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row4" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tMap_5", true);
				end_Hash.put("tMap_5", System.currentTimeMillis());

				/**
				 * [tMap_5 end ] stop
				 */

				/**
				 * [tAggregateRow_2_AGGOUT end ] start
				 */

				currentVirtualComponent = "tAggregateRow_2";

				currentComponent = "tAggregateRow_2_AGGOUT";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("HEURES_FAITES"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAggregateRow_2_AGGOUT", true);
				end_Hash.put("tAggregateRow_2_AGGOUT",
						System.currentTimeMillis());

				/**
				 * [tAggregateRow_2_AGGOUT end ] stop
				 */

				/**
				 * [tAdvancedHash_row8 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row8", false);
				start_Hash
						.put("tAdvancedHash_row8", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row8";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row8" + iterateId, 0, 0);

					}
				}

				int tos_count_tAdvancedHash_row8 = 0;

				class BytesLimit65535_tAdvancedHash_row8 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row8().limitLog4jByte();

				// connection name:row8
				// source node:tAggregateRow_2_AGGIN - inputs:(OnRowsEnd)
				// outputs:(row8,row8) | target node:tAdvancedHash_row8 -
				// inputs:(row8) outputs:()
				// linked node: tMap_4 - inputs:(row7,MODULE,row8)
				// outputs:(MODULES_SEMESTRE,DIM_SEMESTRE_MOD)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row8 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row8Struct> tHash_Lookup_row8 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row8Struct> getLookup(matchingModeEnum_row8);

				globalMap.put("tHash_Lookup_row8", tHash_Lookup_row8);

				/**
				 * [tAdvancedHash_row8 begin ] stop
				 */

				/**
				 * [tAggregateRow_2_AGGIN begin ] start
				 */

				ok_Hash.put("tAggregateRow_2_AGGIN", false);
				start_Hash.put("tAggregateRow_2_AGGIN",
						System.currentTimeMillis());

				currentVirtualComponent = "tAggregateRow_2";

				currentComponent = "tAggregateRow_2_AGGIN";

				int tos_count_tAggregateRow_2_AGGIN = 0;

				class BytesLimit65535_tAggregateRow_2_AGGIN {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAggregateRow_2_AGGIN().limitLog4jByte();

				java.util.Collection<AggOperationStruct_tAggregateRow_2> values_tAggregateRow_2 = hash_tAggregateRow_2
						.values();

				globalMap.put("tAggregateRow_2_NB_LINE",
						values_tAggregateRow_2.size());

				for (AggOperationStruct_tAggregateRow_2 aggregated_row_tAggregateRow_2 : values_tAggregateRow_2) { // G_AggR_600

					/**
					 * [tAggregateRow_2_AGGIN begin ] stop
					 */

					/**
					 * [tAggregateRow_2_AGGIN main ] start
					 */

					currentVirtualComponent = "tAggregateRow_2";

					currentComponent = "tAggregateRow_2_AGGIN";

					row8.codemodsemestre = aggregated_row_tAggregateRow_2.codemodsemestre;

					if (aggregated_row_tAggregateRow_2.CM_FAITES_sum != null) {
						row8.CM_FAITES = aggregated_row_tAggregateRow_2.CM_FAITES_sum
								.doubleValue();

					} else {

						row8.CM_FAITES = 0;

					}

					if (aggregated_row_tAggregateRow_2.TD_FAITES_sum != null) {
						row8.TD_FAITES = aggregated_row_tAggregateRow_2.TD_FAITES_sum
								.doubleValue();

					} else {

						row8.TD_FAITES = 0;

					}

					if (aggregated_row_tAggregateRow_2.TP_FAITES_sum != null) {
						row8.TP_FAITES = aggregated_row_tAggregateRow_2.TP_FAITES_sum
								.doubleValue();

					} else {

						row8.TP_FAITES = 0;

					}

					tos_count_tAggregateRow_2_AGGIN++;

					/**
					 * [tAggregateRow_2_AGGIN main ] stop
					 */

					/**
					 * [tAggregateRow_2_AGGIN process_data_begin ] start
					 */

					currentVirtualComponent = "tAggregateRow_2";

					currentComponent = "tAggregateRow_2_AGGIN";

					/**
					 * [tAggregateRow_2_AGGIN process_data_begin ] stop
					 */

					/**
					 * [tAdvancedHash_row8 main ] start
					 */

					currentComponent = "tAdvancedHash_row8";

					// row8
					// row8

					if (execStat) {
						runStat.updateStatOnConnection("row8" + iterateId, 1, 1);
					}

					row8Struct row8_HashRow = new row8Struct();

					row8_HashRow.codemodsemestre = row8.codemodsemestre;

					row8_HashRow.enseignantID = row8.enseignantID;

					row8_HashRow.CM_FAITES = row8.CM_FAITES;

					row8_HashRow.TD_FAITES = row8.TD_FAITES;

					row8_HashRow.TP_FAITES = row8.TP_FAITES;

					tHash_Lookup_row8.put(row8_HashRow);

					tos_count_tAdvancedHash_row8++;

					/**
					 * [tAdvancedHash_row8 main ] stop
					 */

					/**
					 * [tAdvancedHash_row8 process_data_begin ] start
					 */

					currentComponent = "tAdvancedHash_row8";

					/**
					 * [tAdvancedHash_row8 process_data_begin ] stop
					 */

					/**
					 * [tAdvancedHash_row8 process_data_end ] start
					 */

					currentComponent = "tAdvancedHash_row8";

					/**
					 * [tAdvancedHash_row8 process_data_end ] stop
					 */

					/**
					 * [tAggregateRow_2_AGGIN process_data_end ] start
					 */

					currentVirtualComponent = "tAggregateRow_2";

					currentComponent = "tAggregateRow_2_AGGIN";

					/**
					 * [tAggregateRow_2_AGGIN process_data_end ] stop
					 */

					/**
					 * [tAggregateRow_2_AGGIN end ] start
					 */

					currentVirtualComponent = "tAggregateRow_2";

					currentComponent = "tAggregateRow_2_AGGIN";

				} // G_AggR_600

				ok_Hash.put("tAggregateRow_2_AGGIN", true);
				end_Hash.put("tAggregateRow_2_AGGIN",
						System.currentTimeMillis());

				/**
				 * [tAggregateRow_2_AGGIN end ] stop
				 */

				/**
				 * [tAdvancedHash_row8 end ] start
				 */

				currentComponent = "tAdvancedHash_row8";

				tHash_Lookup_row8.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row8" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAdvancedHash_row8", true);
				end_Hash.put("tAdvancedHash_row8", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row8 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			// free memory for "tAggregateRow_2_AGGIN"
			globalMap.remove("tAggregateRow_2");

			// free memory for "tMap_5"
			globalMap.remove("tHash_Lookup_row6");

			// free memory for "tMap_5"
			globalMap.remove("tHash_Lookup_row5");

			try {

				/**
				 * [tDBInput_9 finally ] start
				 */

				currentComponent = "tDBInput_9";

				/**
				 * [tDBInput_9 finally ] stop
				 */

				/**
				 * [tMap_5 finally ] start
				 */

				currentComponent = "tMap_5";

				/**
				 * [tMap_5 finally ] stop
				 */

				/**
				 * [tAggregateRow_2_AGGOUT finally ] start
				 */

				currentVirtualComponent = "tAggregateRow_2";

				currentComponent = "tAggregateRow_2_AGGOUT";

				/**
				 * [tAggregateRow_2_AGGOUT finally ] stop
				 */

				/**
				 * [tAggregateRow_2_AGGIN finally ] start
				 */

				currentVirtualComponent = "tAggregateRow_2";

				currentComponent = "tAggregateRow_2_AGGIN";

				/**
				 * [tAggregateRow_2_AGGIN finally ] stop
				 */

				/**
				 * [tAdvancedHash_row8 finally ] start
				 */

				currentComponent = "tAdvancedHash_row8";

				/**
				 * [tAdvancedHash_row8 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_9_SUBPROCESS_STATE", 1);
	}

	public static class row6Struct implements
			routines.system.IPersistableComparableLookupRow<row6Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public double heuresTP;

		public double getHeuresTP() {
			return this.heuresTP;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		public boolean paye;

		public boolean getPaye() {
			return this.paye;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row6Struct other = (row6Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(row6Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.heuresTP = this.heuresTP;
			other.verrou = this.verrou;
			other.verrouDPT = this.verrouDPT;
			other.paye = this.paye;

		}

		public void copyKeysDataTo(row6Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.heuresTP = dis.readDouble();

				this.verrou = dis.readBoolean();

				this.verrouDPT = dis.readBoolean();

				this.paye = dis.readBoolean();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				dos.writeDouble(this.heuresTP);

				dos.writeBoolean(this.verrou);

				dos.writeBoolean(this.verrouDPT);

				dos.writeBoolean(this.paye);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresTP=" + String.valueOf(heuresTP));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append(",paye=" + String.valueOf(paye));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row6Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_10Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_10_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row6Struct row6 = new row6Struct();

				/**
				 * [tAdvancedHash_row6 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row6", false);
				start_Hash
						.put("tAdvancedHash_row6", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row6";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row6" + iterateId, 0, 0);

					}
				}

				int tos_count_tAdvancedHash_row6 = 0;

				class BytesLimit65535_tAdvancedHash_row6 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row6().limitLog4jByte();

				// connection name:row6
				// source node:tDBInput_10 - inputs:(after_tDBInput_9)
				// outputs:(row6,row6) | target node:tAdvancedHash_row6 -
				// inputs:(row6) outputs:()
				// linked node: tMap_5 - inputs:(row4,row6,row5)
				// outputs:(HEURES_FAITES)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row6 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row6Struct> tHash_Lookup_row6 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row6Struct> getLookup(matchingModeEnum_row6);

				globalMap.put("tHash_Lookup_row6", tHash_Lookup_row6);

				/**
				 * [tAdvancedHash_row6 begin ] stop
				 */

				/**
				 * [tDBInput_10 begin ] start
				 */

				ok_Hash.put("tDBInput_10", false);
				start_Hash.put("tDBInput_10", System.currentTimeMillis());

				currentComponent = "tDBInput_10";

				int tos_count_tDBInput_10 = 0;

				class BytesLimit65535_tDBInput_10 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_10().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_10 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_10.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_10 = calendar_tDBInput_10
						.getTime();
				int nb_line_tDBInput_10 = 0;
				java.sql.Connection conn_tDBInput_10 = null;
				String driverClass_tDBInput_10 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_10);
				String dbUser_tDBInput_10 = "root";

				final String decryptedPassword_tDBInput_10 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_10 = decryptedPassword_tDBInput_10;

				String url_tDBInput_10 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_10 = java.sql.DriverManager.getConnection(
						url_tDBInput_10, dbUser_tDBInput_10, dbPwd_tDBInput_10);

				java.sql.Statement stmt_tDBInput_10 = conn_tDBInput_10
						.createStatement();

				String dbquery_tDBInput_10 = "SELECT \n  `preserviceTP`.`codemodsemestre`, \n  `preserviceTP`.`enseignantID`, \n  `preserviceTP`.`heuresTP`, \n  `preserv"
						+ "iceTP`.`verrou`, \n  `preserviceTP`.`verrouDPT`, \n  `preserviceTP`.`paye`\nFROM `preserviceTP`";

				globalMap.put("tDBInput_10_QUERY", dbquery_tDBInput_10);
				java.sql.ResultSet rs_tDBInput_10 = null;

				try {
					rs_tDBInput_10 = stmt_tDBInput_10
							.executeQuery(dbquery_tDBInput_10);
					java.sql.ResultSetMetaData rsmd_tDBInput_10 = rs_tDBInput_10
							.getMetaData();
					int colQtyInRs_tDBInput_10 = rsmd_tDBInput_10
							.getColumnCount();

					String tmpContent_tDBInput_10 = null;

					while (rs_tDBInput_10.next()) {
						nb_line_tDBInput_10++;

						if (colQtyInRs_tDBInput_10 < 1) {
							row6.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_10.getObject(1) != null) {
								row6.codemodsemestre = rs_tDBInput_10.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_10 < 2) {
							row6.enseignantID = 0;
						} else {

							if (rs_tDBInput_10.getObject(2) != null) {
								row6.enseignantID = rs_tDBInput_10.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_10 < 3) {
							row6.heuresTP = 0;
						} else {

							if (rs_tDBInput_10.getObject(3) != null) {
								row6.heuresTP = rs_tDBInput_10.getDouble(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_10 < 4) {
							row6.verrou = false;
						} else {

							if (rs_tDBInput_10.getObject(4) != null) {
								row6.verrou = rs_tDBInput_10.getBoolean(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_10 < 5) {
							row6.verrouDPT = false;
						} else {

							if (rs_tDBInput_10.getObject(5) != null) {
								row6.verrouDPT = rs_tDBInput_10.getBoolean(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_10 < 6) {
							row6.paye = false;
						} else {

							if (rs_tDBInput_10.getObject(6) != null) {
								row6.paye = rs_tDBInput_10.getBoolean(6);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_10 begin ] stop
						 */

						/**
						 * [tDBInput_10 main ] start
						 */

						currentComponent = "tDBInput_10";

						tos_count_tDBInput_10++;

						/**
						 * [tDBInput_10 main ] stop
						 */

						/**
						 * [tDBInput_10 process_data_begin ] start
						 */

						currentComponent = "tDBInput_10";

						/**
						 * [tDBInput_10 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row6 main ] start
						 */

						currentComponent = "tAdvancedHash_row6";

						// row6
						// row6

						if (execStat) {
							runStat.updateStatOnConnection("row6" + iterateId,
									1, 1);
						}

						row6Struct row6_HashRow = new row6Struct();

						row6_HashRow.codemodsemestre = row6.codemodsemestre;

						row6_HashRow.enseignantID = row6.enseignantID;

						row6_HashRow.heuresTP = row6.heuresTP;

						row6_HashRow.verrou = row6.verrou;

						row6_HashRow.verrouDPT = row6.verrouDPT;

						row6_HashRow.paye = row6.paye;

						tHash_Lookup_row6.put(row6_HashRow);

						tos_count_tAdvancedHash_row6++;

						/**
						 * [tAdvancedHash_row6 main ] stop
						 */

						/**
						 * [tAdvancedHash_row6 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row6";

						/**
						 * [tAdvancedHash_row6 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row6 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row6";

						/**
						 * [tAdvancedHash_row6 process_data_end ] stop
						 */

						/**
						 * [tDBInput_10 process_data_end ] start
						 */

						currentComponent = "tDBInput_10";

						/**
						 * [tDBInput_10 process_data_end ] stop
						 */

						/**
						 * [tDBInput_10 end ] start
						 */

						currentComponent = "tDBInput_10";

					}
				} finally {
					if (rs_tDBInput_10 != null) {
						rs_tDBInput_10.close();
					}
					stmt_tDBInput_10.close();
					if (conn_tDBInput_10 != null
							&& !conn_tDBInput_10.isClosed()) {

						conn_tDBInput_10.close();

					}

				}

				globalMap.put("tDBInput_10_NB_LINE", nb_line_tDBInput_10);

				ok_Hash.put("tDBInput_10", true);
				end_Hash.put("tDBInput_10", System.currentTimeMillis());

				/**
				 * [tDBInput_10 end ] stop
				 */

				/**
				 * [tAdvancedHash_row6 end ] start
				 */

				currentComponent = "tAdvancedHash_row6";

				tHash_Lookup_row6.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row6" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAdvancedHash_row6", true);
				end_Hash.put("tAdvancedHash_row6", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row6 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_10 finally ] start
				 */

				currentComponent = "tDBInput_10";

				/**
				 * [tDBInput_10 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row6 finally ] start
				 */

				currentComponent = "tAdvancedHash_row6";

				/**
				 * [tAdvancedHash_row6 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_10_SUBPROCESS_STATE", 1);
	}

	public static class row5Struct implements
			routines.system.IPersistableComparableLookupRow<row5Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public double heuresTD;

		public double getHeuresTD() {
			return this.heuresTD;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		public boolean paye;

		public boolean getPaye() {
			return this.paye;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row5Struct other = (row5Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(row5Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.heuresTD = this.heuresTD;
			other.verrou = this.verrou;
			other.verrouDPT = this.verrouDPT;
			other.paye = this.paye;

		}

		public void copyKeysDataTo(row5Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.heuresTD = dis.readDouble();

				this.verrou = dis.readBoolean();

				this.verrouDPT = dis.readBoolean();

				this.paye = dis.readBoolean();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				dos.writeDouble(this.heuresTD);

				dos.writeBoolean(this.verrou);

				dos.writeBoolean(this.verrouDPT);

				dos.writeBoolean(this.paye);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresTD=" + String.valueOf(heuresTD));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append(",paye=" + String.valueOf(paye));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row5Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_11Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_11_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row5Struct row5 = new row5Struct();

				/**
				 * [tAdvancedHash_row5 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row5", false);
				start_Hash
						.put("tAdvancedHash_row5", System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row5";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row5" + iterateId, 0, 0);

					}
				}

				int tos_count_tAdvancedHash_row5 = 0;

				class BytesLimit65535_tAdvancedHash_row5 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row5().limitLog4jByte();

				// connection name:row5
				// source node:tDBInput_11 - inputs:(after_tDBInput_9)
				// outputs:(row5,row5) | target node:tAdvancedHash_row5 -
				// inputs:(row5) outputs:()
				// linked node: tMap_5 - inputs:(row4,row6,row5)
				// outputs:(HEURES_FAITES)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row5 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row5Struct> tHash_Lookup_row5 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row5Struct> getLookup(matchingModeEnum_row5);

				globalMap.put("tHash_Lookup_row5", tHash_Lookup_row5);

				/**
				 * [tAdvancedHash_row5 begin ] stop
				 */

				/**
				 * [tDBInput_11 begin ] start
				 */

				ok_Hash.put("tDBInput_11", false);
				start_Hash.put("tDBInput_11", System.currentTimeMillis());

				currentComponent = "tDBInput_11";

				int tos_count_tDBInput_11 = 0;

				class BytesLimit65535_tDBInput_11 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_11().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_11 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_11.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_11 = calendar_tDBInput_11
						.getTime();
				int nb_line_tDBInput_11 = 0;
				java.sql.Connection conn_tDBInput_11 = null;
				String driverClass_tDBInput_11 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_11);
				String dbUser_tDBInput_11 = "root";

				final String decryptedPassword_tDBInput_11 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_11 = decryptedPassword_tDBInput_11;

				String url_tDBInput_11 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_11 = java.sql.DriverManager.getConnection(
						url_tDBInput_11, dbUser_tDBInput_11, dbPwd_tDBInput_11);

				java.sql.Statement stmt_tDBInput_11 = conn_tDBInput_11
						.createStatement();

				String dbquery_tDBInput_11 = "SELECT \n  `preserviceTD`.`codemodsemestre`, \n  `preserviceTD`.`enseignantID`, \n  `preserviceTD`.`heuresTD`, \n  `preserv"
						+ "iceTD`.`verrou`, \n  `preserviceTD`.`verrouDPT`, \n  `preserviceTD`.`paye`\nFROM `preserviceTD`";

				globalMap.put("tDBInput_11_QUERY", dbquery_tDBInput_11);
				java.sql.ResultSet rs_tDBInput_11 = null;

				try {
					rs_tDBInput_11 = stmt_tDBInput_11
							.executeQuery(dbquery_tDBInput_11);
					java.sql.ResultSetMetaData rsmd_tDBInput_11 = rs_tDBInput_11
							.getMetaData();
					int colQtyInRs_tDBInput_11 = rsmd_tDBInput_11
							.getColumnCount();

					String tmpContent_tDBInput_11 = null;

					while (rs_tDBInput_11.next()) {
						nb_line_tDBInput_11++;

						if (colQtyInRs_tDBInput_11 < 1) {
							row5.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_11.getObject(1) != null) {
								row5.codemodsemestre = rs_tDBInput_11.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_11 < 2) {
							row5.enseignantID = 0;
						} else {

							if (rs_tDBInput_11.getObject(2) != null) {
								row5.enseignantID = rs_tDBInput_11.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_11 < 3) {
							row5.heuresTD = 0;
						} else {

							if (rs_tDBInput_11.getObject(3) != null) {
								row5.heuresTD = rs_tDBInput_11.getDouble(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_11 < 4) {
							row5.verrou = false;
						} else {

							if (rs_tDBInput_11.getObject(4) != null) {
								row5.verrou = rs_tDBInput_11.getBoolean(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_11 < 5) {
							row5.verrouDPT = false;
						} else {

							if (rs_tDBInput_11.getObject(5) != null) {
								row5.verrouDPT = rs_tDBInput_11.getBoolean(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_11 < 6) {
							row5.paye = false;
						} else {

							if (rs_tDBInput_11.getObject(6) != null) {
								row5.paye = rs_tDBInput_11.getBoolean(6);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_11 begin ] stop
						 */

						/**
						 * [tDBInput_11 main ] start
						 */

						currentComponent = "tDBInput_11";

						tos_count_tDBInput_11++;

						/**
						 * [tDBInput_11 main ] stop
						 */

						/**
						 * [tDBInput_11 process_data_begin ] start
						 */

						currentComponent = "tDBInput_11";

						/**
						 * [tDBInput_11 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row5 main ] start
						 */

						currentComponent = "tAdvancedHash_row5";

						// row5
						// row5

						if (execStat) {
							runStat.updateStatOnConnection("row5" + iterateId,
									1, 1);
						}

						row5Struct row5_HashRow = new row5Struct();

						row5_HashRow.codemodsemestre = row5.codemodsemestre;

						row5_HashRow.enseignantID = row5.enseignantID;

						row5_HashRow.heuresTD = row5.heuresTD;

						row5_HashRow.verrou = row5.verrou;

						row5_HashRow.verrouDPT = row5.verrouDPT;

						row5_HashRow.paye = row5.paye;

						tHash_Lookup_row5.put(row5_HashRow);

						tos_count_tAdvancedHash_row5++;

						/**
						 * [tAdvancedHash_row5 main ] stop
						 */

						/**
						 * [tAdvancedHash_row5 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row5";

						/**
						 * [tAdvancedHash_row5 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row5 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row5";

						/**
						 * [tAdvancedHash_row5 process_data_end ] stop
						 */

						/**
						 * [tDBInput_11 process_data_end ] start
						 */

						currentComponent = "tDBInput_11";

						/**
						 * [tDBInput_11 process_data_end ] stop
						 */

						/**
						 * [tDBInput_11 end ] start
						 */

						currentComponent = "tDBInput_11";

					}
				} finally {
					if (rs_tDBInput_11 != null) {
						rs_tDBInput_11.close();
					}
					stmt_tDBInput_11.close();
					if (conn_tDBInput_11 != null
							&& !conn_tDBInput_11.isClosed()) {

						conn_tDBInput_11.close();

					}

				}

				globalMap.put("tDBInput_11_NB_LINE", nb_line_tDBInput_11);

				ok_Hash.put("tDBInput_11", true);
				end_Hash.put("tDBInput_11", System.currentTimeMillis());

				/**
				 * [tDBInput_11 end ] stop
				 */

				/**
				 * [tAdvancedHash_row5 end ] start
				 */

				currentComponent = "tAdvancedHash_row5";

				tHash_Lookup_row5.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row5" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("tAdvancedHash_row5", true);
				end_Hash.put("tAdvancedHash_row5", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row5 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_11 finally ] start
				 */

				currentComponent = "tDBInput_11";

				/**
				 * [tDBInput_11 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row5 finally ] start
				 */

				currentComponent = "tAdvancedHash_row5";

				/**
				 * [tAdvancedHash_row5 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_11_SUBPROCESS_STATE", 1);
	}

	public static class row15Struct implements
			routines.system.IPersistableComparableLookupRow<row15Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		public String nom;

		public String getNom() {
			return this.nom;
		}

		public int responsable;

		public int getResponsable() {
			return this.responsable;
		}

		public int departement;

		public int getDepartement() {
			return this.departement;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codesemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row15Struct other = (row15Struct) obj;

			if (this.codesemestre != other.codesemestre)
				return false;

			return true;
		}

		public void copyDataTo(row15Struct other) {

			other.codesemestre = this.codesemestre;
			other.anneedebut = this.anneedebut;
			other.nom = this.nom;
			other.responsable = this.responsable;
			other.departement = this.departement;

		}

		public void copyKeysDataTo(row15Struct other) {

			other.codesemestre = this.codesemestre;

		}

		private String readString(DataInputStream dis, ObjectInputStream ois)
				throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				byte[] byteArray = new byte[length];
				dis.read(byteArray);
				strReturn = new String(byteArray, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, DataOutputStream dos,
				ObjectOutputStream oos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codesemestre = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codesemestre);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.anneedebut = dis.readInt();

				this.nom = readString(dis, ois);

				this.responsable = dis.readInt();

				this.departement = dis.readInt();

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				dos.writeInt(this.anneedebut);

				writeString(this.nom, dos, oos);

				dos.writeInt(this.responsable);

				dos.writeInt(this.departement);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codesemestre=" + String.valueOf(codesemestre));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append(",nom=" + nom);
			sb.append(",responsable=" + String.valueOf(responsable));
			sb.append(",departement=" + String.valueOf(departement));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row15Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codesemestre,
					other.codesemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_12Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_12_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row15Struct row15 = new row15Struct();

				/**
				 * [tAdvancedHash_row15 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row15", false);
				start_Hash.put("tAdvancedHash_row15",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row15";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row15" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row15 = 0;

				class BytesLimit65535_tAdvancedHash_row15 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row15().limitLog4jByte();

				// connection name:row15
				// source node:tDBInput_12 - inputs:(after_tDBInput_8)
				// outputs:(row15,row15) | target node:tAdvancedHash_row15 -
				// inputs:(row15) outputs:()
				// linked node: tMap_3 - inputs:(row13,row15,row14)
				// outputs:(MODULE,ANNE_MODULE)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row15 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row15Struct> tHash_Lookup_row15 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row15Struct> getLookup(matchingModeEnum_row15);

				globalMap.put("tHash_Lookup_row15", tHash_Lookup_row15);

				/**
				 * [tAdvancedHash_row15 begin ] stop
				 */

				/**
				 * [tDBInput_12 begin ] start
				 */

				ok_Hash.put("tDBInput_12", false);
				start_Hash.put("tDBInput_12", System.currentTimeMillis());

				currentComponent = "tDBInput_12";

				int tos_count_tDBInput_12 = 0;

				class BytesLimit65535_tDBInput_12 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_12().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_12 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_12.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_12 = calendar_tDBInput_12
						.getTime();
				int nb_line_tDBInput_12 = 0;
				java.sql.Connection conn_tDBInput_12 = null;
				String driverClass_tDBInput_12 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_12);
				String dbUser_tDBInput_12 = "root";

				final String decryptedPassword_tDBInput_12 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_12 = decryptedPassword_tDBInput_12;

				String url_tDBInput_12 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_12 = java.sql.DriverManager.getConnection(
						url_tDBInput_12, dbUser_tDBInput_12, dbPwd_tDBInput_12);

				java.sql.Statement stmt_tDBInput_12 = conn_tDBInput_12
						.createStatement();

				String dbquery_tDBInput_12 = "SELECT \n  `semestres`.`codesemestre`, \n  `semestres`.`anneedebut`, \n  `semestres`.`nom`, \n  `semestres`.`responsable`, "
						+ "\n  `semestres`.`departement`\nFROM `semestres`";

				globalMap.put("tDBInput_12_QUERY", dbquery_tDBInput_12);
				java.sql.ResultSet rs_tDBInput_12 = null;

				try {
					rs_tDBInput_12 = stmt_tDBInput_12
							.executeQuery(dbquery_tDBInput_12);
					java.sql.ResultSetMetaData rsmd_tDBInput_12 = rs_tDBInput_12
							.getMetaData();
					int colQtyInRs_tDBInput_12 = rsmd_tDBInput_12
							.getColumnCount();

					String tmpContent_tDBInput_12 = null;

					while (rs_tDBInput_12.next()) {
						nb_line_tDBInput_12++;

						if (colQtyInRs_tDBInput_12 < 1) {
							row15.codesemestre = 0;
						} else {

							if (rs_tDBInput_12.getObject(1) != null) {
								row15.codesemestre = rs_tDBInput_12.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_12 < 2) {
							row15.anneedebut = 0;
						} else {

							if (rs_tDBInput_12.getObject(2) != null) {
								row15.anneedebut = rs_tDBInput_12.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_12 < 3) {
							row15.nom = null;
						} else {

							row15.nom = routines.system.JDBCUtil.getString(
									rs_tDBInput_12, 3, false);
						}
						if (colQtyInRs_tDBInput_12 < 4) {
							row15.responsable = 0;
						} else {

							if (rs_tDBInput_12.getObject(4) != null) {
								row15.responsable = rs_tDBInput_12.getInt(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_12 < 5) {
							row15.departement = 0;
						} else {

							if (rs_tDBInput_12.getObject(5) != null) {
								row15.departement = rs_tDBInput_12.getInt(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_12 begin ] stop
						 */

						/**
						 * [tDBInput_12 main ] start
						 */

						currentComponent = "tDBInput_12";

						tos_count_tDBInput_12++;

						/**
						 * [tDBInput_12 main ] stop
						 */

						/**
						 * [tDBInput_12 process_data_begin ] start
						 */

						currentComponent = "tDBInput_12";

						/**
						 * [tDBInput_12 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row15 main ] start
						 */

						currentComponent = "tAdvancedHash_row15";

						// row15
						// row15

						if (execStat) {
							runStat.updateStatOnConnection("row15" + iterateId,
									1, 1);
						}

						row15Struct row15_HashRow = new row15Struct();

						row15_HashRow.codesemestre = row15.codesemestre;

						row15_HashRow.anneedebut = row15.anneedebut;

						row15_HashRow.nom = row15.nom;

						row15_HashRow.responsable = row15.responsable;

						row15_HashRow.departement = row15.departement;

						tHash_Lookup_row15.put(row15_HashRow);

						tos_count_tAdvancedHash_row15++;

						/**
						 * [tAdvancedHash_row15 main ] stop
						 */

						/**
						 * [tAdvancedHash_row15 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row15";

						/**
						 * [tAdvancedHash_row15 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row15 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row15";

						/**
						 * [tAdvancedHash_row15 process_data_end ] stop
						 */

						/**
						 * [tDBInput_12 process_data_end ] start
						 */

						currentComponent = "tDBInput_12";

						/**
						 * [tDBInput_12 process_data_end ] stop
						 */

						/**
						 * [tDBInput_12 end ] start
						 */

						currentComponent = "tDBInput_12";

					}
				} finally {
					if (rs_tDBInput_12 != null) {
						rs_tDBInput_12.close();
					}
					stmt_tDBInput_12.close();
					if (conn_tDBInput_12 != null
							&& !conn_tDBInput_12.isClosed()) {

						conn_tDBInput_12.close();

					}

				}

				globalMap.put("tDBInput_12_NB_LINE", nb_line_tDBInput_12);

				ok_Hash.put("tDBInput_12", true);
				end_Hash.put("tDBInput_12", System.currentTimeMillis());

				/**
				 * [tDBInput_12 end ] stop
				 */

				/**
				 * [tAdvancedHash_row15 end ] start
				 */

				currentComponent = "tAdvancedHash_row15";

				tHash_Lookup_row15.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row15" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row15", true);
				end_Hash.put("tAdvancedHash_row15", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row15 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_12 finally ] start
				 */

				currentComponent = "tDBInput_12";

				/**
				 * [tDBInput_12 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row15 finally ] start
				 */

				currentComponent = "tAdvancedHash_row15";

				/**
				 * [tAdvancedHash_row15 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_12_SUBPROCESS_STATE", 1);
	}

	public static class row14Struct implements
			routines.system.IPersistableComparableLookupRow<row14Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemod;

		public int getCodemod() {
			return this.codemod;
		}

		public String codeprefixe;

		public String getCodeprefixe() {
			return this.codeprefixe;
		}

		public String codesuffixe;

		public String getCodesuffixe() {
			return this.codesuffixe;
		}

		public String intitule;

		public String getIntitule() {
			return this.intitule;
		}

		public String informations;

		public String getInformations() {
			return this.informations;
		}

		public int responsable;

		public int getResponsable() {
			return this.responsable;
		}

		public String theme;

		public String getTheme() {
			return this.theme;
		}

		public Integer ects;

		public Integer getEcts() {
			return this.ects;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemod;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row14Struct other = (row14Struct) obj;

			if (this.codemod != other.codemod)
				return false;

			return true;
		}

		public void copyDataTo(row14Struct other) {

			other.codemod = this.codemod;
			other.codeprefixe = this.codeprefixe;
			other.codesuffixe = this.codesuffixe;
			other.intitule = this.intitule;
			other.informations = this.informations;
			other.responsable = this.responsable;
			other.theme = this.theme;
			other.ects = this.ects;

		}

		public void copyKeysDataTo(row14Struct other) {

			other.codemod = this.codemod;

		}

		private String readString(DataInputStream dis, ObjectInputStream ois)
				throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				byte[] byteArray = new byte[length];
				dis.read(byteArray);
				strReturn = new String(byteArray, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, DataOutputStream dos,
				ObjectOutputStream oos) throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private Integer readInteger(DataInputStream dis, ObjectInputStream ois)
				throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, DataOutputStream dos,
				ObjectOutputStream oos) throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemod = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemod);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.codeprefixe = readString(dis, ois);

				this.codesuffixe = readString(dis, ois);

				this.intitule = readString(dis, ois);

				this.informations = readString(dis, ois);

				this.responsable = dis.readInt();

				this.theme = readString(dis, ois);

				this.ects = readInteger(dis, ois);

			} catch (IOException e) {
				throw new RuntimeException(e);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				writeString(this.codeprefixe, dos, oos);

				writeString(this.codesuffixe, dos, oos);

				writeString(this.intitule, dos, oos);

				writeString(this.informations, dos, oos);

				dos.writeInt(this.responsable);

				writeString(this.theme, dos, oos);

				writeInteger(this.ects, dos, oos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemod=" + String.valueOf(codemod));
			sb.append(",codeprefixe=" + codeprefixe);
			sb.append(",codesuffixe=" + codesuffixe);
			sb.append(",intitule=" + intitule);
			sb.append(",informations=" + informations);
			sb.append(",responsable=" + String.valueOf(responsable));
			sb.append(",theme=" + theme);
			sb.append(",ects=" + String.valueOf(ects));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row14Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemod, other.codemod);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_13Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_13_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row14Struct row14 = new row14Struct();

				/**
				 * [tAdvancedHash_row14 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row14", false);
				start_Hash.put("tAdvancedHash_row14",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row14";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row14" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row14 = 0;

				class BytesLimit65535_tAdvancedHash_row14 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row14().limitLog4jByte();

				// connection name:row14
				// source node:tDBInput_13 - inputs:() outputs:(row14,row14) |
				// target node:tAdvancedHash_row14 - inputs:(row14) outputs:()
				// linked node: tMap_3 - inputs:(row13,row15,row14)
				// outputs:(MODULE,ANNE_MODULE)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row14 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row14Struct> tHash_Lookup_row14 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row14Struct> getLookup(matchingModeEnum_row14);

				globalMap.put("tHash_Lookup_row14", tHash_Lookup_row14);

				/**
				 * [tAdvancedHash_row14 begin ] stop
				 */

				/**
				 * [tDBInput_13 begin ] start
				 */

				ok_Hash.put("tDBInput_13", false);
				start_Hash.put("tDBInput_13", System.currentTimeMillis());

				currentComponent = "tDBInput_13";

				int tos_count_tDBInput_13 = 0;

				class BytesLimit65535_tDBInput_13 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_13().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_13 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_13.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_13 = calendar_tDBInput_13
						.getTime();
				int nb_line_tDBInput_13 = 0;
				java.sql.Connection conn_tDBInput_13 = null;
				String driverClass_tDBInput_13 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_13);
				String dbUser_tDBInput_13 = "root";

				final String decryptedPassword_tDBInput_13 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_13 = decryptedPassword_tDBInput_13;

				String url_tDBInput_13 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_13 = java.sql.DriverManager.getConnection(
						url_tDBInput_13, dbUser_tDBInput_13, dbPwd_tDBInput_13);

				java.sql.Statement stmt_tDBInput_13 = conn_tDBInput_13
						.createStatement();

				String dbquery_tDBInput_13 = "SELECT \n  `modules`.`codemod`, \n  `modules`.`codeprefixe`, \n  `modules`.`codesuffixe`, \n  `modules`.`intitule`, \n  `mod"
						+ "ules`.`informations`, \n  `modules`.`responsable`, \n  `modules`.`theme`, \n  `modules`.`ects`\nFROM `modules`";

				globalMap.put("tDBInput_13_QUERY", dbquery_tDBInput_13);
				java.sql.ResultSet rs_tDBInput_13 = null;

				try {
					rs_tDBInput_13 = stmt_tDBInput_13
							.executeQuery(dbquery_tDBInput_13);
					java.sql.ResultSetMetaData rsmd_tDBInput_13 = rs_tDBInput_13
							.getMetaData();
					int colQtyInRs_tDBInput_13 = rsmd_tDBInput_13
							.getColumnCount();

					String tmpContent_tDBInput_13 = null;

					while (rs_tDBInput_13.next()) {
						nb_line_tDBInput_13++;

						if (colQtyInRs_tDBInput_13 < 1) {
							row14.codemod = 0;
						} else {

							if (rs_tDBInput_13.getObject(1) != null) {
								row14.codemod = rs_tDBInput_13.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_13 < 2) {
							row14.codeprefixe = null;
						} else {

							row14.codeprefixe = routines.system.JDBCUtil
									.getString(rs_tDBInput_13, 2, false);
						}
						if (colQtyInRs_tDBInput_13 < 3) {
							row14.codesuffixe = null;
						} else {

							row14.codesuffixe = routines.system.JDBCUtil
									.getString(rs_tDBInput_13, 3, false);
						}
						if (colQtyInRs_tDBInput_13 < 4) {
							row14.intitule = null;
						} else {

							row14.intitule = routines.system.JDBCUtil
									.getString(rs_tDBInput_13, 4, false);
						}
						if (colQtyInRs_tDBInput_13 < 5) {
							row14.informations = null;
						} else {

							row14.informations = routines.system.JDBCUtil
									.getString(rs_tDBInput_13, 5, false);
						}
						if (colQtyInRs_tDBInput_13 < 6) {
							row14.responsable = 0;
						} else {

							if (rs_tDBInput_13.getObject(6) != null) {
								row14.responsable = rs_tDBInput_13.getInt(6);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_13 < 7) {
							row14.theme = null;
						} else {

							row14.theme = routines.system.JDBCUtil.getString(
									rs_tDBInput_13, 7, false);
						}
						if (colQtyInRs_tDBInput_13 < 8) {
							row14.ects = null;
						} else {

							if (rs_tDBInput_13.getObject(8) != null) {
								row14.ects = rs_tDBInput_13.getInt(8);
							} else {
								row14.ects = null;
							}
						}

						/**
						 * [tDBInput_13 begin ] stop
						 */

						/**
						 * [tDBInput_13 main ] start
						 */

						currentComponent = "tDBInput_13";

						tos_count_tDBInput_13++;

						/**
						 * [tDBInput_13 main ] stop
						 */

						/**
						 * [tDBInput_13 process_data_begin ] start
						 */

						currentComponent = "tDBInput_13";

						/**
						 * [tDBInput_13 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row14 main ] start
						 */

						currentComponent = "tAdvancedHash_row14";

						// row14
						// row14

						if (execStat) {
							runStat.updateStatOnConnection("row14" + iterateId,
									1, 1);
						}

						row14Struct row14_HashRow = new row14Struct();

						row14_HashRow.codemod = row14.codemod;

						row14_HashRow.codeprefixe = row14.codeprefixe;

						row14_HashRow.codesuffixe = row14.codesuffixe;

						row14_HashRow.intitule = row14.intitule;

						row14_HashRow.informations = row14.informations;

						row14_HashRow.responsable = row14.responsable;

						row14_HashRow.theme = row14.theme;

						row14_HashRow.ects = row14.ects;

						tHash_Lookup_row14.put(row14_HashRow);

						tos_count_tAdvancedHash_row14++;

						/**
						 * [tAdvancedHash_row14 main ] stop
						 */

						/**
						 * [tAdvancedHash_row14 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row14";

						/**
						 * [tAdvancedHash_row14 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row14 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row14";

						/**
						 * [tAdvancedHash_row14 process_data_end ] stop
						 */

						/**
						 * [tDBInput_13 process_data_end ] start
						 */

						currentComponent = "tDBInput_13";

						/**
						 * [tDBInput_13 process_data_end ] stop
						 */

						/**
						 * [tDBInput_13 end ] start
						 */

						currentComponent = "tDBInput_13";

					}
				} finally {
					if (rs_tDBInput_13 != null) {
						rs_tDBInput_13.close();
					}
					stmt_tDBInput_13.close();
					if (conn_tDBInput_13 != null
							&& !conn_tDBInput_13.isClosed()) {

						conn_tDBInput_13.close();

					}

				}

				globalMap.put("tDBInput_13_NB_LINE", nb_line_tDBInput_13);

				ok_Hash.put("tDBInput_13", true);
				end_Hash.put("tDBInput_13", System.currentTimeMillis());

				/**
				 * [tDBInput_13 end ] stop
				 */

				/**
				 * [tAdvancedHash_row14 end ] start
				 */

				currentComponent = "tAdvancedHash_row14";

				tHash_Lookup_row14.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row14" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row14", true);
				end_Hash.put("tAdvancedHash_row14", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row14 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_13 finally ] start
				 */

				currentComponent = "tDBInput_13";

				/**
				 * [tDBInput_13 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row14 finally ] start
				 */

				currentComponent = "tAdvancedHash_row14";

				/**
				 * [tAdvancedHash_row14 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_13_SUBPROCESS_STATE", 1);
	}

	public static class joint_ens_cmStruct implements
			routines.system.IPersistableRow<joint_ens_cmStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public BigDecimal heuresCM;

		public BigDecimal getHeuresCM() {
			return this.heuresCM;
		}

		public BigDecimal heuresTD;

		public BigDecimal getHeuresTD() {
			return this.heuresTD;
		}

		public BigDecimal heuresTP;

		public BigDecimal getHeuresTP() {
			return this.heuresTP;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final joint_ens_cmStruct other = (joint_ens_cmStruct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(joint_ens_cmStruct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.heuresCM = this.heuresCM;
			other.heuresTD = this.heuresTD;
			other.heuresTP = this.heuresTP;

		}

		public void copyKeysDataTo(joint_ens_cmStruct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

					this.heuresCM = (BigDecimal) dis.readObject();

					this.heuresTD = (BigDecimal) dis.readObject();

					this.heuresTP = (BigDecimal) dis.readObject();

				} catch (IOException e) {
					throw new RuntimeException(e);

				} catch (ClassNotFoundException eCNFE) {
					throw new RuntimeException(eCNFE);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

				// BigDecimal

				dos.writeObject(this.heuresCM);

				// BigDecimal

				dos.writeObject(this.heuresTD);

				// BigDecimal

				dos.writeObject(this.heuresTP);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresCM=" + String.valueOf(heuresCM));
			sb.append(",heuresTD=" + String.valueOf(heuresTD));
			sb.append(",heuresTP=" + String.valueOf(heuresTP));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(joint_ens_cmStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class TEST_JOINTURE_SORTIEStruct implements
			routines.system.IPersistableRow<TEST_JOINTURE_SORTIEStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public BigDecimal heuresCM;

		public BigDecimal getHeuresCM() {
			return this.heuresCM;
		}

		public BigDecimal heuresTD;

		public BigDecimal getHeuresTD() {
			return this.heuresTD;
		}

		public BigDecimal heuresTP;

		public BigDecimal getHeuresTP() {
			return this.heuresTP;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final TEST_JOINTURE_SORTIEStruct other = (TEST_JOINTURE_SORTIEStruct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(TEST_JOINTURE_SORTIEStruct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.heuresCM = this.heuresCM;
			other.heuresTD = this.heuresTD;
			other.heuresTP = this.heuresTP;

		}

		public void copyKeysDataTo(TEST_JOINTURE_SORTIEStruct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

					this.heuresCM = (BigDecimal) dis.readObject();

					this.heuresTD = (BigDecimal) dis.readObject();

					this.heuresTP = (BigDecimal) dis.readObject();

				} catch (IOException e) {
					throw new RuntimeException(e);

				} catch (ClassNotFoundException eCNFE) {
					throw new RuntimeException(eCNFE);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

				// BigDecimal

				dos.writeObject(this.heuresCM);

				// BigDecimal

				dos.writeObject(this.heuresTD);

				// BigDecimal

				dos.writeObject(this.heuresTP);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresCM=" + String.valueOf(heuresCM));
			sb.append(",heuresTD=" + String.valueOf(heuresTD));
			sb.append(",heuresTP=" + String.valueOf(heuresTP));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(TEST_JOINTURE_SORTIEStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row17Struct implements
			routines.system.IPersistableRow<row17Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public BigDecimal heuresCM;

		public BigDecimal getHeuresCM() {
			return this.heuresCM;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		public boolean paye;

		public boolean getPaye() {
			return this.paye;
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

					this.heuresCM = (BigDecimal) dis.readObject();

					this.verrou = dis.readBoolean();

					this.verrouDPT = dis.readBoolean();

					this.paye = dis.readBoolean();

				} catch (IOException e) {
					throw new RuntimeException(e);

				} catch (ClassNotFoundException eCNFE) {
					throw new RuntimeException(eCNFE);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

				// BigDecimal

				dos.writeObject(this.heuresCM);

				// boolean

				dos.writeBoolean(this.verrou);

				// boolean

				dos.writeBoolean(this.verrouDPT);

				// boolean

				dos.writeBoolean(this.paye);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresCM=" + String.valueOf(heuresCM));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append(",paye=" + String.valueOf(paye));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row17Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_14Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_14_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row17Struct row17 = new row17Struct();
				joint_ens_cmStruct joint_ens_cm = new joint_ens_cmStruct();
				TEST_JOINTURE_SORTIEStruct TEST_JOINTURE_SORTIE = new TEST_JOINTURE_SORTIEStruct();

				/**
				 * [tDBOutput_4 begin ] start
				 */

				ok_Hash.put("tDBOutput_4", false);
				start_Hash.put("tDBOutput_4", System.currentTimeMillis());

				currentComponent = "tDBOutput_4";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("joint_ens_cm"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tDBOutput_4 = 0;

				class BytesLimit65535_tDBOutput_4 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBOutput_4().limitLog4jByte();

				int nb_line_tDBOutput_4 = 0;
				int nb_line_update_tDBOutput_4 = 0;
				int nb_line_inserted_tDBOutput_4 = 0;
				int nb_line_deleted_tDBOutput_4 = 0;
				int nb_line_rejected_tDBOutput_4 = 0;

				int deletedCount_tDBOutput_4 = 0;
				int updatedCount_tDBOutput_4 = 0;
				int insertedCount_tDBOutput_4 = 0;

				int rejectedCount_tDBOutput_4 = 0;

				String tableName_tDBOutput_4 = "DIM_MODULE_DETAILS";
				boolean whetherReject_tDBOutput_4 = false;

				java.util.Calendar calendar_tDBOutput_4 = java.util.Calendar
						.getInstance();
				calendar_tDBOutput_4.set(1, 0, 1, 0, 0, 0);
				long year1_tDBOutput_4 = calendar_tDBOutput_4.getTime()
						.getTime();
				calendar_tDBOutput_4.set(10000, 0, 1, 0, 0, 0);
				long year10000_tDBOutput_4 = calendar_tDBOutput_4.getTime()
						.getTime();
				long date_tDBOutput_4;

				java.sql.Connection conn_tDBOutput_4 = null;
				String dbProperties_tDBOutput_4 = "noDatetimeStringSync=true";
				String url_tDBOutput_4 = null;
				if (dbProperties_tDBOutput_4 == null
						|| dbProperties_tDBOutput_4.trim().length() == 0) {
					url_tDBOutput_4 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ "rewriteBatchedStatements=true";
				} else {
					String properties_tDBOutput_4 = "noDatetimeStringSync=true";
					if (!properties_tDBOutput_4
							.contains("rewriteBatchedStatements")) {
						properties_tDBOutput_4 += "&rewriteBatchedStatements=true";
					}

					url_tDBOutput_4 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ properties_tDBOutput_4;
				}
				String driverClass_tDBOutput_4 = "org.gjt.mm.mysql.Driver";

				String dbUser_tDBOutput_4 = "root";

				final String decryptedPassword_tDBOutput_4 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBOutput_4 = decryptedPassword_tDBOutput_4;
				java.lang.Class.forName(driverClass_tDBOutput_4);

				conn_tDBOutput_4 = java.sql.DriverManager.getConnection(
						url_tDBOutput_4, dbUser_tDBOutput_4, dbPwd_tDBOutput_4);

				resourceMap.put("conn_tDBOutput_4", conn_tDBOutput_4);
				conn_tDBOutput_4.setAutoCommit(false);
				int commitEvery_tDBOutput_4 = 10000;
				int commitCounter_tDBOutput_4 = 0;

				int count_tDBOutput_4 = 0;

				java.sql.Statement stmtDrop_tDBOutput_4 = conn_tDBOutput_4
						.createStatement();
				stmtDrop_tDBOutput_4.execute("DROP TABLE `"
						+ tableName_tDBOutput_4 + "`");
				stmtDrop_tDBOutput_4.close();
				java.sql.Statement stmtCreate_tDBOutput_4 = conn_tDBOutput_4
						.createStatement();
				stmtCreate_tDBOutput_4
						.execute("CREATE TABLE `"
								+ tableName_tDBOutput_4
								+ "`(`codemodsemestre` INT(10)  default 0  not null ,`enseignantID` INT(10)  default 0  not null ,`heuresCM` DECIMAL(11,2)  default 0.00  not null ,`heuresTD` DECIMAL(11,2)  default 0.00  not null ,`heuresTP` DECIMAL(11,2)  default 0.00  not null ,primary key(`codemodsemestre`,`enseignantID`))");
				stmtCreate_tDBOutput_4.close();

				String insert_tDBOutput_4 = "INSERT INTO `"
						+ "DIM_MODULE_DETAILS"
						+ "` (`codemodsemestre`,`enseignantID`,`heuresCM`,`heuresTD`,`heuresTP`) VALUES (?,?,?,?,?)";
				int batchSize_tDBOutput_4 = 100;
				int batchSizeCounter_tDBOutput_4 = 0;

				java.sql.PreparedStatement pstmt_tDBOutput_4 = conn_tDBOutput_4
						.prepareStatement(insert_tDBOutput_4);

				/**
				 * [tDBOutput_4 begin ] stop
				 */

				/**
				 * [tFileOutputExcel_4 begin ] start
				 */

				ok_Hash.put("tFileOutputExcel_4", false);
				start_Hash
						.put("tFileOutputExcel_4", System.currentTimeMillis());

				currentComponent = "tFileOutputExcel_4";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("TEST_JOINTURE_SORTIE"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tFileOutputExcel_4 = 0;

				class BytesLimit65535_tFileOutputExcel_4 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tFileOutputExcel_4().limitLog4jByte();

				int columnIndex_tFileOutputExcel_4 = 0;

				String fileName_tFileOutputExcel_4 = "/home/cbma/Data/TOS_DI-20180411_1414-V7.0.1/workspace/DIM_MODULE_DETAILS.xls";
				int nb_line_tFileOutputExcel_4 = 0;
				org.talend.ExcelTool xlsxTool_tFileOutputExcel_4 = new org.talend.ExcelTool();
				xlsxTool_tFileOutputExcel_4.setSheet("Sheet1");
				xlsxTool_tFileOutputExcel_4.setAppend(false, false);
				xlsxTool_tFileOutputExcel_4.setRecalculateFormula(false);
				xlsxTool_tFileOutputExcel_4.setXY(false, 0, 0, false);

				xlsxTool_tFileOutputExcel_4
						.prepareXlsxFile(fileName_tFileOutputExcel_4);

				xlsxTool_tFileOutputExcel_4.setFont("");

				if (xlsxTool_tFileOutputExcel_4.getStartRow() == 0) {

					xlsxTool_tFileOutputExcel_4.addRow();

					xlsxTool_tFileOutputExcel_4.addCellValue("codemodsemestre");

					xlsxTool_tFileOutputExcel_4.addCellValue("enseignantID");

					xlsxTool_tFileOutputExcel_4.addCellValue("heuresCM");

					xlsxTool_tFileOutputExcel_4.addCellValue("heuresTD");

					xlsxTool_tFileOutputExcel_4.addCellValue("heuresTP");

					nb_line_tFileOutputExcel_4++;

				}

				/**
				 * [tFileOutputExcel_4 begin ] stop
				 */

				/**
				 * [tMap_6 begin ] start
				 */

				ok_Hash.put("tMap_6", false);
				start_Hash.put("tMap_6", System.currentTimeMillis());

				currentComponent = "tMap_6";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row17" + iterateId, 0,
								0);

					}
				}

				int tos_count_tMap_6 = 0;

				class BytesLimit65535_tMap_6 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tMap_6().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row19Struct> tHash_Lookup_row19 = null;

				row19Struct row19HashKey = new row19Struct();
				row19Struct row19Default = new row19Struct();

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row18Struct> tHash_Lookup_row18 = null;

				row18Struct row18HashKey = new row18Struct();
				row18Struct row18Default = new row18Struct();
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_6__Struct {
				}
				Var__tMap_6__Struct Var__tMap_6 = new Var__tMap_6__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				joint_ens_cmStruct joint_ens_cm_tmp = new joint_ens_cmStruct();
				TEST_JOINTURE_SORTIEStruct TEST_JOINTURE_SORTIE_tmp = new TEST_JOINTURE_SORTIEStruct();
				// ###############################

				/**
				 * [tMap_6 begin ] stop
				 */

				/**
				 * [tDBInput_14 begin ] start
				 */

				ok_Hash.put("tDBInput_14", false);
				start_Hash.put("tDBInput_14", System.currentTimeMillis());

				currentComponent = "tDBInput_14";

				int tos_count_tDBInput_14 = 0;

				class BytesLimit65535_tDBInput_14 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_14().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_14 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_14.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_14 = calendar_tDBInput_14
						.getTime();
				int nb_line_tDBInput_14 = 0;
				java.sql.Connection conn_tDBInput_14 = null;
				String driverClass_tDBInput_14 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_14);
				String dbUser_tDBInput_14 = "root";

				final String decryptedPassword_tDBInput_14 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_14 = decryptedPassword_tDBInput_14;

				String url_tDBInput_14 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_14 = java.sql.DriverManager.getConnection(
						url_tDBInput_14, dbUser_tDBInput_14, dbPwd_tDBInput_14);

				java.sql.Statement stmt_tDBInput_14 = conn_tDBInput_14
						.createStatement();

				String dbquery_tDBInput_14 = "SELECT \n  `preserviceCM`.`codemodsemestre`, \n  `preserviceCM`.`enseignantID`, \n  `preserviceCM`.`heuresCM`, \n  `preserv"
						+ "iceCM`.`verrou`, \n  `preserviceCM`.`verrouDPT`, \n  `preserviceCM`.`paye`\nFROM `preserviceCM`";

				globalMap.put("tDBInput_14_QUERY", dbquery_tDBInput_14);
				java.sql.ResultSet rs_tDBInput_14 = null;

				try {
					rs_tDBInput_14 = stmt_tDBInput_14
							.executeQuery(dbquery_tDBInput_14);
					java.sql.ResultSetMetaData rsmd_tDBInput_14 = rs_tDBInput_14
							.getMetaData();
					int colQtyInRs_tDBInput_14 = rsmd_tDBInput_14
							.getColumnCount();

					String tmpContent_tDBInput_14 = null;

					while (rs_tDBInput_14.next()) {
						nb_line_tDBInput_14++;

						if (colQtyInRs_tDBInput_14 < 1) {
							row17.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_14.getObject(1) != null) {
								row17.codemodsemestre = rs_tDBInput_14
										.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_14 < 2) {
							row17.enseignantID = 0;
						} else {

							if (rs_tDBInput_14.getObject(2) != null) {
								row17.enseignantID = rs_tDBInput_14.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_14 < 3) {
							row17.heuresCM = null;
						} else {

							if (rs_tDBInput_14.getObject(3) != null) {
								row17.heuresCM = rs_tDBInput_14
										.getBigDecimal(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_14 < 4) {
							row17.verrou = false;
						} else {

							if (rs_tDBInput_14.getObject(4) != null) {
								row17.verrou = rs_tDBInput_14.getBoolean(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_14 < 5) {
							row17.verrouDPT = false;
						} else {

							if (rs_tDBInput_14.getObject(5) != null) {
								row17.verrouDPT = rs_tDBInput_14.getBoolean(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_14 < 6) {
							row17.paye = false;
						} else {

							if (rs_tDBInput_14.getObject(6) != null) {
								row17.paye = rs_tDBInput_14.getBoolean(6);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_14 begin ] stop
						 */

						/**
						 * [tDBInput_14 main ] start
						 */

						currentComponent = "tDBInput_14";

						tos_count_tDBInput_14++;

						/**
						 * [tDBInput_14 main ] stop
						 */

						/**
						 * [tDBInput_14 process_data_begin ] start
						 */

						currentComponent = "tDBInput_14";

						/**
						 * [tDBInput_14 process_data_begin ] stop
						 */

						/**
						 * [tMap_6 main ] start
						 */

						currentComponent = "tMap_6";

						// row17
						// row17

						if (execStat) {
							runStat.updateStatOnConnection("row17" + iterateId,
									1, 1);
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_6 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_6 = false;
						boolean mainRowRejected_tMap_6 = false;

						// /////////////////////////////////////////////
						// Starting Lookup Table "row19"
						// /////////////////////////////////////////////

						boolean forceLooprow19 = false;

						row19Struct row19ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_6) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_6 = false;

							Object exprKeyValue_row19__codemodsemestre = row17.codemodsemestre;
							if (exprKeyValue_row19__codemodsemestre == null) {
								hasCasePrimitiveKeyWithNull_tMap_6 = true;
							} else {
								row19HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row19__codemodsemestre;
							}

							Object exprKeyValue_row19__enseignantID = row17.enseignantID;
							if (exprKeyValue_row19__enseignantID == null) {
								hasCasePrimitiveKeyWithNull_tMap_6 = true;
							} else {
								row19HashKey.enseignantID = (int) (Integer) exprKeyValue_row19__enseignantID;
							}

							row19HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_6) { // G_TM_M_091

								tDBInput_16Process(globalMap);

								tHash_Lookup_row19 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row19Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row19Struct>) globalMap
										.get("tHash_Lookup_row19"));

								tHash_Lookup_row19.initGet();

								tHash_Lookup_row19.lookup(row19HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_6
									|| !tHash_Lookup_row19.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_6 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row19 != null
								&& tHash_Lookup_row19.getCount(row19HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row19' and it contains more one result from keys :  row19.codemodsemestre = '"
							// + row19HashKey.codemodsemestre +
							// "', row19.enseignantID = '" +
							// row19HashKey.enseignantID + "'");
						} // G 071

						row19Struct row19 = null;

						row19Struct fromLookup_row19 = null;
						row19 = row19Default;

						if (tHash_Lookup_row19 != null
								&& tHash_Lookup_row19.hasNext()) { // G 099

							fromLookup_row19 = tHash_Lookup_row19.next();

						} // G 099

						if (fromLookup_row19 != null) {
							row19 = fromLookup_row19;
						}

						// /////////////////////////////////////////////
						// Starting Lookup Table "row18"
						// /////////////////////////////////////////////

						boolean forceLooprow18 = false;

						row18Struct row18ObjectFromLookup = null;

						if (!rejectedInnerJoin_tMap_6) { // G_TM_M_020

							hasCasePrimitiveKeyWithNull_tMap_6 = false;

							Object exprKeyValue_row18__codemodsemestre = row17.codemodsemestre;
							if (exprKeyValue_row18__codemodsemestre == null) {
								hasCasePrimitiveKeyWithNull_tMap_6 = true;
							} else {
								row18HashKey.codemodsemestre = (int) (Integer) exprKeyValue_row18__codemodsemestre;
							}

							Object exprKeyValue_row18__enseignantID = row17.enseignantID;
							if (exprKeyValue_row18__enseignantID == null) {
								hasCasePrimitiveKeyWithNull_tMap_6 = true;
							} else {
								row18HashKey.enseignantID = (int) (Integer) exprKeyValue_row18__enseignantID;
							}

							row18HashKey.hashCodeDirty = true;

							if (!hasCasePrimitiveKeyWithNull_tMap_6) { // G_TM_M_091

								tDBInput_15Process(globalMap);

								tHash_Lookup_row18 = (org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row18Struct>) ((org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row18Struct>) globalMap
										.get("tHash_Lookup_row18"));

								tHash_Lookup_row18.initGet();

								tHash_Lookup_row18.lookup(row18HashKey);

							} // G_TM_M_091

							if (hasCasePrimitiveKeyWithNull_tMap_6
									|| !tHash_Lookup_row18.hasNext()) { // G_TM_M_090

								rejectedInnerJoin_tMap_6 = true;

							} // G_TM_M_090

						} // G_TM_M_020

						if (tHash_Lookup_row18 != null
								&& tHash_Lookup_row18.getCount(row18HashKey) > 1) { // G
																					// 071

							// System.out.println("WARNING: UNIQUE MATCH is configured for the lookup 'row18' and it contains more one result from keys :  row18.codemodsemestre = '"
							// + row18HashKey.codemodsemestre +
							// "', row18.enseignantID = '" +
							// row18HashKey.enseignantID + "'");
						} // G 071

						row18Struct row18 = null;

						row18Struct fromLookup_row18 = null;
						row18 = row18Default;

						if (tHash_Lookup_row18 != null
								&& tHash_Lookup_row18.hasNext()) { // G 099

							fromLookup_row18 = tHash_Lookup_row18.next();

						} // G 099

						if (fromLookup_row18 != null) {
							row18 = fromLookup_row18;
						}

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_6__Struct Var = Var__tMap_6;// ###############################
							// ###############################
							// # Output tables

							joint_ens_cm = null;
							TEST_JOINTURE_SORTIE = null;

							if (!rejectedInnerJoin_tMap_6) {

								// # Output table : 'joint_ens_cm'
								joint_ens_cm_tmp.codemodsemestre = row17.codemodsemestre;
								joint_ens_cm_tmp.enseignantID = row17.enseignantID;
								joint_ens_cm_tmp.heuresCM = row17.heuresCM;
								joint_ens_cm_tmp.heuresTD = row18.heuresTD;
								joint_ens_cm_tmp.heuresTP = row19.heuresTP;
								joint_ens_cm = joint_ens_cm_tmp;

								// # Output table : 'TEST_JOINTURE_SORTIE'
								TEST_JOINTURE_SORTIE_tmp.codemodsemestre = row17.codemodsemestre;
								TEST_JOINTURE_SORTIE_tmp.enseignantID = row17.enseignantID;
								TEST_JOINTURE_SORTIE_tmp.heuresCM = row17.heuresCM;
								TEST_JOINTURE_SORTIE_tmp.heuresTD = row18.heuresTD;
								TEST_JOINTURE_SORTIE_tmp.heuresTP = row19.heuresTP;
								TEST_JOINTURE_SORTIE = TEST_JOINTURE_SORTIE_tmp;
							} // closing inner join bracket (2)
								// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_6 = false;

						tos_count_tMap_6++;

						/**
						 * [tMap_6 main ] stop
						 */

						/**
						 * [tMap_6 process_data_begin ] start
						 */

						currentComponent = "tMap_6";

						/**
						 * [tMap_6 process_data_begin ] stop
						 */
						// Start of branch "joint_ens_cm"
						if (joint_ens_cm != null) {

							/**
							 * [tDBOutput_4 main ] start
							 */

							currentComponent = "tDBOutput_4";

							// joint_ens_cm
							// joint_ens_cm

							if (execStat) {
								runStat.updateStatOnConnection("joint_ens_cm"
										+ iterateId, 1, 1);
							}

							whetherReject_tDBOutput_4 = false;
							pstmt_tDBOutput_4.setInt(1,
									joint_ens_cm.codemodsemestre);

							pstmt_tDBOutput_4.setInt(2,
									joint_ens_cm.enseignantID);

							pstmt_tDBOutput_4.setBigDecimal(3,
									joint_ens_cm.heuresCM);

							pstmt_tDBOutput_4.setBigDecimal(4,
									joint_ens_cm.heuresTD);

							pstmt_tDBOutput_4.setBigDecimal(5,
									joint_ens_cm.heuresTP);

							pstmt_tDBOutput_4.addBatch();
							nb_line_tDBOutput_4++;

							batchSizeCounter_tDBOutput_4++;
							if (batchSize_tDBOutput_4 <= batchSizeCounter_tDBOutput_4) {
								try {
									int countSum_tDBOutput_4 = 0;
									for (int countEach_tDBOutput_4 : pstmt_tDBOutput_4
											.executeBatch()) {
										countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0
												: 1);
									}
									insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
								} catch (java.sql.BatchUpdateException e) {
									int countSum_tDBOutput_4 = 0;
									for (int countEach_tDBOutput_4 : e
											.getUpdateCounts()) {
										countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0
												: countEach_tDBOutput_4);
									}
									insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
									System.err.println(e.getMessage());
								}

								batchSizeCounter_tDBOutput_4 = 0;
							}
							commitCounter_tDBOutput_4++;

							if (commitEvery_tDBOutput_4 <= commitCounter_tDBOutput_4) {

								try {
									int countSum_tDBOutput_4 = 0;
									for (int countEach_tDBOutput_4 : pstmt_tDBOutput_4
											.executeBatch()) {
										countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0
												: 1);
									}
									insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
								} catch (java.sql.BatchUpdateException e) {
									int countSum_tDBOutput_4 = 0;
									for (int countEach_tDBOutput_4 : e
											.getUpdateCounts()) {
										countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0
												: countEach_tDBOutput_4);
									}
									insertedCount_tDBOutput_4 += countSum_tDBOutput_4;
									System.err.println(e.getMessage());

								}
								conn_tDBOutput_4.commit();
								commitCounter_tDBOutput_4 = 0;

							}

							tos_count_tDBOutput_4++;

							/**
							 * [tDBOutput_4 main ] stop
							 */

							/**
							 * [tDBOutput_4 process_data_begin ] start
							 */

							currentComponent = "tDBOutput_4";

							/**
							 * [tDBOutput_4 process_data_begin ] stop
							 */

							/**
							 * [tDBOutput_4 process_data_end ] start
							 */

							currentComponent = "tDBOutput_4";

							/**
							 * [tDBOutput_4 process_data_end ] stop
							 */

						} // End of branch "joint_ens_cm"

						// Start of branch "TEST_JOINTURE_SORTIE"
						if (TEST_JOINTURE_SORTIE != null) {

							/**
							 * [tFileOutputExcel_4 main ] start
							 */

							currentComponent = "tFileOutputExcel_4";

							// TEST_JOINTURE_SORTIE
							// TEST_JOINTURE_SORTIE

							if (execStat) {
								runStat.updateStatOnConnection(
										"TEST_JOINTURE_SORTIE" + iterateId, 1,
										1);
							}

							xlsxTool_tFileOutputExcel_4.addRow();

							xlsxTool_tFileOutputExcel_4
									.addCellValue(Double.parseDouble(String
											.valueOf(TEST_JOINTURE_SORTIE.codemodsemestre)));

							xlsxTool_tFileOutputExcel_4
									.addCellValue(Double.parseDouble(String
											.valueOf(TEST_JOINTURE_SORTIE.enseignantID)));

							if (TEST_JOINTURE_SORTIE.heuresCM != null) {

								xlsxTool_tFileOutputExcel_4
										.addCellValue((TEST_JOINTURE_SORTIE.heuresCM
												.setScale(
														2,
														java.math.RoundingMode.HALF_UP))
												.doubleValue());
							} else {
								xlsxTool_tFileOutputExcel_4.addCellNullValue();
							}

							if (TEST_JOINTURE_SORTIE.heuresTD != null) {

								xlsxTool_tFileOutputExcel_4
										.addCellValue((TEST_JOINTURE_SORTIE.heuresTD
												.setScale(
														2,
														java.math.RoundingMode.HALF_UP))
												.doubleValue());
							} else {
								xlsxTool_tFileOutputExcel_4.addCellNullValue();
							}

							if (TEST_JOINTURE_SORTIE.heuresTP != null) {

								xlsxTool_tFileOutputExcel_4
										.addCellValue((TEST_JOINTURE_SORTIE.heuresTP
												.setScale(
														2,
														java.math.RoundingMode.HALF_UP))
												.doubleValue());
							} else {
								xlsxTool_tFileOutputExcel_4.addCellNullValue();
							}

							nb_line_tFileOutputExcel_4++;

							tos_count_tFileOutputExcel_4++;

							/**
							 * [tFileOutputExcel_4 main ] stop
							 */

							/**
							 * [tFileOutputExcel_4 process_data_begin ] start
							 */

							currentComponent = "tFileOutputExcel_4";

							/**
							 * [tFileOutputExcel_4 process_data_begin ] stop
							 */

							/**
							 * [tFileOutputExcel_4 process_data_end ] start
							 */

							currentComponent = "tFileOutputExcel_4";

							/**
							 * [tFileOutputExcel_4 process_data_end ] stop
							 */

						} // End of branch "TEST_JOINTURE_SORTIE"

						/**
						 * [tMap_6 process_data_end ] start
						 */

						currentComponent = "tMap_6";

						/**
						 * [tMap_6 process_data_end ] stop
						 */

						/**
						 * [tDBInput_14 process_data_end ] start
						 */

						currentComponent = "tDBInput_14";

						/**
						 * [tDBInput_14 process_data_end ] stop
						 */

						/**
						 * [tDBInput_14 end ] start
						 */

						currentComponent = "tDBInput_14";

					}
				} finally {
					if (rs_tDBInput_14 != null) {
						rs_tDBInput_14.close();
					}
					stmt_tDBInput_14.close();
					if (conn_tDBInput_14 != null
							&& !conn_tDBInput_14.isClosed()) {

						conn_tDBInput_14.close();

					}

				}

				globalMap.put("tDBInput_14_NB_LINE", nb_line_tDBInput_14);

				ok_Hash.put("tDBInput_14", true);
				end_Hash.put("tDBInput_14", System.currentTimeMillis());

				/**
				 * [tDBInput_14 end ] stop
				 */

				/**
				 * [tMap_6 end ] start
				 */

				currentComponent = "tMap_6";

				// ###############################
				// # Lookup hashes releasing
				if (tHash_Lookup_row19 != null) {
					tHash_Lookup_row19.endGet();
				}
				globalMap.remove("tHash_Lookup_row19");

				if (tHash_Lookup_row18 != null) {
					tHash_Lookup_row18.endGet();
				}
				globalMap.remove("tHash_Lookup_row18");

				// ###############################

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row17" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tMap_6", true);
				end_Hash.put("tMap_6", System.currentTimeMillis());

				/**
				 * [tMap_6 end ] stop
				 */

				/**
				 * [tDBOutput_4 end ] start
				 */

				currentComponent = "tDBOutput_4";

				try {
					if (batchSizeCounter_tDBOutput_4 != 0) {
						int countSum_tDBOutput_4 = 0;

						for (int countEach_tDBOutput_4 : pstmt_tDBOutput_4
								.executeBatch()) {
							countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0
									: 1);
						}

						insertedCount_tDBOutput_4 += countSum_tDBOutput_4;

					}

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_4 = 0;
					for (int countEach_tDBOutput_4 : e.getUpdateCounts()) {
						countSum_tDBOutput_4 += (countEach_tDBOutput_4 < 0 ? 0
								: countEach_tDBOutput_4);
					}

					insertedCount_tDBOutput_4 += countSum_tDBOutput_4;

					globalMap.put(currentComponent + "_ERROR_MESSAGE",
							e.getMessage());
					System.err.println(e.getMessage());

				}
				batchSizeCounter_tDBOutput_4 = 0;

				if (pstmt_tDBOutput_4 != null) {

					pstmt_tDBOutput_4.close();

				}

				if (commitCounter_tDBOutput_4 > 0) {

					conn_tDBOutput_4.commit();

				}

				conn_tDBOutput_4.close();

				resourceMap.put("finish_tDBOutput_4", true);

				nb_line_deleted_tDBOutput_4 = nb_line_deleted_tDBOutput_4
						+ deletedCount_tDBOutput_4;
				nb_line_update_tDBOutput_4 = nb_line_update_tDBOutput_4
						+ updatedCount_tDBOutput_4;
				nb_line_inserted_tDBOutput_4 = nb_line_inserted_tDBOutput_4
						+ insertedCount_tDBOutput_4;
				nb_line_rejected_tDBOutput_4 = nb_line_rejected_tDBOutput_4
						+ rejectedCount_tDBOutput_4;

				globalMap.put("tDBOutput_4_NB_LINE", nb_line_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_4);
				globalMap.put("tDBOutput_4_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_4);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("joint_ens_cm"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tDBOutput_4", true);
				end_Hash.put("tDBOutput_4", System.currentTimeMillis());

				/**
				 * [tDBOutput_4 end ] stop
				 */

				/**
				 * [tFileOutputExcel_4 end ] start
				 */

				currentComponent = "tFileOutputExcel_4";

				xlsxTool_tFileOutputExcel_4.writeExcel(
						fileName_tFileOutputExcel_4, true);

				nb_line_tFileOutputExcel_4 = nb_line_tFileOutputExcel_4 - 1;

				globalMap.put("tFileOutputExcel_4_NB_LINE",
						nb_line_tFileOutputExcel_4);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("TEST_JOINTURE_SORTIE"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tFileOutputExcel_4", true);
				end_Hash.put("tFileOutputExcel_4", System.currentTimeMillis());

				/**
				 * [tFileOutputExcel_4 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			// free memory for "tMap_6"
			globalMap.remove("tHash_Lookup_row18");

			// free memory for "tMap_6"
			globalMap.remove("tHash_Lookup_row19");

			try {

				/**
				 * [tDBInput_14 finally ] start
				 */

				currentComponent = "tDBInput_14";

				/**
				 * [tDBInput_14 finally ] stop
				 */

				/**
				 * [tMap_6 finally ] start
				 */

				currentComponent = "tMap_6";

				/**
				 * [tMap_6 finally ] stop
				 */

				/**
				 * [tDBOutput_4 finally ] start
				 */

				currentComponent = "tDBOutput_4";

				if (resourceMap.get("finish_tDBOutput_4") == null) {
					if (resourceMap.get("conn_tDBOutput_4") != null) {
						try {

							java.sql.Connection ctn_tDBOutput_4 = (java.sql.Connection) resourceMap
									.get("conn_tDBOutput_4");

							ctn_tDBOutput_4.close();

						} catch (java.sql.SQLException sqlEx_tDBOutput_4) {
							String errorMessage_tDBOutput_4 = "failed to close the connection in tDBOutput_4 :"
									+ sqlEx_tDBOutput_4.getMessage();

							System.err.println(errorMessage_tDBOutput_4);
						}
					}
				}

				/**
				 * [tDBOutput_4 finally ] stop
				 */

				/**
				 * [tFileOutputExcel_4 finally ] start
				 */

				currentComponent = "tFileOutputExcel_4";

				/**
				 * [tFileOutputExcel_4 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_14_SUBPROCESS_STATE", 1);
	}

	public static class row18Struct implements
			routines.system.IPersistableComparableLookupRow<row18Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public BigDecimal heuresTD;

		public BigDecimal getHeuresTD() {
			return this.heuresTD;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		public boolean paye;

		public boolean getPaye() {
			return this.paye;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row18Struct other = (row18Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(row18Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.heuresTD = this.heuresTD;
			other.verrou = this.verrou;
			other.verrouDPT = this.verrouDPT;
			other.paye = this.paye;

		}

		public void copyKeysDataTo(row18Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.heuresTD = (BigDecimal) ois.readObject();

				this.verrou = dis.readBoolean();

				this.verrouDPT = dis.readBoolean();

				this.paye = dis.readBoolean();

			} catch (IOException e) {
				throw new RuntimeException(e);

			} catch (ClassNotFoundException eCNFE) {
				throw new RuntimeException(eCNFE);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				oos.writeObject(this.heuresTD);

				dos.writeBoolean(this.verrou);

				dos.writeBoolean(this.verrouDPT);

				dos.writeBoolean(this.paye);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresTD=" + String.valueOf(heuresTD));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append(",paye=" + String.valueOf(paye));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row18Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_15Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_15_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row18Struct row18 = new row18Struct();

				/**
				 * [tAdvancedHash_row18 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row18", false);
				start_Hash.put("tAdvancedHash_row18",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row18";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row18" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row18 = 0;

				class BytesLimit65535_tAdvancedHash_row18 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row18().limitLog4jByte();

				// connection name:row18
				// source node:tDBInput_15 - inputs:() outputs:(row18,row18) |
				// target node:tAdvancedHash_row18 - inputs:(row18) outputs:()
				// linked node: tMap_6 - inputs:(row17,row18,row19)
				// outputs:(joint_ens_cm,TEST_JOINTURE_SORTIE)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row18 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row18Struct> tHash_Lookup_row18 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row18Struct> getLookup(matchingModeEnum_row18);

				globalMap.put("tHash_Lookup_row18", tHash_Lookup_row18);

				/**
				 * [tAdvancedHash_row18 begin ] stop
				 */

				/**
				 * [tDBInput_15 begin ] start
				 */

				ok_Hash.put("tDBInput_15", false);
				start_Hash.put("tDBInput_15", System.currentTimeMillis());

				currentComponent = "tDBInput_15";

				int tos_count_tDBInput_15 = 0;

				class BytesLimit65535_tDBInput_15 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_15().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_15 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_15.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_15 = calendar_tDBInput_15
						.getTime();
				int nb_line_tDBInput_15 = 0;
				java.sql.Connection conn_tDBInput_15 = null;
				String driverClass_tDBInput_15 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_15);
				String dbUser_tDBInput_15 = "root";

				final String decryptedPassword_tDBInput_15 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_15 = decryptedPassword_tDBInput_15;

				String url_tDBInput_15 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_15 = java.sql.DriverManager.getConnection(
						url_tDBInput_15, dbUser_tDBInput_15, dbPwd_tDBInput_15);

				java.sql.Statement stmt_tDBInput_15 = conn_tDBInput_15
						.createStatement();

				String dbquery_tDBInput_15 = "SELECT \n  `preserviceTD`.`codemodsemestre`, \n  `preserviceTD`.`enseignantID`, \n  `preserviceTD`.`heuresTD`, \n  `preserv"
						+ "iceTD`.`verrou`, \n  `preserviceTD`.`verrouDPT`, \n  `preserviceTD`.`paye`\nFROM `preserviceTD`";

				globalMap.put("tDBInput_15_QUERY", dbquery_tDBInput_15);
				java.sql.ResultSet rs_tDBInput_15 = null;

				try {
					rs_tDBInput_15 = stmt_tDBInput_15
							.executeQuery(dbquery_tDBInput_15);
					java.sql.ResultSetMetaData rsmd_tDBInput_15 = rs_tDBInput_15
							.getMetaData();
					int colQtyInRs_tDBInput_15 = rsmd_tDBInput_15
							.getColumnCount();

					String tmpContent_tDBInput_15 = null;

					while (rs_tDBInput_15.next()) {
						nb_line_tDBInput_15++;

						if (colQtyInRs_tDBInput_15 < 1) {
							row18.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_15.getObject(1) != null) {
								row18.codemodsemestre = rs_tDBInput_15
										.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_15 < 2) {
							row18.enseignantID = 0;
						} else {

							if (rs_tDBInput_15.getObject(2) != null) {
								row18.enseignantID = rs_tDBInput_15.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_15 < 3) {
							row18.heuresTD = null;
						} else {

							if (rs_tDBInput_15.getObject(3) != null) {
								row18.heuresTD = rs_tDBInput_15
										.getBigDecimal(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_15 < 4) {
							row18.verrou = false;
						} else {

							if (rs_tDBInput_15.getObject(4) != null) {
								row18.verrou = rs_tDBInput_15.getBoolean(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_15 < 5) {
							row18.verrouDPT = false;
						} else {

							if (rs_tDBInput_15.getObject(5) != null) {
								row18.verrouDPT = rs_tDBInput_15.getBoolean(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_15 < 6) {
							row18.paye = false;
						} else {

							if (rs_tDBInput_15.getObject(6) != null) {
								row18.paye = rs_tDBInput_15.getBoolean(6);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_15 begin ] stop
						 */

						/**
						 * [tDBInput_15 main ] start
						 */

						currentComponent = "tDBInput_15";

						tos_count_tDBInput_15++;

						/**
						 * [tDBInput_15 main ] stop
						 */

						/**
						 * [tDBInput_15 process_data_begin ] start
						 */

						currentComponent = "tDBInput_15";

						/**
						 * [tDBInput_15 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row18 main ] start
						 */

						currentComponent = "tAdvancedHash_row18";

						// row18
						// row18

						if (execStat) {
							runStat.updateStatOnConnection("row18" + iterateId,
									1, 1);
						}

						row18Struct row18_HashRow = new row18Struct();

						row18_HashRow.codemodsemestre = row18.codemodsemestre;

						row18_HashRow.enseignantID = row18.enseignantID;

						row18_HashRow.heuresTD = row18.heuresTD;

						row18_HashRow.verrou = row18.verrou;

						row18_HashRow.verrouDPT = row18.verrouDPT;

						row18_HashRow.paye = row18.paye;

						tHash_Lookup_row18.put(row18_HashRow);

						tos_count_tAdvancedHash_row18++;

						/**
						 * [tAdvancedHash_row18 main ] stop
						 */

						/**
						 * [tAdvancedHash_row18 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row18";

						/**
						 * [tAdvancedHash_row18 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row18 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row18";

						/**
						 * [tAdvancedHash_row18 process_data_end ] stop
						 */

						/**
						 * [tDBInput_15 process_data_end ] start
						 */

						currentComponent = "tDBInput_15";

						/**
						 * [tDBInput_15 process_data_end ] stop
						 */

						/**
						 * [tDBInput_15 end ] start
						 */

						currentComponent = "tDBInput_15";

					}
				} finally {
					if (rs_tDBInput_15 != null) {
						rs_tDBInput_15.close();
					}
					stmt_tDBInput_15.close();
					if (conn_tDBInput_15 != null
							&& !conn_tDBInput_15.isClosed()) {

						conn_tDBInput_15.close();

					}

				}

				globalMap.put("tDBInput_15_NB_LINE", nb_line_tDBInput_15);

				ok_Hash.put("tDBInput_15", true);
				end_Hash.put("tDBInput_15", System.currentTimeMillis());

				/**
				 * [tDBInput_15 end ] stop
				 */

				/**
				 * [tAdvancedHash_row18 end ] start
				 */

				currentComponent = "tAdvancedHash_row18";

				tHash_Lookup_row18.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row18" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row18", true);
				end_Hash.put("tAdvancedHash_row18", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row18 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_15 finally ] start
				 */

				currentComponent = "tDBInput_15";

				/**
				 * [tDBInput_15 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row18 finally ] start
				 */

				currentComponent = "tAdvancedHash_row18";

				/**
				 * [tAdvancedHash_row18 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_15_SUBPROCESS_STATE", 1);
	}

	public static class row19Struct implements
			routines.system.IPersistableComparableLookupRow<row19Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codemodsemestre;

		public int getCodemodsemestre() {
			return this.codemodsemestre;
		}

		public int enseignantID;

		public int getEnseignantID() {
			return this.enseignantID;
		}

		public BigDecimal heuresTP;

		public BigDecimal getHeuresTP() {
			return this.heuresTP;
		}

		public boolean verrou;

		public boolean getVerrou() {
			return this.verrou;
		}

		public boolean verrouDPT;

		public boolean getVerrouDPT() {
			return this.verrouDPT;
		}

		public boolean paye;

		public boolean getPaye() {
			return this.paye;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codemodsemestre;

				result = prime * result + (int) this.enseignantID;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final row19Struct other = (row19Struct) obj;

			if (this.codemodsemestre != other.codemodsemestre)
				return false;

			if (this.enseignantID != other.enseignantID)
				return false;

			return true;
		}

		public void copyDataTo(row19Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;
			other.heuresTP = this.heuresTP;
			other.verrou = this.verrou;
			other.verrouDPT = this.verrouDPT;
			other.paye = this.paye;

		}

		public void copyKeysDataTo(row19Struct other) {

			other.codemodsemestre = this.codemodsemestre;
			other.enseignantID = this.enseignantID;

		}

		public void readKeysData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codemodsemestre = dis.readInt();

					this.enseignantID = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeKeysData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codemodsemestre);

				// int

				dos.writeInt(this.enseignantID);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		/**
		 * Fill Values data by reading ObjectInputStream.
		 */
		public void readValuesData(DataInputStream dis, ObjectInputStream ois) {
			try {

				int length = 0;

				this.heuresTP = (BigDecimal) ois.readObject();

				this.verrou = dis.readBoolean();

				this.verrouDPT = dis.readBoolean();

				this.paye = dis.readBoolean();

			} catch (IOException e) {
				throw new RuntimeException(e);

			} catch (ClassNotFoundException eCNFE) {
				throw new RuntimeException(eCNFE);

			}

		}

		/**
		 * Return a byte array which represents Values data.
		 */
		public void writeValuesData(DataOutputStream dos, ObjectOutputStream oos) {
			try {

				oos.writeObject(this.heuresTP);

				dos.writeBoolean(this.verrou);

				dos.writeBoolean(this.verrouDPT);

				dos.writeBoolean(this.paye);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codemodsemestre=" + String.valueOf(codemodsemestre));
			sb.append(",enseignantID=" + String.valueOf(enseignantID));
			sb.append(",heuresTP=" + String.valueOf(heuresTP));
			sb.append(",verrou=" + String.valueOf(verrou));
			sb.append(",verrouDPT=" + String.valueOf(verrouDPT));
			sb.append(",paye=" + String.valueOf(paye));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row19Struct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codemodsemestre,
					other.codemodsemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			returnValue = checkNullsAndCompare(this.enseignantID,
					other.enseignantID);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_16Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_16_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row19Struct row19 = new row19Struct();

				/**
				 * [tAdvancedHash_row19 begin ] start
				 */

				ok_Hash.put("tAdvancedHash_row19", false);
				start_Hash.put("tAdvancedHash_row19",
						System.currentTimeMillis());

				currentComponent = "tAdvancedHash_row19";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row19" + iterateId, 0,
								0);

					}
				}

				int tos_count_tAdvancedHash_row19 = 0;

				class BytesLimit65535_tAdvancedHash_row19 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tAdvancedHash_row19().limitLog4jByte();

				// connection name:row19
				// source node:tDBInput_16 - inputs:() outputs:(row19,row19) |
				// target node:tAdvancedHash_row19 - inputs:(row19) outputs:()
				// linked node: tMap_6 - inputs:(row17,row18,row19)
				// outputs:(joint_ens_cm,TEST_JOINTURE_SORTIE)

				org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE matchingModeEnum_row19 = org.talend.designer.components.lookup.common.ICommonLookup.MATCHING_MODE.UNIQUE_MATCH;

				org.talend.designer.components.lookup.memory.AdvancedMemoryLookup<row19Struct> tHash_Lookup_row19 = org.talend.designer.components.lookup.memory.AdvancedMemoryLookup
						.<row19Struct> getLookup(matchingModeEnum_row19);

				globalMap.put("tHash_Lookup_row19", tHash_Lookup_row19);

				/**
				 * [tAdvancedHash_row19 begin ] stop
				 */

				/**
				 * [tDBInput_16 begin ] start
				 */

				ok_Hash.put("tDBInput_16", false);
				start_Hash.put("tDBInput_16", System.currentTimeMillis());

				currentComponent = "tDBInput_16";

				int tos_count_tDBInput_16 = 0;

				class BytesLimit65535_tDBInput_16 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_16().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_16 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_16.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_16 = calendar_tDBInput_16
						.getTime();
				int nb_line_tDBInput_16 = 0;
				java.sql.Connection conn_tDBInput_16 = null;
				String driverClass_tDBInput_16 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_16);
				String dbUser_tDBInput_16 = "root";

				final String decryptedPassword_tDBInput_16 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_16 = decryptedPassword_tDBInput_16;

				String url_tDBInput_16 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_16 = java.sql.DriverManager.getConnection(
						url_tDBInput_16, dbUser_tDBInput_16, dbPwd_tDBInput_16);

				java.sql.Statement stmt_tDBInput_16 = conn_tDBInput_16
						.createStatement();

				String dbquery_tDBInput_16 = "SELECT \n  `preserviceTP`.`codemodsemestre`, \n  `preserviceTP`.`enseignantID`, \n  `preserviceTP`.`heuresTP`, \n  `preserv"
						+ "iceTP`.`verrou`, \n  `preserviceTP`.`verrouDPT`, \n  `preserviceTP`.`paye`\nFROM `preserviceTP`";

				globalMap.put("tDBInput_16_QUERY", dbquery_tDBInput_16);
				java.sql.ResultSet rs_tDBInput_16 = null;

				try {
					rs_tDBInput_16 = stmt_tDBInput_16
							.executeQuery(dbquery_tDBInput_16);
					java.sql.ResultSetMetaData rsmd_tDBInput_16 = rs_tDBInput_16
							.getMetaData();
					int colQtyInRs_tDBInput_16 = rsmd_tDBInput_16
							.getColumnCount();

					String tmpContent_tDBInput_16 = null;

					while (rs_tDBInput_16.next()) {
						nb_line_tDBInput_16++;

						if (colQtyInRs_tDBInput_16 < 1) {
							row19.codemodsemestre = 0;
						} else {

							if (rs_tDBInput_16.getObject(1) != null) {
								row19.codemodsemestre = rs_tDBInput_16
										.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_16 < 2) {
							row19.enseignantID = 0;
						} else {

							if (rs_tDBInput_16.getObject(2) != null) {
								row19.enseignantID = rs_tDBInput_16.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_16 < 3) {
							row19.heuresTP = null;
						} else {

							if (rs_tDBInput_16.getObject(3) != null) {
								row19.heuresTP = rs_tDBInput_16
										.getBigDecimal(3);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_16 < 4) {
							row19.verrou = false;
						} else {

							if (rs_tDBInput_16.getObject(4) != null) {
								row19.verrou = rs_tDBInput_16.getBoolean(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_16 < 5) {
							row19.verrouDPT = false;
						} else {

							if (rs_tDBInput_16.getObject(5) != null) {
								row19.verrouDPT = rs_tDBInput_16.getBoolean(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_16 < 6) {
							row19.paye = false;
						} else {

							if (rs_tDBInput_16.getObject(6) != null) {
								row19.paye = rs_tDBInput_16.getBoolean(6);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_16 begin ] stop
						 */

						/**
						 * [tDBInput_16 main ] start
						 */

						currentComponent = "tDBInput_16";

						tos_count_tDBInput_16++;

						/**
						 * [tDBInput_16 main ] stop
						 */

						/**
						 * [tDBInput_16 process_data_begin ] start
						 */

						currentComponent = "tDBInput_16";

						/**
						 * [tDBInput_16 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row19 main ] start
						 */

						currentComponent = "tAdvancedHash_row19";

						// row19
						// row19

						if (execStat) {
							runStat.updateStatOnConnection("row19" + iterateId,
									1, 1);
						}

						row19Struct row19_HashRow = new row19Struct();

						row19_HashRow.codemodsemestre = row19.codemodsemestre;

						row19_HashRow.enseignantID = row19.enseignantID;

						row19_HashRow.heuresTP = row19.heuresTP;

						row19_HashRow.verrou = row19.verrou;

						row19_HashRow.verrouDPT = row19.verrouDPT;

						row19_HashRow.paye = row19.paye;

						tHash_Lookup_row19.put(row19_HashRow);

						tos_count_tAdvancedHash_row19++;

						/**
						 * [tAdvancedHash_row19 main ] stop
						 */

						/**
						 * [tAdvancedHash_row19 process_data_begin ] start
						 */

						currentComponent = "tAdvancedHash_row19";

						/**
						 * [tAdvancedHash_row19 process_data_begin ] stop
						 */

						/**
						 * [tAdvancedHash_row19 process_data_end ] start
						 */

						currentComponent = "tAdvancedHash_row19";

						/**
						 * [tAdvancedHash_row19 process_data_end ] stop
						 */

						/**
						 * [tDBInput_16 process_data_end ] start
						 */

						currentComponent = "tDBInput_16";

						/**
						 * [tDBInput_16 process_data_end ] stop
						 */

						/**
						 * [tDBInput_16 end ] start
						 */

						currentComponent = "tDBInput_16";

					}
				} finally {
					if (rs_tDBInput_16 != null) {
						rs_tDBInput_16.close();
					}
					stmt_tDBInput_16.close();
					if (conn_tDBInput_16 != null
							&& !conn_tDBInput_16.isClosed()) {

						conn_tDBInput_16.close();

					}

				}

				globalMap.put("tDBInput_16_NB_LINE", nb_line_tDBInput_16);

				ok_Hash.put("tDBInput_16", true);
				end_Hash.put("tDBInput_16", System.currentTimeMillis());

				/**
				 * [tDBInput_16 end ] stop
				 */

				/**
				 * [tAdvancedHash_row19 end ] start
				 */

				currentComponent = "tAdvancedHash_row19";

				tHash_Lookup_row19.endPut();

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row19" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tAdvancedHash_row19", true);
				end_Hash.put("tAdvancedHash_row19", System.currentTimeMillis());

				/**
				 * [tAdvancedHash_row19 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_16 finally ] start
				 */

				currentComponent = "tDBInput_16";

				/**
				 * [tDBInput_16 finally ] stop
				 */

				/**
				 * [tAdvancedHash_row19 finally ] start
				 */

				currentComponent = "tAdvancedHash_row19";

				/**
				 * [tAdvancedHash_row19 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_16_SUBPROCESS_STATE", 1);
	}

	public static class DIM_SEMESTREStruct implements
			routines.system.IPersistableRow<DIM_SEMESTREStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		public String nom;

		public String getNom() {
			return this.nom;
		}

		public int responsable;

		public int getResponsable() {
			return this.responsable;
		}

		public int departement;

		public int getDepartement() {
			return this.departement;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codesemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final DIM_SEMESTREStruct other = (DIM_SEMESTREStruct) obj;

			if (this.codesemestre != other.codesemestre)
				return false;

			return true;
		}

		public void copyDataTo(DIM_SEMESTREStruct other) {

			other.codesemestre = this.codesemestre;
			other.anneedebut = this.anneedebut;
			other.nom = this.nom;
			other.responsable = this.responsable;
			other.departement = this.departement;

		}

		public void copyKeysDataTo(DIM_SEMESTREStruct other) {

			other.codesemestre = this.codesemestre;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codesemestre = dis.readInt();

					this.anneedebut = dis.readInt();

					this.nom = readString(dis);

					this.responsable = dis.readInt();

					this.departement = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.anneedebut);

				// String

				writeString(this.nom, dos);

				// int

				dos.writeInt(this.responsable);

				// int

				dos.writeInt(this.departement);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codesemestre=" + String.valueOf(codesemestre));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append(",nom=" + nom);
			sb.append(",responsable=" + String.valueOf(responsable));
			sb.append(",departement=" + String.valueOf(departement));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(DIM_SEMESTREStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codesemestre,
					other.codesemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class DIM_SEM_XLSStruct implements
			routines.system.IPersistableRow<DIM_SEM_XLSStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		protected static final int DEFAULT_HASHCODE = 1;
		protected static final int PRIME = 31;
		protected int hashCode = DEFAULT_HASHCODE;
		public boolean hashCodeDirty = true;

		public String loopKey;

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		public String nom;

		public String getNom() {
			return this.nom;
		}

		public int responsable;

		public int getResponsable() {
			return this.responsable;
		}

		public int departement;

		public int getDepartement() {
			return this.departement;
		}

		@Override
		public int hashCode() {
			if (this.hashCodeDirty) {
				final int prime = PRIME;
				int result = DEFAULT_HASHCODE;

				result = prime * result + (int) this.codesemestre;

				this.hashCode = result;
				this.hashCodeDirty = false;
			}
			return this.hashCode;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			final DIM_SEM_XLSStruct other = (DIM_SEM_XLSStruct) obj;

			if (this.codesemestre != other.codesemestre)
				return false;

			return true;
		}

		public void copyDataTo(DIM_SEM_XLSStruct other) {

			other.codesemestre = this.codesemestre;
			other.anneedebut = this.anneedebut;
			other.nom = this.nom;
			other.responsable = this.responsable;
			other.departement = this.departement;

		}

		public void copyKeysDataTo(DIM_SEM_XLSStruct other) {

			other.codesemestre = this.codesemestre;

		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codesemestre = dis.readInt();

					this.anneedebut = dis.readInt();

					this.nom = readString(dis);

					this.responsable = dis.readInt();

					this.departement = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.anneedebut);

				// String

				writeString(this.nom, dos);

				// int

				dos.writeInt(this.responsable);

				// int

				dos.writeInt(this.departement);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codesemestre=" + String.valueOf(codesemestre));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append(",nom=" + nom);
			sb.append(",responsable=" + String.valueOf(responsable));
			sb.append(",departement=" + String.valueOf(departement));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(DIM_SEM_XLSStruct other) {

			int returnValue = -1;

			returnValue = checkNullsAndCompare(this.codesemestre,
					other.codesemestre);
			if (returnValue != 0) {
				return returnValue;
			}

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public static class row16Struct implements
			routines.system.IPersistableRow<row16Struct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public int codesemestre;

		public int getCodesemestre() {
			return this.codesemestre;
		}

		public int anneedebut;

		public int getAnneedebut() {
			return this.anneedebut;
		}

		public String nom;

		public String getNom() {
			return this.nom;
		}

		public int responsable;

		public int getResponsable() {
			return this.responsable;
		}

		public int departement;

		public int getDepartement() {
			return this.departement;
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.codesemestre = dis.readInt();

					this.anneedebut = dis.readInt();

					this.nom = readString(dis);

					this.responsable = dis.readInt();

					this.departement = dis.readInt();

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// int

				dos.writeInt(this.codesemestre);

				// int

				dos.writeInt(this.anneedebut);

				// String

				writeString(this.nom, dos);

				// int

				dos.writeInt(this.responsable);

				// int

				dos.writeInt(this.departement);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("codesemestre=" + String.valueOf(codesemestre));
			sb.append(",anneedebut=" + String.valueOf(anneedebut));
			sb.append(",nom=" + nom);
			sb.append(",responsable=" + String.valueOf(responsable));
			sb.append(",departement=" + String.valueOf(departement));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row16Struct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void tDBInput_19Process(final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tDBInput_19_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row16Struct row16 = new row16Struct();
				DIM_SEMESTREStruct DIM_SEMESTRE = new DIM_SEMESTREStruct();
				DIM_SEM_XLSStruct DIM_SEM_XLS = new DIM_SEM_XLSStruct();

				/**
				 * [tDBOutput_6 begin ] start
				 */

				ok_Hash.put("tDBOutput_6", false);
				start_Hash.put("tDBOutput_6", System.currentTimeMillis());

				currentComponent = "tDBOutput_6";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("DIM_SEMESTRE"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tDBOutput_6 = 0;

				class BytesLimit65535_tDBOutput_6 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBOutput_6().limitLog4jByte();

				int nb_line_tDBOutput_6 = 0;
				int nb_line_update_tDBOutput_6 = 0;
				int nb_line_inserted_tDBOutput_6 = 0;
				int nb_line_deleted_tDBOutput_6 = 0;
				int nb_line_rejected_tDBOutput_6 = 0;

				int deletedCount_tDBOutput_6 = 0;
				int updatedCount_tDBOutput_6 = 0;
				int insertedCount_tDBOutput_6 = 0;

				int rejectedCount_tDBOutput_6 = 0;

				String tableName_tDBOutput_6 = "DIM_SEMESTRE";
				boolean whetherReject_tDBOutput_6 = false;

				java.util.Calendar calendar_tDBOutput_6 = java.util.Calendar
						.getInstance();
				calendar_tDBOutput_6.set(1, 0, 1, 0, 0, 0);
				long year1_tDBOutput_6 = calendar_tDBOutput_6.getTime()
						.getTime();
				calendar_tDBOutput_6.set(10000, 0, 1, 0, 0, 0);
				long year10000_tDBOutput_6 = calendar_tDBOutput_6.getTime()
						.getTime();
				long date_tDBOutput_6;

				java.sql.Connection conn_tDBOutput_6 = null;
				String dbProperties_tDBOutput_6 = "noDatetimeStringSync=true";
				String url_tDBOutput_6 = null;
				if (dbProperties_tDBOutput_6 == null
						|| dbProperties_tDBOutput_6.trim().length() == 0) {
					url_tDBOutput_6 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ "rewriteBatchedStatements=true";
				} else {
					String properties_tDBOutput_6 = "noDatetimeStringSync=true";
					if (!properties_tDBOutput_6
							.contains("rewriteBatchedStatements")) {
						properties_tDBOutput_6 += "&rewriteBatchedStatements=true";
					}

					url_tDBOutput_6 = "jdbc:mysql://" + "localhost" + ":"
							+ "3306" + "/" + "DW_BIO_2013" + "?"
							+ properties_tDBOutput_6;
				}
				String driverClass_tDBOutput_6 = "org.gjt.mm.mysql.Driver";

				String dbUser_tDBOutput_6 = "root";

				final String decryptedPassword_tDBOutput_6 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBOutput_6 = decryptedPassword_tDBOutput_6;
				java.lang.Class.forName(driverClass_tDBOutput_6);

				conn_tDBOutput_6 = java.sql.DriverManager.getConnection(
						url_tDBOutput_6, dbUser_tDBOutput_6, dbPwd_tDBOutput_6);

				resourceMap.put("conn_tDBOutput_6", conn_tDBOutput_6);
				conn_tDBOutput_6.setAutoCommit(false);
				int commitEvery_tDBOutput_6 = 10000;
				int commitCounter_tDBOutput_6 = 0;

				int count_tDBOutput_6 = 0;

				java.sql.Statement stmtDrop_tDBOutput_6 = conn_tDBOutput_6
						.createStatement();
				stmtDrop_tDBOutput_6.execute("DROP TABLE `"
						+ tableName_tDBOutput_6 + "`");
				stmtDrop_tDBOutput_6.close();
				java.sql.Statement stmtCreate_tDBOutput_6 = conn_tDBOutput_6
						.createStatement();
				stmtCreate_tDBOutput_6
						.execute("CREATE TABLE `"
								+ tableName_tDBOutput_6
								+ "`(`codesemestre` INT(10)  default 0  not null ,`anneedebut` INT(10)  default 0  not null ,`nom` VARCHAR(100)   not null ,`responsable` INT(10)  default 0  not null ,`departement` INT(10)  default 0  not null ,primary key(`codesemestre`))");
				stmtCreate_tDBOutput_6.close();

				String insert_tDBOutput_6 = "INSERT INTO `"
						+ "DIM_SEMESTRE"
						+ "` (`codesemestre`,`anneedebut`,`nom`,`responsable`,`departement`) VALUES (?,?,?,?,?)";
				int batchSize_tDBOutput_6 = 100;
				int batchSizeCounter_tDBOutput_6 = 0;

				java.sql.PreparedStatement pstmt_tDBOutput_6 = conn_tDBOutput_6
						.prepareStatement(insert_tDBOutput_6);

				/**
				 * [tDBOutput_6 begin ] stop
				 */

				/**
				 * [tFileOutputExcel_5 begin ] start
				 */

				ok_Hash.put("tFileOutputExcel_5", false);
				start_Hash
						.put("tFileOutputExcel_5", System.currentTimeMillis());

				currentComponent = "tFileOutputExcel_5";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("DIM_SEM_XLS"
								+ iterateId, 0, 0);

					}
				}

				int tos_count_tFileOutputExcel_5 = 0;

				class BytesLimit65535_tFileOutputExcel_5 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tFileOutputExcel_5().limitLog4jByte();

				int columnIndex_tFileOutputExcel_5 = 0;

				String fileName_tFileOutputExcel_5 = "/home/cbma/Data/TOS_DI-20180411_1414-V7.0.1/workspace/DIM_SEMESTRE.xls";
				int nb_line_tFileOutputExcel_5 = 0;
				org.talend.ExcelTool xlsxTool_tFileOutputExcel_5 = new org.talend.ExcelTool();
				xlsxTool_tFileOutputExcel_5.setSheet("Sheet1");
				xlsxTool_tFileOutputExcel_5.setAppend(false, false);
				xlsxTool_tFileOutputExcel_5.setRecalculateFormula(false);
				xlsxTool_tFileOutputExcel_5.setXY(false, 0, 0, false);

				xlsxTool_tFileOutputExcel_5
						.prepareXlsxFile(fileName_tFileOutputExcel_5);

				xlsxTool_tFileOutputExcel_5.setFont("");

				if (xlsxTool_tFileOutputExcel_5.getStartRow() == 0) {

					xlsxTool_tFileOutputExcel_5.addRow();

					xlsxTool_tFileOutputExcel_5.addCellValue("codesemestre");

					xlsxTool_tFileOutputExcel_5.addCellValue("anneedebut");

					xlsxTool_tFileOutputExcel_5.addCellValue("nom");

					xlsxTool_tFileOutputExcel_5.addCellValue("responsable");

					xlsxTool_tFileOutputExcel_5.addCellValue("departement");

					nb_line_tFileOutputExcel_5++;

				}

				/**
				 * [tFileOutputExcel_5 begin ] stop
				 */

				/**
				 * [tMap_7 begin ] start
				 */

				ok_Hash.put("tMap_7", false);
				start_Hash.put("tMap_7", System.currentTimeMillis());

				currentComponent = "tMap_7";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("row16" + iterateId, 0,
								0);

					}
				}

				int tos_count_tMap_7 = 0;

				class BytesLimit65535_tMap_7 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tMap_7().limitLog4jByte();

				// ###############################
				// # Lookup's keys initialization
				// ###############################

				// ###############################
				// # Vars initialization
				class Var__tMap_7__Struct {
				}
				Var__tMap_7__Struct Var__tMap_7 = new Var__tMap_7__Struct();
				// ###############################

				// ###############################
				// # Outputs initialization
				DIM_SEMESTREStruct DIM_SEMESTRE_tmp = new DIM_SEMESTREStruct();
				DIM_SEM_XLSStruct DIM_SEM_XLS_tmp = new DIM_SEM_XLSStruct();
				// ###############################

				/**
				 * [tMap_7 begin ] stop
				 */

				/**
				 * [tDBInput_19 begin ] start
				 */

				ok_Hash.put("tDBInput_19", false);
				start_Hash.put("tDBInput_19", System.currentTimeMillis());

				currentComponent = "tDBInput_19";

				int tos_count_tDBInput_19 = 0;

				class BytesLimit65535_tDBInput_19 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tDBInput_19().limitLog4jByte();

				java.util.Calendar calendar_tDBInput_19 = java.util.Calendar
						.getInstance();
				calendar_tDBInput_19.set(0, 0, 0, 0, 0, 0);
				java.util.Date year0_tDBInput_19 = calendar_tDBInput_19
						.getTime();
				int nb_line_tDBInput_19 = 0;
				java.sql.Connection conn_tDBInput_19 = null;
				String driverClass_tDBInput_19 = "org.gjt.mm.mysql.Driver";
				java.lang.Class.forName(driverClass_tDBInput_19);
				String dbUser_tDBInput_19 = "root";

				final String decryptedPassword_tDBInput_19 = routines.system.PasswordEncryptUtil
						.decryptPassword("f4f7aba1746784ea");

				String dbPwd_tDBInput_19 = decryptedPassword_tDBInput_19;

				String url_tDBInput_19 = "jdbc:mysql://" + "localhost" + ":"
						+ "3306" + "/" + "svrbio_2013" + "?"
						+ "noDatetimeStringSync=true";

				conn_tDBInput_19 = java.sql.DriverManager.getConnection(
						url_tDBInput_19, dbUser_tDBInput_19, dbPwd_tDBInput_19);

				java.sql.Statement stmt_tDBInput_19 = conn_tDBInput_19
						.createStatement();

				String dbquery_tDBInput_19 = "SELECT \n  `semestres`.`codesemestre`, \n  `semestres`.`anneedebut`, \n  `semestres`.`nom`, \n  `semestres`.`responsable`, "
						+ "\n  `semestres`.`departement`\nFROM `semestres`";

				globalMap.put("tDBInput_19_QUERY", dbquery_tDBInput_19);
				java.sql.ResultSet rs_tDBInput_19 = null;

				try {
					rs_tDBInput_19 = stmt_tDBInput_19
							.executeQuery(dbquery_tDBInput_19);
					java.sql.ResultSetMetaData rsmd_tDBInput_19 = rs_tDBInput_19
							.getMetaData();
					int colQtyInRs_tDBInput_19 = rsmd_tDBInput_19
							.getColumnCount();

					String tmpContent_tDBInput_19 = null;

					while (rs_tDBInput_19.next()) {
						nb_line_tDBInput_19++;

						if (colQtyInRs_tDBInput_19 < 1) {
							row16.codesemestre = 0;
						} else {

							if (rs_tDBInput_19.getObject(1) != null) {
								row16.codesemestre = rs_tDBInput_19.getInt(1);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_19 < 2) {
							row16.anneedebut = 0;
						} else {

							if (rs_tDBInput_19.getObject(2) != null) {
								row16.anneedebut = rs_tDBInput_19.getInt(2);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_19 < 3) {
							row16.nom = null;
						} else {

							row16.nom = routines.system.JDBCUtil.getString(
									rs_tDBInput_19, 3, false);
						}
						if (colQtyInRs_tDBInput_19 < 4) {
							row16.responsable = 0;
						} else {

							if (rs_tDBInput_19.getObject(4) != null) {
								row16.responsable = rs_tDBInput_19.getInt(4);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}
						if (colQtyInRs_tDBInput_19 < 5) {
							row16.departement = 0;
						} else {

							if (rs_tDBInput_19.getObject(5) != null) {
								row16.departement = rs_tDBInput_19.getInt(5);
							} else {
								throw new RuntimeException(
										"Null value in non-Nullable column");
							}
						}

						/**
						 * [tDBInput_19 begin ] stop
						 */

						/**
						 * [tDBInput_19 main ] start
						 */

						currentComponent = "tDBInput_19";

						tos_count_tDBInput_19++;

						/**
						 * [tDBInput_19 main ] stop
						 */

						/**
						 * [tDBInput_19 process_data_begin ] start
						 */

						currentComponent = "tDBInput_19";

						/**
						 * [tDBInput_19 process_data_begin ] stop
						 */

						/**
						 * [tMap_7 main ] start
						 */

						currentComponent = "tMap_7";

						// row16
						// row16

						if (execStat) {
							runStat.updateStatOnConnection("row16" + iterateId,
									1, 1);
						}

						boolean hasCasePrimitiveKeyWithNull_tMap_7 = false;

						// ###############################
						// # Input tables (lookups)
						boolean rejectedInnerJoin_tMap_7 = false;
						boolean mainRowRejected_tMap_7 = false;

						// ###############################
						{ // start of Var scope

							// ###############################
							// # Vars tables

							Var__tMap_7__Struct Var = Var__tMap_7;// ###############################
							// ###############################
							// # Output tables

							DIM_SEMESTRE = null;
							DIM_SEM_XLS = null;

							// # Output table : 'DIM_SEMESTRE'
							DIM_SEMESTRE_tmp.codesemestre = row16.codesemestre;
							DIM_SEMESTRE_tmp.anneedebut = row16.anneedebut;
							DIM_SEMESTRE_tmp.nom = row16.nom;
							DIM_SEMESTRE_tmp.responsable = row16.responsable;
							DIM_SEMESTRE_tmp.departement = row16.departement;
							DIM_SEMESTRE = DIM_SEMESTRE_tmp;

							// # Output table : 'DIM_SEM_XLS'
							DIM_SEM_XLS_tmp.codesemestre = row16.codesemestre;
							DIM_SEM_XLS_tmp.anneedebut = row16.anneedebut;
							DIM_SEM_XLS_tmp.nom = row16.nom;
							DIM_SEM_XLS_tmp.responsable = row16.responsable;
							DIM_SEM_XLS_tmp.departement = row16.departement;
							DIM_SEM_XLS = DIM_SEM_XLS_tmp;
							// ###############################

						} // end of Var scope

						rejectedInnerJoin_tMap_7 = false;

						tos_count_tMap_7++;

						/**
						 * [tMap_7 main ] stop
						 */

						/**
						 * [tMap_7 process_data_begin ] start
						 */

						currentComponent = "tMap_7";

						/**
						 * [tMap_7 process_data_begin ] stop
						 */
						// Start of branch "DIM_SEMESTRE"
						if (DIM_SEMESTRE != null) {

							/**
							 * [tDBOutput_6 main ] start
							 */

							currentComponent = "tDBOutput_6";

							// DIM_SEMESTRE
							// DIM_SEMESTRE

							if (execStat) {
								runStat.updateStatOnConnection("DIM_SEMESTRE"
										+ iterateId, 1, 1);
							}

							whetherReject_tDBOutput_6 = false;
							pstmt_tDBOutput_6.setInt(1,
									DIM_SEMESTRE.codesemestre);

							pstmt_tDBOutput_6
									.setInt(2, DIM_SEMESTRE.anneedebut);

							if (DIM_SEMESTRE.nom == null) {
								pstmt_tDBOutput_6.setNull(3,
										java.sql.Types.VARCHAR);
							} else {
								pstmt_tDBOutput_6
										.setString(3, DIM_SEMESTRE.nom);
							}

							pstmt_tDBOutput_6.setInt(4,
									DIM_SEMESTRE.responsable);

							pstmt_tDBOutput_6.setInt(5,
									DIM_SEMESTRE.departement);

							pstmt_tDBOutput_6.addBatch();
							nb_line_tDBOutput_6++;

							batchSizeCounter_tDBOutput_6++;
							if (batchSize_tDBOutput_6 <= batchSizeCounter_tDBOutput_6) {
								try {
									int countSum_tDBOutput_6 = 0;
									for (int countEach_tDBOutput_6 : pstmt_tDBOutput_6
											.executeBatch()) {
										countSum_tDBOutput_6 += (countEach_tDBOutput_6 < 0 ? 0
												: 1);
									}
									insertedCount_tDBOutput_6 += countSum_tDBOutput_6;
								} catch (java.sql.BatchUpdateException e) {
									int countSum_tDBOutput_6 = 0;
									for (int countEach_tDBOutput_6 : e
											.getUpdateCounts()) {
										countSum_tDBOutput_6 += (countEach_tDBOutput_6 < 0 ? 0
												: countEach_tDBOutput_6);
									}
									insertedCount_tDBOutput_6 += countSum_tDBOutput_6;
									System.err.println(e.getMessage());
								}

								batchSizeCounter_tDBOutput_6 = 0;
							}
							commitCounter_tDBOutput_6++;

							if (commitEvery_tDBOutput_6 <= commitCounter_tDBOutput_6) {

								try {
									int countSum_tDBOutput_6 = 0;
									for (int countEach_tDBOutput_6 : pstmt_tDBOutput_6
											.executeBatch()) {
										countSum_tDBOutput_6 += (countEach_tDBOutput_6 < 0 ? 0
												: 1);
									}
									insertedCount_tDBOutput_6 += countSum_tDBOutput_6;
								} catch (java.sql.BatchUpdateException e) {
									int countSum_tDBOutput_6 = 0;
									for (int countEach_tDBOutput_6 : e
											.getUpdateCounts()) {
										countSum_tDBOutput_6 += (countEach_tDBOutput_6 < 0 ? 0
												: countEach_tDBOutput_6);
									}
									insertedCount_tDBOutput_6 += countSum_tDBOutput_6;
									System.err.println(e.getMessage());

								}
								conn_tDBOutput_6.commit();
								commitCounter_tDBOutput_6 = 0;

							}

							tos_count_tDBOutput_6++;

							/**
							 * [tDBOutput_6 main ] stop
							 */

							/**
							 * [tDBOutput_6 process_data_begin ] start
							 */

							currentComponent = "tDBOutput_6";

							/**
							 * [tDBOutput_6 process_data_begin ] stop
							 */

							/**
							 * [tDBOutput_6 process_data_end ] start
							 */

							currentComponent = "tDBOutput_6";

							/**
							 * [tDBOutput_6 process_data_end ] stop
							 */

						} // End of branch "DIM_SEMESTRE"

						// Start of branch "DIM_SEM_XLS"
						if (DIM_SEM_XLS != null) {

							/**
							 * [tFileOutputExcel_5 main ] start
							 */

							currentComponent = "tFileOutputExcel_5";

							// DIM_SEM_XLS
							// DIM_SEM_XLS

							if (execStat) {
								runStat.updateStatOnConnection("DIM_SEM_XLS"
										+ iterateId, 1, 1);
							}

							xlsxTool_tFileOutputExcel_5.addRow();

							xlsxTool_tFileOutputExcel_5
									.addCellValue(Double.parseDouble(String
											.valueOf(DIM_SEM_XLS.codesemestre)));

							xlsxTool_tFileOutputExcel_5.addCellValue(Double
									.parseDouble(String
											.valueOf(DIM_SEM_XLS.anneedebut)));

							if (DIM_SEM_XLS.nom != null) {

								xlsxTool_tFileOutputExcel_5.addCellValue(String
										.valueOf(DIM_SEM_XLS.nom));
							} else {
								xlsxTool_tFileOutputExcel_5.addCellNullValue();
							}

							xlsxTool_tFileOutputExcel_5.addCellValue(Double
									.parseDouble(String
											.valueOf(DIM_SEM_XLS.responsable)));

							xlsxTool_tFileOutputExcel_5.addCellValue(Double
									.parseDouble(String
											.valueOf(DIM_SEM_XLS.departement)));
							nb_line_tFileOutputExcel_5++;

							tos_count_tFileOutputExcel_5++;

							/**
							 * [tFileOutputExcel_5 main ] stop
							 */

							/**
							 * [tFileOutputExcel_5 process_data_begin ] start
							 */

							currentComponent = "tFileOutputExcel_5";

							/**
							 * [tFileOutputExcel_5 process_data_begin ] stop
							 */

							/**
							 * [tFileOutputExcel_5 process_data_end ] start
							 */

							currentComponent = "tFileOutputExcel_5";

							/**
							 * [tFileOutputExcel_5 process_data_end ] stop
							 */

						} // End of branch "DIM_SEM_XLS"

						/**
						 * [tMap_7 process_data_end ] start
						 */

						currentComponent = "tMap_7";

						/**
						 * [tMap_7 process_data_end ] stop
						 */

						/**
						 * [tDBInput_19 process_data_end ] start
						 */

						currentComponent = "tDBInput_19";

						/**
						 * [tDBInput_19 process_data_end ] stop
						 */

						/**
						 * [tDBInput_19 end ] start
						 */

						currentComponent = "tDBInput_19";

					}
				} finally {
					if (rs_tDBInput_19 != null) {
						rs_tDBInput_19.close();
					}
					stmt_tDBInput_19.close();
					if (conn_tDBInput_19 != null
							&& !conn_tDBInput_19.isClosed()) {

						conn_tDBInput_19.close();

					}

				}

				globalMap.put("tDBInput_19_NB_LINE", nb_line_tDBInput_19);

				ok_Hash.put("tDBInput_19", true);
				end_Hash.put("tDBInput_19", System.currentTimeMillis());

				/**
				 * [tDBInput_19 end ] stop
				 */

				/**
				 * [tMap_7 end ] start
				 */

				currentComponent = "tMap_7";

				// ###############################
				// # Lookup hashes releasing
				// ###############################

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("row16" + iterateId, 2,
								0);
					}
				}

				ok_Hash.put("tMap_7", true);
				end_Hash.put("tMap_7", System.currentTimeMillis());

				/**
				 * [tMap_7 end ] stop
				 */

				/**
				 * [tDBOutput_6 end ] start
				 */

				currentComponent = "tDBOutput_6";

				try {
					if (batchSizeCounter_tDBOutput_6 != 0) {
						int countSum_tDBOutput_6 = 0;

						for (int countEach_tDBOutput_6 : pstmt_tDBOutput_6
								.executeBatch()) {
							countSum_tDBOutput_6 += (countEach_tDBOutput_6 < 0 ? 0
									: 1);
						}

						insertedCount_tDBOutput_6 += countSum_tDBOutput_6;

					}

				} catch (java.sql.BatchUpdateException e) {

					int countSum_tDBOutput_6 = 0;
					for (int countEach_tDBOutput_6 : e.getUpdateCounts()) {
						countSum_tDBOutput_6 += (countEach_tDBOutput_6 < 0 ? 0
								: countEach_tDBOutput_6);
					}

					insertedCount_tDBOutput_6 += countSum_tDBOutput_6;

					globalMap.put(currentComponent + "_ERROR_MESSAGE",
							e.getMessage());
					System.err.println(e.getMessage());

				}
				batchSizeCounter_tDBOutput_6 = 0;

				if (pstmt_tDBOutput_6 != null) {

					pstmt_tDBOutput_6.close();

				}

				if (commitCounter_tDBOutput_6 > 0) {

					conn_tDBOutput_6.commit();

				}

				conn_tDBOutput_6.close();

				resourceMap.put("finish_tDBOutput_6", true);

				nb_line_deleted_tDBOutput_6 = nb_line_deleted_tDBOutput_6
						+ deletedCount_tDBOutput_6;
				nb_line_update_tDBOutput_6 = nb_line_update_tDBOutput_6
						+ updatedCount_tDBOutput_6;
				nb_line_inserted_tDBOutput_6 = nb_line_inserted_tDBOutput_6
						+ insertedCount_tDBOutput_6;
				nb_line_rejected_tDBOutput_6 = nb_line_rejected_tDBOutput_6
						+ rejectedCount_tDBOutput_6;

				globalMap.put("tDBOutput_6_NB_LINE", nb_line_tDBOutput_6);
				globalMap.put("tDBOutput_6_NB_LINE_UPDATED",
						nb_line_update_tDBOutput_6);
				globalMap.put("tDBOutput_6_NB_LINE_INSERTED",
						nb_line_inserted_tDBOutput_6);
				globalMap.put("tDBOutput_6_NB_LINE_DELETED",
						nb_line_deleted_tDBOutput_6);
				globalMap.put("tDBOutput_6_NB_LINE_REJECTED",
						nb_line_rejected_tDBOutput_6);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("DIM_SEMESTRE"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tDBOutput_6", true);
				end_Hash.put("tDBOutput_6", System.currentTimeMillis());

				/**
				 * [tDBOutput_6 end ] stop
				 */

				/**
				 * [tFileOutputExcel_5 end ] start
				 */

				currentComponent = "tFileOutputExcel_5";

				xlsxTool_tFileOutputExcel_5.writeExcel(
						fileName_tFileOutputExcel_5, true);

				nb_line_tFileOutputExcel_5 = nb_line_tFileOutputExcel_5 - 1;

				globalMap.put("tFileOutputExcel_5_NB_LINE",
						nb_line_tFileOutputExcel_5);

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("DIM_SEM_XLS"
								+ iterateId, 2, 0);
					}
				}

				ok_Hash.put("tFileOutputExcel_5", true);
				end_Hash.put("tFileOutputExcel_5", System.currentTimeMillis());

				/**
				 * [tFileOutputExcel_5 end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tDBInput_19 finally ] start
				 */

				currentComponent = "tDBInput_19";

				/**
				 * [tDBInput_19 finally ] stop
				 */

				/**
				 * [tMap_7 finally ] start
				 */

				currentComponent = "tMap_7";

				/**
				 * [tMap_7 finally ] stop
				 */

				/**
				 * [tDBOutput_6 finally ] start
				 */

				currentComponent = "tDBOutput_6";

				if (resourceMap.get("finish_tDBOutput_6") == null) {
					if (resourceMap.get("conn_tDBOutput_6") != null) {
						try {

							java.sql.Connection ctn_tDBOutput_6 = (java.sql.Connection) resourceMap
									.get("conn_tDBOutput_6");

							ctn_tDBOutput_6.close();

						} catch (java.sql.SQLException sqlEx_tDBOutput_6) {
							String errorMessage_tDBOutput_6 = "failed to close the connection in tDBOutput_6 :"
									+ sqlEx_tDBOutput_6.getMessage();

							System.err.println(errorMessage_tDBOutput_6);
						}
					}
				}

				/**
				 * [tDBOutput_6 finally ] stop
				 */

				/**
				 * [tFileOutputExcel_5 finally ] start
				 */

				currentComponent = "tFileOutputExcel_5";

				/**
				 * [tFileOutputExcel_5 finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tDBInput_19_SUBPROCESS_STATE", 1);
	}

	public void tFileOutputExcel_2Process(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("tFileOutputExcel_2_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				/**
				 * [tFileOutputExcel_2 begin ] start
				 */

				ok_Hash.put("tFileOutputExcel_2", false);
				start_Hash
						.put("tFileOutputExcel_2", System.currentTimeMillis());

				currentComponent = "tFileOutputExcel_2";

				int tos_count_tFileOutputExcel_2 = 0;

				class BytesLimit65535_tFileOutputExcel_2 {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_tFileOutputExcel_2().limitLog4jByte();

				/**
				 * [tFileOutputExcel_2 begin ] stop
				 */

				/**
				 * [tFileOutputExcel_2 main ] start
				 */

				currentComponent = "tFileOutputExcel_2";

				tos_count_tFileOutputExcel_2++;

				/**
				 * [tFileOutputExcel_2 main ] stop
				 */

				/**
				 * [tFileOutputExcel_2 process_data_begin ] start
				 */

				currentComponent = "tFileOutputExcel_2";

				/**
				 * [tFileOutputExcel_2 process_data_begin ] stop
				 */

				/**
				 * [tFileOutputExcel_2 process_data_end ] start
				 */

				currentComponent = "tFileOutputExcel_2";

				/**
				 * [tFileOutputExcel_2 process_data_end ] stop
				 */

				/**
				 * [tFileOutputExcel_2 end ] start
				 */

				currentComponent = "tFileOutputExcel_2";

				ok_Hash.put("tFileOutputExcel_2", true);
				end_Hash.put("tFileOutputExcel_2", System.currentTimeMillis());

				/**
				 * [tFileOutputExcel_2 end ] stop
				 */
			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [tFileOutputExcel_2 finally ] start
				 */

				currentComponent = "tFileOutputExcel_2";

				/**
				 * [tFileOutputExcel_2 finally ] stop
				 */
			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("tFileOutputExcel_2_SUBPROCESS_STATE", 1);
	}

	public static class row_talendStats_STATSStruct implements
			routines.system.IPersistableRow<row_talendStats_STATSStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public Long system_pid;

		public Long getSystem_pid() {
			return this.system_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String job_repository_id;

		public String getJob_repository_id() {
			return this.job_repository_id;
		}

		public String job_version;

		public String getJob_version() {
			return this.job_version;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message_type;

		public String getMessage_type() {
			return this.message_type;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Long duration;

		public Long getDuration() {
			return this.duration;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.father_pid = readString(dis);

					this.root_pid = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.system_pid = null;
					} else {
						this.system_pid = dis.readLong();
					}

					this.project = readString(dis);

					this.job = readString(dis);

					this.job_repository_id = readString(dis);

					this.job_version = readString(dis);

					this.context = readString(dis);

					this.origin = readString(dis);

					this.message_type = readString(dis);

					this.message = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.duration = null;
					} else {
						this.duration = dis.readLong();
					}

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.root_pid, dos);

				// Long

				if (this.system_pid == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.system_pid);
				}

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.job_repository_id, dos);

				// String

				writeString(this.job_version, dos);

				// String

				writeString(this.context, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message_type, dos);

				// String

				writeString(this.message, dos);

				// Long

				if (this.duration == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.duration);
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",system_pid=" + String.valueOf(system_pid));
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",job_repository_id=" + job_repository_id);
			sb.append(",job_version=" + job_version);
			sb.append(",context=" + context);
			sb.append(",origin=" + origin);
			sb.append(",message_type=" + message_type);
			sb.append(",message=" + message);
			sb.append(",duration=" + String.valueOf(duration));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendStats_STATSStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void talendStats_STATSProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row_talendStats_STATSStruct row_talendStats_STATS = new row_talendStats_STATSStruct();

				/**
				 * [talendStats_CONSOLE begin ] start
				 */

				ok_Hash.put("talendStats_CONSOLE", false);
				start_Hash.put("talendStats_CONSOLE",
						System.currentTimeMillis());

				currentVirtualComponent = "talendStats_CONSOLE";

				currentComponent = "talendStats_CONSOLE";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendStats_CONSOLE = 0;

				class BytesLimit65535_talendStats_CONSOLE {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendStats_CONSOLE().limitLog4jByte();

				// /////////////////////

				final String OUTPUT_FIELD_SEPARATOR_talendStats_CONSOLE = "|";
				java.io.PrintStream consoleOut_talendStats_CONSOLE = null;

				StringBuilder strBuffer_talendStats_CONSOLE = null;
				int nb_line_talendStats_CONSOLE = 0;
				// /////////////////////

				/**
				 * [talendStats_CONSOLE begin ] stop
				 */

				/**
				 * [talendStats_STATS begin ] start
				 */

				ok_Hash.put("talendStats_STATS", false);
				start_Hash.put("talendStats_STATS", System.currentTimeMillis());

				currentVirtualComponent = "talendStats_STATS";

				currentComponent = "talendStats_STATS";

				int tos_count_talendStats_STATS = 0;

				class BytesLimit65535_talendStats_STATS {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendStats_STATS().limitLog4jByte();

				for (StatCatcherUtils.StatCatcherMessage scm : talendStats_STATS
						.getMessages()) {
					row_talendStats_STATS.pid = pid;
					row_talendStats_STATS.root_pid = rootPid;
					row_talendStats_STATS.father_pid = fatherPid;
					row_talendStats_STATS.project = projectName;
					row_talendStats_STATS.job = jobName;
					row_talendStats_STATS.context = contextStr;
					row_talendStats_STATS.origin = (scm.getOrigin() == null
							|| scm.getOrigin().length() < 1 ? null : scm
							.getOrigin());
					row_talendStats_STATS.message = scm.getMessage();
					row_talendStats_STATS.duration = scm.getDuration();
					row_talendStats_STATS.moment = scm.getMoment();
					row_talendStats_STATS.message_type = scm.getMessageType();
					row_talendStats_STATS.job_version = scm.getJobVersion();
					row_talendStats_STATS.job_repository_id = scm.getJobId();
					row_talendStats_STATS.system_pid = scm.getSystemPid();

					/**
					 * [talendStats_STATS begin ] stop
					 */

					/**
					 * [talendStats_STATS main ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

					tos_count_talendStats_STATS++;

					/**
					 * [talendStats_STATS main ] stop
					 */

					/**
					 * [talendStats_STATS process_data_begin ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

					/**
					 * [talendStats_STATS process_data_begin ] stop
					 */

					/**
					 * [talendStats_CONSOLE main ] start
					 */

					currentVirtualComponent = "talendStats_CONSOLE";

					currentComponent = "talendStats_CONSOLE";

					// Main
					// row_talendStats_STATS

					if (execStat) {
						runStat.updateStatOnConnection("Main" + iterateId, 1, 1);
					}

					// /////////////////////

					strBuffer_talendStats_CONSOLE = new StringBuilder();

					if (row_talendStats_STATS.moment != null) { //

						strBuffer_talendStats_CONSOLE.append(FormatterUtils
								.format_Date(row_talendStats_STATS.moment,
										"yyyy-MM-dd HH:mm:ss"));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.pid != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.pid));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.father_pid != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.father_pid));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.root_pid != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.root_pid));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.system_pid != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.system_pid));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.project != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.project));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.job != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.job));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.job_repository_id != null) { //

						strBuffer_talendStats_CONSOLE
								.append(String
										.valueOf(row_talendStats_STATS.job_repository_id));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.job_version != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.job_version));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.context != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.context));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.origin != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.origin));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.message_type != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.message_type));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.message != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.message));

					} //

					strBuffer_talendStats_CONSOLE.append("|");

					if (row_talendStats_STATS.duration != null) { //

						strBuffer_talendStats_CONSOLE.append(String
								.valueOf(row_talendStats_STATS.duration));

					} //

					if (globalMap.get("tLogRow_CONSOLE") != null) {
						consoleOut_talendStats_CONSOLE = (java.io.PrintStream) globalMap
								.get("tLogRow_CONSOLE");
					} else {
						consoleOut_talendStats_CONSOLE = new java.io.PrintStream(
								new java.io.BufferedOutputStream(System.out));
						globalMap.put("tLogRow_CONSOLE",
								consoleOut_talendStats_CONSOLE);
					}
					consoleOut_talendStats_CONSOLE
							.println(strBuffer_talendStats_CONSOLE.toString());
					consoleOut_talendStats_CONSOLE.flush();
					nb_line_talendStats_CONSOLE++;
					// ////

					// ////

					// /////////////////////

					tos_count_talendStats_CONSOLE++;

					/**
					 * [talendStats_CONSOLE main ] stop
					 */

					/**
					 * [talendStats_CONSOLE process_data_begin ] start
					 */

					currentVirtualComponent = "talendStats_CONSOLE";

					currentComponent = "talendStats_CONSOLE";

					/**
					 * [talendStats_CONSOLE process_data_begin ] stop
					 */

					/**
					 * [talendStats_CONSOLE process_data_end ] start
					 */

					currentVirtualComponent = "talendStats_CONSOLE";

					currentComponent = "talendStats_CONSOLE";

					/**
					 * [talendStats_CONSOLE process_data_end ] stop
					 */

					/**
					 * [talendStats_STATS process_data_end ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

					/**
					 * [talendStats_STATS process_data_end ] stop
					 */

					/**
					 * [talendStats_STATS end ] start
					 */

					currentVirtualComponent = "talendStats_STATS";

					currentComponent = "talendStats_STATS";

				}

				ok_Hash.put("talendStats_STATS", true);
				end_Hash.put("talendStats_STATS", System.currentTimeMillis());

				/**
				 * [talendStats_STATS end ] stop
				 */

				/**
				 * [talendStats_CONSOLE end ] start
				 */

				currentVirtualComponent = "talendStats_CONSOLE";

				currentComponent = "talendStats_CONSOLE";

				// ////
				// ////
				globalMap.put("talendStats_CONSOLE_NB_LINE",
						nb_line_talendStats_CONSOLE);

				// /////////////////////

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendStats_CONSOLE", true);
				end_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());

				/**
				 * [talendStats_CONSOLE end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendStats_STATS finally ] start
				 */

				currentVirtualComponent = "talendStats_STATS";

				currentComponent = "talendStats_STATS";

				/**
				 * [talendStats_STATS finally ] stop
				 */

				/**
				 * [talendStats_CONSOLE finally ] start
				 */

				currentVirtualComponent = "talendStats_CONSOLE";

				currentComponent = "talendStats_CONSOLE";

				/**
				 * [talendStats_CONSOLE finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 1);
	}

	public static class row_talendLogs_LOGSStruct implements
			routines.system.IPersistableRow<row_talendLogs_LOGSStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public Integer priority;

		public Integer getPriority() {
			return this.priority;
		}

		public String type;

		public String getType() {
			return this.type;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String message;

		public String getMessage() {
			return this.message;
		}

		public Integer code;

		public Integer getCode() {
			return this.code;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.root_pid = readString(dis);

					this.father_pid = readString(dis);

					this.project = readString(dis);

					this.job = readString(dis);

					this.context = readString(dis);

					this.priority = readInteger(dis);

					this.type = readString(dis);

					this.origin = readString(dis);

					this.message = readString(dis);

					this.code = readInteger(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.root_pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.context, dos);

				// Integer

				writeInteger(this.priority, dos);

				// String

				writeString(this.type, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.message, dos);

				// Integer

				writeInteger(this.code, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",context=" + context);
			sb.append(",priority=" + String.valueOf(priority));
			sb.append(",type=" + type);
			sb.append(",origin=" + origin);
			sb.append(",message=" + message);
			sb.append(",code=" + String.valueOf(code));
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendLogs_LOGSStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void talendLogs_LOGSProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row_talendLogs_LOGSStruct row_talendLogs_LOGS = new row_talendLogs_LOGSStruct();

				/**
				 * [talendLogs_CONSOLE begin ] start
				 */

				ok_Hash.put("talendLogs_CONSOLE", false);
				start_Hash
						.put("talendLogs_CONSOLE", System.currentTimeMillis());

				currentVirtualComponent = "talendLogs_CONSOLE";

				currentComponent = "talendLogs_CONSOLE";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendLogs_CONSOLE = 0;

				class BytesLimit65535_talendLogs_CONSOLE {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendLogs_CONSOLE().limitLog4jByte();

				// /////////////////////

				final String OUTPUT_FIELD_SEPARATOR_talendLogs_CONSOLE = "|";
				java.io.PrintStream consoleOut_talendLogs_CONSOLE = null;

				StringBuilder strBuffer_talendLogs_CONSOLE = null;
				int nb_line_talendLogs_CONSOLE = 0;
				// /////////////////////

				/**
				 * [talendLogs_CONSOLE begin ] stop
				 */

				/**
				 * [talendLogs_LOGS begin ] start
				 */

				ok_Hash.put("talendLogs_LOGS", false);
				start_Hash.put("talendLogs_LOGS", System.currentTimeMillis());

				currentVirtualComponent = "talendLogs_LOGS";

				currentComponent = "talendLogs_LOGS";

				int tos_count_talendLogs_LOGS = 0;

				class BytesLimit65535_talendLogs_LOGS {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendLogs_LOGS().limitLog4jByte();

				try {
					for (LogCatcherUtils.LogCatcherMessage lcm : talendLogs_LOGS
							.getMessages()) {
						row_talendLogs_LOGS.type = lcm.getType();
						row_talendLogs_LOGS.origin = (lcm.getOrigin() == null
								|| lcm.getOrigin().length() < 1 ? null : lcm
								.getOrigin());
						row_talendLogs_LOGS.priority = lcm.getPriority();
						row_talendLogs_LOGS.message = lcm.getMessage();
						row_talendLogs_LOGS.code = lcm.getCode();

						row_talendLogs_LOGS.moment = java.util.Calendar
								.getInstance().getTime();

						row_talendLogs_LOGS.pid = pid;
						row_talendLogs_LOGS.root_pid = rootPid;
						row_talendLogs_LOGS.father_pid = fatherPid;

						row_talendLogs_LOGS.project = projectName;
						row_talendLogs_LOGS.job = jobName;
						row_talendLogs_LOGS.context = contextStr;

						/**
						 * [talendLogs_LOGS begin ] stop
						 */

						/**
						 * [talendLogs_LOGS main ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

						tos_count_talendLogs_LOGS++;

						/**
						 * [talendLogs_LOGS main ] stop
						 */

						/**
						 * [talendLogs_LOGS process_data_begin ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

						/**
						 * [talendLogs_LOGS process_data_begin ] stop
						 */

						/**
						 * [talendLogs_CONSOLE main ] start
						 */

						currentVirtualComponent = "talendLogs_CONSOLE";

						currentComponent = "talendLogs_CONSOLE";

						// Main
						// row_talendLogs_LOGS

						if (execStat) {
							runStat.updateStatOnConnection("Main" + iterateId,
									1, 1);
						}

						// /////////////////////

						strBuffer_talendLogs_CONSOLE = new StringBuilder();

						if (row_talendLogs_LOGS.moment != null) { //

							strBuffer_talendLogs_CONSOLE.append(FormatterUtils
									.format_Date(row_talendLogs_LOGS.moment,
											"yyyy-MM-dd HH:mm:ss"));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.pid != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.pid));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.root_pid != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.root_pid));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.father_pid != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.father_pid));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.project != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.project));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.job != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.job));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.context != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.context));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.priority != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.priority));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.type != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.type));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.origin != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.origin));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.message != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.message));

						} //

						strBuffer_talendLogs_CONSOLE.append("|");

						if (row_talendLogs_LOGS.code != null) { //

							strBuffer_talendLogs_CONSOLE.append(String
									.valueOf(row_talendLogs_LOGS.code));

						} //

						if (globalMap.get("tLogRow_CONSOLE") != null) {
							consoleOut_talendLogs_CONSOLE = (java.io.PrintStream) globalMap
									.get("tLogRow_CONSOLE");
						} else {
							consoleOut_talendLogs_CONSOLE = new java.io.PrintStream(
									new java.io.BufferedOutputStream(System.out));
							globalMap.put("tLogRow_CONSOLE",
									consoleOut_talendLogs_CONSOLE);
						}
						consoleOut_talendLogs_CONSOLE
								.println(strBuffer_talendLogs_CONSOLE
										.toString());
						consoleOut_talendLogs_CONSOLE.flush();
						nb_line_talendLogs_CONSOLE++;
						// ////

						// ////

						// /////////////////////

						tos_count_talendLogs_CONSOLE++;

						/**
						 * [talendLogs_CONSOLE main ] stop
						 */

						/**
						 * [talendLogs_CONSOLE process_data_begin ] start
						 */

						currentVirtualComponent = "talendLogs_CONSOLE";

						currentComponent = "talendLogs_CONSOLE";

						/**
						 * [talendLogs_CONSOLE process_data_begin ] stop
						 */

						/**
						 * [talendLogs_CONSOLE process_data_end ] start
						 */

						currentVirtualComponent = "talendLogs_CONSOLE";

						currentComponent = "talendLogs_CONSOLE";

						/**
						 * [talendLogs_CONSOLE process_data_end ] stop
						 */

						/**
						 * [talendLogs_LOGS process_data_end ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

						/**
						 * [talendLogs_LOGS process_data_end ] stop
						 */

						/**
						 * [talendLogs_LOGS end ] start
						 */

						currentVirtualComponent = "talendLogs_LOGS";

						currentComponent = "talendLogs_LOGS";

					}
				} catch (Exception e_talendLogs_LOGS) {
					logIgnoredError(
							String.format(
									"talendLogs_LOGS - tLogCatcher failed to process log message(s) due to internal error: %s",
									e_talendLogs_LOGS), e_talendLogs_LOGS);
				}

				ok_Hash.put("talendLogs_LOGS", true);
				end_Hash.put("talendLogs_LOGS", System.currentTimeMillis());

				/**
				 * [talendLogs_LOGS end ] stop
				 */

				/**
				 * [talendLogs_CONSOLE end ] start
				 */

				currentVirtualComponent = "talendLogs_CONSOLE";

				currentComponent = "talendLogs_CONSOLE";

				// ////
				// ////
				globalMap.put("talendLogs_CONSOLE_NB_LINE",
						nb_line_talendLogs_CONSOLE);

				// /////////////////////

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendLogs_CONSOLE", true);
				end_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());

				/**
				 * [talendLogs_CONSOLE end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendLogs_LOGS finally ] start
				 */

				currentVirtualComponent = "talendLogs_LOGS";

				currentComponent = "talendLogs_LOGS";

				/**
				 * [talendLogs_LOGS finally ] stop
				 */

				/**
				 * [talendLogs_CONSOLE finally ] start
				 */

				currentVirtualComponent = "talendLogs_CONSOLE";

				currentComponent = "talendLogs_CONSOLE";

				/**
				 * [talendLogs_CONSOLE finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 1);
	}

	public static class row_talendMeter_METTERStruct implements
			routines.system.IPersistableRow<row_talendMeter_METTERStruct> {
		final static byte[] commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];
		static byte[] commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[0];

		public java.util.Date moment;

		public java.util.Date getMoment() {
			return this.moment;
		}

		public String pid;

		public String getPid() {
			return this.pid;
		}

		public String father_pid;

		public String getFather_pid() {
			return this.father_pid;
		}

		public String root_pid;

		public String getRoot_pid() {
			return this.root_pid;
		}

		public Long system_pid;

		public Long getSystem_pid() {
			return this.system_pid;
		}

		public String project;

		public String getProject() {
			return this.project;
		}

		public String job;

		public String getJob() {
			return this.job;
		}

		public String job_repository_id;

		public String getJob_repository_id() {
			return this.job_repository_id;
		}

		public String job_version;

		public String getJob_version() {
			return this.job_version;
		}

		public String context;

		public String getContext() {
			return this.context;
		}

		public String origin;

		public String getOrigin() {
			return this.origin;
		}

		public String label;

		public String getLabel() {
			return this.label;
		}

		public Integer count;

		public Integer getCount() {
			return this.count;
		}

		public Integer reference;

		public Integer getReference() {
			return this.reference;
		}

		public String thresholds;

		public String getThresholds() {
			return this.thresholds;
		}

		private java.util.Date readDate(ObjectInputStream dis)
				throws IOException {
			java.util.Date dateReturn = null;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				dateReturn = null;
			} else {
				dateReturn = new Date(dis.readLong());
			}
			return dateReturn;
		}

		private void writeDate(java.util.Date date1, ObjectOutputStream dos)
				throws IOException {
			if (date1 == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeLong(date1.getTime());
			}
		}

		private String readString(ObjectInputStream dis) throws IOException {
			String strReturn = null;
			int length = 0;
			length = dis.readInt();
			if (length == -1) {
				strReturn = null;
			} else {
				if (length > commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length) {
					if (length < 1024
							&& commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO.length == 0) {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[1024];
					} else {
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO = new byte[2 * length];
					}
				}
				dis.readFully(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length);
				strReturn = new String(
						commonByteArray_DW_TER_TRANS_CHARG_DW_VERSION_BIO, 0,
						length, utf8Charset);
			}
			return strReturn;
		}

		private void writeString(String str, ObjectOutputStream dos)
				throws IOException {
			if (str == null) {
				dos.writeInt(-1);
			} else {
				byte[] byteArray = str.getBytes(utf8Charset);
				dos.writeInt(byteArray.length);
				dos.write(byteArray);
			}
		}

		private Integer readInteger(ObjectInputStream dis) throws IOException {
			Integer intReturn;
			int length = 0;
			length = dis.readByte();
			if (length == -1) {
				intReturn = null;
			} else {
				intReturn = dis.readInt();
			}
			return intReturn;
		}

		private void writeInteger(Integer intNum, ObjectOutputStream dos)
				throws IOException {
			if (intNum == null) {
				dos.writeByte(-1);
			} else {
				dos.writeByte(0);
				dos.writeInt(intNum);
			}
		}

		public void readData(ObjectInputStream dis) {

			synchronized (commonByteArrayLock_DW_TER_TRANS_CHARG_DW_VERSION_BIO) {

				try {

					int length = 0;

					this.moment = readDate(dis);

					this.pid = readString(dis);

					this.father_pid = readString(dis);

					this.root_pid = readString(dis);

					length = dis.readByte();
					if (length == -1) {
						this.system_pid = null;
					} else {
						this.system_pid = dis.readLong();
					}

					this.project = readString(dis);

					this.job = readString(dis);

					this.job_repository_id = readString(dis);

					this.job_version = readString(dis);

					this.context = readString(dis);

					this.origin = readString(dis);

					this.label = readString(dis);

					this.count = readInteger(dis);

					this.reference = readInteger(dis);

					this.thresholds = readString(dis);

				} catch (IOException e) {
					throw new RuntimeException(e);

				}

			}

		}

		public void writeData(ObjectOutputStream dos) {
			try {

				// java.util.Date

				writeDate(this.moment, dos);

				// String

				writeString(this.pid, dos);

				// String

				writeString(this.father_pid, dos);

				// String

				writeString(this.root_pid, dos);

				// Long

				if (this.system_pid == null) {
					dos.writeByte(-1);
				} else {
					dos.writeByte(0);
					dos.writeLong(this.system_pid);
				}

				// String

				writeString(this.project, dos);

				// String

				writeString(this.job, dos);

				// String

				writeString(this.job_repository_id, dos);

				// String

				writeString(this.job_version, dos);

				// String

				writeString(this.context, dos);

				// String

				writeString(this.origin, dos);

				// String

				writeString(this.label, dos);

				// Integer

				writeInteger(this.count, dos);

				// Integer

				writeInteger(this.reference, dos);

				// String

				writeString(this.thresholds, dos);

			} catch (IOException e) {
				throw new RuntimeException(e);
			}

		}

		public String toString() {

			StringBuilder sb = new StringBuilder();
			sb.append(super.toString());
			sb.append("[");
			sb.append("moment=" + String.valueOf(moment));
			sb.append(",pid=" + pid);
			sb.append(",father_pid=" + father_pid);
			sb.append(",root_pid=" + root_pid);
			sb.append(",system_pid=" + String.valueOf(system_pid));
			sb.append(",project=" + project);
			sb.append(",job=" + job);
			sb.append(",job_repository_id=" + job_repository_id);
			sb.append(",job_version=" + job_version);
			sb.append(",context=" + context);
			sb.append(",origin=" + origin);
			sb.append(",label=" + label);
			sb.append(",count=" + String.valueOf(count));
			sb.append(",reference=" + String.valueOf(reference));
			sb.append(",thresholds=" + thresholds);
			sb.append("]");

			return sb.toString();
		}

		/**
		 * Compare keys
		 */
		public int compareTo(row_talendMeter_METTERStruct other) {

			int returnValue = -1;

			return returnValue;
		}

		private int checkNullsAndCompare(Object object1, Object object2) {
			int returnValue = 0;
			if (object1 instanceof Comparable && object2 instanceof Comparable) {
				returnValue = ((Comparable) object1).compareTo(object2);
			} else if (object1 != null && object2 != null) {
				returnValue = compareStrings(object1.toString(),
						object2.toString());
			} else if (object1 == null && object2 != null) {
				returnValue = 1;
			} else if (object1 != null && object2 == null) {
				returnValue = -1;
			} else {
				returnValue = 0;
			}

			return returnValue;
		}

		private int compareStrings(String string1, String string2) {
			return string1.compareTo(string2);
		}

	}

	public void talendMeter_METTERProcess(
			final java.util.Map<String, Object> globalMap)
			throws TalendException {
		globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 0);

		final boolean execStat = this.execStat;
		String currentVirtualComponent = null;

		String iterateId = "";

		String currentComponent = "";
		java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

		try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception()
						.getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { // start the resume
				globalResumeTicket = true;

				row_talendMeter_METTERStruct row_talendMeter_METTER = new row_talendMeter_METTERStruct();

				/**
				 * [talendMeter_CONSOLE begin ] start
				 */

				ok_Hash.put("talendMeter_CONSOLE", false);
				start_Hash.put("talendMeter_CONSOLE",
						System.currentTimeMillis());

				currentVirtualComponent = "talendMeter_CONSOLE";

				currentComponent = "talendMeter_CONSOLE";

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null) {

						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);

					}
				}

				int tos_count_talendMeter_CONSOLE = 0;

				class BytesLimit65535_talendMeter_CONSOLE {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendMeter_CONSOLE().limitLog4jByte();

				// /////////////////////

				final String OUTPUT_FIELD_SEPARATOR_talendMeter_CONSOLE = "|";
				java.io.PrintStream consoleOut_talendMeter_CONSOLE = null;

				StringBuilder strBuffer_talendMeter_CONSOLE = null;
				int nb_line_talendMeter_CONSOLE = 0;
				// /////////////////////

				/**
				 * [talendMeter_CONSOLE begin ] stop
				 */

				/**
				 * [talendMeter_METTER begin ] start
				 */

				ok_Hash.put("talendMeter_METTER", false);
				start_Hash
						.put("talendMeter_METTER", System.currentTimeMillis());

				currentVirtualComponent = "talendMeter_METTER";

				currentComponent = "talendMeter_METTER";

				int tos_count_talendMeter_METTER = 0;

				class BytesLimit65535_talendMeter_METTER {
					public void limitLog4jByte() throws Exception {

					}
				}

				new BytesLimit65535_talendMeter_METTER().limitLog4jByte();

				for (MetterCatcherUtils.MetterCatcherMessage mcm : talendMeter_METTER
						.getMessages()) {
					row_talendMeter_METTER.pid = pid;
					row_talendMeter_METTER.root_pid = rootPid;
					row_talendMeter_METTER.father_pid = fatherPid;
					row_talendMeter_METTER.project = projectName;
					row_talendMeter_METTER.job = jobName;
					row_talendMeter_METTER.context = contextStr;
					row_talendMeter_METTER.origin = (mcm.getOrigin() == null
							|| mcm.getOrigin().length() < 1 ? null : mcm
							.getOrigin());
					row_talendMeter_METTER.moment = mcm.getMoment();
					row_talendMeter_METTER.job_version = mcm.getJobVersion();
					row_talendMeter_METTER.job_repository_id = mcm.getJobId();
					row_talendMeter_METTER.system_pid = mcm.getSystemPid();
					row_talendMeter_METTER.label = mcm.getLabel();
					row_talendMeter_METTER.count = mcm.getCount();
					row_talendMeter_METTER.reference = talendMeter_METTER
							.getConnLinesCount(mcm.getReferense() + "_count");
					row_talendMeter_METTER.thresholds = mcm.getThresholds();

					/**
					 * [talendMeter_METTER begin ] stop
					 */

					/**
					 * [talendMeter_METTER main ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

					tos_count_talendMeter_METTER++;

					/**
					 * [talendMeter_METTER main ] stop
					 */

					/**
					 * [talendMeter_METTER process_data_begin ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

					/**
					 * [talendMeter_METTER process_data_begin ] stop
					 */

					/**
					 * [talendMeter_CONSOLE main ] start
					 */

					currentVirtualComponent = "talendMeter_CONSOLE";

					currentComponent = "talendMeter_CONSOLE";

					// Main
					// row_talendMeter_METTER

					if (execStat) {
						runStat.updateStatOnConnection("Main" + iterateId, 1, 1);
					}

					// /////////////////////

					strBuffer_talendMeter_CONSOLE = new StringBuilder();

					if (row_talendMeter_METTER.moment != null) { //

						strBuffer_talendMeter_CONSOLE.append(FormatterUtils
								.format_Date(row_talendMeter_METTER.moment,
										"yyyy-MM-dd HH:mm:ss"));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.pid != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.pid));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.father_pid != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.father_pid));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.root_pid != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.root_pid));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.system_pid != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.system_pid));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.project != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.project));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.job != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.job));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.job_repository_id != null) { //

						strBuffer_talendMeter_CONSOLE
								.append(String
										.valueOf(row_talendMeter_METTER.job_repository_id));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.job_version != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.job_version));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.context != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.context));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.origin != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.origin));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.label != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.label));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.count != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.count));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.reference != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.reference));

					} //

					strBuffer_talendMeter_CONSOLE.append("|");

					if (row_talendMeter_METTER.thresholds != null) { //

						strBuffer_talendMeter_CONSOLE.append(String
								.valueOf(row_talendMeter_METTER.thresholds));

					} //

					if (globalMap.get("tLogRow_CONSOLE") != null) {
						consoleOut_talendMeter_CONSOLE = (java.io.PrintStream) globalMap
								.get("tLogRow_CONSOLE");
					} else {
						consoleOut_talendMeter_CONSOLE = new java.io.PrintStream(
								new java.io.BufferedOutputStream(System.out));
						globalMap.put("tLogRow_CONSOLE",
								consoleOut_talendMeter_CONSOLE);
					}
					consoleOut_talendMeter_CONSOLE
							.println(strBuffer_talendMeter_CONSOLE.toString());
					consoleOut_talendMeter_CONSOLE.flush();
					nb_line_talendMeter_CONSOLE++;
					// ////

					// ////

					// /////////////////////

					tos_count_talendMeter_CONSOLE++;

					/**
					 * [talendMeter_CONSOLE main ] stop
					 */

					/**
					 * [talendMeter_CONSOLE process_data_begin ] start
					 */

					currentVirtualComponent = "talendMeter_CONSOLE";

					currentComponent = "talendMeter_CONSOLE";

					/**
					 * [talendMeter_CONSOLE process_data_begin ] stop
					 */

					/**
					 * [talendMeter_CONSOLE process_data_end ] start
					 */

					currentVirtualComponent = "talendMeter_CONSOLE";

					currentComponent = "talendMeter_CONSOLE";

					/**
					 * [talendMeter_CONSOLE process_data_end ] stop
					 */

					/**
					 * [talendMeter_METTER process_data_end ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

					/**
					 * [talendMeter_METTER process_data_end ] stop
					 */

					/**
					 * [talendMeter_METTER end ] start
					 */

					currentVirtualComponent = "talendMeter_METTER";

					currentComponent = "talendMeter_METTER";

				}

				ok_Hash.put("talendMeter_METTER", true);
				end_Hash.put("talendMeter_METTER", System.currentTimeMillis());

				/**
				 * [talendMeter_METTER end ] stop
				 */

				/**
				 * [talendMeter_CONSOLE end ] start
				 */

				currentVirtualComponent = "talendMeter_CONSOLE";

				currentComponent = "talendMeter_CONSOLE";

				// ////
				// ////
				globalMap.put("talendMeter_CONSOLE_NB_LINE",
						nb_line_talendMeter_CONSOLE);

				// /////////////////////

				if (execStat) {
					if (resourceMap.get("inIterateVComp") == null
							|| !((Boolean) resourceMap.get("inIterateVComp"))) {
						runStat.updateStatOnConnection("Main" + iterateId, 2, 0);
					}
				}

				ok_Hash.put("talendMeter_CONSOLE", true);
				end_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());

				/**
				 * [talendMeter_CONSOLE end ] stop
				 */

			}// end the resume

		} catch (java.lang.Exception e) {

			TalendException te = new TalendException(e, currentComponent,
					globalMap);

			te.setVirtualComponentName(currentVirtualComponent);

			throw te;
		} catch (java.lang.Error error) {

			runStat.stopThreadStat();

			throw error;
		} finally {

			try {

				/**
				 * [talendMeter_METTER finally ] start
				 */

				currentVirtualComponent = "talendMeter_METTER";

				currentComponent = "talendMeter_METTER";

				/**
				 * [talendMeter_METTER finally ] stop
				 */

				/**
				 * [talendMeter_CONSOLE finally ] start
				 */

				currentVirtualComponent = "talendMeter_CONSOLE";

				currentComponent = "talendMeter_CONSOLE";

				/**
				 * [talendMeter_CONSOLE finally ] stop
				 */

			} catch (java.lang.Exception e) {
				// ignore
			} catch (java.lang.Error error) {
				// ignore
			}
			resourceMap = null;
		}

		globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 1);
	}

	public String resuming_logs_dir_path = null;
	public String resuming_checkpoint_path = null;
	public String parent_part_launcher = null;
	private String resumeEntryMethodName = null;
	private boolean globalResumeTicket = false;

	public boolean watch = false;
	// portStats is null, it means don't execute the statistics
	public Integer portStats = null;
	public int portTraces = 4334;
	public String clientHost;
	public String defaultClientHost = "localhost";
	public String contextStr = "Default";
	public boolean isDefaultContext = true;
	public String pid = "0";
	public String rootPid = null;
	public String fatherPid = null;
	public String fatherNode = null;
	public long startTime = 0;
	public boolean isChildJob = false;
	public String log4jLevel = "";

	private boolean execStat = true;

	private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
		protected java.util.Map<String, String> initialValue() {
			java.util.Map<String, String> threadRunResultMap = new java.util.HashMap<String, String>();
			threadRunResultMap.put("errorCode", null);
			threadRunResultMap.put("status", "");
			return threadRunResultMap;
		};
	};

	private PropertiesWithType context_param = new PropertiesWithType();
	public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

	public String status = "";

	public static void main(String[] args) {
		final TRANS_CHARG_DW_VERSION_BIO TRANS_CHARG_DW_VERSION_BIOClass = new TRANS_CHARG_DW_VERSION_BIO();

		int exitCode = TRANS_CHARG_DW_VERSION_BIOClass.runJobInTOS(args);

		System.exit(exitCode);
	}

	public String[][] runJob(String[] args) {

		int exitCode = runJobInTOS(args);
		String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

		return bufferValue;
	}

	public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;

		return hastBufferOutput;
	}

	public int runJobInTOS(String[] args) {
		// reset status
		status = "";

		String lastStr = "";
		for (String arg : args) {
			if (arg.equalsIgnoreCase("--context_param")) {
				lastStr = arg;
			} else if (lastStr.equals("")) {
				evalParam(arg);
			} else {
				evalParam(lastStr + " " + arg);
				lastStr = "";
			}
		}

		if (clientHost == null) {
			clientHost = defaultClientHost;
		}

		if (pid == null || "0".equals(pid)) {
			pid = TalendString.getAsciiRandomString(6);
		}

		if (rootPid == null) {
			rootPid = pid;
		}
		if (fatherPid == null) {
			fatherPid = pid;
		} else {
			isChildJob = true;
		}

		if (portStats != null) {
			// portStats = -1; //for testing
			if (portStats < 0 || portStats > 65535) {
				// issue:10869, the portStats is invalid, so this client socket
				// can't open
				System.err.println("The statistics socket port " + portStats
						+ " is invalid.");
				execStat = false;
			}
		} else {
			execStat = false;
		}

		try {
			// call job/subjob with an existing context, like:
			// --context=production. if without this parameter, there will use
			// the default context instead.
			java.io.InputStream inContext = TRANS_CHARG_DW_VERSION_BIO.class
					.getClassLoader().getResourceAsStream(
							"dw_ter/trans_charg_dw_version_bio_0_1/contexts/"
									+ contextStr + ".properties");
			if (inContext == null) {
				inContext = TRANS_CHARG_DW_VERSION_BIO.class
						.getClassLoader()
						.getResourceAsStream(
								"config/contexts/" + contextStr + ".properties");
			}
			if (inContext != null) {
				// defaultProps is in order to keep the original context value
				defaultProps.load(inContext);
				inContext.close();
				context = new ContextProperties(defaultProps);
			} else if (!isDefaultContext) {
				// print info and job continue to run, for case: context_param
				// is not empty.
				System.err.println("Could not find the context " + contextStr);
			}

			if (!context_param.isEmpty()) {
				context.putAll(context_param);
				// set types for params from parentJobs
				for (Object key : context_param.keySet()) {
					String context_key = key.toString();
					String context_type = context_param
							.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
			}
		} catch (java.io.IOException ie) {
			System.err.println("Could not load context " + contextStr);
			ie.printStackTrace();
		}

		// get context value from parent directly
		if (parentContextMap != null && !parentContextMap.isEmpty()) {
		}

		// Resume: init the resumeUtil
		resumeEntryMethodName = ResumeUtil
				.getResumeEntryMethodName(resuming_checkpoint_path);
		resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
		resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName,
				jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
		// Resume: jobStart
		resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName,
				parent_part_launcher, Thread.currentThread().getId() + "", "",
				"", "", "",
				resumeUtil.convertToJsonText(context, parametersToEncrypt));

		if (execStat) {
			try {
				runStat.openSocket(!isChildJob);
				runStat.setAllPID(rootPid, fatherPid, pid, jobName);
				runStat.startThreadStat(clientHost, portStats);
				runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
			} catch (java.io.IOException ioException) {
				ioException.printStackTrace();
			}
		}

		java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
		globalMap.put("concurrentHashMap", concurrentHashMap);

		long startUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		long endUsedMemory = 0;
		long end = 0;

		startTime = System.currentTimeMillis();
		talendStats_STATS.addMessage("begin");

		this.globalResumeTicket = true;// to run tPreJob

		try {
			talendStats_STATSProcess(globalMap);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}

		this.globalResumeTicket = false;// to run others jobs

		try {
			errorCode = null;
			tDBInput_1Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tDBInput_1) {
			globalMap.put("tDBInput_1_SUBPROCESS_STATE", -1);

			e_tDBInput_1.printStackTrace();

		}
		try {
			errorCode = null;
			tDBInput_14Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tDBInput_14) {
			globalMap.put("tDBInput_14_SUBPROCESS_STATE", -1);

			e_tDBInput_14.printStackTrace();

		}
		try {
			errorCode = null;
			tDBInput_19Process(globalMap);
			if (!"failure".equals(status)) {
				status = "end";
			}
		} catch (TalendException e_tDBInput_19) {
			globalMap.put("tDBInput_19_SUBPROCESS_STATE", -1);

			e_tDBInput_19.printStackTrace();

		}

		this.globalResumeTicket = true;// to run tPostJob

		end = System.currentTimeMillis();

		if (watch) {
			System.out.println((end - startTime) + " milliseconds");
		}

		endUsedMemory = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		if (false) {
			System.out
					.println((endUsedMemory - startUsedMemory)
							+ " bytes memory increase when running : TRANS_CHARG_DW_VERSION_BIO");
		}
		talendStats_STATS.addMessage(status == "" ? "end" : status,
				(end - startTime));
		try {
			talendStats_STATSProcess(globalMap);
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}

		if (execStat) {
			runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
			runStat.stopThreadStat();
		}
		int returnCode = 0;
		if (errorCode == null) {
			returnCode = status != null && status.equals("failure") ? 1 : 0;
		} else {
			returnCode = errorCode.intValue();
		}
		resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher,
				Thread.currentThread().getId() + "", "", "" + returnCode, "",
				"", "");

		return returnCode;

	}

	// only for OSGi env
	public void destroy() {

	}

	private java.util.Map<String, Object> getSharedConnections4REST() {
		java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();

		return connections;
	}

	private void evalParam(String arg) {
		if (arg.startsWith("--resuming_logs_dir_path")) {
			resuming_logs_dir_path = arg.substring(25);
		} else if (arg.startsWith("--resuming_checkpoint_path")) {
			resuming_checkpoint_path = arg.substring(27);
		} else if (arg.startsWith("--parent_part_launcher")) {
			parent_part_launcher = arg.substring(23);
		} else if (arg.startsWith("--watch")) {
			watch = true;
		} else if (arg.startsWith("--stat_port=")) {
			String portStatsStr = arg.substring(12);
			if (portStatsStr != null && !portStatsStr.equals("null")) {
				portStats = Integer.parseInt(portStatsStr);
			}
		} else if (arg.startsWith("--trace_port=")) {
			portTraces = Integer.parseInt(arg.substring(13));
		} else if (arg.startsWith("--client_host=")) {
			clientHost = arg.substring(14);
		} else if (arg.startsWith("--context=")) {
			contextStr = arg.substring(10);
			isDefaultContext = false;
		} else if (arg.startsWith("--father_pid=")) {
			fatherPid = arg.substring(13);
		} else if (arg.startsWith("--root_pid=")) {
			rootPid = arg.substring(11);
		} else if (arg.startsWith("--father_node=")) {
			fatherNode = arg.substring(14);
		} else if (arg.startsWith("--pid=")) {
			pid = arg.substring(6);
		} else if (arg.startsWith("--context_type")) {
			String keyValue = arg.substring(15);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.setContextType(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.setContextType(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}

			}

		} else if (arg.startsWith("--context_param")) {
			String keyValue = arg.substring(16);
			int index = -1;
			if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
				if (fatherPid == null) {
					context_param.put(keyValue.substring(0, index),
							replaceEscapeChars(keyValue.substring(index + 1)));
				} else { // the subjob won't escape the especial chars
					context_param.put(keyValue.substring(0, index),
							keyValue.substring(index + 1));
				}
			}
		} else if (arg.startsWith("--log4jLevel=")) {
			log4jLevel = arg.substring(13);
		}

	}

	private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

	private final String[][] escapeChars = { { "\\\\", "\\" }, { "\\n", "\n" },
			{ "\\'", "\'" }, { "\\r", "\r" }, { "\\f", "\f" }, { "\\b", "\b" },
			{ "\\t", "\t" } };

	private String replaceEscapeChars(String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0], currIndex);
				if (index >= 0) {

					result.append(keyValue.substring(currIndex,
							index + strArray[0].length()).replace(strArray[0],
							strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left
			// into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public String getStatus() {
		return status;
	}

	ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 * 663326 characters generated by Talend Open Studio for Data Integration on the
 * 29 mai 2018 23:27:07 CEST
 ************************************************************************************************/
