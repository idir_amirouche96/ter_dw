
package dw_ter.sql_to_xsl_0_1;

import routines.Numeric;
import routines.DataOperation;
import routines.TalendDataGenerator;
import routines.TalendStringUtil;
import routines.TalendString;
import routines.StringHandling;
import routines.Relational;
import routines.TalendDate;
import routines.Mathematical;
import routines.system.*;
import routines.system.api.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.math.BigDecimal;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.Comparator;
 





@SuppressWarnings("unused")

/**
 * Job: SQL_TO_XSL Purpose: SQL_TO_XSL<br>
 * Description: SQL_TO_XSL <br>
 * @author belaidcherfa2012@hotmail.com
 * @version 7.0.1.20180411_1414
 * @status 
 */
public class SQL_TO_XSL implements TalendJob {

protected static void logIgnoredError(String message, Throwable cause) {
       System.err.println(message);
       if (cause != null) {
               cause.printStackTrace();
       }

}


	public final Object obj = new Object();

	// for transmiting parameters purpose
	private Object valueObject = null;

	public Object getValueObject() {
		return this.valueObject;
	}

	public void setValueObject(Object valueObject) {
		this.valueObject = valueObject;
	}
	
	private final static String defaultCharset = java.nio.charset.Charset.defaultCharset().name();

	
	private final static String utf8Charset = "UTF-8";
	//contains type for every context property
	public class PropertiesWithType extends java.util.Properties {
		private static final long serialVersionUID = 1L;
		private java.util.Map<String,String> propertyTypes = new java.util.HashMap<>();
		
		public PropertiesWithType(java.util.Properties properties){
			super(properties);
		}
		public PropertiesWithType(){
			super();
		}
		
		public void setContextType(String key, String type) {
			propertyTypes.put(key,type);
		}
	
		public String getContextType(String key) {
			return propertyTypes.get(key);
		}
	}
	
	// create and load default properties
	private java.util.Properties defaultProps = new java.util.Properties();
	// create application properties with default
	public class ContextProperties extends PropertiesWithType {

		private static final long serialVersionUID = 1L;

		public ContextProperties(java.util.Properties properties){
			super(properties);
		}
		public ContextProperties(){
			super();
		}

		public void synchronizeContext(){
			
		}

	}
	private ContextProperties context = new ContextProperties();
	public ContextProperties getContext() {
		return this.context;
	}
	private final String jobVersion = "0.1";
	private final String jobName = "SQL_TO_XSL";
	private final String projectName = "DW_TER";
	public Integer errorCode = null;
	private String currentComponent = "";
	
		private final java.util.Map<String, Object> globalMap = new java.util.HashMap<String, Object>();
        private final static java.util.Map<String, Object> junitGlobalMap = new java.util.HashMap<String, Object>();
	
		private final java.util.Map<String, Long> start_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Long> end_Hash = new java.util.HashMap<String, Long>();
		private final java.util.Map<String, Boolean> ok_Hash = new java.util.HashMap<String, Boolean>();
		public  final java.util.List<String[]> globalBuffer = new java.util.ArrayList<String[]>();
	

private RunStat runStat = new RunStat();

	// OSGi DataSource
	private final static String KEY_DB_DATASOURCES = "KEY_DB_DATASOURCES";
	
	private final static String KEY_DB_DATASOURCES_RAW = "KEY_DB_DATASOURCES_RAW";

	public void setDataSources(java.util.Map<String, javax.sql.DataSource> dataSources) {
		java.util.Map<String, routines.system.TalendDataSource> talendDataSources = new java.util.HashMap<String, routines.system.TalendDataSource>();
		for (java.util.Map.Entry<String, javax.sql.DataSource> dataSourceEntry : dataSources.entrySet()) {
			talendDataSources.put(dataSourceEntry.getKey(), new routines.system.TalendDataSource(dataSourceEntry.getValue()));
		}
		globalMap.put(KEY_DB_DATASOURCES, talendDataSources);
		globalMap.put(KEY_DB_DATASOURCES_RAW, new java.util.HashMap<String, javax.sql.DataSource>(dataSources));
	}

	LogCatcherUtils talendLogs_LOGS = new LogCatcherUtils();
	StatCatcherUtils talendStats_STATS = new StatCatcherUtils("_NjcE4GOhEeiYfcKGxutq_Q", "0.1");
	MetterCatcherUtils talendMeter_METTER = new MetterCatcherUtils("_NjcE4GOhEeiYfcKGxutq_Q", "0.1");

private final java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
private final java.io.PrintStream errorMessagePS = new java.io.PrintStream(new java.io.BufferedOutputStream(baos));

public String getExceptionStackTrace() {
	if ("failure".equals(this.getStatus())) {
		errorMessagePS.flush();
		return baos.toString();
	}
	return null;
}

private Exception exception;

public Exception getException() {
	if ("failure".equals(this.getStatus())) {
		return this.exception;
	}
	return null;
}

private class TalendException extends Exception {

	private static final long serialVersionUID = 1L;

	private java.util.Map<String, Object> globalMap = null;
	private Exception e = null;
	private String currentComponent = null;
	private String virtualComponentName = null;
	
	public void setVirtualComponentName (String virtualComponentName){
		this.virtualComponentName = virtualComponentName;
	}

	private TalendException(Exception e, String errorComponent, final java.util.Map<String, Object> globalMap) {
		this.currentComponent= errorComponent;
		this.globalMap = globalMap;
		this.e = e;
	}

	public Exception getException() {
		return this.e;
	}

	public String getCurrentComponent() {
		return this.currentComponent;
	}

	
    public String getExceptionCauseMessage(Exception e){
        Throwable cause = e;
        String message = null;
        int i = 10;
        while (null != cause && 0 < i--) {
            message = cause.getMessage();
            if (null == message) {
                cause = cause.getCause();
            } else {
                break;          
            }
        }
        if (null == message) {
            message = e.getClass().getName();
        }   
        return message;
    }

	@Override
	public void printStackTrace() {
		if (!(e instanceof TalendException || e instanceof TDieException)) {
			if(virtualComponentName!=null && currentComponent.indexOf(virtualComponentName+"_")==0){
				globalMap.put(virtualComponentName+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			}
			globalMap.put(currentComponent+"_ERROR_MESSAGE",getExceptionCauseMessage(e));
			System.err.println("Exception in component " + currentComponent + " (" + jobName + ")");
		}
		if (!(e instanceof TDieException)) {
			if(e instanceof TalendException){
				e.printStackTrace();
			} else {
				e.printStackTrace();
				e.printStackTrace(errorMessagePS);
				SQL_TO_XSL.this.exception = e;
			}
		}
		if (!(e instanceof TalendException)) {
		try {
			for (java.lang.reflect.Method m : this.getClass().getEnclosingClass().getMethods()) {
				if (m.getName().compareTo(currentComponent + "_error") == 0) {
					m.invoke(SQL_TO_XSL.this, new Object[] { e , currentComponent, globalMap});
					break;
				}
			}

			if(!(e instanceof TDieException)){
				talendLogs_LOGS.addMessage("Java Exception", currentComponent, 6, e.getClass().getName() + ":" + e.getMessage(), 1);
				talendLogs_LOGSProcess(globalMap);
			}
				} catch (TalendException e) {
					// do nothing
				
		} catch (Exception e) {
			this.e.printStackTrace();
		}
		}
	}
}

			public void tDBInput_2_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tMap_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tFileOutputExcel_1_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					tDBInput_2_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendStats_STATS_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendStats_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendStats_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendStats_STATS_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendLogs_LOGS_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendLogs_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendLogs_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendLogs_LOGS_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void talendMeter_METTER_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
							talendMeter_CONSOLE_error(exception, errorComponent, globalMap);
						
						}
					
			public void talendMeter_CONSOLE_error(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {
				
				end_Hash.put(errorComponent, System.currentTimeMillis());
				
				status = "failure";
				
					talendMeter_METTER_onSubJobError(exception, errorComponent, globalMap);
			}
			
			public void tDBInput_2_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendStats_STATS_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendLogs_LOGS_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
			public void talendMeter_METTER_onSubJobError(Exception exception, String errorComponent, final java.util.Map<String, Object> globalMap) throws TalendException {

resumeUtil.addLog("SYSTEM_LOG", "NODE:"+ errorComponent, "", Thread.currentThread().getId()+ "", "FATAL", "", exception.getMessage(), ResumeUtil.getExceptionStackTrace(exception),"");

			}
		






public static class OUT1Struct implements routines.system.IPersistableRow<OUT1Struct> {
    final static byte[] commonByteArrayLock_DW_TER_SQL_TO_XSL = new byte[0];
    static byte[] commonByteArray_DW_TER_SQL_TO_XSL = new byte[0];
	protected static final int DEFAULT_HASHCODE = 1;
    protected static final int PRIME = 31;
    protected int hashCode = DEFAULT_HASHCODE;
    public boolean hashCodeDirty = true;

    public String loopKey;



	
			    public int codedept;

				public int getCodedept () {
					return this.codedept;
				}
				
			    public int enseignantID;

				public int getEnseignantID () {
					return this.enseignantID;
				}
				
			    public int ANNNE_ENS;

				public int getANNNE_ENS () {
					return this.ANNNE_ENS;
				}
				
			    public int codesemestre;

				public int getCodesemestre () {
					return this.codesemestre;
				}
				
			    public int codemodsemestre;

				public int getCodemodsemestre () {
					return this.codemodsemestre;
				}
				
			    public int ID_RESP_MOD;

				public int getID_RESP_MOD () {
					return this.ID_RESP_MOD;
				}
				
			    public int DIRECTEUR_DPT;

				public int getDIRECTEUR_DPT () {
					return this.DIRECTEUR_DPT;
				}
				
			    public int departement;

				public int getDepartement () {
					return this.departement;
				}
				
			    public String NOM_SEMESTRE;

				public String getNOM_SEMESTRE () {
					return this.NOM_SEMESTRE;
				}
				
			    public int ID_RES_SEM;

				public int getID_RES_SEM () {
					return this.ID_RES_SEM;
				}
				
			    public String NOM_PRENOM_ENS;

				public String getNOM_PRENOM_ENS () {
					return this.NOM_PRENOM_ENS;
				}
				
			    public int codegrade;

				public int getCodegrade () {
					return this.codegrade;
				}
				
			    public String codecourt;

				public String getCodecourt () {
					return this.codecourt;
				}
				
			    public String nomlong;

				public String getNomlong () {
					return this.nomlong;
				}
				
			    public String type;

				public String getType () {
					return this.type;
				}
				
			    public int HEURE_SELON_GRADE;

				public int getHEURE_SELON_GRADE () {
					return this.HEURE_SELON_GRADE;
				}
				
			    public String INTITULE_MODULE;

				public String getINTITULE_MODULE () {
					return this.INTITULE_MODULE;
				}
				
			    public BigDecimal heuresCM;

				public BigDecimal getHeuresCM () {
					return this.heuresCM;
				}
				
			    public BigDecimal heuresTD;

				public BigDecimal getHeuresTD () {
					return this.heuresTD;
				}
				
			    public BigDecimal heuresTP;

				public BigDecimal getHeuresTP () {
					return this.heuresTP;
				}
				
			    public String codeprefixe;

				public String getCodeprefixe () {
					return this.codeprefixe;
				}
				
			    public BigDecimal CM_A_FAIRE;

				public BigDecimal getCM_A_FAIRE () {
					return this.CM_A_FAIRE;
				}
				
			    public BigDecimal TD_A_FAIRE;

				public BigDecimal getTD_A_FAIRE () {
					return this.TD_A_FAIRE;
				}
				
			    public BigDecimal TP_A_FAIRE;

				public BigDecimal getTP_A_FAIRE () {
					return this.TP_A_FAIRE;
				}
				
			    public double HEURES_A_FAIRE;

				public double getHEURES_A_FAIRE () {
					return this.HEURES_A_FAIRE;
				}
				
			    public BigDecimal CM_FAITES;

				public BigDecimal getCM_FAITES () {
					return this.CM_FAITES;
				}
				
			    public BigDecimal TD_FAITES;

				public BigDecimal getTD_FAITES () {
					return this.TD_FAITES;
				}
				
			    public BigDecimal TP_FAITES;

				public BigDecimal getTP_FAITES () {
					return this.TP_FAITES;
				}
				
			    public double HEURES_FAITES;

				public double getHEURES_FAITES () {
					return this.HEURES_FAITES;
				}
				
			    public BigDecimal BILAN_CM;

				public BigDecimal getBILAN_CM () {
					return this.BILAN_CM;
				}
				
			    public BigDecimal BILAN_TD;

				public BigDecimal getBILAN_TD () {
					return this.BILAN_TD;
				}
				
			    public BigDecimal BILAN_TP;

				public BigDecimal getBILAN_TP () {
					return this.BILAN_TP;
				}
				
			    public double BILAN_UE;

				public double getBILAN_UE () {
					return this.BILAN_UE;
				}
				


	@Override
	public int hashCode() {
		if (this.hashCodeDirty) {
			final int prime = PRIME;
			int result = DEFAULT_HASHCODE;
	
							result = prime * result + (int) this.codedept;
						
							result = prime * result + (int) this.enseignantID;
						
							result = prime * result + (int) this.ANNNE_ENS;
						
							result = prime * result + (int) this.codesemestre;
						
							result = prime * result + (int) this.codemodsemestre;
						
    		this.hashCode = result;
    		this.hashCodeDirty = false;
		}
		return this.hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final OUT1Struct other = (OUT1Struct) obj;
		
						if (this.codedept != other.codedept)
							return false;
					
						if (this.enseignantID != other.enseignantID)
							return false;
					
						if (this.ANNNE_ENS != other.ANNNE_ENS)
							return false;
					
						if (this.codesemestre != other.codesemestre)
							return false;
					
						if (this.codemodsemestre != other.codemodsemestre)
							return false;
					

		return true;
    }

	public void copyDataTo(OUT1Struct other) {

		other.codedept = this.codedept;
	            other.enseignantID = this.enseignantID;
	            other.ANNNE_ENS = this.ANNNE_ENS;
	            other.codesemestre = this.codesemestre;
	            other.codemodsemestre = this.codemodsemestre;
	            other.ID_RESP_MOD = this.ID_RESP_MOD;
	            other.DIRECTEUR_DPT = this.DIRECTEUR_DPT;
	            other.departement = this.departement;
	            other.NOM_SEMESTRE = this.NOM_SEMESTRE;
	            other.ID_RES_SEM = this.ID_RES_SEM;
	            other.NOM_PRENOM_ENS = this.NOM_PRENOM_ENS;
	            other.codegrade = this.codegrade;
	            other.codecourt = this.codecourt;
	            other.nomlong = this.nomlong;
	            other.type = this.type;
	            other.HEURE_SELON_GRADE = this.HEURE_SELON_GRADE;
	            other.INTITULE_MODULE = this.INTITULE_MODULE;
	            other.heuresCM = this.heuresCM;
	            other.heuresTD = this.heuresTD;
	            other.heuresTP = this.heuresTP;
	            other.codeprefixe = this.codeprefixe;
	            other.CM_A_FAIRE = this.CM_A_FAIRE;
	            other.TD_A_FAIRE = this.TD_A_FAIRE;
	            other.TP_A_FAIRE = this.TP_A_FAIRE;
	            other.HEURES_A_FAIRE = this.HEURES_A_FAIRE;
	            other.CM_FAITES = this.CM_FAITES;
	            other.TD_FAITES = this.TD_FAITES;
	            other.TP_FAITES = this.TP_FAITES;
	            other.HEURES_FAITES = this.HEURES_FAITES;
	            other.BILAN_CM = this.BILAN_CM;
	            other.BILAN_TD = this.BILAN_TD;
	            other.BILAN_TP = this.BILAN_TP;
	            other.BILAN_UE = this.BILAN_UE;
	            
	}

	public void copyKeysDataTo(OUT1Struct other) {

		other.codedept = this.codedept;
	            	other.enseignantID = this.enseignantID;
	            	other.ANNNE_ENS = this.ANNNE_ENS;
	            	other.codesemestre = this.codesemestre;
	            	other.codemodsemestre = this.codemodsemestre;
	            	
	}




	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_DW_TER_SQL_TO_XSL.length) {
				if(length < 1024 && commonByteArray_DW_TER_SQL_TO_XSL.length == 0) {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[1024];
				} else {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_DW_TER_SQL_TO_XSL, 0, length);
			strReturn = new String(commonByteArray_DW_TER_SQL_TO_XSL, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_DW_TER_SQL_TO_XSL) {

        	try {

        		int length = 0;
		
			        this.codedept = dis.readInt();
					
			        this.enseignantID = dis.readInt();
					
			        this.ANNNE_ENS = dis.readInt();
					
			        this.codesemestre = dis.readInt();
					
			        this.codemodsemestre = dis.readInt();
					
			        this.ID_RESP_MOD = dis.readInt();
					
			        this.DIRECTEUR_DPT = dis.readInt();
					
			        this.departement = dis.readInt();
					
					this.NOM_SEMESTRE = readString(dis);
					
			        this.ID_RES_SEM = dis.readInt();
					
					this.NOM_PRENOM_ENS = readString(dis);
					
			        this.codegrade = dis.readInt();
					
					this.codecourt = readString(dis);
					
					this.nomlong = readString(dis);
					
					this.type = readString(dis);
					
			        this.HEURE_SELON_GRADE = dis.readInt();
					
					this.INTITULE_MODULE = readString(dis);
					
						this.heuresCM = (BigDecimal) dis.readObject();
					
						this.heuresTD = (BigDecimal) dis.readObject();
					
						this.heuresTP = (BigDecimal) dis.readObject();
					
					this.codeprefixe = readString(dis);
					
						this.CM_A_FAIRE = (BigDecimal) dis.readObject();
					
						this.TD_A_FAIRE = (BigDecimal) dis.readObject();
					
						this.TP_A_FAIRE = (BigDecimal) dis.readObject();
					
			        this.HEURES_A_FAIRE = dis.readDouble();
					
						this.CM_FAITES = (BigDecimal) dis.readObject();
					
						this.TD_FAITES = (BigDecimal) dis.readObject();
					
						this.TP_FAITES = (BigDecimal) dis.readObject();
					
			        this.HEURES_FAITES = dis.readDouble();
					
						this.BILAN_CM = (BigDecimal) dis.readObject();
					
						this.BILAN_TD = (BigDecimal) dis.readObject();
					
						this.BILAN_TP = (BigDecimal) dis.readObject();
					
			        this.BILAN_UE = dis.readDouble();
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// int
				
		            	dos.writeInt(this.codedept);
					
					// int
				
		            	dos.writeInt(this.enseignantID);
					
					// int
				
		            	dos.writeInt(this.ANNNE_ENS);
					
					// int
				
		            	dos.writeInt(this.codesemestre);
					
					// int
				
		            	dos.writeInt(this.codemodsemestre);
					
					// int
				
		            	dos.writeInt(this.ID_RESP_MOD);
					
					// int
				
		            	dos.writeInt(this.DIRECTEUR_DPT);
					
					// int
				
		            	dos.writeInt(this.departement);
					
					// String
				
						writeString(this.NOM_SEMESTRE,dos);
					
					// int
				
		            	dos.writeInt(this.ID_RES_SEM);
					
					// String
				
						writeString(this.NOM_PRENOM_ENS,dos);
					
					// int
				
		            	dos.writeInt(this.codegrade);
					
					// String
				
						writeString(this.codecourt,dos);
					
					// String
				
						writeString(this.nomlong,dos);
					
					// String
				
						writeString(this.type,dos);
					
					// int
				
		            	dos.writeInt(this.HEURE_SELON_GRADE);
					
					// String
				
						writeString(this.INTITULE_MODULE,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.heuresCM);
					
					// BigDecimal
				
       			    	dos.writeObject(this.heuresTD);
					
					// BigDecimal
				
       			    	dos.writeObject(this.heuresTP);
					
					// String
				
						writeString(this.codeprefixe,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CM_A_FAIRE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TD_A_FAIRE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TP_A_FAIRE);
					
					// double
				
		            	dos.writeDouble(this.HEURES_A_FAIRE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CM_FAITES);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TD_FAITES);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TP_FAITES);
					
					// double
				
		            	dos.writeDouble(this.HEURES_FAITES);
					
					// BigDecimal
				
       			    	dos.writeObject(this.BILAN_CM);
					
					// BigDecimal
				
       			    	dos.writeObject(this.BILAN_TD);
					
					// BigDecimal
				
       			    	dos.writeObject(this.BILAN_TP);
					
					// double
				
		            	dos.writeDouble(this.BILAN_UE);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("codedept="+String.valueOf(codedept));
		sb.append(",enseignantID="+String.valueOf(enseignantID));
		sb.append(",ANNNE_ENS="+String.valueOf(ANNNE_ENS));
		sb.append(",codesemestre="+String.valueOf(codesemestre));
		sb.append(",codemodsemestre="+String.valueOf(codemodsemestre));
		sb.append(",ID_RESP_MOD="+String.valueOf(ID_RESP_MOD));
		sb.append(",DIRECTEUR_DPT="+String.valueOf(DIRECTEUR_DPT));
		sb.append(",departement="+String.valueOf(departement));
		sb.append(",NOM_SEMESTRE="+NOM_SEMESTRE);
		sb.append(",ID_RES_SEM="+String.valueOf(ID_RES_SEM));
		sb.append(",NOM_PRENOM_ENS="+NOM_PRENOM_ENS);
		sb.append(",codegrade="+String.valueOf(codegrade));
		sb.append(",codecourt="+codecourt);
		sb.append(",nomlong="+nomlong);
		sb.append(",type="+type);
		sb.append(",HEURE_SELON_GRADE="+String.valueOf(HEURE_SELON_GRADE));
		sb.append(",INTITULE_MODULE="+INTITULE_MODULE);
		sb.append(",heuresCM="+String.valueOf(heuresCM));
		sb.append(",heuresTD="+String.valueOf(heuresTD));
		sb.append(",heuresTP="+String.valueOf(heuresTP));
		sb.append(",codeprefixe="+codeprefixe);
		sb.append(",CM_A_FAIRE="+String.valueOf(CM_A_FAIRE));
		sb.append(",TD_A_FAIRE="+String.valueOf(TD_A_FAIRE));
		sb.append(",TP_A_FAIRE="+String.valueOf(TP_A_FAIRE));
		sb.append(",HEURES_A_FAIRE="+String.valueOf(HEURES_A_FAIRE));
		sb.append(",CM_FAITES="+String.valueOf(CM_FAITES));
		sb.append(",TD_FAITES="+String.valueOf(TD_FAITES));
		sb.append(",TP_FAITES="+String.valueOf(TP_FAITES));
		sb.append(",HEURES_FAITES="+String.valueOf(HEURES_FAITES));
		sb.append(",BILAN_CM="+String.valueOf(BILAN_CM));
		sb.append(",BILAN_TD="+String.valueOf(BILAN_TD));
		sb.append(",BILAN_TP="+String.valueOf(BILAN_TP));
		sb.append(",BILAN_UE="+String.valueOf(BILAN_UE));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(OUT1Struct other) {

		int returnValue = -1;
		
						returnValue = checkNullsAndCompare(this.codedept, other.codedept);
						if(returnValue != 0) {
							return returnValue;
						}

					
						returnValue = checkNullsAndCompare(this.enseignantID, other.enseignantID);
						if(returnValue != 0) {
							return returnValue;
						}

					
						returnValue = checkNullsAndCompare(this.ANNNE_ENS, other.ANNNE_ENS);
						if(returnValue != 0) {
							return returnValue;
						}

					
						returnValue = checkNullsAndCompare(this.codesemestre, other.codesemestre);
						if(returnValue != 0) {
							return returnValue;
						}

					
						returnValue = checkNullsAndCompare(this.codemodsemestre, other.codemodsemestre);
						if(returnValue != 0) {
							return returnValue;
						}

					
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}

public static class row1Struct implements routines.system.IPersistableRow<row1Struct> {
    final static byte[] commonByteArrayLock_DW_TER_SQL_TO_XSL = new byte[0];
    static byte[] commonByteArray_DW_TER_SQL_TO_XSL = new byte[0];

	
			    public int codedept;

				public int getCodedept () {
					return this.codedept;
				}
				
			    public int enseignantID;

				public int getEnseignantID () {
					return this.enseignantID;
				}
				
			    public int ANNNE_ENS;

				public int getANNNE_ENS () {
					return this.ANNNE_ENS;
				}
				
			    public int codesemestre;

				public int getCodesemestre () {
					return this.codesemestre;
				}
				
			    public int codemodsemestre;

				public int getCodemodsemestre () {
					return this.codemodsemestre;
				}
				
			    public int ID_RESP_MOD;

				public int getID_RESP_MOD () {
					return this.ID_RESP_MOD;
				}
				
			    public int DIRECTEUR_DPT;

				public int getDIRECTEUR_DPT () {
					return this.DIRECTEUR_DPT;
				}
				
			    public int departement;

				public int getDepartement () {
					return this.departement;
				}
				
			    public String NOM_SEMESTRE;

				public String getNOM_SEMESTRE () {
					return this.NOM_SEMESTRE;
				}
				
			    public int ID_RES_SEM;

				public int getID_RES_SEM () {
					return this.ID_RES_SEM;
				}
				
			    public String NOM_PRENOM_ENS;

				public String getNOM_PRENOM_ENS () {
					return this.NOM_PRENOM_ENS;
				}
				
			    public int codegrade;

				public int getCodegrade () {
					return this.codegrade;
				}
				
			    public String codecourt;

				public String getCodecourt () {
					return this.codecourt;
				}
				
			    public String nomlong;

				public String getNomlong () {
					return this.nomlong;
				}
				
			    public String type;

				public String getType () {
					return this.type;
				}
				
			    public int HEURE_SELON_GRADE;

				public int getHEURE_SELON_GRADE () {
					return this.HEURE_SELON_GRADE;
				}
				
			    public String INTITULE_MODULE;

				public String getINTITULE_MODULE () {
					return this.INTITULE_MODULE;
				}
				
			    public BigDecimal heuresCM;

				public BigDecimal getHeuresCM () {
					return this.heuresCM;
				}
				
			    public BigDecimal heuresTD;

				public BigDecimal getHeuresTD () {
					return this.heuresTD;
				}
				
			    public BigDecimal heuresTP;

				public BigDecimal getHeuresTP () {
					return this.heuresTP;
				}
				
			    public String codeprefixe;

				public String getCodeprefixe () {
					return this.codeprefixe;
				}
				
			    public BigDecimal CM_A_FAIRE;

				public BigDecimal getCM_A_FAIRE () {
					return this.CM_A_FAIRE;
				}
				
			    public BigDecimal TD_A_FAIRE;

				public BigDecimal getTD_A_FAIRE () {
					return this.TD_A_FAIRE;
				}
				
			    public BigDecimal TP_A_FAIRE;

				public BigDecimal getTP_A_FAIRE () {
					return this.TP_A_FAIRE;
				}
				
			    public double HEURES_A_FAIRE;

				public double getHEURES_A_FAIRE () {
					return this.HEURES_A_FAIRE;
				}
				
			    public BigDecimal CM_FAITES;

				public BigDecimal getCM_FAITES () {
					return this.CM_FAITES;
				}
				
			    public BigDecimal TD_FAITES;

				public BigDecimal getTD_FAITES () {
					return this.TD_FAITES;
				}
				
			    public BigDecimal TP_FAITES;

				public BigDecimal getTP_FAITES () {
					return this.TP_FAITES;
				}
				
			    public double HEURES_FAITES;

				public double getHEURES_FAITES () {
					return this.HEURES_FAITES;
				}
				
			    public BigDecimal BILAN_CM;

				public BigDecimal getBILAN_CM () {
					return this.BILAN_CM;
				}
				
			    public BigDecimal BILAN_TD;

				public BigDecimal getBILAN_TD () {
					return this.BILAN_TD;
				}
				
			    public BigDecimal BILAN_TP;

				public BigDecimal getBILAN_TP () {
					return this.BILAN_TP;
				}
				
			    public double BILAN_UE;

				public double getBILAN_UE () {
					return this.BILAN_UE;
				}
				



	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_DW_TER_SQL_TO_XSL.length) {
				if(length < 1024 && commonByteArray_DW_TER_SQL_TO_XSL.length == 0) {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[1024];
				} else {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_DW_TER_SQL_TO_XSL, 0, length);
			strReturn = new String(commonByteArray_DW_TER_SQL_TO_XSL, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_DW_TER_SQL_TO_XSL) {

        	try {

        		int length = 0;
		
			        this.codedept = dis.readInt();
					
			        this.enseignantID = dis.readInt();
					
			        this.ANNNE_ENS = dis.readInt();
					
			        this.codesemestre = dis.readInt();
					
			        this.codemodsemestre = dis.readInt();
					
			        this.ID_RESP_MOD = dis.readInt();
					
			        this.DIRECTEUR_DPT = dis.readInt();
					
			        this.departement = dis.readInt();
					
					this.NOM_SEMESTRE = readString(dis);
					
			        this.ID_RES_SEM = dis.readInt();
					
					this.NOM_PRENOM_ENS = readString(dis);
					
			        this.codegrade = dis.readInt();
					
					this.codecourt = readString(dis);
					
					this.nomlong = readString(dis);
					
					this.type = readString(dis);
					
			        this.HEURE_SELON_GRADE = dis.readInt();
					
					this.INTITULE_MODULE = readString(dis);
					
						this.heuresCM = (BigDecimal) dis.readObject();
					
						this.heuresTD = (BigDecimal) dis.readObject();
					
						this.heuresTP = (BigDecimal) dis.readObject();
					
					this.codeprefixe = readString(dis);
					
						this.CM_A_FAIRE = (BigDecimal) dis.readObject();
					
						this.TD_A_FAIRE = (BigDecimal) dis.readObject();
					
						this.TP_A_FAIRE = (BigDecimal) dis.readObject();
					
			        this.HEURES_A_FAIRE = dis.readDouble();
					
						this.CM_FAITES = (BigDecimal) dis.readObject();
					
						this.TD_FAITES = (BigDecimal) dis.readObject();
					
						this.TP_FAITES = (BigDecimal) dis.readObject();
					
			        this.HEURES_FAITES = dis.readDouble();
					
						this.BILAN_CM = (BigDecimal) dis.readObject();
					
						this.BILAN_TD = (BigDecimal) dis.readObject();
					
						this.BILAN_TP = (BigDecimal) dis.readObject();
					
			        this.BILAN_UE = dis.readDouble();
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		
			} catch(ClassNotFoundException eCNFE) {
				 throw new RuntimeException(eCNFE);
		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// int
				
		            	dos.writeInt(this.codedept);
					
					// int
				
		            	dos.writeInt(this.enseignantID);
					
					// int
				
		            	dos.writeInt(this.ANNNE_ENS);
					
					// int
				
		            	dos.writeInt(this.codesemestre);
					
					// int
				
		            	dos.writeInt(this.codemodsemestre);
					
					// int
				
		            	dos.writeInt(this.ID_RESP_MOD);
					
					// int
				
		            	dos.writeInt(this.DIRECTEUR_DPT);
					
					// int
				
		            	dos.writeInt(this.departement);
					
					// String
				
						writeString(this.NOM_SEMESTRE,dos);
					
					// int
				
		            	dos.writeInt(this.ID_RES_SEM);
					
					// String
				
						writeString(this.NOM_PRENOM_ENS,dos);
					
					// int
				
		            	dos.writeInt(this.codegrade);
					
					// String
				
						writeString(this.codecourt,dos);
					
					// String
				
						writeString(this.nomlong,dos);
					
					// String
				
						writeString(this.type,dos);
					
					// int
				
		            	dos.writeInt(this.HEURE_SELON_GRADE);
					
					// String
				
						writeString(this.INTITULE_MODULE,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.heuresCM);
					
					// BigDecimal
				
       			    	dos.writeObject(this.heuresTD);
					
					// BigDecimal
				
       			    	dos.writeObject(this.heuresTP);
					
					// String
				
						writeString(this.codeprefixe,dos);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CM_A_FAIRE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TD_A_FAIRE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TP_A_FAIRE);
					
					// double
				
		            	dos.writeDouble(this.HEURES_A_FAIRE);
					
					// BigDecimal
				
       			    	dos.writeObject(this.CM_FAITES);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TD_FAITES);
					
					// BigDecimal
				
       			    	dos.writeObject(this.TP_FAITES);
					
					// double
				
		            	dos.writeDouble(this.HEURES_FAITES);
					
					// BigDecimal
				
       			    	dos.writeObject(this.BILAN_CM);
					
					// BigDecimal
				
       			    	dos.writeObject(this.BILAN_TD);
					
					// BigDecimal
				
       			    	dos.writeObject(this.BILAN_TP);
					
					// double
				
		            	dos.writeDouble(this.BILAN_UE);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("codedept="+String.valueOf(codedept));
		sb.append(",enseignantID="+String.valueOf(enseignantID));
		sb.append(",ANNNE_ENS="+String.valueOf(ANNNE_ENS));
		sb.append(",codesemestre="+String.valueOf(codesemestre));
		sb.append(",codemodsemestre="+String.valueOf(codemodsemestre));
		sb.append(",ID_RESP_MOD="+String.valueOf(ID_RESP_MOD));
		sb.append(",DIRECTEUR_DPT="+String.valueOf(DIRECTEUR_DPT));
		sb.append(",departement="+String.valueOf(departement));
		sb.append(",NOM_SEMESTRE="+NOM_SEMESTRE);
		sb.append(",ID_RES_SEM="+String.valueOf(ID_RES_SEM));
		sb.append(",NOM_PRENOM_ENS="+NOM_PRENOM_ENS);
		sb.append(",codegrade="+String.valueOf(codegrade));
		sb.append(",codecourt="+codecourt);
		sb.append(",nomlong="+nomlong);
		sb.append(",type="+type);
		sb.append(",HEURE_SELON_GRADE="+String.valueOf(HEURE_SELON_GRADE));
		sb.append(",INTITULE_MODULE="+INTITULE_MODULE);
		sb.append(",heuresCM="+String.valueOf(heuresCM));
		sb.append(",heuresTD="+String.valueOf(heuresTD));
		sb.append(",heuresTP="+String.valueOf(heuresTP));
		sb.append(",codeprefixe="+codeprefixe);
		sb.append(",CM_A_FAIRE="+String.valueOf(CM_A_FAIRE));
		sb.append(",TD_A_FAIRE="+String.valueOf(TD_A_FAIRE));
		sb.append(",TP_A_FAIRE="+String.valueOf(TP_A_FAIRE));
		sb.append(",HEURES_A_FAIRE="+String.valueOf(HEURES_A_FAIRE));
		sb.append(",CM_FAITES="+String.valueOf(CM_FAITES));
		sb.append(",TD_FAITES="+String.valueOf(TD_FAITES));
		sb.append(",TP_FAITES="+String.valueOf(TP_FAITES));
		sb.append(",HEURES_FAITES="+String.valueOf(HEURES_FAITES));
		sb.append(",BILAN_CM="+String.valueOf(BILAN_CM));
		sb.append(",BILAN_TD="+String.valueOf(BILAN_TD));
		sb.append(",BILAN_TP="+String.valueOf(BILAN_TP));
		sb.append(",BILAN_UE="+String.valueOf(BILAN_UE));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row1Struct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void tDBInput_2Process(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("tDBInput_2_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row1Struct row1 = new row1Struct();
OUT1Struct OUT1 = new OUT1Struct();





	
	/**
	 * [tFileOutputExcel_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tFileOutputExcel_1", false);
		start_Hash.put("tFileOutputExcel_1", System.currentTimeMillis());
		
	
	currentComponent="tFileOutputExcel_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("OUT1" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tFileOutputExcel_1 = 0;
		
    	class BytesLimit65535_tFileOutputExcel_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tFileOutputExcel_1().limitLog4jByte();


		int columnIndex_tFileOutputExcel_1 = 0;
		
		String fileName_tFileOutputExcel_1="data/DW_DATA.xls";
		int nb_line_tFileOutputExcel_1 = 0;
		org.talend.ExcelTool xlsxTool_tFileOutputExcel_1 = new org.talend.ExcelTool();
		xlsxTool_tFileOutputExcel_1.setSheet("Sheet1");
		xlsxTool_tFileOutputExcel_1.setAppend(false,false);
		xlsxTool_tFileOutputExcel_1.setRecalculateFormula(false);
		xlsxTool_tFileOutputExcel_1.setXY(false,0,0,false);
		
		xlsxTool_tFileOutputExcel_1.prepareXlsxFile(fileName_tFileOutputExcel_1);
		
		xlsxTool_tFileOutputExcel_1.setFont("");
		
		if (xlsxTool_tFileOutputExcel_1.getStartRow() == 0){
		
		xlsxTool_tFileOutputExcel_1.addRow();
		
		xlsxTool_tFileOutputExcel_1.addCellValue("codedept");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("enseignantID");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("ANNNE_ENS");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("codesemestre");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("codemodsemestre");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("ID_RESP_MOD");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("DIRECTEUR_DPT");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("departement");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("NOM_SEMESTRE");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("ID_RES_SEM");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("NOM_PRENOM_ENS");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("codegrade");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("codecourt");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("nomlong");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("type");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("HEURE_SELON_GRADE");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("INTITULE_MODULE");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("heuresCM");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("heuresTD");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("heuresTP");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("codeprefixe");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("CM_A_FAIRE");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("TD_A_FAIRE");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("TP_A_FAIRE");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("HEURES_A_FAIRE");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("CM_FAITES");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("TD_FAITES");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("TP_FAITES");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("HEURES_FAITES");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("BILAN_CM");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("BILAN_TD");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("BILAN_TP");
		
		xlsxTool_tFileOutputExcel_1.addCellValue("BILAN_UE");
		
		nb_line_tFileOutputExcel_1++; 
		
	}
		

 



/**
 * [tFileOutputExcel_1 begin ] stop
 */



	
	/**
	 * [tMap_1 begin ] start
	 */

	

	
		
		ok_Hash.put("tMap_1", false);
		start_Hash.put("tMap_1", System.currentTimeMillis());
		
	
	currentComponent="tMap_1";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("row1" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_tMap_1 = 0;
		
    	class BytesLimit65535_tMap_1{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tMap_1().limitLog4jByte();




// ###############################
// # Lookup's keys initialization
// ###############################        

// ###############################
// # Vars initialization
class  Var__tMap_1__Struct  {
}
Var__tMap_1__Struct Var__tMap_1 = new Var__tMap_1__Struct();
// ###############################

// ###############################
// # Outputs initialization
OUT1Struct OUT1_tmp = new OUT1Struct();
// ###############################

        
        



        









 



/**
 * [tMap_1 begin ] stop
 */



	
	/**
	 * [tDBInput_2 begin ] start
	 */

	

	
		
		ok_Hash.put("tDBInput_2", false);
		start_Hash.put("tDBInput_2", System.currentTimeMillis());
		
	
	currentComponent="tDBInput_2";

	
		int tos_count_tDBInput_2 = 0;
		
    	class BytesLimit65535_tDBInput_2{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_tDBInput_2().limitLog4jByte();
	
	
		    java.util.Calendar calendar_tDBInput_2 = java.util.Calendar.getInstance();
		    calendar_tDBInput_2.set(0, 0, 0, 0, 0, 0);
		    java.util.Date year0_tDBInput_2 = calendar_tDBInput_2.getTime();
		    int nb_line_tDBInput_2 = 0;
		    java.sql.Connection conn_tDBInput_2 = null;
				String driverClass_tDBInput_2 = "org.gjt.mm.mysql.Driver";
			    java.lang.Class.forName(driverClass_tDBInput_2);
			   	String dbUser_tDBInput_2 = "root";
			   	
        		
        		
        		 
	final String decryptedPassword_tDBInput_2 = routines.system.PasswordEncryptUtil.decryptPassword("f4f7aba1746784ea");
			   	
		        String dbPwd_tDBInput_2 = decryptedPassword_tDBInput_2;
		        
				
				String url_tDBInput_2 = "jdbc:mysql://" + "localhost" + ":" + "3306" + "/" + "DW_DB_SVRENS" + "?" + "noDatetimeStringSync=true";
				
				conn_tDBInput_2 = java.sql.DriverManager.getConnection(url_tDBInput_2,dbUser_tDBInput_2,dbPwd_tDBInput_2);
		        
		    
			java.sql.Statement stmt_tDBInput_2 = conn_tDBInput_2.createStatement();

		    String dbquery_tDBInput_2 = "SELECT \n  `FACT_TABLE`.`codedept`, \n  `FACT_TABLE`.`enseignantID`, \n  `FACT_TABLE`.`ANNNE_ENS`, \n  `FACT_TABLE`.`codese"
+"mestre`, \n  `FACT_TABLE`.`codemodsemestre`, \n  `FACT_TABLE`.`ID_RESP_MOD`, \n  `FACT_TABLE`.`DIRECTEUR_DPT`, \n  `FACT_TAB"
+"LE`.`departement`, \n  `FACT_TABLE`.`NOM_SEMESTRE`, \n  `FACT_TABLE`.`ID_RES_SEM`, \n  `FACT_TABLE`.`NOM_PRENOM_ENS`, \n  `F"
+"ACT_TABLE`.`codegrade`, \n  `FACT_TABLE`.`codecourt`, \n  `FACT_TABLE`.`nomlong`, \n  `FACT_TABLE`.`type`, \n  `FACT_TABLE`."
+"`HEURE_SELON_GRADE`, \n  `FACT_TABLE`.`INTITULE_MODULE`, \n  `FACT_TABLE`.`heuresCM`, \n  `FACT_TABLE`.`heuresTD`, \n  `FACT"
+"_TABLE`.`heuresTP`, \n  `FACT_TABLE`.`codeprefixe`, \n  `FACT_TABLE`.`CM_A_FAIRE`, \n  `FACT_TABLE`.`TD_A_FAIRE`, \n  `FACT_"
+"TABLE`.`TP_A_FAIRE`, \n  `FACT_TABLE`.`HEURES_A_FAIRE`, \n  `FACT_TABLE`.`CM_FAITES`, \n  `FACT_TABLE`.`TD_FAITES`, \n  `FAC"
+"T_TABLE`.`TP_FAITES`, \n  `FACT_TABLE`.`HEURES_FAITES`, \n  `FACT_TABLE`.`BILAN_CM`, \n  `FACT_TABLE`.`BILAN_TD`, \n  `FACT_"
+"TABLE`.`BILAN_TP`, \n  `FACT_TABLE`.`BILAN_UE`\nFROM `FACT_TABLE`";
			

            	globalMap.put("tDBInput_2_QUERY",dbquery_tDBInput_2);
		    java.sql.ResultSet rs_tDBInput_2 = null;

		    try {
		    	rs_tDBInput_2 = stmt_tDBInput_2.executeQuery(dbquery_tDBInput_2);
		    	java.sql.ResultSetMetaData rsmd_tDBInput_2 = rs_tDBInput_2.getMetaData();
		    	int colQtyInRs_tDBInput_2 = rsmd_tDBInput_2.getColumnCount();

		    String tmpContent_tDBInput_2 = null;
		    
		    
		    while (rs_tDBInput_2.next()) {
		        nb_line_tDBInput_2++;
		        
							if(colQtyInRs_tDBInput_2 < 1) {
								row1.codedept = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(1) != null) {
                row1.codedept = rs_tDBInput_2.getInt(1);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 2) {
								row1.enseignantID = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(2) != null) {
                row1.enseignantID = rs_tDBInput_2.getInt(2);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 3) {
								row1.ANNNE_ENS = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(3) != null) {
                row1.ANNNE_ENS = rs_tDBInput_2.getInt(3);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 4) {
								row1.codesemestre = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(4) != null) {
                row1.codesemestre = rs_tDBInput_2.getInt(4);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 5) {
								row1.codemodsemestre = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(5) != null) {
                row1.codemodsemestre = rs_tDBInput_2.getInt(5);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 6) {
								row1.ID_RESP_MOD = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(6) != null) {
                row1.ID_RESP_MOD = rs_tDBInput_2.getInt(6);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 7) {
								row1.DIRECTEUR_DPT = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(7) != null) {
                row1.DIRECTEUR_DPT = rs_tDBInput_2.getInt(7);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 8) {
								row1.departement = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(8) != null) {
                row1.departement = rs_tDBInput_2.getInt(8);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 9) {
								row1.NOM_SEMESTRE = null;
							} else {
	                         		
        	row1.NOM_SEMESTRE = routines.system.JDBCUtil.getString(rs_tDBInput_2, 9, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 10) {
								row1.ID_RES_SEM = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(10) != null) {
                row1.ID_RES_SEM = rs_tDBInput_2.getInt(10);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 11) {
								row1.NOM_PRENOM_ENS = null;
							} else {
	                         		
        	row1.NOM_PRENOM_ENS = routines.system.JDBCUtil.getString(rs_tDBInput_2, 11, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 12) {
								row1.codegrade = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(12) != null) {
                row1.codegrade = rs_tDBInput_2.getInt(12);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 13) {
								row1.codecourt = null;
							} else {
	                         		
        	row1.codecourt = routines.system.JDBCUtil.getString(rs_tDBInput_2, 13, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 14) {
								row1.nomlong = null;
							} else {
	                         		
        	row1.nomlong = routines.system.JDBCUtil.getString(rs_tDBInput_2, 14, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 15) {
								row1.type = null;
							} else {
	                         		
        	row1.type = routines.system.JDBCUtil.getString(rs_tDBInput_2, 15, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 16) {
								row1.HEURE_SELON_GRADE = 0;
							} else {
		                          
            if(rs_tDBInput_2.getObject(16) != null) {
                row1.HEURE_SELON_GRADE = rs_tDBInput_2.getInt(16);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 17) {
								row1.INTITULE_MODULE = null;
							} else {
	                         		
        	row1.INTITULE_MODULE = routines.system.JDBCUtil.getString(rs_tDBInput_2, 17, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 18) {
								row1.heuresCM = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(18) != null) {
                row1.heuresCM = rs_tDBInput_2.getBigDecimal(18);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 19) {
								row1.heuresTD = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(19) != null) {
                row1.heuresTD = rs_tDBInput_2.getBigDecimal(19);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 20) {
								row1.heuresTP = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(20) != null) {
                row1.heuresTP = rs_tDBInput_2.getBigDecimal(20);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 21) {
								row1.codeprefixe = null;
							} else {
	                         		
        	row1.codeprefixe = routines.system.JDBCUtil.getString(rs_tDBInput_2, 21, false);
		                    }
							if(colQtyInRs_tDBInput_2 < 22) {
								row1.CM_A_FAIRE = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(22) != null) {
                row1.CM_A_FAIRE = rs_tDBInput_2.getBigDecimal(22);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 23) {
								row1.TD_A_FAIRE = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(23) != null) {
                row1.TD_A_FAIRE = rs_tDBInput_2.getBigDecimal(23);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 24) {
								row1.TP_A_FAIRE = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(24) != null) {
                row1.TP_A_FAIRE = rs_tDBInput_2.getBigDecimal(24);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 25) {
								row1.HEURES_A_FAIRE = 0;
							} else {
	                         		
            if(rs_tDBInput_2.getObject(25) != null) {
                row1.HEURES_A_FAIRE = rs_tDBInput_2.getDouble(25);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 26) {
								row1.CM_FAITES = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(26) != null) {
                row1.CM_FAITES = rs_tDBInput_2.getBigDecimal(26);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 27) {
								row1.TD_FAITES = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(27) != null) {
                row1.TD_FAITES = rs_tDBInput_2.getBigDecimal(27);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 28) {
								row1.TP_FAITES = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(28) != null) {
                row1.TP_FAITES = rs_tDBInput_2.getBigDecimal(28);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 29) {
								row1.HEURES_FAITES = 0;
							} else {
	                         		
            if(rs_tDBInput_2.getObject(29) != null) {
                row1.HEURES_FAITES = rs_tDBInput_2.getDouble(29);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 30) {
								row1.BILAN_CM = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(30) != null) {
                row1.BILAN_CM = rs_tDBInput_2.getBigDecimal(30);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 31) {
								row1.BILAN_TD = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(31) != null) {
                row1.BILAN_TD = rs_tDBInput_2.getBigDecimal(31);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 32) {
								row1.BILAN_TP = null;
							} else {
		                          
            if(rs_tDBInput_2.getObject(32) != null) {
                row1.BILAN_TP = rs_tDBInput_2.getBigDecimal(32);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
							if(colQtyInRs_tDBInput_2 < 33) {
								row1.BILAN_UE = 0;
							} else {
	                         		
            if(rs_tDBInput_2.getObject(33) != null) {
                row1.BILAN_UE = rs_tDBInput_2.getDouble(33);
            } else {
                    throw new RuntimeException("Null value in non-Nullable column");
            }
		                    }
					

 



/**
 * [tDBInput_2 begin ] stop
 */
	
	/**
	 * [tDBInput_2 main ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 


	tos_count_tDBInput_2++;

/**
 * [tDBInput_2 main ] stop
 */
	
	/**
	 * [tDBInput_2 process_data_begin ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 process_data_begin ] stop
 */

	
	/**
	 * [tMap_1 main ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

			//row1
			//row1


			
				if(execStat){
					runStat.updateStatOnConnection("row1"+iterateId,1, 1);
				} 
			

		

			


		
		
		boolean hasCasePrimitiveKeyWithNull_tMap_1 = false;
		
        // ###############################
        // # Input tables (lookups)
		  boolean rejectedInnerJoin_tMap_1 = false;
		  boolean mainRowRejected_tMap_1 = false;
            				    								  
		// ###############################
        { // start of Var scope
        
	        // ###############################
        	// # Vars tables
        
Var__tMap_1__Struct Var = Var__tMap_1;// ###############################
        // ###############################
        // # Output tables

OUT1 = null;


// # Output table : 'OUT1'
OUT1_tmp.codedept = row1.codedept ;
OUT1_tmp.enseignantID = row1.enseignantID ;
OUT1_tmp.ANNNE_ENS = row1.ANNNE_ENS ;
OUT1_tmp.codesemestre = row1.codesemestre ;
OUT1_tmp.codemodsemestre = row1.codemodsemestre ;
OUT1_tmp.ID_RESP_MOD = row1.ID_RESP_MOD ;
OUT1_tmp.DIRECTEUR_DPT = row1.DIRECTEUR_DPT ;
OUT1_tmp.departement = row1.departement ;
OUT1_tmp.NOM_SEMESTRE = row1.NOM_SEMESTRE ;
OUT1_tmp.ID_RES_SEM = row1.ID_RES_SEM ;
OUT1_tmp.NOM_PRENOM_ENS = row1.NOM_PRENOM_ENS ;
OUT1_tmp.codegrade = row1.codegrade ;
OUT1_tmp.codecourt = row1.codecourt ;
OUT1_tmp.nomlong = row1.nomlong ;
OUT1_tmp.type = row1.type ;
OUT1_tmp.HEURE_SELON_GRADE = row1.HEURE_SELON_GRADE ;
OUT1_tmp.INTITULE_MODULE = row1.INTITULE_MODULE ;
OUT1_tmp.heuresCM = row1.heuresCM ;
OUT1_tmp.heuresTD = row1.heuresTD ;
OUT1_tmp.heuresTP = row1.heuresTP ;
OUT1_tmp.codeprefixe = row1.codeprefixe ;
OUT1_tmp.CM_A_FAIRE = row1.CM_A_FAIRE ;
OUT1_tmp.TD_A_FAIRE = row1.TD_A_FAIRE ;
OUT1_tmp.TP_A_FAIRE = row1.TP_A_FAIRE ;
OUT1_tmp.HEURES_A_FAIRE = row1.HEURES_A_FAIRE ;
OUT1_tmp.CM_FAITES = row1.CM_FAITES ;
OUT1_tmp.TD_FAITES = row1.TD_FAITES ;
OUT1_tmp.TP_FAITES = row1.TP_FAITES ;
OUT1_tmp.HEURES_FAITES = row1.HEURES_FAITES ;
OUT1_tmp.BILAN_CM = row1.BILAN_CM ;
OUT1_tmp.BILAN_TD = row1.BILAN_TD ;
OUT1_tmp.BILAN_TP = row1.BILAN_TP ;
OUT1_tmp.BILAN_UE = row1.BILAN_UE ;
OUT1 = OUT1_tmp;
// ###############################

} // end of Var scope

rejectedInnerJoin_tMap_1 = false;










 


	tos_count_tMap_1++;

/**
 * [tMap_1 main ] stop
 */
	
	/**
	 * [tMap_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_begin ] stop
 */
// Start of branch "OUT1"
if(OUT1 != null) { 



	
	/**
	 * [tFileOutputExcel_1 main ] start
	 */

	

	
	
	currentComponent="tFileOutputExcel_1";

	

			//OUT1
			//OUT1


			
				if(execStat){
					runStat.updateStatOnConnection("OUT1"+iterateId,1, 1);
				} 
			

		

				xlsxTool_tFileOutputExcel_1.addRow();
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.codedept)));
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.enseignantID)));
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.ANNNE_ENS)));
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.codesemestre)));
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.codemodsemestre)));
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.ID_RESP_MOD)));
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.DIRECTEUR_DPT)));
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.departement)));
									   				
	    				if(OUT1.NOM_SEMESTRE != null) {
    				
							xlsxTool_tFileOutputExcel_1.addCellValue(String.valueOf(OUT1.NOM_SEMESTRE));
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.ID_RES_SEM)));
									   				
	    				if(OUT1.NOM_PRENOM_ENS != null) {
    				
							xlsxTool_tFileOutputExcel_1.addCellValue(String.valueOf(OUT1.NOM_PRENOM_ENS));
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.codegrade)));
									   				
	    				if(OUT1.codecourt != null) {
    				
							xlsxTool_tFileOutputExcel_1.addCellValue(String.valueOf(OUT1.codecourt));
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.nomlong != null) {
    				
							xlsxTool_tFileOutputExcel_1.addCellValue(String.valueOf(OUT1.nomlong));
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.type != null) {
    				
							xlsxTool_tFileOutputExcel_1.addCellValue(String.valueOf(OUT1.type));
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									
							xlsxTool_tFileOutputExcel_1.addCellValue(Double.parseDouble(String.valueOf(OUT1.HEURE_SELON_GRADE)));
									   				
	    				if(OUT1.INTITULE_MODULE != null) {
    				
							xlsxTool_tFileOutputExcel_1.addCellValue(String.valueOf(OUT1.INTITULE_MODULE));
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.heuresCM != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.heuresCM.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.heuresTD != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.heuresTD.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.heuresTP != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.heuresTP.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.codeprefixe != null) {
    				
							xlsxTool_tFileOutputExcel_1.addCellValue(String.valueOf(OUT1.codeprefixe));
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.CM_A_FAIRE != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.CM_A_FAIRE.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.TD_A_FAIRE != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.TD_A_FAIRE.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.TP_A_FAIRE != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.TP_A_FAIRE.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									
							xlsxTool_tFileOutputExcel_1.addCellValue(OUT1.HEURES_A_FAIRE);
									   				
	    				if(OUT1.CM_FAITES != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.CM_FAITES.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.TD_FAITES != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.TD_FAITES.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.TP_FAITES != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.TP_FAITES.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									
							xlsxTool_tFileOutputExcel_1.addCellValue(OUT1.HEURES_FAITES);
									   				
	    				if(OUT1.BILAN_CM != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.BILAN_CM.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.BILAN_TD != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.BILAN_TD.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									   				
	    				if(OUT1.BILAN_TP != null) {
    						
							xlsxTool_tFileOutputExcel_1.addCellValue((OUT1.BILAN_TP.setScale(2, java.math.RoundingMode.HALF_UP)).doubleValue());
	    				} else {
	    					xlsxTool_tFileOutputExcel_1.addCellNullValue();
	    				}
					
									
							xlsxTool_tFileOutputExcel_1.addCellValue(OUT1.BILAN_UE);
    			nb_line_tFileOutputExcel_1++;
				
 


	tos_count_tFileOutputExcel_1++;

/**
 * [tFileOutputExcel_1 main ] stop
 */
	
	/**
	 * [tFileOutputExcel_1 process_data_begin ] start
	 */

	

	
	
	currentComponent="tFileOutputExcel_1";

	

 



/**
 * [tFileOutputExcel_1 process_data_begin ] stop
 */
	
	/**
	 * [tFileOutputExcel_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tFileOutputExcel_1";

	

 



/**
 * [tFileOutputExcel_1 process_data_end ] stop
 */

} // End of branch "OUT1"




	
	/**
	 * [tMap_1 process_data_end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 process_data_end ] stop
 */



	
	/**
	 * [tDBInput_2 process_data_end ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 process_data_end ] stop
 */
	
	/**
	 * [tDBInput_2 end ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

	}
}finally{
	if(rs_tDBInput_2 !=null){
		rs_tDBInput_2.close();
	}
	stmt_tDBInput_2.close();
		if(conn_tDBInput_2 != null && !conn_tDBInput_2.isClosed()) {
			
			conn_tDBInput_2.close();
			
		}
		
}

		   globalMap.put("tDBInput_2_NB_LINE",nb_line_tDBInput_2);
		


 

ok_Hash.put("tDBInput_2", true);
end_Hash.put("tDBInput_2", System.currentTimeMillis());




/**
 * [tDBInput_2 end ] stop
 */

	
	/**
	 * [tMap_1 end ] start
	 */

	

	
	
	currentComponent="tMap_1";

	


// ###############################
// # Lookup hashes releasing
// ###############################      





			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("row1"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tMap_1", true);
end_Hash.put("tMap_1", System.currentTimeMillis());




/**
 * [tMap_1 end ] stop
 */

	
	/**
	 * [tFileOutputExcel_1 end ] start
	 */

	

	
	
	currentComponent="tFileOutputExcel_1";

	

	
	
	
			xlsxTool_tFileOutputExcel_1.writeExcel(fileName_tFileOutputExcel_1,true);
	
		
			nb_line_tFileOutputExcel_1 = nb_line_tFileOutputExcel_1 -1;
		
		globalMap.put("tFileOutputExcel_1_NB_LINE",nb_line_tFileOutputExcel_1);
		
		

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("OUT1"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("tFileOutputExcel_1", true);
end_Hash.put("tFileOutputExcel_1", System.currentTimeMillis());




/**
 * [tFileOutputExcel_1 end ] stop
 */






				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [tDBInput_2 finally ] start
	 */

	

	
	
	currentComponent="tDBInput_2";

	

 



/**
 * [tDBInput_2 finally ] stop
 */

	
	/**
	 * [tMap_1 finally ] start
	 */

	

	
	
	currentComponent="tMap_1";

	

 



/**
 * [tMap_1 finally ] stop
 */

	
	/**
	 * [tFileOutputExcel_1 finally ] start
	 */

	

	
	
	currentComponent="tFileOutputExcel_1";

	

 



/**
 * [tFileOutputExcel_1 finally ] stop
 */






				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("tDBInput_2_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendStats_STATSStruct implements routines.system.IPersistableRow<row_talendStats_STATSStruct> {
    final static byte[] commonByteArrayLock_DW_TER_SQL_TO_XSL = new byte[0];
    static byte[] commonByteArray_DW_TER_SQL_TO_XSL = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message_type;

				public String getMessage_type () {
					return this.message_type;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Long duration;

				public Long getDuration () {
					return this.duration;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_DW_TER_SQL_TO_XSL.length) {
				if(length < 1024 && commonByteArray_DW_TER_SQL_TO_XSL.length == 0) {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[1024];
				} else {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_DW_TER_SQL_TO_XSL, 0, length);
			strReturn = new String(commonByteArray_DW_TER_SQL_TO_XSL, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_DW_TER_SQL_TO_XSL) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.message_type = readString(dis);
					
					this.message = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.duration = null;
           				} else {
           			    	this.duration = dis.readLong();
           				}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message_type,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Long
				
						if(this.duration == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.duration);
		            	}
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",message_type="+message_type);
		sb.append(",message="+message);
		sb.append(",duration="+String.valueOf(duration));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendStats_STATSStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendStats_STATSProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendStats_STATSStruct row_talendStats_STATS = new row_talendStats_STATSStruct();




	
	/**
	 * [talendStats_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_CONSOLE", false);
		start_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendStats_CONSOLE = 0;
		
    	class BytesLimit65535_talendStats_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendStats_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendStats_CONSOLE = null;	

 		StringBuilder strBuffer_talendStats_CONSOLE = null;
		int nb_line_talendStats_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendStats_CONSOLE begin ] stop
 */



	
	/**
	 * [talendStats_STATS begin ] start
	 */

	

	
		
		ok_Hash.put("talendStats_STATS", false);
		start_Hash.put("talendStats_STATS", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	
		int tos_count_talendStats_STATS = 0;
		
    	class BytesLimit65535_talendStats_STATS{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendStats_STATS().limitLog4jByte();

	for (StatCatcherUtils.StatCatcherMessage scm : talendStats_STATS.getMessages()) {
		row_talendStats_STATS.pid = pid;
		row_talendStats_STATS.root_pid = rootPid;
		row_talendStats_STATS.father_pid = fatherPid;	
    	row_talendStats_STATS.project = projectName;
    	row_talendStats_STATS.job = jobName;
    	row_talendStats_STATS.context = contextStr;
		row_talendStats_STATS.origin = (scm.getOrigin()==null || scm.getOrigin().length()<1 ? null : scm.getOrigin());
		row_talendStats_STATS.message = scm.getMessage();
		row_talendStats_STATS.duration = scm.getDuration();
		row_talendStats_STATS.moment = scm.getMoment();
		row_talendStats_STATS.message_type = scm.getMessageType();
		row_talendStats_STATS.job_version = scm.getJobVersion();
		row_talendStats_STATS.job_repository_id = scm.getJobId();
		row_talendStats_STATS.system_pid = scm.getSystemPid();

 



/**
 * [talendStats_STATS begin ] stop
 */
	
	/**
	 * [talendStats_STATS main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 


	tos_count_talendStats_STATS++;

/**
 * [talendStats_STATS main ] stop
 */
	
	/**
	 * [talendStats_STATS process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS process_data_begin ] stop
 */

	
	/**
	 * [talendStats_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

			//Main
			//row_talendStats_STATS


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendStats_CONSOLE = new StringBuilder();




   				
	    		if(row_talendStats_STATS.moment != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
								FormatterUtils.format_Date(row_talendStats_STATS.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.father_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.root_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.system_pid != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.system_pid)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.project != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.project)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.job != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.job)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.job_repository_id != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.job_repository_id)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.job_version != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.job_version)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.context != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.context)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.origin != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.origin)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.message_type != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.message_type)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.message != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.message)							
				);


							
	    		} //  			

    			strBuffer_talendStats_CONSOLE.append("|");
    			


   				
	    		if(row_talendStats_STATS.duration != null) { //              
                    							
       
				strBuffer_talendStats_CONSOLE.append(
				                String.valueOf(row_talendStats_STATS.duration)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendStats_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendStats_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendStats_CONSOLE);
                    }
                    consoleOut_talendStats_CONSOLE.println(strBuffer_talendStats_CONSOLE.toString());
                    consoleOut_talendStats_CONSOLE.flush();
                    nb_line_talendStats_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendStats_CONSOLE++;

/**
 * [talendStats_CONSOLE main ] stop
 */
	
	/**
	 * [talendStats_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendStats_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE process_data_end ] stop
 */



	
	/**
	 * [talendStats_STATS process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS process_data_end ] stop
 */
	
	/**
	 * [talendStats_STATS end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

	}


 

ok_Hash.put("talendStats_STATS", true);
end_Hash.put("talendStats_STATS", System.currentTimeMillis());




/**
 * [talendStats_STATS end ] stop
 */

	
	/**
	 * [talendStats_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	


//////
//////
globalMap.put("talendStats_CONSOLE_NB_LINE",nb_line_talendStats_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendStats_CONSOLE", true);
end_Hash.put("talendStats_CONSOLE", System.currentTimeMillis());




/**
 * [talendStats_CONSOLE end ] stop
 */



				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendStats_STATS finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_STATS";
	
	currentComponent="talendStats_STATS";

	

 



/**
 * [talendStats_STATS finally ] stop
 */

	
	/**
	 * [talendStats_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendStats_CONSOLE";
	
	currentComponent="talendStats_CONSOLE";

	

 



/**
 * [talendStats_CONSOLE finally ] stop
 */



				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendStats_STATS_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendLogs_LOGSStruct implements routines.system.IPersistableRow<row_talendLogs_LOGSStruct> {
    final static byte[] commonByteArrayLock_DW_TER_SQL_TO_XSL = new byte[0];
    static byte[] commonByteArray_DW_TER_SQL_TO_XSL = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public Integer priority;

				public Integer getPriority () {
					return this.priority;
				}
				
			    public String type;

				public String getType () {
					return this.type;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String message;

				public String getMessage () {
					return this.message;
				}
				
			    public Integer code;

				public Integer getCode () {
					return this.code;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_DW_TER_SQL_TO_XSL.length) {
				if(length < 1024 && commonByteArray_DW_TER_SQL_TO_XSL.length == 0) {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[1024];
				} else {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_DW_TER_SQL_TO_XSL, 0, length);
			strReturn = new String(commonByteArray_DW_TER_SQL_TO_XSL, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_DW_TER_SQL_TO_XSL) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.root_pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.context = readString(dis);
					
						this.priority = readInteger(dis);
					
					this.type = readString(dis);
					
					this.origin = readString(dis);
					
					this.message = readString(dis);
					
						this.code = readInteger(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// Integer
				
						writeInteger(this.priority,dos);
					
					// String
				
						writeString(this.type,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.message,dos);
					
					// Integer
				
						writeInteger(this.code,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",context="+context);
		sb.append(",priority="+String.valueOf(priority));
		sb.append(",type="+type);
		sb.append(",origin="+origin);
		sb.append(",message="+message);
		sb.append(",code="+String.valueOf(code));
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendLogs_LOGSStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendLogs_LOGSProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendLogs_LOGSStruct row_talendLogs_LOGS = new row_talendLogs_LOGSStruct();




	
	/**
	 * [talendLogs_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_CONSOLE", false);
		start_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendLogs_CONSOLE = 0;
		
    	class BytesLimit65535_talendLogs_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendLogs_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendLogs_CONSOLE = null;	

 		StringBuilder strBuffer_talendLogs_CONSOLE = null;
		int nb_line_talendLogs_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendLogs_CONSOLE begin ] stop
 */



	
	/**
	 * [talendLogs_LOGS begin ] start
	 */

	

	
		
		ok_Hash.put("talendLogs_LOGS", false);
		start_Hash.put("talendLogs_LOGS", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	
		int tos_count_talendLogs_LOGS = 0;
		
    	class BytesLimit65535_talendLogs_LOGS{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendLogs_LOGS().limitLog4jByte();

try {
	for (LogCatcherUtils.LogCatcherMessage lcm : talendLogs_LOGS.getMessages()) {
		row_talendLogs_LOGS.type = lcm.getType();
		row_talendLogs_LOGS.origin = (lcm.getOrigin()==null || lcm.getOrigin().length()<1 ? null : lcm.getOrigin());
		row_talendLogs_LOGS.priority = lcm.getPriority();
		row_talendLogs_LOGS.message = lcm.getMessage();
		row_talendLogs_LOGS.code = lcm.getCode();
		
		row_talendLogs_LOGS.moment = java.util.Calendar.getInstance().getTime();
	
    	row_talendLogs_LOGS.pid = pid;
		row_talendLogs_LOGS.root_pid = rootPid;
		row_talendLogs_LOGS.father_pid = fatherPid;
	
    	row_talendLogs_LOGS.project = projectName;
    	row_talendLogs_LOGS.job = jobName;
    	row_talendLogs_LOGS.context = contextStr;
    		
 



/**
 * [talendLogs_LOGS begin ] stop
 */
	
	/**
	 * [talendLogs_LOGS main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 


	tos_count_talendLogs_LOGS++;

/**
 * [talendLogs_LOGS main ] stop
 */
	
	/**
	 * [talendLogs_LOGS process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS process_data_begin ] stop
 */

	
	/**
	 * [talendLogs_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

			//Main
			//row_talendLogs_LOGS


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendLogs_CONSOLE = new StringBuilder();




   				
	    		if(row_talendLogs_LOGS.moment != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
								FormatterUtils.format_Date(row_talendLogs_LOGS.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.root_pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.father_pid != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.project != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.project)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.job != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.job)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.context != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.context)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.priority != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.priority)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.type != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.type)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.origin != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.origin)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.message != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.message)							
				);


							
	    		} //  			

    			strBuffer_talendLogs_CONSOLE.append("|");
    			


   				
	    		if(row_talendLogs_LOGS.code != null) { //              
                    							
       
				strBuffer_talendLogs_CONSOLE.append(
				                String.valueOf(row_talendLogs_LOGS.code)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendLogs_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendLogs_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendLogs_CONSOLE);
                    }
                    consoleOut_talendLogs_CONSOLE.println(strBuffer_talendLogs_CONSOLE.toString());
                    consoleOut_talendLogs_CONSOLE.flush();
                    nb_line_talendLogs_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendLogs_CONSOLE++;

/**
 * [talendLogs_CONSOLE main ] stop
 */
	
	/**
	 * [talendLogs_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendLogs_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE process_data_end ] stop
 */



	
	/**
	 * [talendLogs_LOGS process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS process_data_end ] stop
 */
	
	/**
	 * [talendLogs_LOGS end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	
	}
} catch (Exception e_talendLogs_LOGS) {
	logIgnoredError(String.format("talendLogs_LOGS - tLogCatcher failed to process log message(s) due to internal error: %s", e_talendLogs_LOGS), e_talendLogs_LOGS);
}

 

ok_Hash.put("talendLogs_LOGS", true);
end_Hash.put("talendLogs_LOGS", System.currentTimeMillis());




/**
 * [talendLogs_LOGS end ] stop
 */

	
	/**
	 * [talendLogs_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	


//////
//////
globalMap.put("talendLogs_CONSOLE_NB_LINE",nb_line_talendLogs_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendLogs_CONSOLE", true);
end_Hash.put("talendLogs_CONSOLE", System.currentTimeMillis());




/**
 * [talendLogs_CONSOLE end ] stop
 */



				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendLogs_LOGS finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_LOGS";
	
	currentComponent="talendLogs_LOGS";

	

 



/**
 * [talendLogs_LOGS finally ] stop
 */

	
	/**
	 * [talendLogs_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendLogs_CONSOLE";
	
	currentComponent="talendLogs_CONSOLE";

	

 



/**
 * [talendLogs_CONSOLE finally ] stop
 */



				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendLogs_LOGS_SUBPROCESS_STATE", 1);
	}
	


public static class row_talendMeter_METTERStruct implements routines.system.IPersistableRow<row_talendMeter_METTERStruct> {
    final static byte[] commonByteArrayLock_DW_TER_SQL_TO_XSL = new byte[0];
    static byte[] commonByteArray_DW_TER_SQL_TO_XSL = new byte[0];

	
			    public java.util.Date moment;

				public java.util.Date getMoment () {
					return this.moment;
				}
				
			    public String pid;

				public String getPid () {
					return this.pid;
				}
				
			    public String father_pid;

				public String getFather_pid () {
					return this.father_pid;
				}
				
			    public String root_pid;

				public String getRoot_pid () {
					return this.root_pid;
				}
				
			    public Long system_pid;

				public Long getSystem_pid () {
					return this.system_pid;
				}
				
			    public String project;

				public String getProject () {
					return this.project;
				}
				
			    public String job;

				public String getJob () {
					return this.job;
				}
				
			    public String job_repository_id;

				public String getJob_repository_id () {
					return this.job_repository_id;
				}
				
			    public String job_version;

				public String getJob_version () {
					return this.job_version;
				}
				
			    public String context;

				public String getContext () {
					return this.context;
				}
				
			    public String origin;

				public String getOrigin () {
					return this.origin;
				}
				
			    public String label;

				public String getLabel () {
					return this.label;
				}
				
			    public Integer count;

				public Integer getCount () {
					return this.count;
				}
				
			    public Integer reference;

				public Integer getReference () {
					return this.reference;
				}
				
			    public String thresholds;

				public String getThresholds () {
					return this.thresholds;
				}
				



	private java.util.Date readDate(ObjectInputStream dis) throws IOException{
		java.util.Date dateReturn = null;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			dateReturn = null;
		} else {
	    	dateReturn = new Date(dis.readLong());
		}
		return dateReturn;
	}

    private void writeDate(java.util.Date date1, ObjectOutputStream dos) throws IOException{
		if(date1 == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeLong(date1.getTime());
    	}
    }

	private String readString(ObjectInputStream dis) throws IOException{
		String strReturn = null;
		int length = 0;
        length = dis.readInt();
		if (length == -1) {
			strReturn = null;
		} else {
			if(length > commonByteArray_DW_TER_SQL_TO_XSL.length) {
				if(length < 1024 && commonByteArray_DW_TER_SQL_TO_XSL.length == 0) {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[1024];
				} else {
   					commonByteArray_DW_TER_SQL_TO_XSL = new byte[2 * length];
   				}
			}
			dis.readFully(commonByteArray_DW_TER_SQL_TO_XSL, 0, length);
			strReturn = new String(commonByteArray_DW_TER_SQL_TO_XSL, 0, length, utf8Charset);
		}
		return strReturn;
	}

    private void writeString(String str, ObjectOutputStream dos) throws IOException{
		if(str == null) {
            dos.writeInt(-1);
		} else {
            byte[] byteArray = str.getBytes(utf8Charset);
	    	dos.writeInt(byteArray.length);
			dos.write(byteArray);
    	}
    }
	private Integer readInteger(ObjectInputStream dis) throws IOException{
		Integer intReturn;
        int length = 0;
        length = dis.readByte();
		if (length == -1) {
			intReturn = null;
		} else {
	    	intReturn = dis.readInt();
		}
		return intReturn;
	}

	private void writeInteger(Integer intNum, ObjectOutputStream dos) throws IOException{
		if(intNum == null) {
            dos.writeByte(-1);
		} else {
			dos.writeByte(0);
	    	dos.writeInt(intNum);
    	}
	}

    public void readData(ObjectInputStream dis) {

		synchronized(commonByteArrayLock_DW_TER_SQL_TO_XSL) {

        	try {

        		int length = 0;
		
					this.moment = readDate(dis);
					
					this.pid = readString(dis);
					
					this.father_pid = readString(dis);
					
					this.root_pid = readString(dis);
					
			            length = dis.readByte();
           				if (length == -1) {
           	    			this.system_pid = null;
           				} else {
           			    	this.system_pid = dis.readLong();
           				}
					
					this.project = readString(dis);
					
					this.job = readString(dis);
					
					this.job_repository_id = readString(dis);
					
					this.job_version = readString(dis);
					
					this.context = readString(dis);
					
					this.origin = readString(dis);
					
					this.label = readString(dis);
					
						this.count = readInteger(dis);
					
						this.reference = readInteger(dis);
					
					this.thresholds = readString(dis);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);

		

        }

		

      }


    }

    public void writeData(ObjectOutputStream dos) {
        try {

		
					// java.util.Date
				
						writeDate(this.moment,dos);
					
					// String
				
						writeString(this.pid,dos);
					
					// String
				
						writeString(this.father_pid,dos);
					
					// String
				
						writeString(this.root_pid,dos);
					
					// Long
				
						if(this.system_pid == null) {
			                dos.writeByte(-1);
						} else {
               				dos.writeByte(0);
           			    	dos.writeLong(this.system_pid);
		            	}
					
					// String
				
						writeString(this.project,dos);
					
					// String
				
						writeString(this.job,dos);
					
					// String
				
						writeString(this.job_repository_id,dos);
					
					// String
				
						writeString(this.job_version,dos);
					
					// String
				
						writeString(this.context,dos);
					
					// String
				
						writeString(this.origin,dos);
					
					// String
				
						writeString(this.label,dos);
					
					// Integer
				
						writeInteger(this.count,dos);
					
					// Integer
				
						writeInteger(this.reference,dos);
					
					// String
				
						writeString(this.thresholds,dos);
					
        	} catch (IOException e) {
	            throw new RuntimeException(e);
        }


    }


    public String toString() {

		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append("[");
		sb.append("moment="+String.valueOf(moment));
		sb.append(",pid="+pid);
		sb.append(",father_pid="+father_pid);
		sb.append(",root_pid="+root_pid);
		sb.append(",system_pid="+String.valueOf(system_pid));
		sb.append(",project="+project);
		sb.append(",job="+job);
		sb.append(",job_repository_id="+job_repository_id);
		sb.append(",job_version="+job_version);
		sb.append(",context="+context);
		sb.append(",origin="+origin);
		sb.append(",label="+label);
		sb.append(",count="+String.valueOf(count));
		sb.append(",reference="+String.valueOf(reference));
		sb.append(",thresholds="+thresholds);
	    sb.append("]");

	    return sb.toString();
    }

    /**
     * Compare keys
     */
    public int compareTo(row_talendMeter_METTERStruct other) {

		int returnValue = -1;
		
	    return returnValue;
    }


    private int checkNullsAndCompare(Object object1, Object object2) {
        int returnValue = 0;
		if (object1 instanceof Comparable && object2 instanceof Comparable) {
            returnValue = ((Comparable) object1).compareTo(object2);
        } else if (object1 != null && object2 != null) {
            returnValue = compareStrings(object1.toString(), object2.toString());
        } else if (object1 == null && object2 != null) {
            returnValue = 1;
        } else if (object1 != null && object2 == null) {
            returnValue = -1;
        } else {
            returnValue = 0;
        }

        return returnValue;
    }

    private int compareStrings(String string1, String string2) {
        return string1.compareTo(string2);
    }


}
public void talendMeter_METTERProcess(final java.util.Map<String, Object> globalMap) throws TalendException {
	globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 0);

 final boolean execStat = this.execStat;
		String currentVirtualComponent = null;
	
		String iterateId = "";
	
	
	String currentComponent = "";
	java.util.Map<String, Object> resourceMap = new java.util.HashMap<String, Object>();

	try {
			// TDI-39566 avoid throwing an useless Exception
			boolean resumeIt = true;
			if (globalResumeTicket == false && resumeEntryMethodName != null) {
				String currentMethodName = new java.lang.Exception().getStackTrace()[0].getMethodName();
				resumeIt = resumeEntryMethodName.equals(currentMethodName);
			}
			if (resumeIt || globalResumeTicket) { //start the resume
				globalResumeTicket = true;



		row_talendMeter_METTERStruct row_talendMeter_METTER = new row_talendMeter_METTERStruct();




	
	/**
	 * [talendMeter_CONSOLE begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_CONSOLE", false);
		start_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	
			if (execStat) {
				if(resourceMap.get("inIterateVComp") == null){
					
						runStat.updateStatOnConnection("Main" + iterateId, 0, 0);
					
				}
			} 

		
		int tos_count_talendMeter_CONSOLE = 0;
		
    	class BytesLimit65535_talendMeter_CONSOLE{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_CONSOLE().limitLog4jByte();

	///////////////////////
	
		final String OUTPUT_FIELD_SEPARATOR_talendMeter_CONSOLE = "|";
		java.io.PrintStream consoleOut_talendMeter_CONSOLE = null;	

 		StringBuilder strBuffer_talendMeter_CONSOLE = null;
		int nb_line_talendMeter_CONSOLE = 0;
///////////////////////    			



 



/**
 * [talendMeter_CONSOLE begin ] stop
 */



	
	/**
	 * [talendMeter_METTER begin ] start
	 */

	

	
		
		ok_Hash.put("talendMeter_METTER", false);
		start_Hash.put("talendMeter_METTER", System.currentTimeMillis());
		
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	
		int tos_count_talendMeter_METTER = 0;
		
    	class BytesLimit65535_talendMeter_METTER{
    		public void limitLog4jByte() throws Exception{
    			
    		}
    	}
    	
        new BytesLimit65535_talendMeter_METTER().limitLog4jByte();

	for (MetterCatcherUtils.MetterCatcherMessage mcm : talendMeter_METTER.getMessages()) {
		row_talendMeter_METTER.pid = pid;
		row_talendMeter_METTER.root_pid = rootPid;
		row_talendMeter_METTER.father_pid = fatherPid;	
        row_talendMeter_METTER.project = projectName;
        row_talendMeter_METTER.job = jobName;
        row_talendMeter_METTER.context = contextStr;
		row_talendMeter_METTER.origin = (mcm.getOrigin()==null || mcm.getOrigin().length()<1 ? null : mcm.getOrigin());
		row_talendMeter_METTER.moment = mcm.getMoment();
		row_talendMeter_METTER.job_version = mcm.getJobVersion();
		row_talendMeter_METTER.job_repository_id = mcm.getJobId();
		row_talendMeter_METTER.system_pid = mcm.getSystemPid();
		row_talendMeter_METTER.label = mcm.getLabel();
		row_talendMeter_METTER.count = mcm.getCount();
		row_talendMeter_METTER.reference = talendMeter_METTER.getConnLinesCount(mcm.getReferense()+"_count");
		row_talendMeter_METTER.thresholds = mcm.getThresholds();
		

 



/**
 * [talendMeter_METTER begin ] stop
 */
	
	/**
	 * [talendMeter_METTER main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 


	tos_count_talendMeter_METTER++;

/**
 * [talendMeter_METTER main ] stop
 */
	
	/**
	 * [talendMeter_METTER process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER process_data_begin ] stop
 */

	
	/**
	 * [talendMeter_CONSOLE main ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

			//Main
			//row_talendMeter_METTER


			
				if(execStat){
					runStat.updateStatOnConnection("Main"+iterateId,1, 1);
				} 
			

		
///////////////////////		
						



				strBuffer_talendMeter_CONSOLE = new StringBuilder();




   				
	    		if(row_talendMeter_METTER.moment != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
								FormatterUtils.format_Date(row_talendMeter_METTER.moment, "yyyy-MM-dd HH:mm:ss")				
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.father_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.father_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.root_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.root_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.system_pid != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.system_pid)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.project != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.project)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.job != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.job)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.job_repository_id != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.job_repository_id)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.job_version != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.job_version)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.context != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.context)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.origin != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.origin)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.label != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.label)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.count != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.count)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.reference != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.reference)							
				);


							
	    		} //  			

    			strBuffer_talendMeter_CONSOLE.append("|");
    			


   				
	    		if(row_talendMeter_METTER.thresholds != null) { //              
                    							
       
				strBuffer_talendMeter_CONSOLE.append(
				                String.valueOf(row_talendMeter_METTER.thresholds)							
				);


							
	    		} //  			
 

                    if (globalMap.get("tLogRow_CONSOLE")!=null)
                    {
                    	consoleOut_talendMeter_CONSOLE = (java.io.PrintStream) globalMap.get("tLogRow_CONSOLE");
                    }
                    else
                    {
                    	consoleOut_talendMeter_CONSOLE = new java.io.PrintStream(new java.io.BufferedOutputStream(System.out));
                    	globalMap.put("tLogRow_CONSOLE",consoleOut_talendMeter_CONSOLE);
                    }
                    consoleOut_talendMeter_CONSOLE.println(strBuffer_talendMeter_CONSOLE.toString());
                    consoleOut_talendMeter_CONSOLE.flush();
                    nb_line_talendMeter_CONSOLE++;
//////

//////                    
                    
///////////////////////    			

 


	tos_count_talendMeter_CONSOLE++;

/**
 * [talendMeter_CONSOLE main ] stop
 */
	
	/**
	 * [talendMeter_CONSOLE process_data_begin ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE process_data_begin ] stop
 */
	
	/**
	 * [talendMeter_CONSOLE process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE process_data_end ] stop
 */



	
	/**
	 * [talendMeter_METTER process_data_end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER process_data_end ] stop
 */
	
	/**
	 * [talendMeter_METTER end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

	}


 

ok_Hash.put("talendMeter_METTER", true);
end_Hash.put("talendMeter_METTER", System.currentTimeMillis());




/**
 * [talendMeter_METTER end ] stop
 */

	
	/**
	 * [talendMeter_CONSOLE end ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	


//////
//////
globalMap.put("talendMeter_CONSOLE_NB_LINE",nb_line_talendMeter_CONSOLE);

///////////////////////    			

			if(execStat){
				if(resourceMap.get("inIterateVComp") == null || !((Boolean)resourceMap.get("inIterateVComp"))){
			 		runStat.updateStatOnConnection("Main"+iterateId,2, 0); 
			 	}
			}
		
 

ok_Hash.put("talendMeter_CONSOLE", true);
end_Hash.put("talendMeter_CONSOLE", System.currentTimeMillis());




/**
 * [talendMeter_CONSOLE end ] stop
 */



				}//end the resume

				



	
			}catch(java.lang.Exception e){	
				
				TalendException te = new TalendException(e, currentComponent, globalMap);
				
					te.setVirtualComponentName(currentVirtualComponent);
				
				throw te;
			}catch(java.lang.Error error){	
				
					runStat.stopThreadStat();
				
				throw error;
			}finally{
				
				try{
					
	
	/**
	 * [talendMeter_METTER finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_METTER";
	
	currentComponent="talendMeter_METTER";

	

 



/**
 * [talendMeter_METTER finally ] stop
 */

	
	/**
	 * [talendMeter_CONSOLE finally ] start
	 */

	

	
	
		currentVirtualComponent = "talendMeter_CONSOLE";
	
	currentComponent="talendMeter_CONSOLE";

	

 



/**
 * [talendMeter_CONSOLE finally ] stop
 */



				}catch(java.lang.Exception e){	
					//ignore
				}catch(java.lang.Error error){
					//ignore
				}
				resourceMap = null;
			}
		

		globalMap.put("talendMeter_METTER_SUBPROCESS_STATE", 1);
	}
	
    public String resuming_logs_dir_path = null;
    public String resuming_checkpoint_path = null;
    public String parent_part_launcher = null;
    private String resumeEntryMethodName = null;
    private boolean globalResumeTicket = false;

    public boolean watch = false;
    // portStats is null, it means don't execute the statistics
    public Integer portStats = null;
    public int portTraces = 4334;
    public String clientHost;
    public String defaultClientHost = "localhost";
    public String contextStr = "Default";
    public boolean isDefaultContext = true;
    public String pid = "0";
    public String rootPid = null;
    public String fatherPid = null;
    public String fatherNode = null;
    public long startTime = 0;
    public boolean isChildJob = false;
    public String log4jLevel = "";

    private boolean execStat = true;

    private ThreadLocal<java.util.Map<String, String>> threadLocal = new ThreadLocal<java.util.Map<String, String>>() {
        protected java.util.Map<String, String> initialValue() {
            java.util.Map<String,String> threadRunResultMap = new java.util.HashMap<String, String>();
            threadRunResultMap.put("errorCode", null);
            threadRunResultMap.put("status", "");
            return threadRunResultMap;
        };
    };


    private PropertiesWithType context_param = new PropertiesWithType();
    public java.util.Map<String, Object> parentContextMap = new java.util.HashMap<String, Object>();

    public String status= "";
    

    public static void main(String[] args){
        final SQL_TO_XSL SQL_TO_XSLClass = new SQL_TO_XSL();

        int exitCode = SQL_TO_XSLClass.runJobInTOS(args);

        System.exit(exitCode);
    }


    public String[][] runJob(String[] args) {

        int exitCode = runJobInTOS(args);
        String[][] bufferValue = new String[][] { { Integer.toString(exitCode) } };

        return bufferValue;
    }

    public boolean hastBufferOutputComponent() {
		boolean hastBufferOutput = false;
    	
        return hastBufferOutput;
    }

    public int runJobInTOS(String[] args) {
	   	// reset status
	   	status = "";

        String lastStr = "";
        for (String arg : args) {
            if (arg.equalsIgnoreCase("--context_param")) {
                lastStr = arg;
            } else if (lastStr.equals("")) {
                evalParam(arg);
            } else {
                evalParam(lastStr + " " + arg);
                lastStr = "";
            }
        }


        if(clientHost == null) {
            clientHost = defaultClientHost;
        }

        if(pid == null || "0".equals(pid)) {
            pid = TalendString.getAsciiRandomString(6);
        }

        if (rootPid==null) {
            rootPid = pid;
        }
        if (fatherPid==null) {
            fatherPid = pid;
        }else{
            isChildJob = true;
        }

        if (portStats != null) {
            // portStats = -1; //for testing
            if (portStats < 0 || portStats > 65535) {
                // issue:10869, the portStats is invalid, so this client socket can't open
                System.err.println("The statistics socket port " + portStats + " is invalid.");
                execStat = false;
            }
        } else {
            execStat = false;
        }

        try {
            //call job/subjob with an existing context, like: --context=production. if without this parameter, there will use the default context instead.
            java.io.InputStream inContext = SQL_TO_XSL.class.getClassLoader().getResourceAsStream("dw_ter/sql_to_xsl_0_1/contexts/" + contextStr + ".properties");
            if (inContext == null) {
                inContext = SQL_TO_XSL.class.getClassLoader().getResourceAsStream("config/contexts/" + contextStr + ".properties");
            }
            if (inContext != null) {
                //defaultProps is in order to keep the original context value
                defaultProps.load(inContext);
                inContext.close();
                context = new ContextProperties(defaultProps);
            } else if (!isDefaultContext) {
                //print info and job continue to run, for case: context_param is not empty.
                System.err.println("Could not find the context " + contextStr);
            }

            if(!context_param.isEmpty()) {
                context.putAll(context_param);
				//set types for params from parentJobs
				for (Object key: context_param.keySet()){
					String context_key = key.toString();
					String context_type = context_param.getContextType(context_key);
					context.setContextType(context_key, context_type);

				}
            }
        } catch (java.io.IOException ie) {
            System.err.println("Could not load context "+contextStr);
            ie.printStackTrace();
        }


        // get context value from parent directly
        if (parentContextMap != null && !parentContextMap.isEmpty()) {
        }

        //Resume: init the resumeUtil
        resumeEntryMethodName = ResumeUtil.getResumeEntryMethodName(resuming_checkpoint_path);
        resumeUtil = new ResumeUtil(resuming_logs_dir_path, isChildJob, rootPid);
        resumeUtil.initCommonInfo(pid, rootPid, fatherPid, projectName, jobName, contextStr, jobVersion);

		List<String> parametersToEncrypt = new java.util.ArrayList<String>();
        //Resume: jobStart
        resumeUtil.addLog("JOB_STARTED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","","","",resumeUtil.convertToJsonText(context,parametersToEncrypt));

if(execStat) {
    try {
        runStat.openSocket(!isChildJob);
        runStat.setAllPID(rootPid, fatherPid, pid, jobName);
        runStat.startThreadStat(clientHost, portStats);
        runStat.updateStatOnJob(RunStat.JOBSTART, fatherNode);
    } catch (java.io.IOException ioException) {
        ioException.printStackTrace();
    }
}



	
	    java.util.concurrent.ConcurrentHashMap<Object, Object> concurrentHashMap = new java.util.concurrent.ConcurrentHashMap<Object, Object>();
	    globalMap.put("concurrentHashMap", concurrentHashMap);
	

    long startUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    long endUsedMemory = 0;
    long end = 0;

    startTime = System.currentTimeMillis();
        talendStats_STATS.addMessage("begin");




this.globalResumeTicket = true;//to run tPreJob



        try {
            talendStats_STATSProcess(globalMap);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }

this.globalResumeTicket = false;//to run others jobs

try {
errorCode = null;tDBInput_2Process(globalMap);
if(!"failure".equals(status)) { status = "end"; }
}catch (TalendException e_tDBInput_2) {
globalMap.put("tDBInput_2_SUBPROCESS_STATE", -1);

e_tDBInput_2.printStackTrace();

}

this.globalResumeTicket = true;//to run tPostJob




        end = System.currentTimeMillis();

        if (watch) {
            System.out.println((end-startTime)+" milliseconds");
        }

        endUsedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        if (false) {
            System.out.println((endUsedMemory - startUsedMemory) + " bytes memory increase when running : SQL_TO_XSL");
        }
        talendStats_STATS.addMessage(status==""?"end":status, (end-startTime));
        try {
            talendStats_STATSProcess(globalMap);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }



if (execStat) {
    runStat.updateStatOnJob(RunStat.JOBEND, fatherNode);
    runStat.stopThreadStat();
}
    int returnCode = 0;
    if(errorCode == null) {
         returnCode = status != null && status.equals("failure") ? 1 : 0;
    } else {
         returnCode = errorCode.intValue();
    }
    resumeUtil.addLog("JOB_ENDED", "JOB:" + jobName, parent_part_launcher, Thread.currentThread().getId() + "", "","" + returnCode,"","","");

    return returnCode;

  }

    // only for OSGi env
    public void destroy() {


    }














    private java.util.Map<String, Object> getSharedConnections4REST() {
        java.util.Map<String, Object> connections = new java.util.HashMap<String, Object>();







        return connections;
    }

    private void evalParam(String arg) {
        if (arg.startsWith("--resuming_logs_dir_path")) {
            resuming_logs_dir_path = arg.substring(25);
        } else if (arg.startsWith("--resuming_checkpoint_path")) {
            resuming_checkpoint_path = arg.substring(27);
        } else if (arg.startsWith("--parent_part_launcher")) {
            parent_part_launcher = arg.substring(23);
        } else if (arg.startsWith("--watch")) {
            watch = true;
        } else if (arg.startsWith("--stat_port=")) {
            String portStatsStr = arg.substring(12);
            if (portStatsStr != null && !portStatsStr.equals("null")) {
                portStats = Integer.parseInt(portStatsStr);
            }
        } else if (arg.startsWith("--trace_port=")) {
            portTraces = Integer.parseInt(arg.substring(13));
        } else if (arg.startsWith("--client_host=")) {
            clientHost = arg.substring(14);
        } else if (arg.startsWith("--context=")) {
            contextStr = arg.substring(10);
            isDefaultContext = false;
        } else if (arg.startsWith("--father_pid=")) {
            fatherPid = arg.substring(13);
        } else if (arg.startsWith("--root_pid=")) {
            rootPid = arg.substring(11);
        } else if (arg.startsWith("--father_node=")) {
            fatherNode = arg.substring(14);
        } else if (arg.startsWith("--pid=")) {
            pid = arg.substring(6);
        } else if (arg.startsWith("--context_type")) {
            String keyValue = arg.substring(15);
			int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.setContextType(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.setContextType(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }

            }

		} else if (arg.startsWith("--context_param")) {
            String keyValue = arg.substring(16);
            int index = -1;
            if (keyValue != null && (index = keyValue.indexOf('=')) > -1) {
                if (fatherPid==null) {
                    context_param.put(keyValue.substring(0, index), replaceEscapeChars(keyValue.substring(index + 1)));
                } else { // the subjob won't escape the especial chars
                    context_param.put(keyValue.substring(0, index), keyValue.substring(index + 1) );
                }
            }
        }else if (arg.startsWith("--log4jLevel=")) {
            log4jLevel = arg.substring(13);
		}

    }
    
    private static final String NULL_VALUE_EXPRESSION_IN_COMMAND_STRING_FOR_CHILD_JOB_ONLY = "<TALEND_NULL>";

    private final String[][] escapeChars = {
        {"\\\\","\\"},{"\\n","\n"},{"\\'","\'"},{"\\r","\r"},
        {"\\f","\f"},{"\\b","\b"},{"\\t","\t"}
        };
    private String replaceEscapeChars (String keyValue) {

		if (keyValue == null || ("").equals(keyValue.trim())) {
			return keyValue;
		}

		StringBuilder result = new StringBuilder();
		int currIndex = 0;
		while (currIndex < keyValue.length()) {
			int index = -1;
			// judege if the left string includes escape chars
			for (String[] strArray : escapeChars) {
				index = keyValue.indexOf(strArray[0],currIndex);
				if (index>=0) {

					result.append(keyValue.substring(currIndex, index + strArray[0].length()).replace(strArray[0], strArray[1]));
					currIndex = index + strArray[0].length();
					break;
				}
			}
			// if the left string doesn't include escape chars, append the left into the result
			if (index < 0) {
				result.append(keyValue.substring(currIndex));
				currIndex = currIndex + keyValue.length();
			}
		}

		return result.toString();
    }

    public Integer getErrorCode() {
        return errorCode;
    }


    public String getStatus() {
        return status;
    }

    ResumeUtil resumeUtil = null;
}
/************************************************************************************************
 *     145163 characters generated by Talend Open Studio for Data Integration 
 *     on the 30 mai 2018 02:47:56 CEST
 ************************************************************************************************/