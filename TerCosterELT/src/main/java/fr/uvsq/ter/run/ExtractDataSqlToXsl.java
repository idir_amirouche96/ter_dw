package fr.uvsq.ter.run;

/**
 * Cette classe permet de lancer l'application et d'extraire la base de données de notre DW vers un fichier .xsl
 * 
 * Penser à changer le chemin de destination car ce n'est pas le même
 * 
 * "/home/cbma/Data/TOS_DI-20180411_1414-V7.0.1/workspace/DIM_ENS_DPT.xls"
 */
import dw_ter.sql_to_xsl_0_1.SQL_TO_XSL;

public class ExtractDataSqlToXsl {

	public static void main(String[] args) {
		SQL_TO_XSL sqlToXsl = new SQL_TO_XSL();
		sqlToXsl.runJob(new String[]{});
	}

}
