package fr.uvsq.ter.run;

/**
 * Cette classe permet de lancer l'application et de fusionner les données
 * avec ce qui se trouve déjà dans le DW
 * 
 * Penser à changer le chemin de destination car ce n'est pas le même
 * 
 * "/home/cbma/Data/TOS_DI-20180411_1414-V7.0.1/workspace/DIM_ENS_DPT.xls" !
 */

import dw_ter.fusion_fact_tables_0_1.FUSION_FACT_TABLES;

public class MergeDataFactTab {

	public static void main(String[] args) {
		System.out.println("Debut de la fusion de la base de donnée !");
		FUSION_FACT_TABLES fusion = new FUSION_FACT_TABLES();
		fusion.runJob(new String[]{});
		System.out.println("Fin de la fusion de la base de donnée !");
	}

}
