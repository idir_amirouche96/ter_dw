package fr.uvsq.ter.run;

/**
 * Cette classe permet de lancer l'application et de charger une base de données du service informatique
 * extrait les données les transforme puis les stock dans une nouvelle base de données 
 * créee au préalable et les tables vides aussi doivent être créee avant de lancer le main
 * 
 * ps: lors de l'ecriture des données (Output), on supprime l'ancienne table et on la créer à nouveau 
 *  
 * 
 * Penser à changer le chemin de destination car ce n'est pas le même
 * 
 * "/home/cbma/Data/TOS_DI-20180411_1414-V7.0.1/workspace/DIM_ENS_DPT.xls"
 */

import dw_ter.transformation_chargement_dw_0_1.TRANSFORMATION_CHARGEMENT_DW;

public class TransformationMain {

	public static void main(String[] args) {

		System.out.println("Debut de la transformation de la base de donnée !");
		TRANSFORMATION_CHARGEMENT_DW transforme = new TRANSFORMATION_CHARGEMENT_DW();
		transforme.runJob(new String[]{});
		System.out.println("Fin de la transformation de la base de donnée !");

	}

}
