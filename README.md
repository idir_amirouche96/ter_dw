# README #

### Titre du projet 
Construire un entrepôt de données pour les services d'enseignement

### But du projet 

Le suivi des services d'enseignement des enseignants,tache importante dans une composante d'université, se fait depuis plusieurs années avec l'application "_coster_ " .
Nous allons donc récuperer les données de cette application et les intégrer dans un entrepot de donées dont nous expliquerons la structure .
Nous pourorns grace à cette entrepot regénérer les tableaux de bord de l'application.

### Conception de l'entrepot de donnée
 
Un entrepôt de données, ou data Warehouse, est une vision centralisée et universelle de toutes les informations de l'entreprise. 
C'est une structure (comme une base de données) qui à pour but, contrairement aux bases de données, de regrouper les données de l'entreprise pour des fins analytiques et pour aider à la décision stratégique.
Lorsqu'on fait un schéma de BD en BI il est impératif de construire les dimensions et les faits . 

Les dimensions sont les axes avec lesquels on veut faire l'analyse ,aprés une longue analyse des bases de données de l'aplication nous avons choisi d'en extraire les dimensions suivantes :
* DIM_ENSEIGANTS_DEPARTEMENT
* DIM_TEMPS
* DIM_MODULE_PAR_SEMESTRE
* DIM_DETAIL_UE


Les faits sont les tables sur lesquels va porter notre analyse . Ces tables contiennent des informations operationnels tel que des aggrégas ,des bilans...
Les clés primaires des dimentions sont les clés étrangéres de notre table de fait :
		codedept
		enseigantsID
		codesemestre
		codemodesemestre
		année 
 
Les dimensions et les faits devants étre mis en relation dans un entrepot de donnée, nous avons choisis un modéle en étoile . En effet nous aurons qu'une seul table de fait reliés directement aux différentes dimensions .

### ETL

Un ETL(Extract Transform Load) sert a transformer le modèle entitées-relations des bases de données ainsi que les autres modèles utilisés en modèle à base de dimensions et de faits .

* Extration (Exctract) :D
Nous avons tout d'abord deployé l'aplication avec Docker_compose .Nous en avons extrait les tables des differentes années et des differents département.
Sur Talend nous avons créer une nouvelle conexion a la base de donnée et nous avons 

* Transformation :
Pour cette partie nous avons transformé nos bases de données pour ne garder que les données utiles a la constructions des tableaux de bord demandé .
Nous avons d'abord suprimé les tables et les tuples inutiles . 

* Load(chargement) : 
Aprés les transformations et la création du modéle nous pouvons les charger dans notre entrepot de donnée .

### ETL utilisé : pourquoi TALEND ?

Nous avons choisi talend pour ses multiples aventages :

    L'esthétique et l'ergonomie sont agréables et conviviales
    L'arbre de navigation est clair et bien organisé
    Le souci apporté à la documentation est un gros plus
    L'assistant d'installation est convivial


### exemples de transformations effectués sur les tables pour construire les dimensions  

* L'application contenait au départ trois tables regroupant chaqu'une les heures faites par les enseignants :
		Preservice_CM
		Preservice_TP
		Preservice_TD
  Puis nous avons reconstruit une nouvelle table contenant ces trois champs ainsi que leur somme , en effet cela permet de réduire considerablement le nombre de 	  table et la rondendece : les atributs CodeModuleSemestre et enseignantID reviennent dans chaqu'unes des tables. 
 Nous avons aussi fait de méme pour les tables : HeurTD , HeurCM et HeurTP pour les regrouper en une seul table , HEUR_A_FAIRE , et ainsi que leur total .

* Au niveau de la table enseignants plusieurs attributs ne sont pas nécessaires pour l'analyse des données tel que : passeword ,email et année 

* Nous avons également supprimé les table Log et theme que nous avons égalements jugé inutiles .

### Choix pour la construction des dimensions :

Les dimensions ont été choisi afin que l'on puisse ,à partir de celles-ci , faire des analyses de données adéquate aux tableaux de bords .Expliquons les  :

*	DIMENSION ENSEIGNANT_DEPARTEMENT : nous avons tout d'abord concaténé le nom et le prenom de l'enseignant pour plus de lisibilité . nous avons joint la table enseignants et grades (ajout des attributs codecourt , nomlong, type , heures ) , nous avont ajouté l'attribut  DIRECTEUR provenant de la table département . 
Comme sa définition le dit , une dimension doit permetre de retracer l'ensemble des données de notre base initial .
Ainsi pour retrouver un enseigneignant du departement informatique ou de biologie il est obligatoire d'ajouter le tuple codept (de la table département) afin de pouvoir filtrer la dimension DIM_ENSEIGNANT et de se placer sur le département désirérer . 
Il en est de méme pour l'étribut année ajouté depuis la table semestre qui permet de se placer sur l'année désiré .


*	DIMENSION MODULE_PAR_SEMESTRE : cette dimension regroupe à la fois la table dimension et la table semestre , nous avont ainsi choisi codemodsemestre et codesemestre comme clé primaire . 
Nous avons aussi concaténé à cette dimension les tables HEUR_FAITES et HEURE_FAIRE que nous avons nouvellement construit . A partire de la nous avons pu calculé le Bilan ( BILANTD , BILAN_TP et BILAN_CM, BILAN_UE) qui résulte de la différence entre le TOTAL_HEURFAITE et TOTAL_HEUREFAIRE .

*	DIMENSION DETAIL_UE : Cette dimension nous donne toutes les informations combiné concernant les UE . En effet, elle regroupe l'attribut codemodsemestre , qui est clé primaire de la dimension, l'attribut EnseigantsID pour pouvoir retrouver tout les enseignants ayant enseigné une UE choisi , l'attribut ID_RESPONSABLE nous donne le reponsable de chaque et UE . Nous aussi concaténé a celle ci la table HEURE_A_FAIRE pour savoir le nombre d'heure faite par les enseignant dans une UE ainsi que le total d'heur enseigné dans une UE .

* 	DIMENSION Temps :

### choix pour la construction de la table de FAIT :
Nous avions deux possibilité différente pour la construction de la table des faits :

*	Une permiére méthode était de généré une table de fait propre aux dimensions d'une seul base de donnée . Puis regénérer les méme dimensions et faits pour chaque années et chaque départements . Et ensuite les regrouper en seul modéle en étoile a partir de jointure, chose pas évidente. nous n'avons pas opter pour celle ci car dés que nous l'avons esssayé nous avons remarqué des pertes de tuples . Il y a aussi une difficulté d'insertion des tuples causé par les contraintes d'integrité.

*	Générer une table de fait contenant tout les tuple sur toute les bases de donnée ( sur les différentes années et sur les différents départements) . A partir de ce fichier nous pouvons reconstituer n'importe quelle mesure et cela en appliquant des requétes et filtres (jointure adéquate) . A la différence cette méthode permet de génerer directement la table de sans passer par une étape intermédiaire contrairement à la premiére méthode .

### Partie réstitution => PowerBI :
Nous avons choisi pour la partie réstitution d'utiliser powerBI et cela pour ses mulitples aventage :
	```

	D’extraire et transformer les données depuis différentes sources de données (fichiers, bases de données, services web).
	De concevoir des modèles de données.
	De créer des visualisations et des rapports interactifs.
	De publier sur Power BI.
	De faire des cartographie grace au moteur Bing 
	Connexion à différentes sources de données et extraction de ces données ;
	Nettoyage et transformation des données;
	Modélisation;
	Visualisation (rapports et tableaux de bords interactifs);
	Collaboration;

	```

A partir de PowerBI nous chargons la table de fait et nous appliquons les filtres souhaités afin de retrouver les tablaux de bord désirés.


*	Reconstitution du tableau bilan_enseigants_dpt  : ce résultat consiste à prendre la table DIM_ENSEIGANT_DEPARTEMENT puis appliquer un filtre sur codedept , l'année souhaité et l'atribut Nom_Prenom pour choisir quel enseignant dont on désire voir les résultats.
Ce résultat peut étre retrouver par la requéte suivante : 
	```
	SELECT NOM_PRENOM_ENS , INTITULE_MODULE, heuresCM, heuresTD, heuresTP
	FROM fact_table
	WHERE codedept=1
	AND ANNNE_ENS=2016
	AND enseignantID=63

	```

*	Reconstitution du tableau bilan_UE : Dans ce tableau nous avons repris toute les UE d'une année choisi, nous y ajoutons les Heure_A_Faire et les Heures_Faites . Ainsi nous pouvons déduire le Bilan_UE en faisant la soustraction des deux champs . Nous pouvons alors comparer le bilan entre chaque UE.

*	Reconstitution du tableau Liste_UE_Semestre : Ici nous avons toutes les UEs enseignés ainsi que les heures correspendant en appliquant comme filtre le choix d'un semestre , d'une année et d'un departement 

*	Reconstitution du tableau detail_UE : Nous pouvons ici retouver tout les enseignant ayant enseigné un module choisi au cours d'un semestre 

*	Reconstitution du tableau Service : Nous pouvons reconstitué toute les UEs d'un enseignant en faisant un filtre sur une année, et département et un enseigants pour avoir toutes les heures faite pour un module enseigné . en prenant comme clé primaire le codesemestre .
