# Archives de Coster
Dans ce projet se trouve les archives de l'application de gestion des services d'enseignement Coster.
Les données et les anciennes versions de l'application sont déployables avec [docker](https://docs.docker.com/get-started/) et [docker-compose](https://docs.docker.com/compose/overview/).

## Construire les conteneurs
```
$ docker-compose -f coster.yml build
```

## Démarrer l'application
L'application sera déployer à l'adresse [`http://localhost:8081`](http://localhost:8081).
Pour l'application, tous les mots de passe sont `nopass`.

Une version de phpmyadmin pouvant se connecter au SGBD est disponible à l'adresse [`http://localhost:8080`](http://localhost:8080).
Le nom du serveur MySQL doit être `coster-db` et les mots de passe sont `nopass` (pour `root` par exemple).

```
$ docker-compose -f coster.yml up
```

La combinaison de touches `Ctrl+C` stoppera l'ensemble du service.

Pour supprimer l'ensemble des conteneurs, il suffit de taper la commande suivante :

```
$ docker-compose -f coster.yml rm
```

