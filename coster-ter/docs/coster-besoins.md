# Besoins fonctionnels

L’application de gestion des services d’enseignement doit permettre de gérer l’ensemble des aspects liés aux services des enseignants. Elle doit fournir aux différents acteurs les outils adaptés à leurs tâches.

## Utilisateur
Chaque utilisateur dispose d’un profil comportant son nom, son prénom, son email et son mot de passe qu’il peut modifier. Il doit pouvoir consulter l’ensemble des informations disponibles dans l’application (services, …).

L’application doit permettre à un utilisateur de retrouver un mot de passe oublié. Une option serait d’utiliser le système d’authentification de l’UVSQ (CAS).

Les super-utilisateurs ont la possibilité de gérer les comptes.

## Visualisation des services
À tout moment, il doit être possible de visualiser les maquettes et les services (bilan détaillé d’un enseignant, bilans synthétiques pour un diplôme, pour un département, pour les enseignants d’un département, …) et de naviguer dans les informations.

L’application devra mettre en évidence les UE pour lesquelles les déclarations ne sont pas conforme à la maquette. De même, les écarts entre les services saisis et les services dus seront signalés.

Des exemples de visualisation sont disponibles.

## Saisie des services
Un enseignant doit pouvoir saisir les heures constituant son service. Pour cela, l’application présentera la liste des UE organisée selon une structure hiérarchique et permettra une recherche par mot-clé (voire par facettes).

Le responsable d’une UE peut saisir dans cette UE des heures pour d’autres enseignants.

Le directeur de département peut saisir des heures dans les UE de son département pour d’autres enseignants. De plus, il peut saisir/attribuer les réductions de service ainsi que les primes et décharges.

## Validation des services
Le responsable d’UE peut empêcher les modifications de son UE. Dans ce cas, aucune nouvelle saisie ne peut y avoir lieu tant qu’il n’a pas autorisé de nouveau les modifications.

De même, le directeur de département peut empêcher les modifications de toutes UE de son département.

## Gestion des maquettes
Le directeur du département saisit les maquettes des diplômes de son département.

Les super-utilisateurs ont la possibilité de modifier la structure des composantes et des départements.

## Gestion des enseignants
Le directeur du département ou le service de scolarité du département ajoutent les enseignants rattachés à leur département.

L’application propose la visualisation de la liste des enseignants avec filtre sur le département de rattachement et tri selon la catégorie et le nom.

