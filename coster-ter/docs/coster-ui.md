# Captures d'écran de l'ancienne application

## Liste des UE
![Liste des UE](img/liste_ue.png "Liste des UE")

## Détail d'une UE
![Détail d'une UE](img/detail_ue.png "Détail d'une UE")

## Service d’un enseignant
![Service d’un enseignant](img/service.png "Service d’un enseignant")

## Bilan par enseignant
![Bilan par enseignant](img/bilan_ens.png "Bilan par enseignant")

## Bilan d’un département
![Bilan d’un département](img/bilan_dtp.png "Bilan d’un département")

