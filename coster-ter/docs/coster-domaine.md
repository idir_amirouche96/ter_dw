# Description du domaine

## Structure de l’université
L’université est constituée de composantes (UFR, institut, école, …) qui regroupent des départements. Chaque département possède un nom (en général celui de la discipline) et est dirigé par un directeur.

## Diplôme et maquette
Un diplôme est rattaché à un département. Il est décrit par une maquette. Cette dernière comporte un certain nombre de semestres, au cours desquels sont dispensés des unités d’enseignement (UE). La maquette peut évoluer d’une année à l’autre. Certaines UE (ou partie d’UE) peuvent être mutualisées entre diplômes.

Le coût d’un diplôme représente l’ensemble des heures dispensées dans ce diplôme. Le coût total des formations d’une structure (département, UFR, …) dépend de la mutualisation des UE.

## Unités d’enseignement
Une UE est décrite par un code UE, un libellé et dispose d’un enseignant responsable.

Dans la maquette, un volume horaire est attribué à chaque UE. Il se décompose en heures de cours (CM), heures de TD et heures de TP. Ce volume horaire peut être complété par de l’accompagnement pédagogique. Il s’agit d’un séance de CM supplémentaire pour aider les étudiants à préparer l’examen. Elle a lieu dans les UE où le délai entre l’examen de 1ère et de 2ème session est insuffisant.

Pour une année donnée, la décision d’ouvrir une UE dépend du nombre d’étudiants inscrits à cette UE. Si ce nombre est supérieur au seuil fixé au niveau du diplôme, l’UE est ouverte. Le nombre de groupe pour chaque type d’heure dépend également de l’effectif. À partir d’un certain seuil spécifique à un type d’heure, un nouveau groupe est ouvert. Toutefois, le directeur de département peut décider d’ouvrir ou de ne pas ouvrir un groupe dans les cas limites.

Certaines UE (UE projet, …) peuvent avoir des règles de gestion spécifiques.

Les informations concernant les effectifs d’étudiant inscrits dans une UE est disponible dans l’application Apogée utilisée par les services scolarité de l’université.

## Service d’enseignement
Les heures assignées aux UE dans la maquette sont dispensées au cours du semestre par des enseignants. Un service d’enseignement regroupe l’ensemble des heures effectuées par un enseignant au cours d’une année. En fonction de son type (enseignant statutaire, ATER, contrat doctoral, …), un enseignant peut avoir un service statutaire. Ce dernier correspond au nombre d’heures que l’enseignant doit au minimum assurer durant une année.

Les heures faites en plus de son service statutaire sont des heures complémentaires. Certains statuts ne permettent pas de faire des heures complémentaires, d’autres en limitent le nombre. Par exemple, un maître de conférences ou un professeur ne peut assurer plus de 1,75 fois son service statutaire.

Le total des heures de service est exprimé en heure équivalent TD (hqTD). Chaque type d’heure (CM, TD, TP) est convertible en hqTD selon un certain coefficient (en général, 1h CM = 1,5 hqTD, 1h TD = 1 hqTD, 1h TP = 1 hqTD).

Le service prévisionnel, déclaré par les enseignants en début d’année, représente les heures que l’enseignant prévoit de faire durant une année. Le service réalisé, également déclaré par l’enseignant mais en fin d’année, précise les heures qu’il a effectivement assurées. Il conduit à l’édition d’un état liquidatif que l’enseignant devra signer.

## Primes, décharges et réductions de service
Pour une année donnée, le service d’un enseignant peut être influencé pour différents motifs.

Un enseignant peut disposer d’une réduction de service (congé maternité, …) exprimée en hqTD. Elle est décomptée de son service statutaire pour établir son service dû. Les réductions de service sont affectées par le directeur de département après notification par le service du personnel.

Un enseignant peut également disposer de primes et/ou décharges exprimées en hqTD. Ces dernières sont comptées dans son service de la même façon que des heures d’enseignement.

Les primes, décharges ou réductions de service sont toujours associées à un motif expliquant leur rôle.

## Validation des services
Chaque année, le directeur de département doit valider les services des enseignants de son département.

En début d’année, il s’assure que les services prévisionnel des enseignants sont en conformité avec les services dus. De même, il doit vérifier que l’ensemble des enseignements prévus sont couverts par des enseignants. Les services prévisionnels sont ensuite soumis au vote du conseil de la composante. Ils sont également saisis dans l’application Geïsha de l’UVSQ par le secrétariat.

En fin d’année, le directeur de département vérifie que les services effectués sont conforme aux services dus et que l’ensemble de heures ont été couvertes. Dans les cas où une différence apparaît, il prend contact avec les enseignants concernés pour identifier la cause de l’écart.

Après validation, si des écarts par rapport à la maquette persistent, ils sont considérés comme justifiés. Les heures de la maquette qui n’ont pas été assurées (UE non ouverte, séance annulée, ...) sont dites « économisées » par le département. Les heures ajoutées à la maquette (cours supplémentaire, …) sont également enregistrées dans les heures économisées (en négatif).

## Gestion des enseignants
Les enseignants sont recrutés par le service du personnel de l’UVSQ et géré par l’application Harpège. Chaque enseignant est rattaché à un unique département mais peut enseigner dans d’autres.

Le directeur du département ainsi que le secrétariat du département sont informés de la liste des enseignants rattachés pour une année donnée. Le potentiel enseignant d’un département est la somme des services statutaires de ses enseignants. Le taux d’encadrement est le rapport entre le coût des formations du département sur son potentiel enseignant.

Plusieurs catégories d’enseignants interviennent dans un département. Les permanents sont les fonctionnaires ou les CDI (professeur (PR), maître de conférences (MC), PRAG, PAST, ATER, Demi-ATER, ATER 6 mois, autre enseignant-chercheur UVSQ). Les contractuels peuvent être ATER, Demi-ATER, ATER 6 mois, en contrat doctoral, ou en CDD. Enfin, les vacataires (du public ou du privé) interviennent de façon ponctuelle dans les formations.

