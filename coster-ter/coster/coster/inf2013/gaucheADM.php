<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

?>

<html >
<head>
	<title>Untitled Document</title>
	<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="171" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="167"     background="Images/back.gif"><font color="#0000FF"><strong>Semestre</strong></font></td>
  </tr>
  <tr>
    <td><a   target="mainFrame"  href="new_semestre.php?annee&nom&dept&resp">Nouveau</a>    </td>
  </tr>
  <tr>
    <td height="28"  ><a  href="delete_semestre.php?id"  target="mainFrame">Supprimer</a></td>
  </tr>
  <tr>
    <td background="Images/back.gif"><font color="#0000FF"><strong>Module</strong></font></td>
  </tr>
  <tr>
    <td height="26"  ><a   target="mainFrame" href="new_module.php?pref&suf&intitule&infos&resp"></a>    <a   target="mainFrame" href="new_module.php?pref&suf&intitule&infos&resp">Nouveau</a></td>
  </tr>
<tr><td><a href="edit_modules.php?login&dtl"  target="mainFrame">Modifier Module / Horaires</a></td></tr>

  <td height="2"><a href="delete_module.php?id&cat=IN"  href="new_modulesemestreV1.php?annee&dtl" target="mainFrame">Supprimer</a> </td>
  </tr>
  <tr> 
  <td height="2"><a  href="affiche_service.php?type=horaires&dtl&ens&annee&ctg=L1 S1&sem&codeens=<?php echo $_SESSION['id_user'];?>"  target="mainFrame">S'inscrire et infos</a><a  target="mainFrame"  href="new_semestre.php?annee&nom&dept&resp"></a></td>
  </tr>
  <tr>
    <td background="Images/back.gif"><strong><font color="#0000FF">Enseignant</font></strong></td>
  </tr>
  <tr>
  <td><a   target="mainFrame"  href="new_modulesemestreV1.php?annee&dtl"></a>  <a   target="mainFrame" href="new_enseignant.php?nom&prenom&grade&dept">Nouveau</a></td>
  </tr>
  <tr>
  <td>  <a   target="mainFrame" href="delete_enseignant.php?id&car=A"> Supprimer</a>|<a   target="mainFrame" href="edit_enseignant.php">Modifier</a></td>
  </tr>
   
  <tr>
    <td background="Images/back.gif"><font color="#0000FF"><strong>Bilan</strong></font></td>
  </tr>
  <tr>
    <td ><a   target="mainFrame"  href="affiche_service.php?type=enseignant&annee&sem&codeens&ctg&dtl&ens"> Enseignant </a></td>
  </tr>
  <tr>
    <td ><a   target="mainFrame"  href="bilanParSemestre.php"> Semestres </a></td>
  </tr>
  <tr>
    <td ><a   target="mainFrame"  href="new_modulesemestreV1.php?annee&dtl">Module</a></td>
  </tr>
    <tr>
    <td ><a  target="mainFrame"  href="affiche_service.php?dtl&ens=ok&type=enseignant&annee&ctg&sem&codprof&codeens=<?php echo $_SESSION['id_user'] ?>">Bilan Personnel</a><a   target="mainFrame"  href="delete_enseignant.php?id"></a></td>
  </tr>
  <tr>
    <td ><a   target="mainFrame"  href="listExportCSV.php"> Export CSV </a></td>
  </tr>
<tr>
    <td background="Images/back.gif"><font color="#0000FF"><strong>Export</strong></font></td>
  </tr>
    <tr>
    <td ><a   target="mainFrame"  href="pdf/affiche_feuille_heures.php">Etats de Service en pdf</a></td>
  </tr>    
    <tr>
    <td ><a   target="mainFrame"  href="email.php">Email</a></td>
  </tr></table>
</body>
</html>
