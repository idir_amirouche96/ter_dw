<?php
/**
 * Ajoute l'ent�te dans le PDF.
 * @param string $pdf le PDF � construire.
 * @param string $nom nom de l'enseignant
 * @param string $prenom pr�nom de l'enseignant
 * @param string $grade grade de l'enseignant
 */
function pdf_add_header($pdf, $nom, $prenom, $grade) {
    $pdf->Image('logouvsq_st.jpg',10,8,33);
    $pdf->Cell(40);
    $pdf->SetFont('Arial','B',11); 
    $pdf->Cell(100,10,'Etat de service UVSQ (titulaire ou contractuel) dpt ' . DPT_NAME . ' ' . ANNEE_DEBUT . '/' . ANNEE_FIN);
    $pdf->Ln(5);
    $pdf->Cell(40);
    $pdf->SetFont('Arial','',11); 
    $pdf->Cell(80,10,"NOM : $nom");
    $pdf->Cell(50,10,"PRENOM : $prenom");
    $pdf->Ln(5);
    $pdf->Cell(40);
    $pdf->Cell(80,10,"Composante : UFR Sciences");
    $pdf->Cell(50,10,"GRADE ou STATUT :$grade");
    $pdf->SetDrawColor(0,0,0);
    $pdf->Line(0,33, 298, 33);
    $pdf->Ln(15);
}

/**
 * Ajoute le bas de page dans le PDF.
 * @param string $pdf le PDF � construire.
 * @param float $heqtd nombre total d'heures effectu�es
 * @param float $gradeHeures nombre d'heures � faire pour ce grade
 */
function pdf_add_footer($pdf, $heqtd, $gradeHeures) {
    $pdf->SetX(150);
    $pdf->Cell(55,7,"Total Cadre A+B+C+D�charges : $heqtd",1,0,'L',1);
    $pdf->Ln(7);
    $pdf->SetX(150);
    $pdf->Cell(55,7,"dont heures supp. : " . ($heqtd - $gradeHeures),1,0,'L',1);
    $pdf->Ln(7);
    
    $pdf->SetFont('','B',8);
    $pdf->Cell(20,10,"Signature de l'agent");
    $pdf->SetX(120);
    $pdf->Cell(20,10,"Signature du Directeur du D�partement");
}

/**
 * Ajoute la case � cocher pour la PES dans le PDF.
 * @param string $pdf le PDF � construire.
 */
function pdf_add_pes($pdf) {
    $pdf->Cell(3,3,"",1,0,'C',0);
    $pdf->Cell (117,3," Prime d'Encadrement Doctoral et de Recherche (PEDR)");
    $pdf->Ln(5);
}

/**
 * Ajoute l'ent�te des d�charges dans le PDF.
 * @param string $pdf le PDF � construire.
 */
function pdf_add_decharge_header($pdf) {
    $pdf->SetFont('Arial','U',11);
    $s = "D�charges de service:";
    $pdf->Cell ($pdf->GetStringWidth($s),5,$s);
    $pdf->SetFont('Arial','',11);
    $pdf->Ln(5);
}

/**
 * Ajoute une ligne de d�charge dans le PDF.
 * @param string $pdf le PDF � construire.
 * @param string $intitule l'intitul� de la d�charge.
 * @param float $heqtd nombre d'heures de d�charge
 */
function pdf_add_decharge_detail($pdf, $intitule, $heqtd) {
    $pdf->Cell(120,5,$intitule);
    $pdf->Cell(40,5,"Volume (en HETD)");
    $pdf->Cell(18,5,$heqtd,1,0,'C',0);
    $pdf->Ln(5);
}

/**
 * Ajoute le pied de page des d�charges dans le PDF.
 * @param string $pdf le PDF � construire.
 * @param string $total nombre total d'heures de d�charge
 */
function pdf_add_decharge_footer($pdf, $total) {
    //TODO : afficher le nombre total d'heures
    $pdf->Cell(40,5,"Total (en HETD)");
    $pdf->Cell(18,5,$total,1,0,'C',0);
    $pdf->Ln(5);
    $pdf->Line(0,$pdf->GetY(), 298, $pdf->GetY());
    $pdf->Ln(5);
}

/**
 * Ajoute l'ent�te du tableau dans le PDF.
 * @param string $pdf le PDF � construire.
 * @param string $intitule titre du cadre.
 * @param string $col1 intitul� de la premi�re colonne
 */
function pdf_add_cadre_header($pdf, $intitule, $col1) {
    $pdf->SetFillColor(200,200,200); 
    $pdf->SetTextColor(0,0,0); 
    $pdf->SetDrawColor(0,0,0); 
    $pdf->SetLineWidth(.3); 
    $pdf->SetFont('','B',8);
    $pdf->Cell(195,7,$intitule,1,0,'C',1);
    $pdf->Ln();

    $pdf->SetFont('','',8);
    $pdf->Cell(40,14,$col1,1,0,'C',0);
    $pdf->Cell(60,7,"UE (ou �tape)",1,0,'C',0);
    $pdf->Cell(10,14,"R�gime",1,0,'C',0);
    $pdf->Cell(10,14,"P�riode",1,0,'C',0);
    $pdf->Cell(45,7,"Nb d'heures",1,0,'C',0);
    $pdf->Cell(15,14,"Nb grp.",1,0,'C',0);
    $pdf->Cell(15,14,"Tot (HETD)",1,0,'C',0);
    $pdf->Ln(7);
    
    $pdf->SetX(50);
    $pdf->Cell(40,7,"Libell� exact",1,0,'C',0);
    $pdf->Cell(20,7,"Code UE",1,0,'C',0);
    $pdf->SetX(130);
    $pdf->Cell(15,7,"CM",1,0,'C',0);
    $pdf->Cell(15,7,"TD",1,0,'C',0);
    $pdf->Cell(15,7,"TP",1,0,'C',0);
    $pdf->Ln(7); 

    $pdf->SetFillColor(224,235,255); 
    $pdf->SetTextColor(0); 
    $pdf->SetFont(''); 
}

/**
 * Ajoute une ligne du tableau dans le PDF.
 * @param string $pdf le PDF � construire.
 * @param string $semestre nom du semestre
 * @param boolean $fill true s'il faut colorier l'arri�re-plan
 * @param string $intitule intitul� du module
 * @param string $prefixe pr�fixe du code module
 * @param string $suffixe suffixe du code module
 * @param float $cm nombre d'heures de CM
 * @param float $td nombre d'heures de TD
 * @param float $tp nombre d'heures de TP
 * @param float $heqtd nombre total d'heures (H. eq. TD) 
 */
function pdf_add_cadre_detail($pdf, $fill, $semestre, $intitule, $prefixe, $suffixe, $cm, $td, $tp, $heqtd) {
    $pdf->Cell(40,6,substr($semestre,0,10).".",1,0,1,$fill);
    $pdf->Cell(40,6,substr($intitule,0,24).".",1,0,1,$fill);
    $pdf->Cell(20,6,"$prefixe $suffixe",1,0,1,$fill);
    //TODO : G�rer ici : I (initiale) C (continue) A (alternance) (On met par d�faut rien)
    $pdf->Cell(10,6,"",1,0,1,$fill);
    //TODO : G�rer ici la p�riode (le semestre S1 ou S2)
    $pdf->Cell(10,6,"",1,0,1,$fill);
    $pdf->Cell(15,6,"$cm",1,0,'C',$fill);
    $pdf->Cell(15,6,"$td",1,0,'C',$fill);
    $pdf->Cell(15,6,"$tp",1,0,'C',$fill);
    $pdf->Cell(15,6,"1",1,0,'C',$fill);
    $pdf->Cell(15,6,"$heqtd",1,0,'C',$fill);
    $pdf->Ln();
}

/**
 * Ajoute la fin du tableau dans le PDF.
 * @param string $pdf le PDF � construire.
 * @param string $intitule intitul� du total
 * @param float $heqtd nombre total d'heures
 */
function pdf_add_cadre_footer($pdf, $intitule, $heqtd) {
    $pdf->SetX(160);
    $pdf->Cell(45,7,"$intitule : $heqtd",1,0,'L',1);
    $pdf->Ln();
}

/**
 * Ajoute une page contenant le service d'un enseignant.
 * @param string $pdf le PDF � construire.
 * @param string $codeens identifiant de l'enseignant
 */
function pdf_service_enseignant($pdf, $codeens) {
    $query = 'SELECT nom, prenom, g.nomlong, g.codecourt, g.heures
                FROM enseignants e, grades g
               WHERE e.enseignantID = ' . $codeens .
               ' AND g.codegrade = e.codegrade';

    $result_ens = mysql_query($query)
        or die("SELECT Error: ".mysql_error());
    $row_ens = mysql_fetch_object($result_ens);

    $nom = $row_ens->nom;
    $prenom = $row_ens->prenom;
    $grade = $row_ens->nomlong;
    $gradeCourt = $row_ens->codecourt;
    $gradeHeures = $row_ens->heures;

    // Remplissage du PDF
    $pdf->AddPage();
    pdf_add_header($pdf, $nom, $prenom, $grade);
    pdf_add_pes($pdf);

    // Le service
    $query = 'SELECT intitule, preserviceCM.codemodsemestre as cms, enseignants.nom as nom, prenom,
                     codeprefixe, codesuffixe, cadre, prime, heuresCM, heuresTD, heuresTP, preserviceCM.verrou,
                     (heuresCM*1.5+heuresTD+heuresTP) as TOT, semestres.nom as semnom
                FROM enseignants, modules, menusemestre, preserviceCM, preserviceTD, preserviceTP, semestres
               WHERE preserviceCM.codemodsemestre=menusemestre.codemodsemestre
                 AND menusemestre.codemod=modules.codemod
                 AND preserviceCM.enseignantID=enseignants.enseignantID
                 AND preserviceTD.enseignantID=enseignants.enseignantID
                 AND preserviceTP.enseignantID=enseignants.enseignantID
                 AND preserviceTD.codemodsemestre=preserviceCM.codemodsemestre
                 AND preserviceTP.codemodsemestre=preserviceCM.codemodsemestre
                 AND semestres.codesemestre = menusemestre.codesemestre
                 AND enseignants.enseignantID=' . $codeens .
          ' ORDER BY codesuffixe';

    $result_serv = mysql_query( $query )
        or die("SELECT Error: ".mysql_error());

    // Les d�charges
    pdf_add_decharge_header($pdf);    
    $total_decharges = 0;
    while ($row_serv = mysql_fetch_object ($result_serv)) { 
        if ($row_serv->prime != 0){
            pdf_add_decharge_detail($pdf, $row_serv->intitule, $row_serv->TOT);
            $total_decharges += $row_serv->TOT;
        }
    }
    pdf_add_decharge_footer($pdf, $total_decharges);

    if (mysql_num_rows($result_serv) > 0) {
        mysql_data_seek($result_serv, 0); // retourne au d�but du r�sultat
    }
    
    // Premier cadre
    pdf_add_cadre_header($pdf,
                         "Cadre A: Service d'enseignement dans votre composante d'affectation",
                         "Intitul� et niveau de dipl�me");
    
    $total_cadre_A['CM'] = 0;
    $total_cadre_A['TD'] = 0;
    $total_cadre_A['TP'] = 0;
    $fill=0;
    while ($row_serv = mysql_fetch_object ($result_serv)) { 
        if ($row_serv->prime == 0 && $row_serv->cadre == 'A'){
            pdf_add_cadre_detail($pdf, $fill, $row_serv->semnom, $row_serv->intitule,
                $row_serv->codeprefixe, $row_serv->codesuffixe,
                $row_serv->heuresCM, $row_serv->heuresTD, $row_serv->heuresTP, $row_serv->TOT);
                $total_cadre_A['CM'] += $row_serv->heuresCM;
                $total_cadre_A['TD'] += $row_serv->heuresTD;
                $total_cadre_A['TP'] += $row_serv->heuresTP;
                $fill = !$fill; 
        }
    } 

    $total_A = $total_cadre_A['CM'] * COUT_HEURE_CM
                + $total_cadre_A['TD'] * COUT_HEURE_TD
                + $total_cadre_A['TP'] * COUT_HEURE_TP;
    pdf_add_cadre_footer($pdf, "Total Cadre A", $total_A);
    
    $pdf->Ln(10); // Un peu d'espace entre les deux cadres
    if (mysql_num_rows($result_serv) > 0) {
        mysql_data_seek($result_serv, 0); // retourne au d�but du r�sultat
    }
        
    // Deuxi�me cadre
    pdf_add_cadre_header($pdf,
                         "Cadre B: Service d'enseignement dans une autre composante de l'UVSQ",
                         "Composante");
    
    //Donn�es 
    $total_cadre_B['CM'] = 0;
    $total_cadre_B['TD'] = 0;
    $total_cadre_B['TP'] = 0;
    $fill=0;
    while ($row_serv = mysql_fetch_object ($result_serv)) { 
        if ($row_serv->prime == 0 && $row_serv->cadre == 'B'){
            pdf_add_cadre_detail($pdf, $fill, $row_serv->semnom, $row_serv->intitule,
                $row_serv->codeprefixe, $row_serv->codesuffixe,
                $row_serv->heuresCM, $row_serv->heuresTD, $row_serv->heuresTP, $row_serv->TOT);
                $total_cadre_B['CM'] += $row_serv->heuresCM;
                $total_cadre_B['TD'] += $row_serv->heuresTD;
                $total_cadre_B['TP'] += $row_serv->heuresTP;
                $fill = !$fill; 
        }
    } 
    
    $total_B = $total_cadre_B['CM'] * COUT_HEURE_CM
                + $total_cadre_B['TD'] * COUT_HEURE_TD
                + $total_cadre_B['TP'] * COUT_HEURE_TP;
    pdf_add_cadre_footer($pdf, "Total Cadre B", $total_B);
    
    $pdf->Ln(10); // Un peu d'espace entre les deux cadres
    
    // Trois�me cadre
    pdf_add_cadre_header($pdf,
                         "CADRE C : Service d'enseignement statutaire dans un autre �tablissement (convention)",
                         "Etablissement");
    
    //Donn�es 
    $total_cadre_C['CM'] = 0;
    $total_cadre_C['TD'] = 0;
    $total_cadre_C['TP'] = 0;

    //TODO : ce cas n'est pas trait�
    
    $total_C = $total_cadre_C['CM'] * COUT_HEURE_CM
                + $total_cadre_C['TD'] * COUT_HEURE_TD
                + $total_cadre_C['TP'] * COUT_HEURE_TP;
    pdf_add_cadre_footer($pdf, "Total Cadre C", $total_C);

    $pdf->Ln(10); // Un peu d'espace � la fin
    
    // R�cap. et bas de page
    pdf_add_footer($pdf, $total_A + $total_B + $total_C + $total_decharges, $gradeHeures);
}

?>
