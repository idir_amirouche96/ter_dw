<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 

$id = $_REQUEST["id"];
$login = $_REQUEST["login"];
$semestre = $_REQUEST["sem"];
if (isset($_REQUEST['codeens'])) {
    $codeens = $_REQUEST['codeens'];
}
if (isset($_REQUEST['heuresCM'])) {
    $heuresCM = $_REQUEST["heuresCM"];
}
if (isset($_REQUEST['heuresTD'])) {
    $heuresTD = $_REQUEST["heuresTD"];
}
if (isset($_REQUEST['heuresTP'])) {
    $heuresTP = $_REQUEST["heuresTP"];
}
if (isset($_REQUEST['sub'])) {
    $sub = $_REQUEST["sub"];
}
if (isset($_REQUEST['paye'])) {
    $paye = $_REQUEST["paye"];
}
$go = isset($_REQUEST['go']) ? $_REQUEST["go"] : ''; 

print "<html><head><title>Mise � jour d'horaires de Module</title></head><body>";


// *********
// Gestion du verrou
// *********

if ( ($go!="" and $sub=="verrou") ){
	$query ="UPDATE menusemestre SET verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceCM set verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
}

if ( ($go!="" and $sub=="deverrouiller") ){
	$query ="UPDATE menusemestre SET verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
  	$query = "UPDATE preserviceCM set verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
}

// *********
// Gestion du verrou DPT (Cut and Paste)
// *********

if ( ($go!="" and $sub=="verrouDPT") ){
	$query ="UPDATE menusemestre SET verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceCM set verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
}

if ( ($go!="" and $sub=="deverrouillerDPT") ){
	$query ="UPDATE menusemestre SET verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
  	$query = "UPDATE preserviceCM set verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
}


// *********
// Gestion du paiement
// *********

if ( ($go!="" and $paye!="") ){
    $query="
    SELECT not paye as p
       FROM preserviceCM
       WHERE codemodsemestre=$id
       AND enseignantID=$paye
       FOR UPDATE
    ";
    $resu = mysql_query($query)
    		or die("query is : ".$query." SELECT Error: ".mysql_error());
    $res=mysql_fetch_object($resu);
    
    	$query ="
    UPDATE preserviceCM 
    SET paye=$res->p
    WHERE codemodsemestre=$id
    AND enseignantID=$paye";
    
    $resu = my_query($query);
    
    $query ="
    UPDATE preserviceTD 
    SET paye=$res->p
    WHERE codemodsemestre=$id
    AND enseignantID=$paye";
    
    $resu = my_query($query);
    
    $query ="
    UPDATE preserviceTP
    SET paye=$res->p
    WHERE codemodsemestre=$id
    AND enseignantID=$paye";
    
    $resu = my_query($query);



}





// **********
// Gestion de la MAJ
// **********


if ( ($go!="" and $sub=="update") ){
	if ($heuresCM=="")
   		$heuresCM=0;
	if ($heuresTD=="")
   		$heuresTD=0;
	if ($heuresTP=="")
		$heuresTP=0;
	$query="
	SELECT codemodsemestre AS id, verrou
	FROM menusemestre
	WHERE codemodsemestre =".$id. 
	" FOR UPDATE
	";

    $resu = mysql_query($query)
        or die("query is : ".$query." SELECT Error: ".mysql_error());
    
    $res = mysql_fetch_object($resu);

    if ($res->verrou == 0){
    
        // ON drop le service d'abord
        
        $query="
        DELETE FROM preserviceCM
        WHERE codemodsemestre=".$id.
        " AND enseignantID=".$codeens;
        
        $resu = my_query($query);
        
        $query="
        DELETE FROM preserviceTD
        WHERE codemodsemestre=".$id.
        " AND enseignantID=".$codeens;
        
        $resu = my_query($query);
        
        $query="
        DELETE FROM preserviceTP
        WHERE codemodsemestre=".$id.
        " AND enseignantID=".$codeens;
        
        $resu = my_query($query);
        
        // DROP FINI
        
        
        // *** Le probl�me du delete : si HCM = 0 et HTD = 0
        // Alors on insere rien
        
        
        if ( ($heuresCM !=0) or ($heuresTD !=0) or ($heuresTP !=0)){
        
            $query="
            INSERT INTO preserviceCM (codemodsemestre, enseignantID, heuresCM)
            VALUES(".$id.",".$codeens.",".$heuresCM.")";
            
            $resu = my_query($query);
            
            $query="
            INSERT INTO preserviceTD (codemodsemestre, enseignantID, heuresTD)
            VALUES(".$id.",".$codeens.",".$heuresTD.")";
            
            $resu = my_query($query);
            
            $query="
            INSERT INTO preserviceTP (codemodsemestre, enseignantID, heuresTP)
            VALUES(".$id.",".$codeens.",".$heuresTP.")";
            
            $resu = my_query($query);
        
        }
        
        print "<fieldset>
        <legend>
        Mise � jour effectu�e
        </legend>";
        
        print "</fieldset><br/>";
    } else { // $res->verrou != 0
        print "<fieldset><legend>Mise � jour impossible : Les heures sont verouill�es</legend></fieldset><br/>";
    }
} // FIN MISE A JOUR

print '
<fieldset>
<legend>Editer Horaires</legend>
<a href=affiche_service.php?type=horaires&codeens='.$login.'&sem='.$semestre.'>Retour page des modules</a>
<form action="edit_service.php" method="GET">
<input type="hidden" name="go" value=1 />
<input type="hidden" name="login" value='.$login.' />
<input type="hidden" name="id" value="'.$id.'" />
<input type="hidden" name="sem" value="'.$semestre.'" />
<table>';


// on r�cup le nom
if ( $login !=""){
    $query="
    SELECT nom, prenom, enseignantID
    FROM enseignants
    WHERE enseignantID=".$login;
    
    $resu = mysql_query ($query)
        or die("SELECT Error: ".mysql_error());
    
    while ($res=mysql_fetch_object($resu)){
        $mynom = $res->nom;
        $myprenom = $res->prenom;
    }
}


// on r�cup si on est directeur ou pas
// ICI
$query ="
SELECT directeur
FROM departements
WHERE codedept=".DPT_ID.
" AND directeur=".$login;

$resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
$res=mysql_fetch_object($resu);

$directeur = false;
if ($res)
   $directeur = true;

if ($directeur)
  echo "<h3>DEBUG : VOUS ETES DIRECTEUR</h3>";


$query="
SELECT sem.anneedebut as annee, sem.nom as nom, CM.heuresCM as HCM, CM.nombregroupes as GCM, TD.heuresTD as HTD, TD.nombregroupes as GTD, TP.heuresTP as HTP, TP.nombregroupes as GTP, m.codeprefixe as pref, m.codesuffixe as suf, m.intitule as intitule, e.nom as enom, e.prenom as eprenom, m.responsable, s.verrou, s.verrouDPT 
FROM semestres as sem, menusemestre as s, modules as m, horairesCM as CM, horairesTD as TD, horairesTP as TP, enseignants as e
WHERE s.codemodsemestre=".$id."
AND s.codesemestre = sem.codesemestre
AND s.codemodsemestre = CM.codemodsemestre
AND s.codemodsemestre = TD.codemodsemestre
AND s.codemodsemestre = TP.codemodsemestre
AND s.codemod = m.codemod
AND m.responsable = e.enseignantID";

$resu = mysql_query($query)
    or die("query is : ".$query." SELECT Error: ".mysql_error());

$res = mysql_fetch_object ($resu);

$responsable = $res->responsable;

//********
// IMPORTANT
// le directeur est consid�r� comme responsable
//*********

if ($directeur)
   $responsable = true;


print "<h2>POUR INFO: ".$myprenom." ".$mynom."</h2>";
print "<h3>Toute nouvelle entr�e efface votre pr�c�dent service</h3>";
print '<h2>'.$res->nom.' '.$res->annee.' '.$res->pref.' '.$res->suf.' '.$res->intitule.'</h2>';

$varver = $res->verrou;
$varverDPT = $res->verrouDPT;

if ($res->verrou)
   print "<tr><td style=background:LightSalmon>Module VERROUILLE";
else
   print "<tr><td style=background:LightGreen >Module NON VERROUILLE";
print "</td></tr>";

print '<tr><td>Reponsable : '.$res->eprenom.' '.$res->enom.'</td></tr>';

/*
print '<tr><td>CM : <input type="text" readonly="readonly" name="heuresCMP" value="'.$res->HCM.'" size=2 /> Groupes : <input type="text" name="groupesCMP" readonly="readonly" value="'.$res->GCM.'" size=2 /></td></tr>';

print '<tr><td>TD : <input type="text" name="heuresTDP" readonly="readonly" value="'.$res->HTD.'" size=2 /> Groupes : <input type="text" readonly="readonly" name="groupesTDP" value="'.$res->GTD.'" size=2 /></td></tr>';
*/

print '<tr><td>CM : '.$res->HCM.' Groupes : '.$res->GCM.'</td></tr>';

print '<tr><td>TD : '.$res->HTD.' Groupes : '.$res->GTD.'</td></tr>';

print '<tr><td>TP : '.$res->HTP.' Groupes : '.$res->GTP.'</td></tr>';



$CMTOT = $res->HCM*$res->GCM;
$CMTOTeqTD = $CMTOT*COUT_HEURE_CM;
$TDTOT = $res->HTD*$res->GTD;
$TPTOT = $res->HTP*$res->GTP*COUT_HEURE_TP;

print "<tr><td> </td></tr>";

// TODO : OPTIMISER LA QUERY SUIVANTE
$query="

SELECT E.enseignantID, E.nom as nom, E.prenom as prenom, TD.heuresTD as TD, CM.heuresCM as CM, TP.heuresTP as TP, E.codegrade, TD.paye as TDP, CM.paye as CMP, TP.paye as TPP
FROM enseignants as E, preserviceTD as TD, preserviceCM as CM, preserviceTP as TP
WHERE E.enseignantID = TD.enseignantID
AND E.enseignantID = CM.enseignantID
AND E.enseignantID = TP.enseignantID
AND TD.codemodsemestre = ".$id.
" AND CM.codemodsemestre = ".$id.
" AND TP.codemodsemestre = ".$id.
" ORDER BY E.nom, E.prenom";

$resu = mysql_query($query)
    or die("query is : ".$query." SELECT Error: ".mysql_error());

print "<tr><td>NOM</td><td>CM</td><td>TD</td><td>TP</td>";

if ($directeur && $varverDPT)
   print "<td>Pay�</td>";

print "</tr>";
print "<tr><td style=background:PaleGoldenRod >TOTAL A FAIRE :</td><td style=background:PaleGoldenRod >".$CMTOT."</td><td style=background:PaleGoldenRod >".$TDTOT."</td><td style=background:PaleGoldenRod >".$TPTOT."</td>";
print"</tr>";

// On se sert d'un compteur et non plus de SQL m�me si on pourrait le faire.
$CMP =0;
$TDP =0;
$TPP =0;

while ($res=mysql_fetch_object($resu)){
    if ($res->codegrade == 6){ // VACATAIRE
        print '<tr style=background:LightBlue><td>'.$res->nom.' '.$res->prenom.'</td><td>'.$res->CM.'</td><td>'.$res->TD.'</td><td>'.$res->TP.'</td>';
        $CMP+=$res->CM;
        $TDP+=$res->TD;
        $TPP+=$res->TP;
    } else {
        print '<tr><td>'.$res->nom.' '.$res->prenom.'</td><td>'.$res->CM.'</td><td>'.$res->TD.'</td>'.'</td><td>'.$res->TP.'</td>';
        $CMP+=$res->CM;
        $TDP+=$res->TD;
        $TPP+=$res->TP;
    }
    
    // **** on ne traite que les CMP, les 2 �tant parfaitement li�s au niveau de la paye
    if ($directeur && $varverDPT){
       if ($res->CMP) {
           print "<td style=background:LightGreen >OUI</td>";
       } else { 
           print "<td style=background:LightSalmon>NON</td>";
       }
       print "<td><input type='submit' name='paye' value='$res->enseignantID'/>";
    }
    print "</tr>";
}

$bilanCM = $CMTOT-$CMP;
$bilanTD = $TDTOT-$TDP;
$bilanTP = $TPTOT-$TPP;

$c1 = "LightSalmon";
$c2 = "LightSalmon";
$c3 = "LightSalmon";
$c4 = "LightSalmon";

if  ($bilanCM==0)
  $c2 = "lightGreen ";
if  ($bilanTD==0)
  $c3 = "lightgreen";
if ($bilanTP==0)
  $c4 = "lightgreen";

if  ($bilanCM<0)
  $c2 = "LightSkyBlue";
if  ($bilanTD<0)
  $c3 = "LightSkyBlue";
if  ($bilanTP<0)
  $c4 = "LightSkyBlue";

if ( ($bilanCM==0) and ($bilanTD==0) and ($bilanTP==0) )
  $c1 ="lightgreen";
if ( ($bilanCM<0) or ($bilanTD<0) or ($bilanTP<0) )
  $c1 ="LightSkyBlue";



print "<tr><td style=background:".$c1.">Heures non pourvues :</td><td style=background:".$c2.">".$bilanCM."</td><td style=background:".$c3.">".$bilanTD."</td><td style=background:".$c3.">".$bilanTP."</td></tr>";
print "<tr><td>*****************************************</td><td>*******</td><td>*******</td><td>*******</td></tr>";

if ( (!$varver) or ($login==$responsable) ){


print "<tr><td>NOUVELLE ENTREE</td></tr>";
print '<tr><td></td><td>CM : <input type="text"  name="heuresCM"  size=2 /> heures</td><td>TD: <input type="text" name="heuresTD"  size=2 /> heures</td><td>TP: <input type="text" name="heuresTP"  size=2 /> heures</td></tr>';
print '<tr><td>Enseignant : </td><td>';






if ($login == $directeur){    
    print '<td><select name="codeens">';
    $query="
    SELECT nom, prenom, enseignantID
    FROM enseignants
    ORDER BY nom
    ";
    
    $resu = mysql_query ($query)
        or die("SELECT Error: ".mysql_error());
    
    $k =0;
    while ($res=mysql_fetch_object($resu)){
      print "<option value=$res->enseignantID";
      if ($k==0 and $login ==""){
        $k = 1;
        print ' selected="selected" ';
      }
      if ($login==$res->enseignantID){
       print ' selected="selected" ';   
      }
      print ">$res->nom $res->prenom</option>";
    }
    print '</select>';
}

if ($login != $directeur){
    $query="
    SELECT nom, prenom, enseignantID
    FROM enseignants
    WHERE enseignantID=".$login;
    
    $resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
    
    $k =0;
    while ($res=mysql_fetch_object($resu)){      
      print "<td>$res->nom $res->prenom</td></tr>";
      print '<tr><input type="hidden" name="codeens" value="'.$login.'"></input></tr>';
    }
}

// **************************
// * on g�re ici le verrou
// **************************

if ($login == $responsable and $bilanCM==0 and $bilanTD==0 and $bilanTP==0 and !$varverDPT){
 if ($varver==0) 
   print "<tr><td/><td><input type='submit' name='sub' value='verrou'/></td></tr>";
  if ($varver==1) 
   print "<tr><td/><td><input type='submit' name='sub' value='deverrouiller'/></td></tr>";
}


// *** update 

if (!$varver && !$varverDPT)
   print "<tr><td/><td><input type='submit' name='sub' value='update'/></td></tr>";
}


//*******************
//* On g�re le verrou DPT
//*******************
if ($directeur && $varver){
    print "<tr><td>******************************************</td><td>*******</td></tr>";
    if ($varverDPT==0) 
        print "<tr><td style=background:lightgreen>Partie Verrou D�partement</td><td><input type='submit' name='sub' value='verrouDPT'/></td></tr>";
    if ($varverDPT==1) 
        print "<tr><td style=background:LightSalmon>Partie Verrou D�partement</td><td><input type='submit' name='sub' value='deverrouillerDPT'/></td></tr>";
}

print"</table>
</form>
</fieldset>
</body>
</html>
";

mysql_close($link);

?>
