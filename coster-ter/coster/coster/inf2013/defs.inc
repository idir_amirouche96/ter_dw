<?php
/**
 * 
 * Version de Coster.
 * @var string
 */
define('VERSION', '1.3.1');
//$version = "1.3.1";

/**
 * 
 * Chemin de l'installation.
 * @var string
 */
define('INSTALL_PATH', '/var/www/html/svrens/');

/**
 * 
 * Message d'information.
 * @var string
 */
define('AIDE', 'Benjamin Nguyen (D�partement Informatique [benjamin.nguyen]@prism.uvsq.fr)');
//$aide = "Benjamin Nguyen (D�partement Informatique [benjamin.nguyen]@prism.uvsq.fr)";

/**
 * 
 * Adresse du serveur de BD.
 * @var string
 */
define('DB_HOST', 'coster-db');

/**
 * 
 * Login pour se connecter � la BD.
 * @var string
 */
define('DB_LOGIN', 'root');

/**
 * 
 * Mot de passe pour se connecter � la BD.
 * @var string
 */
define('DB_PASS', 'nopass');

/**
 * 
 * Nom de la BD.
 * @var string
 */
define('DB_NAME', 'svrens_2013');
//$db = "info2009";

/**
 * 
 * D�partement copncern�.
 * @var string
 */
define('DPT_NAME', 'Informatique');
//$departement = "Informatique";

/**
 * 
 * Identifiant du d�partement (info = 1, bio = 2).
 * @var int
 */
define('DPT_ID', 1);
//$dptID = 1; // info=1, bio = 2

/**
 * 
 * Ann�e.
 * @var int
 */
define('ANNEE_DEBUT', 2012);
//$annee = 2009;

/**
 * 
 * Ann�e de fin.
 * @var int
 */
define('ANNEE_FIN', ANNEE_DEBUT + 1); // const ne fonctionne pas avec des expressions...
//$anneeFin = $annee+1;

/**
 * 
 * Co�t d'une heure de cours magistral en heure �quivalent TD.
 * @var double
 */
define('COUT_HEURE_CM', 1.5);
//$coutHeureCM = 1.5;

/**
 * 
 * Co�t d'une heure de TD en heure �quivalent TD.
 * @var double
 */
define('COUT_HEURE_TD', 1.0);
//$coutHeureTD = 1.0;

/**
 * 
 * Co�t d'une heure de TP en heure �quivalent TD.
 * @var double
 */
define('COUT_HEURE_TP', 1.0);
//$coutHeureTP = 1.0;

/**
 * 
 * Administration des primes et d�charges ???
 * @var string
 */
define('ADM_PRIME_DECH', 'Administration-Prime-Dech');
//$administrationPrimeDech = "Administration-Prime-Dech";

/**
 * 
 * Administration des r�ductions ???
 * @var string
 */
define('ADM_REDUCTION', 'Administration-Reduction');
//$administrationReduction = "Administration-Reduction";

?>
