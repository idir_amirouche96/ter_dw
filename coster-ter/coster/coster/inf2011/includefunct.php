<?php

/**
 *
 * Redirige le navigateur vers l'URL en utilisant Javascript.
 * @param string $url l'URL de la page � charger
 */
function js_redirect($url) {
    echo '<script type="text/javascript" language="javascript">document.location="' . $url . '";</script>';
}

/**
 * 
 * Performs the query and logs it if suceeded
 * @param string $text the query
 */
function my_query($text) {
    // TODO : Il faudrait pr�voir ici un syst�me qui teste si la session est expir�e (bug d'insert sinon) � pr�voir apr�s la version 1.3.1

    $result = mysql_query($text)
        or die("REQUETE SQL :" . $text . "SELECT Error: " . mysql_error());

    $query = 'INSERT INTO log VALUES (' . $_SESSION['id_user'] . ',"' . $text . '","' . date("Y-m-d G:i:s") . '")';
    $result = mysql_query($query)
        or die("REQUETE SQL :" . $query . "INSERT Error: " . mysql_error());

    mysql_query("COMMIT")
        or die("COMMIT Error: " . mysql_error());
}

/**
 * 
 * Retourne un objet décrivant l'enseignant.
 *  
 * @param int $id Identifiant de l'enseignant
 * @param resource $link Ressource de connexion à la BD
 */
function selectEnseignantById($id, $link) {
	$query = "
SELECT enseignantID AS id, prenom, nom, nomlong AS grade, heures AS servicedu
  FROM enseignants NATURAL JOIN grades 
 WHERE enseignantID = ".$id;

    $result = mysql_query($query, $link)
        or die("SELECT Error: " . mysql_error());
    $row = NULL;
    if (mysql_num_rows($result) > 0) {
    	$row = mysql_fetch_object($result);
    }
    mysql_free_result($result);
	return $row;    
}

/**
 * 
 * Retourne un tableau d'objets décrivant le service d'un enseignant
 * 
 * @param int $id Identifiant de l'enseignant
 * @param resource $link Ressource de connexion à la BD
 */
function selectServiceEnseignant($id, $link) {
	$query = "
SELECT semestres.codesemestre AS 'SemId', semestres.nom AS 'SemNom',
       menusemestre.codemodsemestre AS 'cms',
       modules.codemod AS 'ModCode', modules.codeprefixe AS 'ModPref', modules.codesuffixe AS 'ModSuff',
       modules.intitule AS 'ModNom', modules.prime AS 'ModPrime',
       v_preservice.CM AS 'CM', v_preservice.TD AS 'TD', v_preservice.TP AS 'TP',
       CM * " . COUT_HEURE_CM . " + TD + TP * " . COUT_HEURE_TP . " AS EqTD,
       preserviceCM.verrou AS 'Verrou',preserviceCM.paye AS 'Paye' 
  FROM v_preservice JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
                    JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
                    JOIN modules ON menusemestre.codemod = modules.codemod
                    JOIN preserviceCM ON menusemestre.codemodsemestre = preserviceCM.codemodsemestre
                                     AND v_preservice.enseignantID = preserviceCM.enseignantID
 WHERE v_preservice.enseignantid = " . $id . " 
ORDER BY semestres.nom ASC, modules.codeprefixe ASC, modules.codesuffixe ASC
";
    $result = mysql_query($query, $link)
        or die("SELECT Error: ".mysql_error());
        
    $hEqTDComptabilisees = 0; // le nombre d'heures eq. TD comptabilisées
    $hEqTDCPrimes = 0; // le nombre d'heures eq. TD de primes et/ou décharges
    $hEqTDCReductions = 0; // le nombre d'heures eq. TD de réduction de service
    $hEqTDCEffectuees = 0; // le nombre d'heures eq. TD effectuées
    $faites = array();
    $primes = array();
    $reductions = array();
    while ($row = mysql_fetch_object($result)){
    	$hEqTDComptabilisees += $row->EqTD;
    	if ($row->ModPrime != 0) {
		    if ($row->SemId == 41) { // Semestre Admin -- Réduction de Service
		    	$hEqTDCReductions += $row->EqTD;
		    	$reductions[] = $row;
		    } else {
		    	$hEqTDCPrimes += $row->EqTD;
		    	$primes[] = $row;
		    } 
	    } else {
	    	$hEqTDCEffectuees += $row->EqTD; 
	    	$faites[] = $row;
	    }
    }
    $bilan = array("comptabilisees" => $hEqTDComptabilisees, "effectuees" => $hEqTDCEffectuees, "primes" => $hEqTDCPrimes, "reductions" => $hEqTDCReductions);
    return array("faites" => $faites, "primes" => $primes, "reductions" => $reductions, "bilan" => $bilan);
}

function displayServiceEnseignant($enseignant, $service, $total) {
	if (0 == count($service)) {
		print "Aucun.";
	} else {
	    print "<table border=1><tr><th>MODULE</th><th>CM</th><th>TD</th><th>TP</th><th>Total eq. TD</th><th>Verrou</th><th>En paiement</th></tr>\n";
	    foreach ($service as $i => $ligneService) {
	        print "<tr><td>$ligneService->ModPref $ligneService->ModSuff $ligneService->ModNom</td>
	               <td style=\"text-align: right\">" . number_format($ligneService->CM, 2) . "</td>
	               <td style=\"text-align: right\">" . number_format($ligneService->TD, 2) . "</td>
	               <td style=\"text-align: right\">" . number_format($ligneService->TP, 2) . "</td>
	               <td style=\"text-align: right\">" . number_format($ligneService->EqTD, 2) . "</td>";
	        if (!$ligneService->Verrou) {
	            print "<td>";
	            // On peut modifier uniquement si on est propriétaire du service
	            // ou si on est admin.
	            if ($enseignant->id == $_SESSION['id_user'] || 'ADMIN' == $_SESSION['prvg']) {
// TODO : à quoi sert semestre ci-dessous ?            	
//                    print "<a href=edit_service.php?id=" . $ligneService->cms . "&login=" . $enseignant->id . "&sem=" . $semestre . " >";
	                print "<a href=edit_service.php?id=" . $ligneService->cms . "&login=" . $enseignant->id . "&sem= >";
	            	print "Modifier";
	                print "</a>";
	            }
	            print "</td>";
	        } else {
	           print "<td style=:background:red>Verrou ENS</td>";
	        }
	        if ($ligneService->Paye) {
	            print"<td style=background:lightgreen>OUI</td>";
	        } else {
	           print "<td style=background:red>NON</td>";
	        }
	        print "</tr>";
	    }
	    print "<tr>
	               <th colspan=\"4\">TOTAL</th>
	               <th style=\"text-align: right\">" . number_format($total, 2) . "</th>
	           </tr>";
	    print "</table>";
	}
}

/**
 * 
 * Retourne un tableau d'objets décrivant les services de tous les enseignants
 * 
 * @param resource $link Ressource de connexion à la BD
 */
function selectServiceEnseignants($link) {
	$query = "
SELECT enseignantID, nom, prenom,
       codecourt, heures,
       CM, TD, TP,
	   Reduction,
	   Prime
  FROM (
		SELECT id,
			   SUM(CM) AS 'CM', SUM(TD) AS 'TD', SUM(TP) AS 'TP',
			   SUM(Reduction) AS 'Reduction', SUM(Prime) AS 'Prime'
		  FROM (
				SELECT v_preservice.enseignantID AS 'id',
					   v_preservice.CM AS 'CM', v_preservice.TD AS 'TD', v_preservice.TP AS 'TP',
					   '0' AS 'Reduction',
					   '0' AS 'Prime'
				  FROM v_preservice JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
									JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
									JOIN modules ON menusemestre.codemod = modules.codemod
				 WHERE modules.prime = 0
				UNION ALL
				SELECT v_preservice.enseignantID AS 'id',
					   '0' AS 'CM', '0' AS 'TD', '0' AS 'TP',
					   v_preservice.TD AS 'Reduction',
					   '0' AS 'Prime'
				  FROM v_preservice JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
									JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
									JOIN modules ON menusemestre.codemod = modules.codemod
				 WHERE modules.prime = 1 AND semestres.codesemestre = 41
				UNION ALL
				SELECT v_preservice.enseignantID AS 'id',
					   '0' AS 'CM', '0' AS 'TD', '0' AS 'TP',
					   '0' AS 'Reduction',
					   v_preservice.TD AS 'Prime'
				  FROM v_preservice JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
									JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
									JOIN modules ON menusemestre.codemod = modules.codemod
				 WHERE modules.prime = 1 AND semestres.codesemestre <> 41
				) AS services_detail
		GROUP BY id
		) AS services RIGHT OUTER JOIN enseignants ON services.id = enseignants.enseignantID
		                  NATURAL JOIN grades
ORDER BY codecourt, nom, prenom
";
    $result = mysql_query($query, $link)
        or die("SELECT Error: ".mysql_error());
        
    $services = array();
    while ($row = mysql_fetch_object($result)){
	   	$services[] = $row;
    }
    return $services;
}

function displayServiceEnseignants($services) {
	if (0 == count($services)) {
		print "Aucun.";
	} else {
    	print "<table border=1>";
    	print "<tr><th colspan='3'>ENSEIGNANT</th>
    	           <th colspan='3'>SERVICE DU</th>
    	           <th colspan='3'>SERVICE EFFECTUE</th>
    	           <th colspan='2'>A PAYER</th></tr>";
    	print"<tr><th>Grade</th><th>NOM</th><th>PRENOM</th>
    	          <th>STAT.</th><th>RED.</th><th>DU</th>
    	          <th>EFFECTUE</th><th>PRIMES</th><th>BILAN</th>
    	          <th>HEURES SUPP.</th><th>PRIMES</th>
    	          <th></th><th></th></tr>\n";
    	foreach ($services as $i => $service) {
        	$du = $service->heures - $service->Reduction;
        	$effectue = $service->CM * COUT_HEURE_CM + $service->TD + $service->TP * COUT_HEURE_TP; 
        	$bilan = $effectue + $service->Prime - $du;
        	$color = "background:lightgreen";
	        if ($bilan > 0) {
	        	$color = "background:cyan";
	        } else if ($bilan < 0) {
	        	$color = "background:red";
	        }
    		print "<tr>";
    		print '<td style="' . $color . '">' . $service->codecourt . '</td>';
        	print '<td style="' . $color . '">' . $service->nom . '</td>';
        	print '<td style="' . $color . '">' . $service->prenom . '</td>';
        	print '<td style="text-align: right">' . number_format($service->heures, 2) . '</td>';
        	print '<td style="text-align: right">' . number_format($service->Reduction, 2) . '</td>';
        	print '<td style="text-align: right">' . number_format($du, 2) . '</td>';
        	
        	print '<td style="text-align: right">' . number_format($effectue, 2) . '</td>';
        	print '<td style="text-align: right">' . number_format($service->Prime, 2) . '</td>';
        	print '<td style="text-align: right">' . number_format($bilan, 2) . '</td>';
        	
	    	$decharge = max($du - $effectue, 0);
	    	$primeAPayer = $service->Prime - $decharge;
	    	$heuresSupp = max($effectue - $du, 0);
        	print '<td style="text-align: right">' . number_format($heuresSupp, 2) . '</td>';
        	print '<td style="text-align: right">' . number_format($primeAPayer, 2) . '</td>';
        	
	        // On affiche le lien permettant de changer le service
	        print "<td><a href=affiche_service.php?type=enseignant&codeens=$service->enseignantID>D�tails</a></td>";
	        if ('ADMIN' == $_SESSION['prvg']) {
	            print "<td><a href=edit_enseignant.php?id=$service->enseignantID>Enseignant</a></td>";
	        }
	        print "</tr>\n";
	   	}
	    print '</table>';
	}
}

?>