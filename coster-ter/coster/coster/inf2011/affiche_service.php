<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

if (isset($_REQUEST['pass'])) {
    // TODO : pourquoi stocker le mot de passe dans un cookie ?
    $pass = $_REQUEST['pass'];
    if ($pass != '') setcookie('passw', $pass);
} else {
    $pass = '';
}

$type = $_REQUEST["type"];
$codeens = $_REQUEST["codeens"]; // Id de l'enseignant pour lequel on veut l'information
$iduser = $_SESSION['id_user']; // Id de l'utilisateur connect�
$semestre = $_REQUEST["sem"];

$annee = (isset($_REQUEST['annee']) && $_REQUEST['annee'] != "") ? $_REQUEST["annee"] : ANNEE_DEBUT;
?>

<html>
<head>
	<title>Service par <?php echo $type; ?></title>
	<link rel="stylesheet" type="text/css" href="style-afficher-horaires.css"/>
</head>
<body>
<?php

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 


//********************************
// PARTIE AFFICHAGE PAR ENSEIGNANT
// *******************************

// Toutes les heures d'un enseignant sp�cifique
if ($codeens != '' and $type == 'enseignant'){
	$enseignant = selectEnseignantById($codeens, $link); 
	echo '<h1>Service de ' . $enseignant->prenom . ' ' . $enseignant->nom . '</h1>';

	$service = selectServiceEnseignant($codeens, $link);
	
	print '<h2>D�tail</h2>';
	print '<h3>Enseignements</h3>';
    displayServiceEnseignant($enseignant, $service["faites"], $service["bilan"]["effectuees"]);
    
	print '<h3>Primes/d�charges</h3>';
    displayServiceEnseignant($enseignant, $service["primes"], $service["bilan"]["primes"]);
	
	print '<h3>R�ductions de services</h3>';
    displayServiceEnseignant($enseignant, $service["reductions"], $service["bilan"]["reductions"]);
	
    print '<h2>Bilan</h2>';

    print '<h3>Service d� (' . $enseignant->grade . ')</h3>';
    print '<table border=1>';
    print '<tr>';
    print '<td>Service statutaire</td>';
    print '<td style="text-align: right">' . number_format($enseignant->servicedu, 2) . '</td>';
    print '</tr>';
    print '<tr>';
    print '<td>R�ductions de service</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["reductions"], 2) . '</td>';
    print '</tr>';
    $serviceDu = $enseignant->servicedu - $service["bilan"]["reductions"];
    print '<tr>';
    print '<td>Service d�</td>';
    print '<td style="text-align: right">' . number_format($serviceDu, 2) . '</td>';
    print '</tr>';
    print '</table>';
    
    print '<h3>Service effectu�</h3>';
    print '<table border=1>';
    print '<tr>';
    print '<td>Heures effectu�es</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["effectuees"], 2) . '</td>';
    print '</tr>';
    print '<tr>';
    print '<td>Primes et d�charges</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["primes"], 2) . '</td>';
    print '</tr>';
    print '</table>';
    
    $serviceEffectue = $service["bilan"]["effectuees"] + $service["bilan"]["primes"];
    if ($serviceEffectue > $serviceDu) {
    	$decharge = max($serviceDu - $service["bilan"]["effectuees"], 0);
    	$primeAPayer = $service["bilan"]["primes"] - $decharge;
    	$heuresSupp = max($service["bilan"]["effectuees"] - $serviceDu, 0);
	    print '<h3>Paiement</h3>';
	    print '<table border=1>';
	    print '<tr>';
	    print '<td>D�charge</td>';
	    print '<td style="text-align: right">' . number_format($decharge, 2) . '</td>';
	    print '</tr>';
	    print '<tr>';
	    print '<td>Heures supp. � payer</td>';
	    print '<td style="text-align: right">' . number_format($heuresSupp, 2) . '</td>';
	    print '</tr>';
	    print '<tr>';
	    print '<td>Primes � payer</td>';
	    print '<td style="text-align: right">' . number_format($primeAPayer, 2) . '</td>';
	    print '</tr>';
	    print '</table>';
    } else {
	    print '<h3>Reste � effectuer</h3>';
	    print '<table border=1>';
	    print '<tr>';
	    print '<td>Heures restant � effectuer</td>';
	    print '<td style=\"text-align: right\">' . number_format($serviceDu - $serviceEffectue, 2) . '</td>';
	    print '</tr>';
	    print '</table>';
    }   

	print "<h2>Modules dont vous avez la responsabilit�</h2>";
    $query = 'SELECT m.codemod, codeprefixe, codesuffixe, intitule, s.nom, codemodsemestre, verrou, verrouDPT
                FROM modules as m, semestres as s, menusemestre as ms
               WHERE m.codemod = ms.codemod
                 AND s.codesemestre = ms.codesemestre
                 AND m.responsable = ' . $codeens;
    
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());
    
    if (mysql_num_rows($result) > 0) {
	    print "<table border=1>";
	    while($row = mysql_fetch_object($result)){
	        print "<tr><td>$row->nom</td><td>$row->codeprefixe $row->codesuffixe</td><td>$row->intitule</td>";
	        // On peut modifier uniquement si on est propr�taire du service
	        // ou si on est admin.
	        if ($codeens == $iduser || 'ADMIN' == $_SESSION['prvg']) {
	            print "<td><a href=edit_service.php?id=$row->codemodsemestre&login=$codeens&sem=$semestre >G�rer le module</a></td>";
	        }
	        print (!$row->verrou) ? "<td style=background:lightgreen>Non verrouill�</td>" : "<td style=background:red>Verrouill�</td>"; 
	        print (!$row->verrouDPT) ? "<td style=:background:lightgreen>Non verrouill� DPT</td>" : "<td style=background:red>Verrou DPT</td>"; 
	        print"</tr>";
	    }
	    print "</table>";
    } else {
    	print "Aucun.";
    }
    
	print "<h2>Semestres dont vous avez la responsabilit�</h2>";
    $query = 'SELECT nom
                FROM semestres
               WHERE responsable = ' . $codeens . '
            ORDER BY nom ASC';
    
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());
    
    if (mysql_num_rows($result) > 0) {
	    print "<table border=1>";
	    while($row = mysql_fetch_object($result)){
	        print "<tr><td>$row->nom</td></tr>";
	    }
	    print "</table>";
    } else {
    	print "Aucun.";
    }
} // Fin PARTIE AFFICHAGE PAR ENSEIGNANT


//*************************************
// PARTIE HEURES TOTALES PAR ENSEIGNANT
//*************************************
if($codeens=="" and $type=="enseignant"){
    print "<h1>Total des heures du d�partement</h1>";
    $services = selectServiceEnseignants($link);
	displayServiceEnseignants($services);
} // FIN PARTIE HEURES TOTALES PAR ENSEIGNANT

// **********************************
// HORAIRES
// Appel� par le menu "Infos Modules" quand un enseignant est connect�
//***********************************
if ($type == "horaires"){
    //***************
    // On lance la requ�te
    //***************
    $query = "SELECT inti, verrou, verrouDPT, cms, anneedebut, nom, codeprefixe as pref, codesuffixe as suf, codesem, 
                     (SUM(HCM)*SUM(GCM)) as heuresCMPREV, SUM(HPCM) as heuresCMFAIT,
                     (SUM(HTD)*SUM(GTD)) as heuresTDPREV, SUM(HPTD) as heuresTDFAIT,
                     (SUM(HCM)*SUM(GCM)*1.5) as CMeqTDPREV, SUM(HPCM)*1.5 as CMeqTDFAIT,
                     (SUM(HTP)*SUM(GTP)) as heuresTPPREV, SUM(HPTP) as heuresTPFAIT,
                     (SUM(HCM)*SUM(GCM)*1.5+SUM(HTD)*SUM(GTD)+SUM(HTP)*SUM(GTP)) as TOTPREV,
                     (SUM(HPCM)*1.5+SUM(HPTD)+SUM(HPTP)) as TOTFAIT,
                     ( (SUM(HCM)*SUM(GCM)*1.5+SUM(HTD)*SUM(GTD)+SUM(HTP)*SUM(GTP))- (SUM(HPCM)*1.5+SUM(HPTD)+SUM(HPTP)) ) as bilan
                FROM (SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                             0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                             TP.heuresTP as HTP, 0 as HPTP, TP.nombregroupes as GTP,
                             m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                       FROM modules as m, menusemestre as s, semestres as sem, horairesTP as TP
                      WHERE s.codesemestre = sem.codesemestre
                        AND s.codemod = m.codemod
                        AND s.codemodsemestre = TP.codemodsemestre
                        AND sem.anneedebut = ".$annee."
                   GROUP BY s.codemodsemestre
                      UNION
                     SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                            0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD, 0 as HTP,
                            SUM(PTP.heuresTP) as HPTP, 0 as GTP,
                            m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                       FROM modules as m, menusemestre as s, semestres as sem, preserviceTP as PTP
                      WHERE s.codesemestre = sem.codesemestre
                        AND s.codemod = m.codemod
                        AND s.codemodsemestre = PTP.codemodsemestre
                        AND sem.anneedebut = ".$annee."
                   GROUP BY s.codemodsemestre
                      UNION
                     SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                            0 as HCM, 0 as HPCM, 0 as GCM, TD.heuresTD as HTD, 0 as HPTD, TD.nombregroupes as GTD,
                            0 as HTP, 0 as HPTP, 0 as GTP,
                            m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, horairesTD as TD
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = TD.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                           CM.heuresCM as HCM, 0 as HPCM, CM.nombregroupes as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, horairesCM as CM
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = CM.codemodsemestre
                       AND sem.anneedebut = ".$annee." 
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom,  codeprefixe, codesuffixe,
                           0 as HCM, SUM(PCM.heuresCM) as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, preserviceCM as PCM
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = PCM.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                           0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, SUM(PTD.heuresTD) as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, preserviceTD as PTD
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = PTD.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     ) as sub1
            GROUP BY cms
            ORDER BY nom, pref, suf";

// On va construire plusieurs Tableaux

//print "<table border=1>
//<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td>Module</td><td>Total heures CM pr�vues</td><td>Total heures CM faites</td><td>Total heures CM (eq.TD) pr�vues</td><td>Total heures CM (eq. TD) Faites</td><td>Total heures TD Pr�vues</td><td>Total heures TD faites</td><td>Total heures TP pr�vues</td><td>Total heures TP faites</td><td>Total heures eq. TD Pr�vues</td><td>Total heures eq. TD Faites</td><td>BILAN</td><td></td></tr>";

    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());

    $oldletter = "";
    $oldsem = "";
    while($row = mysql_fetch_object($result)){
        if ($oldletter != substr($row->nom, 0, 1)) {
        	if ($oldletter != ""){
        		print'<hr/>';
        	}
        	$oldletter = substr($row->nom, 0, 1);
        }
    
        if ($oldsem != $row->nom) {
            if ($oldsem != "") {
                print '</table><br />';
            }
            $oldsem = $row->nom;
              print "<a name=#$row->codesem><a href=affiche_service.php?pass=$pass&type=horaires&codeens=$codeens&sem=$row->codesem>$row->nom</a>";
    
            if ($semestre == $row->codesem) {
//                 print "<table border=1>
//                    <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
//                    <td>Module</td>
//                    <td>CM pr�vues (en h)</td>
//                    <td>CM faites (en h)</td>".
//                    //<td>Total heures CM (eq.TD) pr�vues</td>
//                    //<td>Total heures CM (eq. TD) Faites</td>
//                    "<td>TD Pr�vues (en h)</td>
//                    <td>TD faites (en h)</td>
//                    <td>TP Pr�vues (en h)</td>
//                    <td>TP Faites (en h)</td>
//                    <td>Total Pr�vues (en h eq. TD)</td>
//                    <td>Total Faites (en h eq. TD)</td>
//                    <td>BILAN (en h eq. TD)</td><td></td></tr>";

                print '<table border=1>
                <tr><td colspan=2 class="red">'.$row->nom.'</td>
                <td colspan=2 class="black">CM</td>
                <td colspan=2 class="black">TD</td>
                <td colspan=2 class="black">CP</td>
                <td colspan=2 class="black">Total</td>
                </tr>
                <tr>
                <td class="black">Code UE</td>
                <td class="black">Nom UE</td>
                <td class="black">A faire</td>
                <td class="black">Faites</td>
                <td class="black">A faire</td>
                <td class="black">Faites</td>
                <td class="black">A faire</td>
                <td class="black">Faites</td>
                <td class="black">A faire</td>
                <td class="black">Faites</td>
                <td class="black">Bilan</td>
                </tr>';
            }
        }
    
        if ( $semestre == $row->codesem) {
            print "<tr><td>".
                $row->pref." ".$row->suf.
                "</td><td>".$row->inti.
                "</td><td>".$row->heuresCMPREV.
                "</td><td>".$row->heuresCMFAIT.
                //"</td><td>".$row->CMeqTDPREV.
                //"</td><td>".$row->CMeqTDFAIT.
                "</td><td>".$row->heuresTDPREV.
                "</td><td>".$row->heuresTDFAIT.
                "</td><td>".$row->heuresTPPREV.
                "</td><td>".$row->heuresTPFAIT.
                "</td><td>".$row->TOTPREV.
                "</td><td>".$row->TOTFAIT.
                "</td><td style='";
    
            // ***************************
            // Selecteur de couleur !
            //
            // Attention la barre d'erreur est d�finie en DUR ici
            //
            // *******************************
            
            if ( ($row->bilan >= 0) and ($row->bilan <= 0 ) ) // ici aucune barre d'erreur n'est autoris�e !
               print "background:lightgreen";
            else if ( ($row->bilan >0) ) 
               print "background:red";
            else
               print "background:cyan";
            
            print "'>$row->bilan</td>";
            
            if (!$row->verrou)
              print "<td style=background:lightgreen><a href=edit_service.php?id=".$row->cms."&login=".$codeens."&sem=$semestre>S'inscrire</a></td>";
            else 
            if (!$row->verrouDPT)
              print "<td style=background:yellow><a href=edit_service.php?id=".$row->cms."&login=".$codeens."&sem=$semestre>Infos</a><p/>(ENS verrouill�)</td>";
            else  
               print "<td style=background:orange><a href=edit_service.php?id=".$row->cms."&login=".$codeens."&sem=$semestre>Infos</a><p/>(DPT verrouill�)</td>";
            
            print "</tr>\n";
        }
    } // Fin while
} // FIN HORAIRES

print '</table>';
mysql_close($link);
?>

</body>
</html>
