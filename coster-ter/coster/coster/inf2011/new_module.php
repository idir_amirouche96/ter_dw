<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 

$pref=$_REQUEST["pref"];
$suf=$_REQUEST["suf"];
$intitule=$_REQUEST["intitule"];
$infos=$_REQUEST["infos"];
$resp=$_REQUEST["resp"];
$cadre=$_REQUEST["cadre"];
$prime=$_REQUEST["prime"];

if (isset($_REQUEST["codesemestre"])) $codesemestre=$_REQUEST["codesemestre"];
if (isset($_REQUEST["codemod"])) $codemod=$_REQUEST["codemod"];
if (isset($_REQUEST["heuresCM"])) $heuresCM=$_REQUEST["heuresCM"];
if (isset($_REQUEST["heuresTD"])) $heuresTD=$_REQUEST["heuresTD"];
if (isset($_REQUEST["heuresTP"])) $heuresTP=$_REQUEST["heuresTP"];
if (isset($_REQUEST["groupesCM"])) $groupesCM=$_REQUEST["groupesCM"];
if (isset($_REQUEST["groupesTD"])) $groupesTD=$_REQUEST["groupesTD"];
if (isset($_REQUEST["groupesTP"])) $groupesTP=$_REQUEST["groupesTP"];

print"<html>
<head><title>Nouveau Module et horaires</title></head>
<body>";

if ( ($pref!="") and ($suf!="") and ($intitule!="")  and ($resp != "") and ($codesemestre!="") ) { // on autorise le champ info � �tre vide
    if ($heuresCM=="")
      $heuresCM=0;
    if ($heuresTD=="")
      $heuresTD=0;
    if ($heuresTP=="")
      $heuresTP=0;
    if ($groupesCM=="")
      $groupesCM=0;
    if ($groupesTD=="")
      $groupesTD=0;
    if ($groupesTP=="")
      $groupesTP=0;
    if ($prime == "")
      $prime = 0;
    
    
    
    
    //****
    // Utilisation de lock
    //****
    
    $query="
    SELECT MAX(codemod) AS idmod
    FROM modules
    FOR UPDATE
    ";
    
    $resu = mysql_query($query)
    or die("SELECT Error: ".mysql_error());
    
    $res = mysql_fetch_object($resu);
    
    $idmod = $res->idmod+1;
    
    $query="
    INSERT INTO modules (codemod, codeprefixe, codesuffixe, intitule, informations, responsable, cadre, prime)
    VALUES (".$idmod.",'".$pref."','".$suf."','".$intitule."','".$infos."',".$resp.",'".$cadre."',".$prime.")";
    
    $resu = my_query($query);
    
    
    $query="
    SELECT MAX(codemodsemestre) AS idcodmod
    FROM menusemestre
    FOR UPDATE
    ";
    
    $resu = mysql_query($query)
    or die("SELECT Error: ".mysql_error());
    
    $res = mysql_fetch_object($resu);
    
    $idcodmod = $res->idcodmod+1;
    
    $query="
    INSERT INTO menusemestre (codemodsemestre, codemod, codesemestre)
    VALUES (".$idcodmod.",".$idmod.",".$codesemestre.")";
    
    $resu = my_query($query);
    
    
    $query="
    INSERT INTO horairesCM (codemodsemestre, heuresCM, nombregroupes)
    VALUES (".$idcodmod.",".$heuresCM.",".$groupesCM.")";
    
    $resu = my_query($query);
    
    $query="
    INSERT INTO horairesTD (codemodsemestre, heuresTD, nombregroupes)
    VALUES (".$idcodmod.",".$heuresTD.",".$groupesTD.")";
    
    $resu = my_query($query);
    
    $query="
    INSERT INTO horairesTP (codemodsemestre, heuresTP, nombregroupes)
    VALUES (".$idcodmod.",".$heuresTP.",".$groupesTP.")";
    
    $resu = my_query($query);
    
    
    
    print "<fieldset>
    <legend>
    Insertion R�ussie
    </legend>
    Ajout du module $pref $suf r�ussie !
    </fieldset>";
}
?>


<fieldset>
<legend>
Nouveau Module
</legend>
<form action="new_module.php" method="POST">
<table>
<tr><td>Module : </td><td><input type="text" name="pref" size=2/><input type="text" name="suf" size=2/></td></tr>
<tr><td>Intitul� : </td><td><input type="text" name="intitule" size=80/></td></tr>
<tr><td>Informations : </td><td><textarea type="text" name="infos" rows=10 cols=80></textarea></td></tr>


<tr><td>Semestre : </td><td><select name="codesemestre">

<?php
$query="
SELECT codesemestre, nom, anneedebut
FROM semestres
ORDER BY anneedebut, nom
";

$resu = mysql_query ($query)
or die("SELECT Error: ".mysql_error());

$k =0;
while ($res=mysql_fetch_object($resu)){
  print "<option value=$res->codesemestre";
  if ($k=0){
    $k = 1;
    print ' selected="selected" ';
  }
  print ">$res->nom $res->anneedebut</option>";
}
?>

</select></td></tr>
<tr><td>Responsable : </td><td><select name="resp">

<?php
$query="
SELECT nom, prenom, enseignantID
FROM enseignants
ORDER BY nom
";

$resu = mysql_query ($query)
or die("SELECT Error: ".mysql_error());

$k =0;
while ($res=mysql_fetch_object($resu)){
  print "<option value=$res->enseignantID";
  if ($k==0){
    $k = 1;
    print ' selected="selected" ';
  }
  print ">$res->nom $res->prenom</option>";
}

mysql_close($link);
?>
</select></td></tr>

<tr>
	<td>Cadre : </td>
	<td>
		<select name="cadre">
			<option value="A" selected="selected">Cadre A</option>
			<option value="B">Cadre B</option>
		</select>
	</td>
</tr>

<tr>
	<td>Prime ou d�charge : </td>
	<td>
		<input type="checkbox" name="prime" value="1" />
	</td>
</tr>

<tr><td><b>Horaires (optionnel)</b></td></tr>
<tr><td>Heure CM : </td><td><input type="text" name="heuresCM" size=2/> Nombre de Groupes : <input type="text" name="groupesCM" size=2/></td></tr>

<tr><td>Heure TD : </td><td><input type="text" name="heuresTD" size=2/> Nombre de Groupes : <input type="text" name="groupesTD" size=2/></td></tr>

<tr><td>Heure TP : </td><td><input type="text" name="heuresTP" size=2/> Nombre de Groupes : <input type="text" name="groupesTP" size=2/></td></tr>

<tr><td/><td><input type="submit" value="Ajouter"/></td></tr>
</table>
</form>
</fieldset>
</body>
</html>