<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}
?>
<h1>Liste des exports CSV</h1>
<table>
	<tr><th>Etat</th><th>Description</th></tr>
	<tr>
		<td><a target="_blank" href="exportCSV.php?etat=services">Services</a></td>
		<td>Les services de tous les enseignants.</td>
	</tr>
	<tr>
		<td><a target="_blank" href="exportCSV.php?etat=servicesISTY">Services ISTY</a></td>
		<td>Les services des enseignants intervenant à l'ISTY.</td>
	</tr>
</table>



