<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

?>

<html >
<head>
	<title>Untitled Document</title>
	<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="200" border="1" cellspacing="0" cellpadding="0">
	<tr height="40"><td width="167" background="Images/back.gif"><strong><font color="#0000FF">Menu</font></strong></td></tr>
		<tr><td><a href="bas.php" target="mainFrame">Accueil</a></td></tr>
		<tr><td><a href="pass_change.php?id=<?php echo $_SESSION['id_user']; ?>" target="mainFrame">Changer votre mot de passe</a></td></tr>
    	<tr><td><a href="deconnecter.php" target="_parent">D�connecter</a></td></tr>
	<tr height="40"><td background="Images/back.gif"><font color="#0000FF"><strong>Gestion de vos heures</strong></font></td></tr>
		<tr><td><a target="mainFrame"  href="affiche_service.php?type=horaires&dtl&ens&annee&ctg=L1 S1&sem&codeens=<?php echo $_SESSION['id_user']; ?>">Acc�s par modules</a></td></tr>
	<tr height="40"><td background="Images/back.gif"><font color="#0000FF"><strong>Bilan personnel</strong></font></td></tr>
		<tr><td><a href="affiche_service.php?dtl&ens=ok&type=enseignant&annee&ctg&sem&codeens=<?php echo $_SESSION['id_user']; ?>" target="mainFrame">En ligne</a></td></tr>
		<tr><td><a href="pdf/affiche_feuille_heures_enseignant.php?codeens=<?php echo $_SESSION['id_user']; ?>" target="mainFrame">Version PDF</a></td></tr>
	<tr height="40"><td background="Images/back.gif"><font color="#0000FF"><strong>Bilan de tous les enseignants</strong></font></td></tr>
		<tr><td><a target="mainFrame" href="affiche_service.php?type=enseignant&annee&sem&codeens&ctg&dtl&ens">En ligne</a></td></tr>
		<tr><td><a target="mainFrame" href="pdf/affiche_feuille_heures.php">Version PDF</a></td></tr>
		<tr><td><a target="_blank" href="exportCSV.php?etat=services">Version CSV</a></td></tr>
	<tr height="40"><td background="Images/back.gif"><font color="#0000FF"><strong>Bilan des enseignements de l'ISTY</strong></font></td></tr>
		<tr><td><a target="_blank" href="exportCSV.php?etat=servicesISTY">Version CSV</a></td></tr>
</table>
</body>
</html>
