<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

$pass = $_REQUEST["pass"];
if ($pass != "") setcookie("passw", $pass);

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 

print "<table border=1>";
print "<tr><th>Semestre</th><th>Contractuels</th><th>Permanents</th><th>Vacataires</th></tr>";

$query="
SELECT s.nom, grades.type, SUM( heuresCMTOT ) AS CM, SUM( heuresCMTOT ) * 1.5 AS CMeqTD, SUM( heuresTDTOT ) AS TD, SUM( heuresTPTOT ) AS TP, SUM( heuresCMTOT ) * 1.5 + SUM( heuresTDTOT ) + SUM( heuresTPTOT) AS TOTAL 
FROM (

SELECT codesemestre, nom, prenom, sum( heuresCM ) AS heuresCMTOT, 0 AS heuresTDTOT, 0 AS heuresTPTOT, e.enseignantID, codegrade
FROM enseignants AS e, modules, menusemestre, preserviceCM
WHERE menusemestre.codemod = modules.codemod
AND preserviceCM.codemodsemestre = menusemestre.codemodsemestre
AND preserviceCM.enseignantID = e.enseignantID
GROUP BY nom, prenom, codegrade, codesemestre
UNION SELECT codesemestre, nom, prenom, 0 AS heuresCMTOT, SUM( heuresTD ) AS heuresTDTOT, 0 AS heuresTPTOT, e.enseignantID, codegrade
FROM enseignants AS e, modules, menusemestre, preserviceTD
WHERE menusemestre.codemod = modules.codemod
AND preserviceTD.codemodsemestre = menusemestre.codemodsemestre
AND preserviceTD.enseignantID = e.enseignantID
GROUP BY nom, prenom, codegrade, codesemestre
UNION SELECT codesemestre, nom, prenom, 0 AS heuresCMTOT, 0 AS heuresTDTOT, SUM( heuresTP ) AS heuresTPTOT, e.enseignantID, codegrade
FROM enseignants AS e, modules, menusemestre, preserviceTP
WHERE menusemestre.codemod = modules.codemod
AND preserviceTP.codemodsemestre = menusemestre.codemodsemestre
AND preserviceTP.enseignantID = e.enseignantID
GROUP BY nom, prenom, codegrade, codesemestre
) AS sub1 LEFT OUTER JOIN decharges d ON sub1.enseignantID = d.enseignantID LEFT OUTER JOIN reduction r on r.enseignantID = sub1.enseignantID, grades, semestres s
WHERE sub1.codegrade = grades.codegrade
AND sub1.codesemestre = s.codesemestre
GROUP BY grades.type, s.nom
ORDER BY s.nom, type";
 
$resu = mysql_query( $query )    or die("SELECT Error: ".mysql_error());

$sem = "";
$first = 1;


 while($res=mysql_fetch_object($resu)){
	if($res->nom != $sem){
		$perma = 0;
		if($first == 0)
			print "</tr>";
		if($first ==1)
			$first=0;
		print "<tr>";
		$sem = $res->nom;
		print"<td>$sem</td>";
		if($res->type == "contractuel"){
			print "<td>$res->TOTAL</td>";
			$cont = $cont + $res->TOTAL;
		}
		if($res->type == "permanent"){
			print "<td></td><td>$res->TOTAL</td>";
			$perm = $perm + $res->TOTAL;
			$perma = 1;
		}
		if($res->type == "vacataire"){
			print "<td></td><td></td><td>$res->TOTAL</td>";
			$vac = $vac + $res->TOTAL;
		}
	}else{
		if($res->type == "permanent"){
			$perma = 1;
			print "<td>$res->TOTAL</td>";
			$perm = $perm + $res->TOTAL;
		}else if ($res->type == "vacataire"){
			if($perma==1){
				print "<td>$res->TOTAL</td>";
				$perma=0;
			}else
				print "<td></td><td>$res->TOTAL</td>";
			$vac = $vac+$res->TOTAL;
		}
	}
}


print "</tr><th>TOTAL</th><td>$cont</td><td>$perm</td><td>$vac</td>";
print "</table>";


?>



