<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 

$id = $_REQUEST["id"];
$login = $_REQUEST["login"];
$semestre = $_REQUEST["sem"];
if (isset($_REQUEST['codeens'])) {
    $codeens = $_REQUEST['codeens'];
}
if (isset($_REQUEST['heuresCM'])) {
    $heuresCM = $_REQUEST["heuresCM"];
}
if (isset($_REQUEST['heuresTD'])) {
    $heuresTD = $_REQUEST["heuresTD"];
}
if (isset($_REQUEST['heuresTP'])) {
    $heuresTP = $_REQUEST["heuresTP"];
}
if (isset($_REQUEST['sub'])) {
    $sub = $_REQUEST["sub"];
}
if (isset($_REQUEST['paye'])) {
    $paye = $_REQUEST["paye"];
}
$go = isset($_REQUEST['go']) ? $_REQUEST["go"] : ''; 

print "<html><head><title>Mise &agrave; jour d'horaires de Module</title></head><body>";


// *********
// Gestion du verrou
// *********

if ( ($go!="" and $sub=="verrou") ){
	$query ="UPDATE menusemestre SET verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceCM set verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrou=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
}

if ( ($go!="" and $sub=="deverrouiller") ){
	$query ="UPDATE menusemestre SET verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
  	$query = "UPDATE preserviceCM set verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrou=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
}

// *********
// Gestion du verrou DPT (Cut and Paste)
// *********

if ( ($go!="" and $sub=="verrouDPT") ){
	$query ="UPDATE menusemestre SET verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceCM set verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrouDPT=1 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
}

if ( ($go!="" and $sub=="deverrouillerDPT") ){
	$query ="UPDATE menusemestre SET verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTD set verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
		
  	$query = "UPDATE preserviceCM set verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
  	$query = "UPDATE preserviceTP set verrouDPT=0 WHERE codemodsemestre =".$id;
  	$resu = my_query($query);
}


// *********
// Gestion du paiement
// *********

if ( ($go!="" and $paye!="") ){
    $query="
    SELECT not paye as p
       FROM preserviceCM
       WHERE codemodsemestre=$id
       AND enseignantID=$paye
       FOR UPDATE
    ";
    $resu = mysql_query($query)
    		or die("query is : ".$query." SELECT Error: ".mysql_error());
    $res=mysql_fetch_object($resu);
    
    	$query ="
    UPDATE preserviceCM 
    SET paye=$res->p
    WHERE codemodsemestre=$id
    AND enseignantID=$paye";
    
    $resu = my_query($query);
    
    $query ="
    UPDATE preserviceTD 
    SET paye=$res->p
    WHERE codemodsemestre=$id
    AND enseignantID=$paye";
    
    $resu = my_query($query);
    
    $query ="
    UPDATE preserviceTP
    SET paye=$res->p
    WHERE codemodsemestre=$id
    AND enseignantID=$paye";
    
    $resu = my_query($query);
}





// **********
// Gestion de la MAJ
// **********


if ( ($go!="" and $sub=="update") ){
	if ($heuresCM=="")
   		$heuresCM=0;
	if ($heuresTD=="")
   		$heuresTD=0;
	if ($heuresTP=="")
		$heuresTP=0;
	$query="
	SELECT codemodsemestre AS id, verrou
	FROM menusemestre
	WHERE codemodsemestre =".$id. 
	" FOR UPDATE
	";

    $resu = mysql_query($query)
        or die("query is : ".$query." SELECT Error: ".mysql_error());
    
    $res = mysql_fetch_object($resu);

    if ($res->verrou == 0){
    
        // ON drop le service d'abord
        
        $query="
        DELETE FROM preserviceCM
        WHERE codemodsemestre=".$id.
        " AND enseignantID=".$codeens;
        
        $resu = my_query($query);
        
        $query="
        DELETE FROM preserviceTD
        WHERE codemodsemestre=".$id.
        " AND enseignantID=".$codeens;
        
        $resu = my_query($query);
        
        $query="
        DELETE FROM preserviceTP
        WHERE codemodsemestre=".$id.
        " AND enseignantID=".$codeens;
        
        $resu = my_query($query);
        
        // DROP FINI
        
        
        // *** Le probl�me du delete : si HCM = 0 et HTD = 0
        // Alors on insere rien
        
        
        if ( ($heuresCM !=0) or ($heuresTD !=0) or ($heuresTP !=0)){
        
            $query="
            INSERT INTO preserviceCM (codemodsemestre, enseignantID, heuresCM)
            VALUES(".$id.",".$codeens.",".$heuresCM.")";
            
            $resu = my_query($query);
            
            $query="
            INSERT INTO preserviceTD (codemodsemestre, enseignantID, heuresTD)
            VALUES(".$id.",".$codeens.",".$heuresTD.")";
            
            $resu = my_query($query);
            
            $query="
            INSERT INTO preserviceTP (codemodsemestre, enseignantID, heuresTP)
            VALUES(".$id.",".$codeens.",".$heuresTP.")";
            
            $resu = my_query($query);
        
        }
        
        print "<fieldset>
        <legend>
        Mise &agrave; jour effectu&eacute;e
        </legend>";
        
        print "</fieldset><br/>";
    } else { // $res->verrou != 0
        print "<fieldset><legend>Mise &agrave; jour impossible : Les heures sont verouill&eacute;es</legend></fieldset><br/>";
    }
} // FIN MISE A JOUR

print '
<fieldset>
<legend>Editer Horaires</legend>
<a href=affiche_service.php?type=horaires&codeens='.$login.'&sem='.$semestre.'>Retour page des modules</a>
<form action="edit_service.php" method="GET">
<input type="hidden" name="go" value=1 />
<input type="hidden" name="login" value='.$login.' />
<input type="hidden" name="id" value="'.$id.'" />
<input type="hidden" name="sem" value="'.$semestre.'" />';


// on r�cup le nom
if ( $login !=""){
    $query="
    SELECT nom, prenom, enseignantID
    FROM enseignants
    WHERE enseignantID=".$login;
    
    $resu = mysql_query ($query)
        or die("SELECT Error: ".mysql_error());
    
    while ($res=mysql_fetch_object($resu)){
        $mynom = $res->nom;
        $myprenom = $res->prenom;
    }
}


// on r�cup si on est directeur ou pas
// ICI
$query ="
SELECT directeur
FROM departements
WHERE codedept=".DPT_ID.
" AND directeur=".$login;

$resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
$res=mysql_fetch_object($resu);

$directeur = false;
if ($res)
   $directeur = true;

if ($directeur)
  echo "<h3>VOUS &Ecirc;TES DIRECTEUR</h3>";


$query="
SELECT sem.anneedebut as annee, sem.nom as nom, CM.heuresCM as HCM, CM.nombregroupes as GCM, TD.heuresTD as HTD, TD.nombregroupes as GTD, TP.heuresTP as HTP, TP.nombregroupes as GTP, m.codeprefixe as pref, m.codesuffixe as suf, m.intitule as intitule, e.nom as enom, e.prenom as eprenom, m.responsable, s.verrou, s.verrouDPT 
FROM semestres as sem, menusemestre as s, modules as m, horairesCM as CM, horairesTD as TD, horairesTP as TP, enseignants as e
WHERE s.codemodsemestre=".$id."
AND s.codesemestre = sem.codesemestre
AND s.codemodsemestre = CM.codemodsemestre
AND s.codemodsemestre = TD.codemodsemestre
AND s.codemodsemestre = TP.codemodsemestre
AND s.codemod = m.codemod
AND m.responsable = e.enseignantID";

$resu = mysql_query($query)
    or die("query is : ".$query." SELECT Error: ".mysql_error());

$res = mysql_fetch_object ($resu);

$responsable = $res->responsable;

//********
// IMPORTANT
// le directeur est consid�r� comme responsable
//*********

if ($directeur)
   $responsable = true;


// print "<h2>POUR INFO: ".$myprenom." ".$mynom."</h2>";
// print "<h3>Toute nouvelle entr�e efface votre pr�c�dent service</h3>";

$CMTOT = $res->HCM*$res->GCM;
$CMTOTeqTD = $CMTOT*COUT_HEURE_CM;
$TDTOT = $res->HTD*$res->GTD;
$TPTOT = $res->HTP*$res->GTP*COUT_HEURE_TP;
$varver = $res->verrou;
$varverDPT = $res->verrouDPT;

print "<h1>$res->pref $res->suf &mdash; $res->intitule</h1>";

print "<table>";
$anneeplus=$res->annee+1;
print "<tr><th align=left>Semestre</th><td>$res->nom  $res->annee-$anneeplus</td></tr>";
print "<tr><th align=left>Responsable</th><td>$res->eprenom $res->enom</td></tr>";
print "<tr><th align=left>Verrou UE</th><td><font color="; if ($res->verrou) print "green>ON"; else print "red>OFF"; print "</font></td></tr>";
print "<tr><th align=left>Verrou DPT</th><td><font color="; if ($res->verrouDPT) print "green>ON"; else print "red>OFF"; print "</font></td></tr>";
print "</table>";

print "<table>";
print "<tr><th align=left>CM</th><td align=right>$res->HCM</td><td>&times;</td><td align=right>$res->GCM</td><td>=</td><td align=right>".number_format($CMTOT,2)."</td></tr>";
print "<tr><th align=left>TD</th><td align=right>$res->HTD</td><td>&times;</td><td align=right>$res->GTD</td><td>=</td><td align=right>".number_format($TDTOT,2)."</td></tr>";
print "<tr><th align=left>TP</th><td align=right>$res->HTP</td><td>&times;</td><td align=right>$res->GTP</td><td>=</td><td align=right>".number_format($TPTOT,2)."</td></tr>";
print "</table>";


// TODO : OPTIMISER LA QUERY SUIVANTE
$query="
SELECT E.enseignantID, E.nom as nom, E.prenom as prenom, TD.heuresTD as TD, CM.heuresCM as CM, TP.heuresTP as TP, E.codegrade, TD.paye as TDP, CM.paye as CMP, TP.paye as TPP
FROM enseignants as E, preserviceTD as TD, preserviceCM as CM, preserviceTP as TP
WHERE E.enseignantID = TD.enseignantID
AND E.enseignantID = CM.enseignantID
AND E.enseignantID = TP.enseignantID
AND TD.codemodsemestre = ".$id.
" AND CM.codemodsemestre = ".$id.
" AND TP.codemodsemestre = ".$id.
" ORDER BY E.nom, E.prenom";

$resu = mysql_query($query)
    or die("query is : ".$query." SELECT Error: ".mysql_error());

print "<table frame=box rules=all>";
print "<tr bgcolor=lightgrey><th  align=left width=250>ENSEIGNANT</th><th width=50>CM</th><th width=50>TD</th><th width=50>TP</th>";
if ($directeur && $varverDPT) print "<th>Saisie Geisha</th>";
print "</tr>";
print "<tr bgcolor=lightblue><td></td><td></td><td></td><td></td></tr>";

//print "<tr><td style=background:PaleGoldenRod >TOTAL A FAIRE :</td><td style=background:PaleGoldenRod >".$CMTOT."</td><td style=background:PaleGoldenRod >".$TDTOT."</td><td style=background:PaleGoldenRod >".$TPTOT."</td>";
//print"</tr>";

// On se sert d'un compteur et non plus de SQL m�me si on pourrait le faire.
$CMP =0;
$TDP =0;
$TPP =0;
$CMdef="";
$TDdef="";
$TPdef="";
while ($res=mysql_fetch_object($resu)){
    if ($res->enseignantID==$login) {$CMdef = $res->CM; $TDdef=$res->TD; $TPdef=$res->TP; }
    print '<tr';
    if ($res->codegrade == 6) print '<tr style=background:LightBlue>'; else print '<tr>';
    print "<td>$res->nom $res->prenom</td>";
    if ($res->CM!=0) print "<td align=right>$res->CM</td>"; else print "<td></td>";
    if ($res->TD!=0) print "<td align=right>$res->TD</td>"; else print "<td></td>";
    if ($res->TP!=0) print "<td align=right>$res->TP</td>"; else print "<td></td>";
    $CMP+=$res->CM;
    $TDP+=$res->TD;
    $TPP+=$res->TP;
    
    // **** on ne traite que les CMP, les 2 �tant parfaitement li�s au niveau de la paye
    if ($directeur && $varverDPT){
       if ($res->CMP) {
           print "<td align=center style=background:LightGreen >OUI&nbsp;&nbsp;";
       } else { 
           print "<td align=center style=background:LightSalmon>NON&nbsp;";
       }
       print "<input type='submit' name='paye' value='$res->enseignantID'/>";
    }
    print "</tr>";
}
print "<tr bgcolor=lightblue><td></td><td></td><td></td><td></td></tr>";
print "<tr bgcolor=lightgrey><th align=left>TOTAL</th><td align=right>".number_format($CMP,2)."</td><td align=right>".number_format($TDP,2)."</td><td align=right>".number_format($TPP,2)."</td></tr>";
print "<tr bgcolor=lightgrey><th align=left>Heures &agrave; Faire</th><td align=right>".number_format($CMTOT,2)."</td><td align=right>".number_format($TDTOT,2)."</td><td align=right>".number_format($TPTOT,2)."</td><td>Verrou UE</td><td>Verrou DPT</td></tr>";
print "<tr bgcolor=lightblue><td></td><td></td><td></td><td></td><td></td><td></td></tr>";

// Affichage de la ligne Bilan avec les verrous
// 1. Calculs pour les couleurs d'affichage
$bilanCM = $CMP-$CMTOT;
$bilanTD = $TDP-$TDTOT;
$bilanTP = $TPP-$TPTOT;

$bilanok = (($bilanCM==0)&&($bilanTD==0)&&($bilanTP==0));

$cALL = "lightgreen";
$cCM  = "lightgreen";
$cTD  = "lightgreen";
$cTP  = "lightgreen";

if (!$bilanok)  $cALL="lightsalmon";
if ($bilanCM<0) $cCM = "lightsalmon ";
if ($bilanTD<0) $cTD = "lightsalmon";
if ($bilanTP<0) $cTP = "lightsalmon";

if ($bilanCM>0) $cCM = "lightblue";
if ($bilanTD>0) $cTD = "lightblue";
if ($bilanTP>0) $cTP = "lightblue";

if ( ($bilanCM==0) and ($bilanTD==0) and ($bilanTP==0) ) $cALL="lightgreen"; else $cALL="lightsalmon";
if ( ($bilanCM<0) or ($bilanTD<0) or ($bilanTP<0) )
  $c1 ="LightSkyBlue";


// 2. Affichage du bilan
print "<tr>";
print "<th  align=left style=background:".$cALL.">Bilan</th>";
print "<td align=right style=background:".$cCM.">".number_format($bilanCM,2)."</td>";
print "<td align=right style=background:".$cTD.">".number_format($bilanTD,2)."</td>";
print "<td align=right style=background:".$cTP.">".number_format($bilanTP,2)."</td>";

// 3. Affichage des verrous
// 3.1 Verrou UE
if ($varver) print "<td bgcolor=lightgreen align=center>ON";
   else      print "<td bgcolor=lightsalmon align=center>OFF";
if ($login == $responsable and !$varverDPT){
   if ($varver) print "<input type='submit' name='sub' value='deverrouiller'/>";
   if (!$varver and $bilanok) print "<input type='submit' name='sub' value='verrou'/>";
}
print "</td>";
// 3.2 Verrou DPT
if ($varverDPT) print "<td bgcolor=lightgreen align=center>ON";
   else         print "<td bgcolor=lightsalmon align=center>OFF";
if ($directeur && $varver){
    if ($varverDPT) print "<input type='submit' name='sub' value='deverrouillerDPT'/>";
       else         print "<input type='submit' name='sub' value='verrouDPT'/>";
}
print "</td>";
print "</tr>";
// FIN LIGNE BILAN

print "<tr bgcolor=lightblue><td></td><td></td><td></td><td></td><td></td><td></td></tr>";

// Affichage ligne nouvelle entree
if (!$varver){


print "<tr><td>NOUVELLE ENTR&Eacute;E pour ";
if ($login == $directeur){    
    print '<select name="codeens">';
    $query="
    	SELECT nom, prenom, enseignantID
    	FROM enseignants
    	ORDER BY nom
    	";
    
    $resu = mysql_query ($query)
        or die("SELECT Error: ".mysql_error());
    
    $k =0;
    while ($res=mysql_fetch_object($resu)){
      print "<option value=$res->enseignantID";
      if ($k==0 and $login ==""){
        $k = 1;
        print ' selected="selected" ';
      }
      if ($login==$res->enseignantID){
       print ' selected="selected" ';   
      }
      print ">$res->nom $res->prenom</option>";
    }
    print '</select>';
}

if ($login != $directeur){
    $query="
    SELECT nom, prenom, enseignantID
    FROM enseignants
    WHERE enseignantID=".$login;
    
    $resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
    
    $k =0;
    while ($res=mysql_fetch_object($resu)){      
      print "<strong>$res->nom $res->prenom</strong>";
      print '<input type="hidden" name="codeens" value="'.$login.'"></input>';
    }


}

if ($CMdef==0) $CMdef=0;
if ($TDdef==0) $TDdef=0;
if ($TPdef==0) $TPdef=0;


print "</td>";
print '<td align=center><input type="text" value='.$CMdef.' name="heuresCM" size=5/></td>';
print '<td align=center><input type="text" value='.$TDdef.' name="heuresTD" size=5/></td>';
print '<td align=center><input type="text" value='.$TPdef.' name="heuresTP" size=5/></td>';
/*
if ($bilanok) {
   if ($login == $responsable) or $directeur){
      print "<tr><td>Verrou Resp. UE</td><td><input type='submit' name='sub' value='";
      if ($varver==0) print "verrou";
      if ($varver==1) print "deverrouiller";
      print "'/></td></tr>";
   }
}
*/
//print '</tr>';
//if (!$varver && !$varverDPT)
   print "<td align=center><input type='submit' name='sub' value='update'/></td></tr>";
   print "<tr bgcolor=lightblue><td></td><td></td><td></td><td></td><td></td></tr>";

}
// **************************
// * on g�re ici le verrou
// **************************

/*
if ($login == $responsable and !$varverDPT){
 if ($varver==0) 
   print "<tr bgcolor=lightsalmon><td>Verrou Resp. UE $bilanok </td><td colspan=3 align=center><input type='submit' name='sub' value='verrou'/></td></tr>";
  if ($varver==1) 
   print "<tr bgcolor=lightgreen><td>Verrou Resp. UE $bilanok</td><td colspan=3 align=center><input type='submit' name='sub' value='deverrouiller'/></td></tr>";
}
*/


//*******************
//* On g�re le verrou DPT
//*******************
/*
if ($directeur && $varver){
    if ($varverDPT==0) 
        print "<tr style=background:lightsalmon><td>Verrou D&eacute;partement</td><td colspan=3 align=center><input type='submit' name='sub' value='verrouDPT'/></td></tr>";
    if ($varverDPT==1) 
        print "<tr style=background:lightgreen><td>Verrou D&eacute;partement</td><td colspan=3 align=center><input type='submit' name='sub' value='deverrouillerDPT'/></td></tr>";
}
*/
print"</table>
</form>
</fieldset>
</body>
</html>
";

mysql_close($link);

?>
