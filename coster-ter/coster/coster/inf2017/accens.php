<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

?>
<html>
<head>
	<title>Accueil de <?php echo $_SESSION['name_user']; ?></title>
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<frameset rows="95,400,60" cols="*" framespacing="0" frameborder="no" border="0"  >
    <frame src="haut1.php" name="topFrame" scrolling="No" noresize="noresize" id="topFrame" title="topFrame" />
    <frameset cols="210,*" frameborder="no" border="0" framespacing="0">
        <frame src="gauche1.php" name="leftFrame" scrolling="No" noresize="noresize" id="leftFrame" title="leftFrame" />
        <frame src="bas.php"  name="mainFrame" scrolling="auto" id="mainFrame" title="mainFrame"  />
    </frameset>
    <frame src="dessous.php"" name="bottomFrame" scrolling="no" noresize="noresize" id="bottomFrame" title="bottomFrame" />
    <noframes>
    	<p>Votre navigateur ne peut pas afficher ce site.</p>
    </noframes>
</frameset>

</html>
