<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

if (isset($_REQUEST['pass'])) {
    // TODO : pourquoi stocker le mot de passe dans un cookie ?
    $pass = $_REQUEST['pass'];
    if ($pass != '') setcookie('passw', $pass);
} else {
    $pass = '';
}

$type = $_REQUEST["type"];
$codeens = $_REQUEST["codeens"]; // Id de l'enseignant pour lequel on veut l'information
$iduser = $_SESSION['id_user']; // Id de l'utilisateur connect�
$semestre = $_REQUEST["sem"];

$annee = (isset($_REQUEST['annee']) && $_REQUEST['annee'] != "") ? $_REQUEST["annee"] : ANNEE_DEBUT;
?>

<html>
<head>
	<title>Service par <?php echo $type; ?></title>
	<link rel="stylesheet" type="text/css" href="style-afficher-horaires.css"/>
</head>
<body>
<?php

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 


//********************************
// PARTIE AFFICHAGE PAR ENSEIGNANT
// *******************************

// Toutes les heures d'un enseignant sp�cifique
if ($codeens != '' and $type == 'enseignant'){
	$enseignant = selectEnseignantById($codeens, $link); 
	print "<h2>Service de $enseignant->prenom $enseignant->nom</h2>";

	$service = selectServiceEnseignant($codeens, $link);
	
	print "<p>";
        	print "<table frame=box rules=all>";
        	print "<tr bgcolor=lightgrey><th align= left colspan=3>D&Eacute;TAIL</th><th width=40>CM</th><th width=40>TD</th><th width=40>TP</th><th>Total</th></tr>\n";    
		print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/><td/><td/></tr>";
                if ($enseignant->servicedu>0) {
			print "<tr><td>Service D&ucirc;<td colspan=2>$enseignant->grade</td><td/>";
			print "<td align=right>".number_format($enseignant->servicedu,2)."</td><td/><td align=right>".number_format($enseignant->servicedu,2)."</td></tr>";
			print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/><td/><td/></tr>";
		}
		displayServiceEnseignant($enseignant, $service["faites"], $service["bilan"]["effectuees"], $service["bilan"]["hRedTP"],"Enseignement");
                displayServiceEnseignant($enseignant, $service["reductions"], $service["bilan"]["reductions"], 0, "Reduction");
    		displayServiceEnseignant($enseignant, $service["primes"], $service["bilan"]["primes"], 0, "PetD");
        	print "</table></p>";	

    	print "<p>";
                $ServiceStatutaire=$enseignant->servicedu;
		$ReductionService=$service["bilan"]["reductions"];
                $ServiceEffectue=$service["bilan"]["effectuees"];
                $PrimeEtDecharge=$service["bilan"]["primes"];
		$Bilan=$ServiceEffectue+$ReductionService+$PrimeEtDecharge-$ServiceStatutaire;
 		if ($Bilan<0) $Decharge=$PrimeEtDecharge;
			else {
			     if ($ServiceEffectue+$ReductionService-$ServiceStatutaire<0) {
				$Decharge = -($ServiceEffectue+$ReductionService-$ServiceStatutaire);
				$Bilan=0;
				}
				else $Decharge=0;	
			}
		$Prime=$PrimeEtDecharge-$Decharge;
		if (($ServiceStatutaire==0 or $ServiceStatutaire>=192) and $Bilan>=0) {$HeuresCompPayables=$Bilan; $HeuresCompNonPayables=0;}
                                                      else {$HeuresCompPayables=0;      $HeuresCompNonPayables=$Bilan;};
		if ($ServiceStatutaire==0 and $Bilan>96) {$HeuresCompPayables=96; $HeuresCompNonPayables=$Bilan-96;}
                print "<table frame=box rules=all>";
$nbcol=0;
                print "<tr bgcolor=lightgrey>";
			print "<th align=left rowspan=2 width=100>BILAN</th>"; $nbcol++;
			if ($Decharge>0) {print "<th rowspan=2 width=100>D&eacute;charge</th>"; $nbcol++;}
			print "<th colspan=2>Heures</th>"; $nbcol+=2;
			if ($Prime>0) {print "<th rowspan=2 width=100>Prime<br>&agrave; Payer</th>"; $nbcol++; }
		print "</tr>";
		print "<tr bgcolor=lightgrey><th width=100>Payables</th><th width=100>Non Payables</th></tr>";
                print "<tr>";
print "<tr bgcolor=lightblue>"; for ($moncpt=0;$moncpt<$nbcol;$moncpt++) print "<td/>"; print"</tr>\n";
		print "<td align=center bgcolor="; if ($Bilan>=O) print "lightgreen>+"; else print "lightsalmon>"; print "$Bilan</td>";
		if ($Decharge>0) print "<td align=center>$Decharge</td>";
		if ($HeuresCompPayables>0) print "<td align=center>$HeuresCompPayables</td>"; else print "<td/>";
		if ($HeuresCompNonPayables>0) print "<td align=center>$HeuresCompNonPayables</td>"; else print "<td/>";
		if ($Prime) print "<td align=center>$Prime</td>";
		print "</tr>";
print "<tr bgcolor=lightblue>"; for ($moncpt=0;$moncpt<$nbcol;$moncpt++) print "<td/>"; print"</tr>\n";
                print "</table></p>";
/*
    print '<h3>Service d&ucirc; (' . $enseignant->grade . ')</h3>';
    print '<table border=1>';
    print '<tr>';
    print '<td>Service statutaire</td>';
    print '<td style="text-align: right">' . number_format($enseignant->servicedu, 2) . '</td>';
    print '</tr>';
    print '<tr>';
    print '<td>R&eacute;ductions de Service</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["reductions"], 2) . '</td>';
    print '</tr>';
    $serviceDu = $enseignant->servicedu - $service["bilan"]["reductions"];
    print '<tr>';
    print '<td>Service d&ucirc;</td>';
    print '<td style="text-align: right">' . number_format($serviceDu, 2) . '</td>';
    print '</tr>';
    print '</table>';
    
    print '<h3>Service effectu&eacute;</h3>';
    print '<table border=1>';
    print '<tr>';
    print '<td>Heures effectu&eacute;es</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["effectuees"], 2) . '</td>';
    print '</tr>';
    print '<tr>';
    print '<td>Primes et D&eacute;charges</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["primes"], 2) . '</td>';
    print '</tr>';
    print '</table>';
    
    $serviceEffectue = $service["bilan"]["effectuees"] + $service["bilan"]["primes"];
    if ($serviceEffectue > $serviceDu) {
    	$decharge = max($serviceDu - $service["bilan"]["effectuees"], 0);
    	$primeAPayer = $service["bilan"]["primes"] - $decharge;
    	$heuresSupp = max($service["bilan"]["effectuees"] - $serviceDu, 0);
	    print '<h3>Paiement</h3>';
	    print '<table border=1>';
	    print '<tr>';
	    print '<td>D&eacute;charge</td>';
	    print '<td style="text-align: right">' . number_format($decharge, 2) . '</td>';
	    print '</tr>';
	    print '<tr>';
	    print '<td>Heures comp. &agrave; payer</td>';
	    print '<td style="text-align: right">' . number_format($heuresSupp, 2) . '</td>';
	    print '</tr>';
	    print '<tr>';
	    print '<td>Prime &agrave; payer</td>';
	    print '<td style="text-align: right">' . number_format($primeAPayer, 2) . '</td>';
	    print '</tr>';
	    print '</table>';
    } else {
	    print '<h3>Reste &agrave; effectuer</h3>';
	    print '<table border=1>';
	    print '<tr>';
	    print '<td>Heures restant &agrave; effectuer</td>';
	    print '<td style=\"text-align: right\">' . number_format($serviceDu - $serviceEffectue, 2) . '</td>';
	    print '</tr>';
	    print '</table>';
    }   
*/
    $query = 'SELECT m.codemod, codeprefixe, codesuffixe, intitule, s.nom, codemodsemestre, verrou, verrouDPT
                FROM modules as m, semestres as s, menusemestre as ms
               WHERE m.codemod = ms.codemod
                 AND s.codesemestre = ms.codesemestre
                 AND m.responsable = ' . $codeens;
    
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());
    
    if (mysql_num_rows($result) > 0) {
	    print "<p>";
	    print "<table frame=box rules=all>";
            print "<tr bgcolor=lightgrey><th rowspan=2 colspan=3 align=left>UE sous la responsabilit&eacute; de $enseignant->prenom $enseignant->nom</th><th colspan=2>Verrou</th></tr>";
            print "<tr bgcolor=lightgrey><th width=30>UE</th><th width=30>DPT</th></tr>";
print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/></tr>\n";
	    while($row = mysql_fetch_object($result)){
	        print "<tr><td>$row->nom</td>";
                print "<td><a href=edit_service.php?id=$row->codemodsemestre&login=$codeens&sem=$semestre>$row->codeprefixe $row->codesuffixe</a></td>";
	        print "<td><a href=edit_service.php?id=$row->codemodsemestre&login=$codeens&sem=$semestre>$row->intitule</a></td>";
                
	        print (!$row->verrou)    ? "<td align=center style=background:lightsalmon>OFF</td>" : "<td align=center style=background:lightgreen>ON</td>"; 
	        print (!$row->verrouDPT) ? "<td align=center style=background:lightsalmon>OFF</td>" : "<td align=center style=background:lightgreen>ON</td>"; 
	        print"</tr>";
	    }
print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/></tr>\n";
	    print "</table></p>";
    }
/*  
	print "<h2>Semestres dont vous avez la responsabilit&eacute;</h2>";
    $query = 'SELECT nom
                FROM semestres
               WHERE responsable = ' . $codeens . '
            ORDER BY nom ASC';
    
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());
    
    if (mysql_num_rows($result) > 0) {
	    print "<table border=1>";
	    while($row = mysql_fetch_object($result)){
	        print "<tr><td>$row->nom</td></tr>";
	    }
	    print "</table>";
    } else {
    	print "Aucun.";
    }
*/
} // Fin PARTIE AFFICHAGE PAR ENSEIGNANT


//*************************************
// PARTIE HEURES TOTALES PAR ENSEIGNANT
//*************************************
if($codeens=="" and $type=="enseignant"){
    print "<h1>Total des heures du D&eacute;partement</h1>";
    $services = selectServiceEnseignants($link);
	displayServiceEnseignants($services);
} // FIN PARTIE HEURES TOTALES PAR ENSEIGNANT

// **********************************
// HORAIRES
// Appel� par le menu "Infos Modules" quand un enseignant est connect�
//***********************************
if ($type == "horaires"){
    //***************
    // On lance la requ�te
    //***************
    $query = "SELECT inti, verrou, verrouDPT, cms, anneedebut, nom, codeprefixe as pref, codesuffixe as suf, codesem, 
                     (SUM(HCM)*SUM(GCM)) as heuresCMPREV, SUM(HPCM) as heuresCMFAIT,
                     (SUM(HTD)*SUM(GTD)) as heuresTDPREV, SUM(HPTD) as heuresTDFAIT,
                     (SUM(HCM)*SUM(GCM)*1.5) as CMeqTDPREV, SUM(HPCM)*1.5 as CMeqTDFAIT,
                     (SUM(HTP)*SUM(GTP)) as heuresTPPREV, SUM(HPTP) as heuresTPFAIT,
                     (SUM(HCM)*SUM(GCM)*1.5+SUM(HTD)*SUM(GTD)+SUM(HTP)*SUM(GTP)) as TOTPREV,
                     (SUM(HPCM)*1.5+SUM(HPTD)+SUM(HPTP)) as TOTFAIT,
                     ( (SUM(HCM)*SUM(GCM)*1.5+SUM(HTD)*SUM(GTD)+SUM(HTP)*SUM(GTP))- (SUM(HPCM)*1.5+SUM(HPTD)+SUM(HPTP)) ) as bilan
                FROM (SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                             0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                             TP.heuresTP as HTP, 0 as HPTP, TP.nombregroupes as GTP,
                             m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                       FROM modules as m, menusemestre as s, semestres as sem, horairesTP as TP
                      WHERE s.codesemestre = sem.codesemestre
                        AND s.codemod = m.codemod
                        AND s.codemodsemestre = TP.codemodsemestre
                        AND sem.anneedebut = ".$annee."
                   GROUP BY s.codemodsemestre
                      UNION
                     SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                            0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD, 0 as HTP,
                            SUM(PTP.heuresTP) as HPTP, 0 as GTP,
                            m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                       FROM modules as m, menusemestre as s, semestres as sem, preserviceTP as PTP
                      WHERE s.codesemestre = sem.codesemestre
                        AND s.codemod = m.codemod
                        AND s.codemodsemestre = PTP.codemodsemestre
                        AND sem.anneedebut = ".$annee."
                   GROUP BY s.codemodsemestre
                      UNION
                     SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                            0 as HCM, 0 as HPCM, 0 as GCM, TD.heuresTD as HTD, 0 as HPTD, TD.nombregroupes as GTD,
                            0 as HTP, 0 as HPTP, 0 as GTP,
                            m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, horairesTD as TD
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = TD.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                           CM.heuresCM as HCM, 0 as HPCM, CM.nombregroupes as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, horairesCM as CM
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = CM.codemodsemestre
                       AND sem.anneedebut = ".$annee." 
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom,  codeprefixe, codesuffixe,
                           0 as HCM, SUM(PCM.heuresCM) as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, preserviceCM as PCM
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = PCM.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                           0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, SUM(PTD.heuresTD) as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, preserviceTD as PTD
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = PTD.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     ) as sub1
            GROUP BY cms
            ORDER BY nom, pref, suf";

// On va construire plusieurs Tableaux

//print "<table border=1>
//<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td>Module</td><td>Total heures CM pr�vues</td><td>Total heures CM faites</td><td>Total heures CM (eq.TD) pr�vues</td><td>Total heures CM (eq. TD) Faites</td><td>Total heures TD Pr�vues</td><td>Total heures TD faites</td><td>Total heures TP pr�vues</td><td>Total heures TP faites</td><td>Total heures eq. TD Pr�vues</td><td>Total heures eq. TD Faites</td><td>BILAN</td><td></td></tr>";
$TOT_PREV=0;
$TOT_FAIT=0;
$NB_UE=0;
$NB_VER_RESP=0;
$NB_VER_DPT=0;
$listed = 0;
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());

    $old_initiale = "";
    $old_nom = "";
    $old_codesem = "";
print "<table frame=box rules=all>";
print "<tr bgcolor=lightgrey><th rowspan=2 colspan=3>Semestre</th><th rowspan=2 colspan=4>Heures<br>&agrave; Faire</th><th rowspan=2>Heures<br>Faites</th><th rowspan=2>Bilan</th><th colspan=2>Verrou</th></tr>";
print "<tr bgcolor=lightgrey><th>UE</th><th>Dpt</th></tr>";
    while($row = mysql_fetch_object($result)){
        //On teste si on commence un nouveau semestre
	if ($old_nom != $row->nom) {
		// CAS 1 : Le semestre qui vient de se terminer est celui ou l'on liste les UE (et le suivant ne l'est donc forcement pas
                if ($listed) { print "<tr bgcolor=lightgrey><th colspan=6>Total</th><th align=right>".number_format($TOT_PREV,2)."</th><th align=right>".number_format($TOT_FAIT,2)."</th></tr>";
                               print "</tr>";
                               $listed=0;  // On n'est plus dans le semestre dont on a liste les UE
                             }
                    else  { // CAS 2 : Le semestre qui se termine n'est pas celui liste
                            if ($old_nom!="") {
				 	print "<tr><td colspan=3><a name=#$old_codesem><a href=affiche_service.php?pass=$pass&type=horaires&codeens=$codeens&sem=$old_codesem>$old_nom</a></td>";
                            		// print "<tr><td><a name=#$row->codesem><a href=affiche_service.php?pass=$pass&type=horaires&codeens=$codeens&sem=$row->codesem>$row->nom</a></td>";
                            		print "<td colspan=4 align=right>".number_format($TOT_PREV,0)."</td>";
                            		print "<td align=right>".number_format($TOT_FAIT,0)."</td>";
                                        $bilan=$TOT_FAIT-$TOT_PREV;
                            		print "<td align=right bgcolor=";
					if ($bilan==0) print "lightgreen>"; if ($bilan<0) print "lightsalmon>"; if ($bilan>0) print "lightblue>+"; 
                                        print number_format($TOT_FAIT-$TOT_PREV,2)."</td>";
                            		print "<td align=center bgcolor="; if ($NB_VER_RESP<$NB_UE) print "lightsalmon"; else print "lightgreen"; print ">$NB_VER_RESP/$NB_UE</td>";
                            		print "<td align=center bgcolor="; if ($NB_VER_DPT<$NB_UE) print "lightsalmon"; else print "lightgreen"; print">$NB_VER_DPT/$NB_UE</td>";
                            		print "</tr>";
                                        }
                            if ($semestre == $row->codesem) { // CAS 2.1 : On commence le semestre dont on doit lister les UE
		                $listed=1;
                                //print "<tr><td><a name=#$row->codesem><a href=affiche_service.php?pass=$pass&type=horaires&codeens=$codeens&sem=$row->codesem>$row->nom</a></td></tr>";
                	        print "<tr bgcolor=lightgrey>".
                                           "<th align=left colspan=3><font color=red>$row->nom</font></th>".
                                           "<th colspan=4>Heures &agrave; Faire</th>".
                                           "<th rowspan=2>Heures<br>Faites</th>".
                                           "<th rowspan=2>Bilan</th>".
                                           "<th colspan=2>Verrous</th>".
                                      "</tr>";
                                print "<tr bgcolor=lightgrey><th></th>".
                                           "<th>Code UE</th>".
                                           "<th>Nom UE</th>".
                                           "<th>CM</th><th>TD</th><th>TP</th><th>Total</th>".
                                           "<th>UE</th><th>DPT</th>".
                                     "</tr>";
                               }
                          }
                 $old_nom = $row->nom;
                 $old_codesem = $row->codesem; 
                 $TOT_PREV=0;
                 $TOT_FAIT=0;
                 $NB_UE=0;
                 $NB_VER_RESP=0;
                 $NB_VER_DPT=0;
               }
        $TOT_PREV += $row->TOTPREV;
        $TOT_FAIT += $row->TOTFAIT;
        $NB_UE++; 
        if ($row->verrou) $NB_VER_RESP++;
        if ($row->verrouDPT) $NB_VER_DPT++;

       // Affichage d'un trait horizontal quand on change d'initiale de semestre
       if ($old_initiale != substr($row->nom, 0, 1)) {
           print '<tr bgcolor=lightblue>'; for ($i=0;$i<11;$i++) print '<td></td>'; print '</tr>';
           $old_initiale = substr($row->nom, 0, 1);
           }
    
        if ( $semestre == $row->codesem) {
            print "<tr><td width=30 align=right>$NB_UE</td>".
                       "<td><a href=edit_service.php?id=$row->cms&login=$codeens&sem=$semestre>$row->pref $row->suf</a></td>".
                       "<td><a href=edit_service.php?id=$row->cms&login=$codeens&sem=$semestre>$row->inti</a></td>";
                 if ($row->heuresCMPREV+$row->heuresTDPREV+$row->heuresTPPREV== 0) 
				print "<td colspan=5 bgcolor=lightgrey align=center>UE Ferm&eacute;e</td>";
			else {
                 		if ($row->heuresCMPREV>0) print "<td align=right>".number_format($row->heuresCMPREV,0)."</td>"; else print "<td/>";
                       		//"<td>$row->heuresCMFAIT</td>.
                       		//"<td>$row->CMeqTDPREV</td>.
                       		//"<td>$row->CMeqTDFAIT</td>.
                 		if ($row->heuresTDPREV>0) print "<td align=right>".number_format($row->heuresTDPREV,0)."</td>"; else print "<td/>";
                       		//"<td>$row->heuresTDFAIT</td>".
                 		if ($row->heuresTPPREV>0) print "<td align=right>".number_format($row->heuresTPPREV,0)."</td>"; else print "<td/>";
                       		//"<td>$row->heuresTPFAIT</td>.
                 		print "<td align=right>".number_format($row->TOTPREV,2)."</td>".
                       		"<td align=right>".number_format($row->TOTFAIT,2)."</td>";
			      }
           print "<td align=right style=background:"; 
           if ($row->bilan>0) print "LightSalmon>";
               else if ($row->bilan<0) print "LightBlue>+";
                       else print "lightgreen>";
           print number_format(-$row->bilan,2)."</td>";
           

           if ($row->verrou) print "<td align=center><font color=green>On</font></td>";
              else print "<td align=center><font color=red>OFF</font></td>";
            if ($row->verrouDPT) print "<td align=center><font color=green>On</font></td>";
              else print "<td align=center><font color=red>OFF</font></td>";
            print "</tr>\n";
        }

    } // Fin while
print '<tr>'; for ($i=0;$i<11;$i++) print '<td bgcolor=lightblue></td>'; print '</tr>';
print "</table>\n";
} // FIN HORAIRES

//print '</table>';
mysql_close($link);
?>

</body>
</html>
