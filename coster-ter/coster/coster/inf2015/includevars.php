<?php

$version = "1.3.1";
$aide = "Benjamin Nguyen (Département Informatique [benjamin.nguyen]@prism.uvsq.fr)";
$db = "info2009";
$departement = "Informatique";
$dptID = 1; // info=1, bio = 2
$annee = 2009;
$anneeFin = $annee+1;
$coutHeureCM = 1.5;
$coutHeureTD = 1.0;
$coutHeureTP = 1.0;
$administrationPrimeDech = "Administration-Prime-Dech";
$administrationReduction = "Administration-Reduction";


?>