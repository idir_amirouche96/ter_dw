<?php
error_reporting(E_ALL);
ini_set('display_errors','On');

require_once 'defs.inc';
require_once 'includefunct.php';
require_once 'exportCSVQueries.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME);

$resu = mysql_query( get_query_services() )
        or die("SELECT Error: ".mysql_error());

$num_fields = mysql_num_fields($resu);

function semestre_type($semestre){
  if ( ($semestre=="L1 S1") ||
       ($semestre=="L1 S2") ||
       ($semestre=="L2 S3") ||
       ($semestre=="L2 S4") ||
       ($semestre=="L3 S5") ||
       ($semestre=="L3 S6") ||
       ($semestre=="UFR Sciences Licence") 
     ) return 1;

  if ( ($semestre=="M1 IHP S1")         ||
       ($semestre=="M1 IHP S2")         ||
       ($semestre=="M1 Info. S1")       ||
       ($semestre=="M1 Info. S2")       ||
       ($semestre=="M1-M2 IRS AFTI")    ||
       ($semestre=="M1-M2 IRS ITESCIA") ||
       ($semestre=="M2 ACSIS S3")       ||
       ($semestre=="M2 ACSIS S4")       ||
       ($semestre=="M2 CoSy S3")        ||
       ($semestre=="M2 CoSy S4")        ||
       ($semestre=="M2 IHPS S3")        ||
       ($semestre=="M2 IHPS S4")        ||
       ($semestre=="M2 IRS S3")         ||
       ($semestre=="M2 IRS S4")         ||
       ($semestre=="M2 SeCReTS S3")     ||
       ($semestre=="M2 SeCReTS S4")     ||
       ($semestre=="UFR Sciences Master")    
     ) return 2;

  if ( ($semestre=="M2 IHPS S3") ||
       ($semestre=="M2 IHPS S4") ) return 3; 
 
  if ($semestre=="ISTY CPI Sem. 01-04") return 11;

  if ( ($semestre=="ISTY IATIC3 Sem. 05") ||
       ($semestre=="ISTY IATIC3 Sem. 06") ||
       ($semestre=="ISTY IATIC4 Sem. 07") ||
       ($semestre=="ISTY IATIC4 Sem. 08") ||
       ($semestre=="ISTY IATIC5 Sem. 09") ||
       ($semestre=="ISTY IATIC5 Sem. 10") ) return 12;

  if ($semestre=="UFR SSH") return 21;
  if ($semestre=="UFR SJP") return 22;
  if (strstr($semestre,"UFR-IUT V")) return 23;
  if (strstr($semestre,"Admin -- Prime et D")) return 91;
  if (strstr($semestre,"Admin -- R")) return 92;

  return 0;
}

function enseignant_type($grade){
  if ( ($grade=="MCF-info")   ||
       ($grade=="PR-info")    ||
       ($grade=="ATER")        ||
       ($grade=="ATER-6mois") ||
       ($grade=="DOCT-Ens")   ||
       ($grade=="EC-UFR")     ) return 1;

  if ( ($grade=="VAC-Pub")    ||
       ($grade=="VAC-Pri")    ||
       ($grade=="VAC-IS-Pub") || 
       ($grade=="VAC-IS-Pri") ) return 2;

  if ($grade=="EC-IUT-Vel") return 3;

  if ( ($grade=="EC-ISTY")   ||
       ($grade=="PAST-ISTY") ) return 4;

  if ($grade=="EC-IUFM")  return  5;
  if ($grade=="EC-ECP")   return  6;
  if ($grade=="EC-TSP")   return  7; 
  if ($grade=="ECTI")     return  8;
  if ($grade=="Heures")   return  9;
  if ($grade=="Non info.") return 10;


return 0;
}

// for ($i = 0; $i < $num_fields; ++$i) {
//   if ($i > 0) print ",";
//   $field = mysql_fetch_field($resu, $i);
//   print "\"$field->name\"";
//   }

$H_TOT = 0;
$LICENCE_TOT  = 0;
$MASTER_TOT   = 0;
$CPI_TOT      = 0;
$ISTY_TOT     = 0;
$SSH_SJP_TOT  = 0;
$IUT_VEL_TOT  = 0;
$MIHPS2_TOT   = 0;
$P_ET_D_TOT   = 0;
$RED_SERV_TOT = 0;

$ENS_UFR_TOT  = 0;
$ENS_VAC_TOT  = 0;
$ENS_VEL_TOT  = 0;
$ENS_ISTY_TOT = 0;
$ENS_IUFM_TOT = 0;
$ENS_ECP_TOT  = 0;
$ENS_TSP_TOT  = 0;
$ENS_ECTI_TOT = 0;
$ENS_HEURES_TOT = 0;
$ENS_NONINFO_TOT = 0;

$ENS_deux_tiers_tp = 0;
$VAC_deux_tiers_tp = 0;

$licence = array(0,0,0,0,0,0,0,0,0,0,0);
$master  = array(0,0,0,0,0,0,0,0,0,0,0);
$cpi     = array(0,0,0,0,0,0,0,0,0,0,0);
$isty    = array(0,0,0,0,0,0,0,0,0,0,0);
$ssh_sjp = array(0,0,0,0,0,0,0,0,0,0,0);
$iut_vel = array(0,0,0,0,0,0,0,0,0,0,0);
$mihps2  = array(0,0,0,0,0,0,0,0,0,0,0);
//$cpt = 0;
while($res = mysql_fetch_row($resu)) {
  $nom = $res[0];
  $prenom = $res[1];
  $ens = $res[2];
  $ens_t = enseignant_type($ens);
  $sem = $res[3];
  $sem_t = semestre_type($sem);


  $code_UE = $res[4];
  if ($code_UE=="ADM-ISTY") $sem_t = 0;
  if ($code_UE=="ADM-FC")   $sem_t = 0;

  $h = $res[7]*1.5 + $res[8] + $res[9];
 
// For debug
// if (($sem_t==2) && ($h>0)) print "BBB sem_t=$sem_t ens_t=$ens_t $nom $ens $sem $h<br>\n";
// if (($ens_t==0) && ($h>0)) print "ens_t=0: $nom $ens $sem $h<br>\n";
// if ($sem_t==92) print "$nom $ens $h<br>\n";
 
  if ( ($h>0) && ($sem_t>0) ) {
//     $cpt++;
     $H_TOT += $h;
     $hvac = $res[7]*1.5 + $res[8] + 2*$res[9]/3;
     if (($ens_t==2) && ($nom!="H NON STATUT")) { $VAC_deux_tiers_tp += $h-$hvac; $h = $hvac; }

     if (($ens_t==2) && ($sem_t==11)) $ens_t = 4; // Les vacataires en cpi sont comptés pour l'isty
     if (($ens_t==2) && ($sem_t==12)) $ens_t = 4; // Les vacataires en isty sont comptés pour l'isty


     if ($ens_t) { // On ne compte pas les heures ou ens_t vaut 0 par exemple, les heures economosées, perdues, mutualis�es
         if ($sem_t== 1) { if ($ens_t<9) $LICENCE_TOT += $h; $licence[$ens_t] += $h; }
         if ($sem_t== 2) { if ($ens_t<9) $MASTER_TOT  += $h; $master[$ens_t]  += $h; }
         if ($sem_t== 3) { if ($ens_t<9) $MIHPS2_TOT  += $h; $mihps2[$ens_t]  += $h; }
         if ($sem_t==11) { if ($ens_t<9) $CPI_TOT     += $h; $cpi[$ens_t]     += $h; }
         if ($sem_t==12) { if ($ens_t<9) $ISTY_TOT    += $h; $isty[$ens_t]    += $h; }
         if ($sem_t==21) { if ($ens_t<9) $SSH_SJP_TOT += $h; $ssh_sjp[$ens_t] += $h; }
         if ($sem_t==22) { if ($ens_t<9) $SSH_SJP_TOT += $h; $ssh_sjp[$ens_t] += $h; }
         if ($sem_t==23) { if ($ens_t<9) $IUT_VEL_TOT += $h; $iut_vel[$ens_t] += $h; }
         if ($sem_t==91) $P_ET_D_TOT   += $h;
         if ($sem_t==92) $RED_SERV_TOT += $h;
     }

     if (($sem_t!= 3) && ($sem_t!=91) && ($sem_t!=92)) { // On ne compte pas MIHPS2 ni les PetD ni les Reductions de Services
        if ($ens_t== 1) $ENS_UFR_TOT     += $h;
        if ($ens_t== 2) $ENS_VAC_TOT     += $h;
        if ($ens_t== 3) $ENS_VEL_TOT     += $h;
        if ($ens_t== 4) $ENS_ISTY_TOT    += $h;
        if ($ens_t== 5) $ENS_IUFM_TOT    += $h;
        if ($ens_t== 6) $ENS_ECP_TOT     += $h;
        if ($ens_t== 7) $ENS_TSP_TOT     += $h;
        if ($ens_t== 8) $ENS_ECTI_TOT    += $h;
        if ($ens_t== 9) $ENS_HEURES_TOT  += $h;
        if ($ens_t==10) $ENS_NONINFO_TOT += $h;
     }


  /*
    print "<tr>";
  // for ($i = 0; $i < $num_fields; ++$i) { print "<td>$res[$i]</td>"; }
  $h = $res[7] * 1.5 + $res[8] + $res[9];
  print "<td align=\"right\">$cpt</td>";
  print "<td>$nom $prenom</td>";
  print "<td>$ens</td><td>yyyy$ens_t</td>";
  print "<td>$sem</td><td>xxxx$sem_t</td>";
  printf("<td align=\"right\">%.2f</td>",$h);
  printf("<td align=\"right\">%.2f</td>",$hvac);
  print "</tr>";
  */
  }
}
// print "$H_TOT<br>";
   print "<h2>Potentiel</h2>\n";

$NB_MCF  = 24;
$NB_PROF = 9;
$NB_ATER = 3;
// $NB_CONTRAT_DOCT = 6.875; Si on veut compter Huyhn en partiel et Vial prado en partiel
$NB_CONTRAT_DOCT = 7;
   print "<table frame=box rules=all>\n";

$POTENTIEL_BRUT = 0;
   print "<tr bgcolor=lightgrey><th>Potentiel</th><th>Nombre</th><th>NB heures</th></tr>\n";
   print "<tr bgcolor=lightblue><td/><td/><td/></tr>\n";
   printf("<tr><td>MCF</td>            <td align=\"right\">%d</td>  <td align=\"right\">%.2f</td></tr>\n",$NB_MCF,$NB_MCF*192); $POTENTIEL_BRUT += $NB_MCF*192;
   printf("<tr><td>Prof</td>           <td align=\"right\">%d</td>  <td align=\"right\">%.2f</td></tr>\n",$NB_PROF,$NB_PROF*192); $POTENTIEL_BRUT += $NB_PROF*192;
   printf("<tr><td>ATER</td>           <td align=\"right\">%d</td>  <td align=\"right\">%.2f</td></tr>\n",$NB_ATER,$NB_ATER*192); $POTENTIEL_BRUT += $NB_ATER*192;
   printf("<tr><td>Contrats Doct.</td> <td align=\"right\">%d</td><td align=\"right\">%.2f</td></tr>\n",$NB_CONTRAT_DOCT,$NB_CONTRAT_DOCT*64); $POTENTIEL_BRUT += $NB_CONTRAT_DOCT*64;
   printf("<tr bgcolor=lightgrey><th>Potentiel Brut</th>     <th></th>                    <th align=\"right\">%.2f</th></tr>\n",$POTENTIEL_BRUT);

$REDUCTION_TOT = 0;
   print "<tr bgcolor=lightgrey><th>R&eacute;duction</th><th>Nombre</th><th>NB heures</th></tr>\n";
   print "<tr bgcolor=lightblue><td/><td/><td/></tr>\n";
   printf("<tr><td>Primes et D&eacute;charges</td>  <td></td>                  <td align=\"right\">%.2f</td><td>Hors PetD ISTY et FC</td></tr>\n",$P_ET_D_TOT); $REDUCTION_TOT += $P_ET_D_TOT;
   printf("<tr><td>R&eacute;ductions de Service</td><td></td>                  <td align=\"right\">%.2f</td></tr>\n",$RED_SERV_TOT); $REDUCTION_TOT += $RED_SERV_TOT;
   printf("<tr bgcolor=lightgrey><th>Sous-Total</th><th></th><th align=\"right\">%.2f</th></tr>\n",$REDUCTION_TOT);

$POTENTIEL_NET = $POTENTIEL_BRUT-$REDUCTION_TOT;
   printf("<tr bgcolor=lightgrey><th><font color=\"red\">POTENTIEL NET</font></th><th></th><th align=\"right\"><font color=\"red\">%.2f</font></th></tr>\n",$POTENTIEL_NET);
   print "<tr bgcolor=lightblue><td/><td/><td/></tr>\n";
   print "</table>\n\n";


   print "<h2>Charge</h2>\n";

   print "<table frame=box rules=all>\n";
   print "<tr bgcolor=lightgrey>
              <th rowspan=2>Dipl&ocirc;mes</th>
              <th rowspan=2>Total</th>
              <th colspan=2>UFR Sciences</th>
              <th colspan=2>Autres Composantes</th>
              <th colspan=4>Autres Etablissement</th>
              <th colspan=2 bgcolor=\"lightgrey\">Heures non comptabilis&eacute;es</th>
         </tr>\n";
   print "<tr bgcolor=lightgrey>
              <th>Permanents</th>
              <th>Vacataires</th>
              <th>IUT V&eacute;l.</th>
              <th>ISTY</th>
              <th>IUFM</th>
              <th>ECP</th>
              <th>TSP</th>
              <th>ECTI</th>
              <th bgcolor=\"lightgrey\">Economis&eacute;es</th>
              <th bgcolor=\"lightgrey\">Non Info</th>
        </tr>\n";
  print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/></tr>\n"; 
   $TOT = 0;

   print "<tr>\n\t<td>Licence</td>\n";
   printf("\t<td align=\"right\">%.2f</td>\n",$LICENCE_TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<td align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; print">"; if ($licence[$i]>0) printf("%.2f",$licence[$i]); print "</td>\n"; }
   print "</tr>\n";
   $TOT += $LICENCE_TOT;

   print "<tr>\n\t<td>Master</td>\n";
   printf("\t<td align=\"right\">%.2f</td>\n",$MASTER_TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<td align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; print ">"; if ($master[$i]>0) printf("%.2f",$master[$i]); print "</td>\n"; }
   print "</tr>\n";
   $TOT += $MASTER_TOT;

   print "<tr bgcolor=lightgrey>\n\t<th>Sous-Total Sciences</th>\n";
   printf("\t<th align=\"right\">%.2f</th>\n",$TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<th align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; printf(">%.2f</th>\n",$licence[$i]+$master[$i]); }
   print "</tr>\n";

   $TOT = 0;
   print "<tr>\n\t<td>SSH SJP</td>\n";
   printf("\t<td align=\"right\">%.2f</td>\n",$SSH_SJP_TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<td align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; print">"; if ($ssh_sjp[$i]>0) printf("%.2f",$ssh_sjp[$i]); print "</td>\n"; }
   print "</tr>\n";
   $TOT += $SSH_SJP_TOT;

   print "<tr>\n\t<td>IUT V&eacute;l.</td>\n";
   printf("<td align=\"right\">%.2f</td>\n",$IUT_VEL_TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<td align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; print">"; if ($iut_vel[$i]>0) printf("%.2f",$iut_vel[$i]); print "</td>\n"; }
   print "</tr>\n";
   $TOT += $IUT_VEL_TOT;

   print "<tr>\n\t<td>CPI</td>\n";
   printf("<td align=\"right\">%.2f</td>\n",$CPI_TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<td align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; print">"; if ($cpi[$i]>0) printf("%.2f",$cpi[$i]); print "</td>\n"; }
   print "</tr>\n";
   $TOT += $CPI_TOT;

   print "<tr>\n\t<td>ISTY</td>\n";
   printf("<td align=\"right\">%.2f</td>\n",$ISTY_TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<td align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; print">"; if ($isty[$i]>0) printf("%.2f",$isty[$i]); print "</td>\n"; }
   print "</tr>\n";
   $TOT += $ISTY_TOT;

   print "<tr bgcolor=lightgrey>\n\t<th>Sous-Total Autres Comp.</th>\n";
   printf("<th align=\"right\">%.2f</th>\n",$TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<th align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; printf(">%.2f</th>\n",$ssh_sjp[$i]+$iut_vel[$i]+$cpi[$i]+$isty[$i]); }
   print "</tr>\n";


   $ENS_CHARGE_TOTAL = $ENS_UFR_TOT + $ENS_VAC_TOT + $ENS_VEL_TOT + $ENS_ISTY_TOT + $ENS_IUFM_TOT + $ENS_ECP_TOT + $ENS_TSP_TOT + $ENS_ECTI_TOT; 
   $CHARGE_TOTAL = $LICENCE_TOT + $MASTER_TOT + $SSH_SJP_TOT + $IUT_VEL_TOT + $CPI_TOT + $ISTY_TOT; 
 

   print "<tr bgcolor=lightgrey>\n\t<th><font color=\"red\">CHARGE</font></th>\n";
   if ($CHARGE_TOTAL!=$ENS_CHARGE_TOTAL) printf("\t<th align=\"right\"><font color=\"red\">%.2f/%.2f</font></th>\n",$CHARGE_TOTAL,$ENS_CHARGE_TOTAL);
      else printf("\t<th align=\"right\"><font color=\"red\">%.2f</font></th>\n",$CHARGE_TOTAL);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_UFR_TOT);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_VAC_TOT);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_VEL_TOT);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_ISTY_TOT);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_IUFM_TOT);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_ECP_TOT);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_TSP_TOT);
   printf("\t<th align=\"right\">%.2f</th>\n",$ENS_ECTI_TOT);
   printf("\t<th align=\"right\" bgcolor=\"lightgrey\">%.2f</th>\n",$ENS_HEURES_TOT);
   printf("\t<th align=\"right\" bgcolor=\"lightgrey\">%.2f</th>\n",$ENS_NONINFO_TOT);


 
   print "</tr>\n";
print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/></tr>\n";
   print "</table>\n";

   print "<br>\n";
   print "<table frame=box rules=all>\n";
   print "<tr bgcolor=lightgrey><th colspan=2>Bilan Global</th></tr>";
print "<tr bgcolor=lightblue><td/><td/></tr>\n";
   printf("\t<tr><td>Taux Encadrement Brut = POTENTIEL BRUT / CHARGE</td><td align=\"right\">%.2f</td></tr>\n",(1.0*$POTENTIEL_BRUT)/$CHARGE_TOTAL);
   printf("\t<tr><td>Taux Encadrement Net  = POTENTIEL NET  / CHARGE</td><th align=\"right\"><font color=red>%.2f</font></th></tr>\n",(1.0*$POTENTIEL_NET)/$CHARGE_TOTAL);
   printf("\t<tr><td>&Eacute;conomie TP=2/3TD sur les heures comp. des permanents</td><td align=\"right\">%.2f</td><td>(Pas effectif cette ann&eacute;e)</td></tr>\n",$ENS_deux_tiers_tp);
   printf("\t<tr><td>&Eacute;conomie TP=2/3TD sur les vacataires</td>                <td align=\"right\">%.2f</td></tr>\n",$VAC_deux_tiers_tp);
print "<tr bgcolor=lightblue><td/><td/></tr>\n";
   print "</table>\n\n";
 
   print "<h2>Heures de MIHPS 2</h2>\n";
   print "<table frame=box rules=all>\n";
   print "<tr bgcolor=lightgrey><th>Total MIHPS2</th><th>Perm.</th><th>Vac.</th><th>IUT V&eacute;l.</th><th>ISTY</th><th>IUFM</th><th>ECP</th><th>TSP</th><th>ECTI</th><th>H Econ.</th><th>Non Info</th></tr>\n";
   print "<tr>";
print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/></tr>\n";
   printf("\t<th align=center>%.2f</th>\n",$MIHPS2_TOT);
   for ($i=1; $i<11 ; ++$i) { print "\t<td align=\"right\""; if ($i>8) print " bgcolor=\"lightgrey\""; print">"; if ($mihps2[$i]>0) printf("%.2f",$mihps2[$i]); print "</td>\n"; }
   print "</tr>\n";
print "<tr bgcolor=lightblue><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/><td/></tr>\n";
   print "</table>\n";

mysql_free_result($resu);
mysql_close($link);

?>

