<?php
require_once 'defs.inc';
require_once 'includefunct.php';
require_once 'exportCSVQueries.php';

/**
 * Cette fonction envoie sur la sortie standard le contenu de $result
 * au format CSV.
 * 
 * @param string $etat le nom de l'état
 * @param unknown_type $result le résultat d'une requête
 */
function export_csv($etat, $result) {
	header("Content-type: text/csv; charset=iso-8859-1"); 
	header("Content-disposition: attachment; filename=\"$etat.csv\"");
	
	$num_fields = mysql_num_fields($result);
	
	// La ligne d'entête
	for ($i = 0; $i < $num_fields; ++$i) {
		if ($i > 0) print ",";
	    $field = mysql_fetch_field($result, $i);
	    print "\"$field->name\"";
	}
	print "\n";
	
	while($res = mysql_fetch_row($result)) {
		for ($i = 0; $i < $num_fields; ++$i) {
			if ($i > 0) print ",";
	    	print "\"$res[$i]\"";
		}
		print "\n";
	}
}

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME);
 
$query_function = "get_query_" . $_REQUEST['etat'];
 
$resu = mysql_query( $query_function() )
	or die("SELECT Error: ".mysql_error());

export_csv($_REQUEST['etat'], $resu);

mysql_free_result($resu);
mysql_close($link);

?>



