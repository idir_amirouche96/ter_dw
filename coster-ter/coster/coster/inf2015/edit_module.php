<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 

print "<html>
<head><title>Mise � jour Module</title></head>
<body>";

$id = $_REQUEST["id"];
$pref=$_REQUEST["pref"];
$suf=$_REQUEST["suf"];
$intitule=$_REQUEST["intitule"];
$infos=$_REQUEST["infos"];
$resp=$_REQUEST["resp"];
$go = $_REQUEST["go"];
$delete = $_REQUEST["del"];
$theme = $_REQUEST["theme"];
if ($theme = "") $theme = "INFO";
$semestre = $_REQUEST["semestre"];
$cadre=$_REQUEST["cadre"];
$prime=$_REQUEST["prime"];
if ($prime == "") $prime = 0;

//****
// Utilisation de lock
//****


if ( ($go!="" and $delete=="") ){
    $query="
    SELECT codemod AS id
    FROM modules
    WHERE codemod =".$id. 
    " FOR UPDATE
    ";
    
    $resu = mysql_query($query)
    or die("query is : ".$query." SELECT Error: ".mysql_error());
    
    $res = mysql_fetch_object($resu);
    
    $query="
    UPDATE modules
    SET codeprefixe='".$pref."', codesuffixe='".$suf."', intitule='".$intitule."', informations='".$infos.
                             "', responsable=".$resp.",theme='".$theme."', cadre='".$cadre."', prime=".$prime.
    " WHERE codemod=".$id;
    
    $resu = my_query($query);
    
    $query="
    UPDATE menusemestre
    SET codesemestre =".$semestre." 
    WHERE codemod=".$id;
    
    $resu = my_query($query);
    
    print "<fieldset>
    <legend>
    Mise � jour effectu�e
    </legend>";
    
    print "<a href=bas.php>Retour page principale</a><br/>";
    print "</fieldset><br/>";
} // FIN MISE A JOUR

if ($delete==""){
    print '
    <fieldset>
    <legend>
    Editer Module
    </legend>
    <form action="edit_module.php" method="POST">
    <input type="hidden" name="go" value=1 />
    <input type="hidden" name="id" value="'.$id.
    '" /><table>';
    
    $query="
    SELECT codeprefixe, codesuffixe, intitule, informations, responsable, theme, cadre, prime
    FROM modules
    WHERE codemod=".$id;
    
    $resu = mysql_query($query)
    or die("query is : ".$query." SELECT Error: ".mysql_error());
    
    $res = mysql_fetch_object ($resu);
    $theme = $res->theme;
    
    print '<tr><td>Nom : </td><td><input type="text" name="pref" value="'.$res->codeprefixe.'" size=10 /><input type="text" name="suf" value="'.$res->codesuffixe.'" size=10/></td></tr>';
    
    print '<tr><td>Intitul� : </td><td><input type="text" name="intitule" size=80 value="'.$res->intitule.'" /></td></tr>';
    
    print '<tr><td>Informations : </td><td><textarea type="text" name="infos" rows=10 cols=80>'.$res->informations.'</textarea></td></tr>';
    
    $cadre = $res->cadre;
    $prime = $res->prime;
    $i = $res->responsable;
    
    print "<tr><td>Semestre : </td><td><select name='semestre'>";
    
    // on choppe d'abord le bon semestre
    
    $query="
    SELECT codesemestre
    FROM menusemestre ms
    WHERE ms.codemod =".$id;
    
    $resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
    
    $res=mysql_fetch_object($resu);
    $semestre = $res->codesemestre;
    
    
    $query="
    SELECT codesemestre, nom, anneedebut
    FROM semestres s
    ORDER BY anneedebut, nom
    ";
    
    $resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
    
    while ($res=mysql_fetch_object($resu)){
      print "<option value=$res->codesemestre";
      if ($res->codesemestre == $semestre){
        $k = 1;
        print ' selected="selected" ';
      }
      print ">$res->nom $res->anneedebut</option>";
    }
    
    print"</select></td></tr>";
    
    print "<tr><td>Responsable : </td><td><select name='resp'>";
    
    
    
    $query="
    SELECT nom, prenom, enseignantID
    FROM enseignants
    ORDER BY nom
    ";
    
    $resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
    
    
    while ($res=mysql_fetch_object($resu)){
      print "<option value=$res->enseignantID";
      if ( $i == $res->enseignantID)
        print " selected='selected' ";
      print ">$res->nom $res->prenom</option>";
    }
    
    print "</select></td></tr>";
    
    // CHOIX DU THEME
    
    print "<tr><td>Theme : </td><td><select name='theme'>";
    
    
    
    $query="
    SELECT theme
    FROM themes
    ORDER BY theme
    ";
    
    $resu = mysql_query ($query)
    or die("SELECT Error: ".mysql_error());
    
    
    while ($res=mysql_fetch_object($resu)){
      print "<option value=$res->theme";
      if ( $theme == $res->theme)
        print " selected='selected' ";
      print ">$res->theme</option>";
    }
    
    print "</select></td></tr>";
?>
    <tr>
    	<td>Cadre : </td>
    	<td>
    		<select name="cadre">
    			<option value="A" <?php if ($cadre == 'A') echo "selected='selected'"; ?> >Cadre A</option>
    			<option value="B" <?php if ($cadre == 'B') echo "selected='selected'"; ?> >Cadre B</option>
    		</select>
    	</td>
    </tr>
    
    <tr>
    	<td>Prime ou d�charge : </td>
    	<td>
    		<input type="checkbox" name="prime" value="1" <?php if ($prime != 0) echo "checked='checked'"; ?> />
    	</td>
    </tr>

<?php
    
    
    // DELETE
    
    print "
    <tr><td/><td><input type='submit' value='Mise � jour'/></td></tr>
    </table>
    </form>
    </fieldset>
    <a href=edit_modules.php>Retour � la liste des modules</a>
    </body>
    </html>
    ";
}
    
mysql_close($link);

?>
