<?php
// Utilis� pour le bandeau d'un utilisateur lambda
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

?>

<html>
<head>
	<title>Accueil enseignant</title>
	<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<table align="center" width="100%" height="76" border="" cellpadding="0" cellspacing="0" >
	<tr >
		<td>D�partement <?php echo DPT_NAME; ?> Ann�e <?php echo ANNEE_DEBUT . '/' . ANNEE_FIN; ?></td>
		<td height="48"  colspan="3"><img  src="Images/new_uvsq.gif" height="25" align="right"/></td>
	</tr>

    <tr style="background:url(bar_up.gif)">
        <td>
        	<img src="Images/b_usrcheck.png"/>
            <?php echo '<strong>Bienvenue ' . $_SESSION['name_user'] . '</strong>'; ?>
        </td>
    	<td align="right">
    		<img src="Images/b_home.png" align="middle"/>
    		<a target="mainFrame" href="bas.php"><strong>Accueil</strong></a>
    		<img src="Images/s_passwd.png" align="middle"/>
    		<a target="mainFrame" href="pass_change.php?id=<?php echo $_SESSION['id_user']; ?>"><strong>Changer Mot de Passe</strong></a>
    		<img src="Images/logout.png"  align="middle"/>
    		<a target="_parent" href="deconnecter.php"><strong>D�connecter</strong></a>
		</td>   
    </tr>
	</table>
</body>
</html>
