<?php

/**
 * Récupère le service de tous les enseignants.
 * Cette requête exige que les deux vues ci-dessous soient définies.
 * 
 * CREATE OR REPLACE VIEW v_preservice_detail AS
 * SELECT codemodsemestre, enseignantid, heuresCM AS 'CM', 0.0 AS 'TD', 0.0 AS 'TP'
 *   FROM preserviceCM
 * UNION ALL
 * SELECT codemodsemestre, enseignantid, 0.0 AS 'CM', heuresTD AS 'TD', 0.0 AS 'TP'
 *   FROM preserviceTD
 * UNION ALL
 * SELECT codemodsemestre, enseignantid, 0.0 AS 'CM', 0.0 AS 'TD', heuresTP AS 'TP'
 *   FROM preserviceTP
 * 
 * CREATE OR REPLACE VIEW v_preservice AS
 * SELECT codemodsemestre, enseignantid, SUM(CM) AS 'CM', SUM(TD) AS 'TD', SUM(TP) AS 'TP'
 *   FROM v_preservice_detail
 * GROUP BY codemodsemestre, enseignantid
 */
function get_query_services() {
	return "
SELECT enseignants.nom AS 'Nom', enseignants.prenom AS 'Prenom',
       grades.codecourt AS 'Grade',
       semestres.nom AS 'Semestre',
       modules.codeprefixe AS 'ModPref', modules.codesuffixe AS 'Modsuff', modules.intitule AS 'ModNom',
       v_preservice.CM AS 'CM',
       v_preservice.TD AS 'TD',
       v_preservice.TP AS 'TP'
  FROM enseignants JOIN grades ON enseignants.codegrade = grades.codegrade
        LEFT OUTER JOIN v_preservice ON enseignants.enseignantid = v_preservice.enseignantid
        LEFT OUTER JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
        LEFT OUTER JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
        LEFT OUTER JOIN modules ON menusemestre.codemod = modules.codemod
ORDER BY enseignants.nom ASC, semestres.nom ASC, modules.codeprefixe ASC, modules.codesuffixe ASC
";
}

/**
 * 
 * Récupère le service des enseignants de l'ISTY.
 */
function get_query_servicesISTY() {
	return "
SELECT enseignants.nom AS 'Nom', enseignants.prenom AS 'Prenom',
       grades.codecourt AS 'Grade',
       semestres.nom AS 'Semestre',
       modules.codeprefixe AS 'ModPref', modules.codesuffixe AS 'Modsuff', modules.intitule AS 'ModNom',
       v_preservice.CM AS 'CM',
       v_preservice.TD AS 'TD',
       v_preservice.TP AS 'TP'
  FROM enseignants JOIN grades ON enseignants.codegrade = grades.codegrade
        LEFT OUTER JOIN v_preservice ON enseignants.enseignantid = v_preservice.enseignantid
        LEFT OUTER JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
        LEFT OUTER JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
        LEFT OUTER JOIN modules ON menusemestre.codemod = modules.codemod
 WHERE enseignants.enseignantID IN (
       SELECT inner_e.enseignantID
         FROM enseignants inner_e JOIN grades ON inner_e.codegrade = grades.codegrade
              LEFT OUTER JOIN v_preservice ON inner_e.enseignantid = v_preservice.enseignantid
              LEFT OUTER JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
              LEFT OUTER JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
              LEFT OUTER JOIN modules ON menusemestre.codemod = modules.codemod
        WHERE semestres.nom LIKE 'ISTY%'
           OR modules.codeprefixe = 'ADM-ISTY')
ORDER BY enseignants.nom ASC, semestres.nom ASC, modules.codeprefixe ASC, modules.codesuffixe ASC
";
}

/**
 * 
 * Récupère le service que font les enseignants à l'ISTY (obsoléte).
 * Pour chaque enseignant de l'ISTY, seuls les modules concernant l'ISTY sont retournés.
 */
function get_query_servicesISTY2() {
	return "
SELECT enseignants.nom AS 'Nom', enseignants.prenom AS 'Prenom',
       grades.codecourt AS 'Grade',
       semestres.nom AS 'Semestre',
       modules.codeprefixe AS 'ModPref', modules.codesuffixe AS 'Modsuff', modules.intitule AS 'ModNom',
       v_preservice.CM AS 'CM',
       v_preservice.TD AS 'TD',
       v_preservice.TP AS 'TP'
  FROM enseignants JOIN grades ON enseignants.codegrade = grades.codegrade
        LEFT OUTER JOIN v_preservice ON enseignants.enseignantid = v_preservice.enseignantid
        LEFT OUTER JOIN menusemestre ON v_preservice.codemodsemestre = menusemestre.codemodsemestre
        LEFT OUTER JOIN semestres ON menusemestre.codesemestre = semestres.codesemestre
        LEFT OUTER JOIN modules ON menusemestre.codemod = modules.codemod
 WHERE semestres.nom LIKE 'ISTY%'
    OR modules.codeprefixe = 'ADM-ISTY'
ORDER BY enseignants.nom ASC, semestres.nom ASC, modules.codeprefixe ASC, modules.codesuffixe ASC
";
}

?>
