<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

?>

<html >
<head>
	<title>Untitled Document</title>
	<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="171" border="1" cellspacing="0" cellpadding="0">

  <tr><td width="167" background="Images/back.gif"><font color="#0000FF"><strong>Semestre</strong></font></td></tr>
      <tr><td><a target="mainFrame" href="new_semestre.php?annee&nom&dept&resp">Nouveau</a></td></tr>
      <tr><td><a target="mainFrame" href="delete_semestre.php?id">Supprimer</a></td></tr>

  <tr><td background="Images/back.gif"><font color="#0000FF"><strong>Module</strong></font></td></tr>
      <tr><td><a target="mainFrame" href="new_module.php?pref&suf&intitule&infos&resp">Nouveau</a></td></tr>
      <tr><td><a target="mainFrame" href="edit_modules.php?login&dtl">Modifier</a></td></tr>
      <tr><td><a target="mainFrame" href="delete_module.php?id&cat=IN">Supprimer</a> </td></tr>
      <tr><td height="2"><a  href="affiche_service.php?type=horaires&dtl&ens&annee&ctg=L1 S1&sem&codeens=<?php echo $_SESSION['id_user'];?>"  target="mainFrame">S'inscrire et infos</a></td></tr>
  
  <tr><td background="Images/back.gif"><font color="#0000FF"><strong>Enseignant</strong></font></td></tr>
      <tr><td><a target="mainFrame" href="new_enseignant.php?nom&prenom&grade&dept">Nouveau</a></td></tr>
      <tr><td><a target="mainFrame" href="edit_enseignant.php">Modifier</a></td></tr>
      <tr><td><a target="mainFrame" href="delete_enseignant.php?id&car=A"> Supprimer</a></td></tr>
   
  <tr><td background="Images/back.gif"><font color="#0000FF"><strong>Bilan</strong></font></td></tr>
      <tr><td><a target="mainFrame" href="affiche_service.php?type=enseignant&annee&sem&codeens&ctg&dtl&ens"> Enseignant </a></td></tr>
      <tr><td><a target="mainFrame" href="bilanParSemestre.php"> Semestres </a></td></tr>
      <tr><td><a target="mainFrame" href="new_modulesemestreV1.php?annee&dtl">Module</a></td></tr>
      <tr><td><a target="mainFrame" href="affiche_service.php?dtl&ens=ok&type=enseignant&annee&ctg&sem&codprof&codeens=<?php echo $_SESSION['id_user'] ?>">Bilan Personnel</a></td></tr>
      <tr><td><a target="mainFrame" href="listExportCSV.php"> Export CSV </a></td></tr>

  <tr><td background="Images/back.gif"><font color="#0000FF"><strong>Export</strong></font></td></tr>
      <tr><td ><a target="mainFrame" href="pdf/affiche_feuille_heures.php">Etats de Service en pdf</a></td></tr>
      <tr><td ><a target="mainFrame" href="email.php">Email</a></td></tr>
        
  <tr><td background="Images/back.gif"><font color="#0000FF"><strong>Statistiques</strong></font></td></tr>
      <tr><td><a target="mainFrame" href="charge.php">R&eacute;partition de la Charge</td></tr>

</table>
</body>
</html>
