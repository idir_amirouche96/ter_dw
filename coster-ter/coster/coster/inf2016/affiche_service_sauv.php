<?php
require_once 'defs.inc';
require_once 'includefunct.php';

session_start();

if (!isset($_SESSION['valid_user'])) {
    js_redirect('index.php');
}

if (isset($_REQUEST['pass'])) {
    // TODO : pourquoi stocker le mot de passe dans un cookie ?
    $pass = $_REQUEST['pass'];
    if ($pass != '') setcookie('passw', $pass);
} else {
    $pass = '';
}

$type = $_REQUEST["type"];
$codeens = $_REQUEST["codeens"]; // Id de l'enseignant pour lequel on veut l'information
$iduser = $_SESSION['id_user']; // Id de l'utilisateur connect�
$semestre = $_REQUEST["sem"];

$annee = (isset($_REQUEST['annee']) && $_REQUEST['annee'] != "") ? $_REQUEST["annee"] : ANNEE_DEBUT;
?>

<html>
<head>
	<title>Service par <?php echo $type; ?></title>
	<link rel="stylesheet" type="text/css" href="style-afficher-horaires.css"/>
</head>
<body>
<?php

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 


//********************************
// PARTIE AFFICHAGE PAR ENSEIGNANT
// *******************************

// Toutes les heures d'un enseignant sp�cifique
if ($codeens != '' and $type == 'enseignant'){
	$enseignant = selectEnseignantById($codeens, $link); 
	echo '<h1>Service de ' . $enseignant->prenom . ' ' . $enseignant->nom . '</h1>';

	$service = selectServiceEnseignant($codeens, $link);
	
	print '<h2>D&eacute;tail</h2>';
	print '<h3>Enseignements</h3>';
    displayServiceEnseignant($enseignant, $service["faites"], $service["bilan"]["effectuees"], $service["bilan"]["hRedTP"]);
    
	print '<h3>Primes et D&eacute;charges</h3>';
    displayServiceEnseignant($enseignant, $service["primes"], $service["bilan"]["primes"], 0);
	
	print '<h3>R&eacute;ductions de Services</h3>';
    displayServiceEnseignant($enseignant, $service["reductions"], $service["bilan"]["reductions"], 0);
	
    print '<h2>Bilan</h2>';

    print '<h3>Service d&ucirc; (' . $enseignant->grade . ')</h3>';
    print '<table border=1>';
    print '<tr>';
    print '<td>Service statutaire</td>';
    print '<td style="text-align: right">' . number_format($enseignant->servicedu, 2) . '</td>';
    print '</tr>';
    print '<tr>';
    print '<td>R&eacute;ductions de Service</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["reductions"], 2) . '</td>';
    print '</tr>';
    $serviceDu = $enseignant->servicedu - $service["bilan"]["reductions"];
    print '<tr>';
    print '<td>Service d&ucirc;</td>';
    print '<td style="text-align: right">' . number_format($serviceDu, 2) . '</td>';
    print '</tr>';
    print '</table>';
    
    print '<h3>Service effectu&eacute;</h3>';
    print '<table border=1>';
    print '<tr>';
    print '<td>Heures effectu&eacute;es</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["effectuees"], 2) . '</td>';
    print '</tr>';
    print '<tr>';
    print '<td>Primes et D&eacute;charges</td>';
    print '<td style="text-align: right">' . number_format($service["bilan"]["primes"], 2) . '</td>';
    print '</tr>';
    print '</table>';
    
    $serviceEffectue = $service["bilan"]["effectuees"] + $service["bilan"]["primes"];
    if ($serviceEffectue > $serviceDu) {
    	$decharge = max($serviceDu - $service["bilan"]["effectuees"], 0);
    	$primeAPayer = $service["bilan"]["primes"] - $decharge;
    	$heuresSupp = max($service["bilan"]["effectuees"] - $serviceDu, 0);
	    print '<h3>Paiement</h3>';
	    print '<table border=1>';
	    print '<tr>';
	    print '<td>D&eacute;charge</td>';
	    print '<td style="text-align: right">' . number_format($decharge, 2) . '</td>';
	    print '</tr>';
	    print '<tr>';
	    print '<td>Heures comp. &agrave; payer</td>';
	    print '<td style="text-align: right">' . number_format($heuresSupp, 2) . '</td>';
	    print '</tr>';
	    print '<tr>';
	    print '<td>Prime &agrave; payer</td>';
	    print '<td style="text-align: right">' . number_format($primeAPayer, 2) . '</td>';
	    print '</tr>';
	    print '</table>';
    } else {
	    print '<h3>Reste &agrave; effectuer</h3>';
	    print '<table border=1>';
	    print '<tr>';
	    print '<td>Heures restant &agrave; effectuer</td>';
	    print '<td style=\"text-align: right\">' . number_format($serviceDu - $serviceEffectue, 2) . '</td>';
	    print '</tr>';
	    print '</table>';
    }   

	print "<h2>Modules dont vous avez la responsabilit�</h2>";
    $query = 'SELECT m.codemod, codeprefixe, codesuffixe, intitule, s.nom, codemodsemestre, verrou, verrouDPT
                FROM modules as m, semestres as s, menusemestre as ms
               WHERE m.codemod = ms.codemod
                 AND s.codesemestre = ms.codesemestre
                 AND m.responsable = ' . $codeens;
    
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());
    
    if (mysql_num_rows($result) > 0) {
	    print "<table border=1>";
            print "<tr><th rowspan=2>Semestre</th><th rowspan=2>Code UE</th rowspan=2><th rowspan=2>Nom UE</th><th colspan=2>Verrou</th></tr>";
            print "<tr><th>Resp. UE</th><th>DPT</th></tr>";
	    while($row = mysql_fetch_object($result)){
	        print "<tr><td>$row->nom</td><td>$row->codeprefixe $row->codesuffixe</td>";
	        // On peut modifier uniquement si on est propr�taire du service
	        // ou si on est admin.
	        if ($codeens == $iduser || 'ADMIN' == $_SESSION['prvg']) {
	            print "<td><a href=edit_service.php?id=$row->codemodsemestre&login=$codeens&sem=$semestre>$row->intitule</a></td>";
	            }
                    else print "<td>$row->intitule</td>";
                
	        print (!$row->verrou)    ? "<td align=center style=background:lightgreen>Non verrouill&eacute;</td>" : "<td align=center style=background:orange>Verrouill&eacute;</td>"; 
	        print (!$row->verrouDPT) ? "<td align=center style=background:lightgreen>Non verrouill&eacute;</td>" : "<td align=center style=background:orange>Verrouill&eacute;</td>"; 
	        print"</tr>";
	    }
	    print "</table>";
    } else {
    	print "Aucun.";
    }
    
	print "<h2>Semestres dont vous avez la responsabilit&eacute;</h2>";
    $query = 'SELECT nom
                FROM semestres
               WHERE responsable = ' . $codeens . '
            ORDER BY nom ASC';
    
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());
    
    if (mysql_num_rows($result) > 0) {
	    print "<table border=1>";
	    while($row = mysql_fetch_object($result)){
	        print "<tr><td>$row->nom</td></tr>";
	    }
	    print "</table>";
    } else {
    	print "Aucun.";
    }
} // Fin PARTIE AFFICHAGE PAR ENSEIGNANT


//*************************************
// PARTIE HEURES TOTALES PAR ENSEIGNANT
//*************************************
if($codeens=="" and $type=="enseignant"){
    print "<h1>Total des heures du D&eacute;partement</h1>";
    $services = selectServiceEnseignants($link);
	displayServiceEnseignants($services);
} // FIN PARTIE HEURES TOTALES PAR ENSEIGNANT

// **********************************
// HORAIRES
// Appel� par le menu "Infos Modules" quand un enseignant est connect�
//***********************************
if ($type == "horaires"){
    //***************
    // On lance la requ�te
    //***************
    $query = "SELECT inti, verrou, verrouDPT, cms, anneedebut, nom, codeprefixe as pref, codesuffixe as suf, codesem, 
                     (SUM(HCM)*SUM(GCM)) as heuresCMPREV, SUM(HPCM) as heuresCMFAIT,
                     (SUM(HTD)*SUM(GTD)) as heuresTDPREV, SUM(HPTD) as heuresTDFAIT,
                     (SUM(HCM)*SUM(GCM)*1.5) as CMeqTDPREV, SUM(HPCM)*1.5 as CMeqTDFAIT,
                     (SUM(HTP)*SUM(GTP)) as heuresTPPREV, SUM(HPTP) as heuresTPFAIT,
                     (SUM(HCM)*SUM(GCM)*1.5+SUM(HTD)*SUM(GTD)+SUM(HTP)*SUM(GTP)) as TOTPREV,
                     (SUM(HPCM)*1.5+SUM(HPTD)+SUM(HPTP)) as TOTFAIT,
                     ( (SUM(HCM)*SUM(GCM)*1.5+SUM(HTD)*SUM(GTD)+SUM(HTP)*SUM(GTP))- (SUM(HPCM)*1.5+SUM(HPTD)+SUM(HPTP)) ) as bilan
                FROM (SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                             0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                             TP.heuresTP as HTP, 0 as HPTP, TP.nombregroupes as GTP,
                             m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                       FROM modules as m, menusemestre as s, semestres as sem, horairesTP as TP
                      WHERE s.codesemestre = sem.codesemestre
                        AND s.codemod = m.codemod
                        AND s.codemodsemestre = TP.codemodsemestre
                        AND sem.anneedebut = ".$annee."
                   GROUP BY s.codemodsemestre
                      UNION
                     SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                            0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD, 0 as HTP,
                            SUM(PTP.heuresTP) as HPTP, 0 as GTP,
                            m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                       FROM modules as m, menusemestre as s, semestres as sem, preserviceTP as PTP
                      WHERE s.codesemestre = sem.codesemestre
                        AND s.codemod = m.codemod
                        AND s.codemodsemestre = PTP.codemodsemestre
                        AND sem.anneedebut = ".$annee."
                   GROUP BY s.codemodsemestre
                      UNION
                     SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                            0 as HCM, 0 as HPCM, 0 as GCM, TD.heuresTD as HTD, 0 as HPTD, TD.nombregroupes as GTD,
                            0 as HTP, 0 as HPTP, 0 as GTP,
                            m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, horairesTD as TD
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = TD.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                           CM.heuresCM as HCM, 0 as HPCM, CM.nombregroupes as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, horairesCM as CM
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = CM.codemodsemestre
                       AND sem.anneedebut = ".$annee." 
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom,  codeprefixe, codesuffixe,
                           0 as HCM, SUM(PCM.heuresCM) as HPCM, 0 as GCM, 0 as HTD, 0 as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, preserviceCM as PCM
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = PCM.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     UNION
                    SELECT s.codemodsemestre as cms, anneedebut, sem.nom as nom, codeprefixe, codesuffixe,
                           0 as HCM, 0 as HPCM, 0 as GCM, 0 as HTD, SUM(PTD.heuresTD) as HPTD, 0 as GTD,
                           0 as HTP, 0 as HPTP, 0 as GTP,
                           m.intitule as inti, sem.codesemestre as codesem, s.verrou, s.verrouDPT
                      FROM modules as m, menusemestre as s, semestres as sem, preserviceTD as PTD
                     WHERE s.codesemestre = sem.codesemestre
                       AND s.codemod = m.codemod
                       AND s.codemodsemestre = PTD.codemodsemestre
                       AND sem.anneedebut = ".$annee."
                  GROUP BY s.codemodsemestre
                     ) as sub1
            GROUP BY cms
            ORDER BY nom, pref, suf";

// On va construire plusieurs Tableaux

//print "<table border=1>
//<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td>Module</td><td>Total heures CM pr�vues</td><td>Total heures CM faites</td><td>Total heures CM (eq.TD) pr�vues</td><td>Total heures CM (eq. TD) Faites</td><td>Total heures TD Pr�vues</td><td>Total heures TD faites</td><td>Total heures TP pr�vues</td><td>Total heures TP faites</td><td>Total heures eq. TD Pr�vues</td><td>Total heures eq. TD Faites</td><td>BILAN</td><td></td></tr>";
$TOTPREV=0;
$TOTFAIT=0;
$listed = 0;
    $result = mysql_query ($query)
        or die("QUERY:".$query."SELECT Error: ".mysql_error());

    $oldletter = "";
    $oldsem = "";
    while($row = mysql_fetch_object($result)){
        // Affichage d'un trait horizontal quand on change d'initiale de semestre
        if ($oldletter != substr($row->nom, 0, 1)) {
           print'<hr/>';
           $oldletter = substr($row->nom, 0, 1);
           }
        $TOTPREV += $row->TOTPREV;
        $TOTFAIT += $row->TOTFAIT;

        //On teste si on commence un nouveau semestre
	if ($oldsem != $row->nom) {
                $oldsem = $row->nom;
		// CAS 1 : Le semestre qui vient de se terminer est celui ou l'on liste les UE (et le suivant ne l'est donc forcement pas
                if ($listed) { print "<tr><th colspan=5>Total</th><th align=right>".number_format($TOTPREV,2)."</th><th align=right>".number_format($TOTFAIT,2)."</th></tr>";
                               print "</table>";
                               $listed=0;  // On n'est plus dans le semestre dont on a liste les UE
                             }
                    else  { // CAS 2 : Le semestre qui se termine n'est pas celui listé
                            print "<a name=#$row->codesem><a href=affiche_service.php?pass=$pass&type=horaires&codeens=$codeens&sem=$row->codesem>$row->nom</a>";
                            print " Heures &agrave; Faire=$TOTPREV";
                            print " Heures Faites=$TOTFAIT<br>";
                            if ($semestre == $row->codesem) { // CAS 2.1 : On commence le semestre dont on doit lister les UE
		                $listed=1;
                	        print "<table border=1>";
                	        print "<tr>".
                                           "<th colspan=2>$row->nom</th>".
                                           "<th colspan=4>Heures &agrave; Faire</th>".
                                           "<th rowspan=2>Heures<br>Faites</th>".
                                           "<th rowspan=2>Bilan</th>".
                                           "<th colspan=2>Verrous</th>".
                                      "</tr>";
                                print "<tr>".
                                           "<th>Code UE</th>".
                                           "<th>Nom UE</th>".
                                           "<th>CM</th><th>TD</th><th>TP</th><th>Total</th>".
                                           "<th>Resp. UE</th><th>DPT</th>".
                                     "</tr>";
                               }
                          } 
                 $TOTPREV=0;
                 $TOTFAIT=0;
                 }
    
        if ( $semestre == $row->codesem) {
            print "<tr>".
                       "<td><a href=edit_service.php?id=$row->cms&login=$codeens&sem=$semestre>$row->pref $row->suf</a></td>".
                       "<td><a href=edit_service.php?id=$row->cms&login=$codeens&sem=$semestre>$row->inti</a></td>".
                       "<td align=right>".number_format($row->heuresCMPREV,0)."</td>".
                       //"<td>$row->heuresCMFAIT</td>.
                       //"<td>$row->CMeqTDPREV</td>.
                       //"<td>$row->CMeqTDFAIT</td>.
                       "<td align=right>".number_format($row->heuresTDPREV,0)."</td>".
                       //"<td>$row->heuresTDFAIT</td>".
                       "<td align=right>".number_format($row->heuresTPPREV,0)."</td>".
                       //"<td>$row->heuresTPFAIT</td>.
                       "<td align=right>".number_format($row->TOTPREV,2)."</td>".
                       "<td align=right>".number_format($row->TOTFAIT,2)."</td>";
           print "<td align=right style=background:"; 
           if ($row->bilan>0) print "LightSalmon>";
               else if ($row->bilan<0) print "LightBlue>+";
                       else print "lightgreen>";
           print number_format(-$row->bilan,2)."</td>";
            
           if ($row->verrou) print "<td align=center style=background:orange>Verrouill&eacute;</td>";
              else print "<td align=center style=background:lightgreen>Non Verrouill&eacute;</td>";
            if ($row->verrouDPT) print "<td align=center style=background:orange>Verrouill&eacute;</td>";
              else print "<td align=center style=background:lightgreen>Non Verrouill&eacute;</td>";
            print "</tr>\n";
        }

    } // Fin while
print "</table>\n";
} // FIN HORAIRES

//print '</table>';
mysql_close($link);
?>

</body>
</html>
