<?php
require_once '../defs.inc';
require_once '../includefunct.php';
require_once 'fpdf.php';
require_once 'pdf_funct.php';

session_start();

$link = mysql_connect (DB_HOST, DB_LOGIN, DB_PASS)
    or die ('I cannot connect to the database because: ' . mysql_error());
mysql_select_db (DB_NAME); 

//*******************
// Affichage de tous les enseignants et sortie pdf
//*******************

// R�cup�rer tous les enseignants qui ont enseign� une ann�e donn�e.
// Ici on prend simplement tous les enseignants de la base.

$query = 'SELECT MAX(enseignantID) as maxcodeens FROM enseignants';
$result = mysql_query($query)
    or die("SELECT Error: ".mysql_error());
$row = mysql_fetch_object($result);
$maxcodeens = $row->maxcodeens;

// Initialisation du PDF
$pdf = new FPDF(); 
$pdf->SetFont('Arial', '', 14); 

// Toutes les heures d'un enseignant sp�cifique
$query = 'SELECT e.enseignantID as ID
            FROM enseignants e
        ORDER BY e.nom, e.prenom';
$result = mysql_query($query)
    or die("SELECT Error: ".mysql_error());

while ($row = mysql_fetch_object($result)) {
    pdf_service_enseignant($pdf, $row->ID);
}

mysql_close($link);
$pdf->Output(); 

?>
