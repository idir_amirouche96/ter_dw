CREATE TABLE `DIM_ENS_DPT` (
  `codedept` int(10) NOT NULL DEFAULT '0',
  `DIRECTEUR_DPT` int(10) NOT NULL DEFAULT '0',
  `enseignantID` int(10) NOT NULL DEFAULT '0',
  `NOM_PRENOM_ENS` varchar(100) NOT NULL,
  `codegrade` int(10) NOT NULL DEFAULT '0',
  `codecourt` varchar(10) NOT NULL,
  `nomlong` varchar(100) NOT NULL,
  `type` varchar(32) NOT NULL,
  `heures` int(10) NOT NULL DEFAULT '0',
  `ANNNE_ENS` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codedept`,`enseignantID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `DIM_MODULE` (
  `codesemestre` int(10) NOT NULL DEFAULT '0',
  `codemodsemestre` int(10) NOT NULL DEFAULT '0',
  `codeprefixe` varchar(10) NOT NULL,
  `intitule` varchar(100) NOT NULL,
  `CM_A_FAIRE` decimal(11,2) NOT NULL DEFAULT '0.00',
  `TD_A_FAIRE` decimal(11,2) NOT NULL DEFAULT '0.00',
  `TP_A_FAIRE` decimal(11,2) NOT NULL DEFAULT '0.00',
  `HEURES_A_FAIRE` double NOT NULL,
  `CM_FAITES` decimal(11,2) NOT NULL DEFAULT '0.00',
  `TD_FAITES` decimal(11,2) NOT NULL DEFAULT '0.00',
  `TP_FAITES` decimal(11,2) NOT NULL DEFAULT '0.00',
  `HEURES_FAITES` double NOT NULL,
  `BILAN_CM` decimal(11,2) NOT NULL DEFAULT '0.00',
  `BILAN_TD` decimal(11,2) NOT NULL DEFAULT '0.00',
  `BILAN_TP` decimal(11,2) NOT NULL DEFAULT '0.00',
  `BILAN_UE` double NOT NULL,
  `anneedebut` int(10) NOT NULL DEFAULT '0',
  `ID_RESP_MOD` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codesemestre`,`codemodsemestre`,`anneedebut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `DIM_MODULE_DETAILS` (
  `codemodsemestre` int(10) NOT NULL DEFAULT '0',
  `enseignantID` int(10) NOT NULL DEFAULT '0',
  `heuresCM` decimal(11,2) NOT NULL DEFAULT '0.00',
  `heuresTD` decimal(11,2) NOT NULL DEFAULT '0.00',
  `heuresTP` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`codemodsemestre`,`enseignantID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `DIM_SEMESTRE` (
  `codesemestre` int(10) NOT NULL DEFAULT '0',
  `anneedebut` int(10) NOT NULL DEFAULT '0',
  `nom` varchar(100) NOT NULL,
  `responsable` int(10) NOT NULL DEFAULT '0',
  `departement` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codesemestre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `DIM_TEMPS` (
  `SK_DIM_TEMPS` int(11) NOT NULL,
  `anneedebut` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SK_DIM_TEMPS`,`anneedebut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


