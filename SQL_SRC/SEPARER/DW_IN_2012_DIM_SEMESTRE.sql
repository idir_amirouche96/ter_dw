-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: DW_IN_2012
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DIM_SEMESTRE`
--

DROP TABLE IF EXISTS `DIM_SEMESTRE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DIM_SEMESTRE` (
  `codesemestre` int(10) NOT NULL DEFAULT '0',
  `anneedebut` int(10) NOT NULL DEFAULT '0',
  `nom` varchar(100) NOT NULL,
  `responsable` int(10) NOT NULL DEFAULT '0',
  `departement` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codesemestre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DIM_SEMESTRE`
--

LOCK TABLES `DIM_SEMESTRE` WRITE;
/*!40000 ALTER TABLE `DIM_SEMESTRE` DISABLE KEYS */;
INSERT INTO `DIM_SEMESTRE` VALUES (1,2011,'L1 S1',81,1),(2,2011,'L1 S2',81,1),(3,2011,'L2 S3',81,1),(4,2011,'L2 S4',81,1),(5,2011,'L3 S5',59,1),(6,2011,'L3 S6',59,1),(7,2011,'M1 Info. S1',69,1),(8,2011,'M1 Info. S2',69,1),(9,2011,'M2P IRS S3',19,1),(10,2011,'M2P ACSIS S4',39,1),(11,2011,'M2R CoSy S4',3,1),(12,2011,'ISTY Info 1 S1',48,1),(13,2011,'ISTY Info 2 S3',36,1),(19,2011,'ISTY CPI',98,1),(20,2011,'M2P SeCReTS S4',47,1),(26,2011,'Admin -- Prime et Décharge',81,1),(33,2011,'UFR SSH',81,1),(34,2011,'UFR Sciences',81,1),(35,2011,'UFR SJP',81,1),(41,2011,'Admin -- Réduction de Service',81,1),(42,2011,'M2P ACSIS S3',39,1),(44,2011,'M2P SeCReTS S3',47,1),(45,2011,'M2R CoSy S3',3,1),(46,2011,'ISTY Info 2 S4',36,1),(47,2011,'M2P IRS S4',19,1),(48,2011,'ISTY Info 1 S2',48,1),(49,2011,'ISTY Info 3 S6',186,1),(50,2011,'ISTY Info 3 S5',186,1),(51,2011,'UFR-IUT Vélizy',81,1),(52,2011,'M1 IHP S1',35,1),(53,2011,'M1 IHP S2',35,1),(54,2011,'M2PR IHP S3',35,1),(55,2011,'Formation Continue',81,1),(56,2011,'M2PR IHP S4',35,1),(57,2011,'OVSQ',81,1);
/*!40000 ALTER TABLE `DIM_SEMESTRE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-30 14:27:02
