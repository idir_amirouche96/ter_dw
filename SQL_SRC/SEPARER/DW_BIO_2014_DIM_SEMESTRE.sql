-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: DW_BIO_2014
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DIM_SEMESTRE`
--

DROP TABLE IF EXISTS `DIM_SEMESTRE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DIM_SEMESTRE` (
  `codesemestre` int(10) NOT NULL DEFAULT '0',
  `anneedebut` int(10) NOT NULL DEFAULT '0',
  `nom` varchar(100) NOT NULL,
  `responsable` int(10) NOT NULL DEFAULT '0',
  `departement` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codesemestre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DIM_SEMESTRE`
--

LOCK TABLES `DIM_SEMESTRE` WRITE;
/*!40000 ALTER TABLE `DIM_SEMESTRE` DISABLE KEYS */;
INSERT INTO `DIM_SEMESTRE` VALUES (3,2014,'LSTS1 Semestre 1 1° année licence',20,2),(4,2014,'LBS3 Semestre 1 2° année licence',3,2),(5,2014,'LBS5 Semestre 1 3° année licence',20,2),(6,2014,'LSTS2 Semestre 2 1° année licence',20,2),(7,2014,'LBS4 Semestre 2 2° année licence',20,2),(8,2014,'LBS6 Semestre 2 3° année licence',20,2),(9,2014,'LBPS1 Semestre 1  licence Pro Biotechnologie',20,2),(10,2014,'LBPS2 Semestre 2  licence Pro Biotechnologie',20,2),(11,2014,'LCPS1 Semestre 1  licence Pro Eau',20,2),(12,2014,'MBS1 Semestre 1 1° année master',20,2),(13,2014,'MBS2 Semestre 2 1° année master',20,2),(14,2014,'P1S1 Semestre 1 PACES',20,2),(15,2014,'MBS3 Semestre 1 2° année master',20,2),(16,2014,'MBS4 Semestre 2 2° année master',20,2),(19,2014,'P1S2 Semestre 2 mise à niveau P1',20,2),(20,2014,'MSMPN1',11,2),(21,2014,'MNSPN 2',11,2),(22,2014,'MISP2',24,2),(23,2014,'MSMPN3',11,2),(24,2014,'MSMPN4',11,2),(25,2014,'CEDS1',24,2),(26,2014,'MSMPN3',11,2);
/*!40000 ALTER TABLE `DIM_SEMESTRE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-30 14:26:57
