-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: DW_IN_2014
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DIM_ENS_DPT`
--

DROP TABLE IF EXISTS `DIM_ENS_DPT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DIM_ENS_DPT` (
  `codedept` int(10) NOT NULL DEFAULT '0',
  `DIRECTEUR_DPT` int(10) NOT NULL DEFAULT '0',
  `enseignantID` int(10) NOT NULL DEFAULT '0',
  `NOM_PRENOM_ENS` varchar(100) NOT NULL,
  `codegrade` int(10) NOT NULL DEFAULT '0',
  `codecourt` varchar(10) NOT NULL,
  `nomlong` varchar(100) NOT NULL,
  `type` varchar(32) NOT NULL,
  `heures` int(10) NOT NULL DEFAULT '0',
  `ANNNE_ENS` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codedept`,`enseignantID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DIM_ENS_DPT`
--

LOCK TABLES `DIM_ENS_DPT` WRITE;
/*!40000 ALTER TABLE `DIM_ENS_DPT` DISABLE KEYS */;
INSERT INTO `DIM_ENS_DPT` VALUES (1,81,3,'Barth Dominique',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,14,'Bouzeghoub Mokrane',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,19,'Claudé Jean-Pierre',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,35,'Emad-Petiton Nahid',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013),(1,81,38,'Finance Béatrice',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,39,'Fourneau Jean-Michel',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,44,'Gardy Danièle',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,47,'Goubin Louis',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,49,'Gueroui Mourad',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013),(1,81,54,'Jalby William',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,58,'Kedad-Cointot Zoubida',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,59,'Kloul Leïla',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,61,'Le Cun Bertrand',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,63,'Lopes Stéphane',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,69,'Mautor Thierry',24,'EC-IUFM','Enseignant-Chercheur IUFM','permanent',0,2013),(1,81,73,'Nguyen Benjamin',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,76,'Patarin Jacques',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,80,'Pucheral Philippe',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,81,'Quessette Franck',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,87,'Sayous Jean-Guy',20,'PAST-ISTY','PAST ISTY','contractuel',0,2013),(1,81,90,'Tohmé Samir',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,94,'Tseveendorj Ider',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,98,'Yeh Laurent',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,100,'Zeitouni Karine',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,101,'Zertal Soraya',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,112,'Quisquater Michael',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,152,'Vial Sandrine',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,169,'HEURES EN TROP',27,'Heures','Heures','vacataire',0,2013),(1,81,170,'HEURES ECONOMISEES',27,'Heures','Heures','vacataire',0,2013),(1,81,186,'Khawam Kinda',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,247,'Bui Alain',1,'PR-info','Professeur du Dpt Info','permanent',192,2013),(1,81,253,'Pilard Laurence',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,256,'Sohier Devan',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,265,'Blazevic Pierre',13,'ADM','Administratif','permanent',0,2013),(1,81,276,'Chevalier Fabienne',13,'ADM','Administratif','permanent',0,2013),(1,81,292,'David Andrée',13,'ADM','Administratif','permanent',0,2013),(1,81,302,' DEPARTEMENT INFO',13,'ADM','Administratif','permanent',0,2013),(1,81,336,'Marinca Dana',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,365,'Preda Nicoleta',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,371,'Gama Nicolas',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,377,'Mitton Dominique',13,'ADM','Administratif','permanent',0,2013),(1,81,439,'Hugel Vincent',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,442,'Fages Christian',20,'PAST-ISTY','PAST ISTY','contractuel',0,2013),(1,81,465,'Cardot Eric',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,474,'Quilio Isabelle',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,480,'Sandu-Popa Iulian',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,483,'De Feo Luca',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,485,'Garcia Thierry',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,569,'Ramdame-Cherif Amar',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,577,'HEURES DPT ANGLAIS',28,'Non info.','Non info.','vacataire',0,2013),(1,81,589,'Ben Amor Soufiane',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,591,'Calcado Fabien',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013),(1,81,592,'Hoguin Fabrice',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,593,'Martel Yves',19,'EC-UFR','Enseignant-Chercheur UFR Sciences','permanent',0,2013),(1,81,594,'Boulmezaoud Tahar Zamene',19,'EC-UFR','Enseignant-Chercheur UFR Sciences','permanent',0,2013),(1,81,601,'Llop Xavier',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,604,'Strozecki Yann',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,605,'Coucheney Pierre',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,606,'De Oliveira Castro Pablo',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,607,'Taher Yehia',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,614,'Khemiri-Kallel Sondes',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,682,'ECP Centrale',26,'EC-ECP','Enseignant-chercheur Ecole Centrale Paris','vacataire',0,2013),(1,81,698,'ECTI ecti',25,'ECTI','ECTI','vacataire',0,2013),(1,81,699,'Telecom Sud-Paris',13,'ADM','Administratif','permanent',0,2013),(1,81,702,'OSERET Emmanuel',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,703,'Boura Christina',2,'MCF-info','Maître de Conférences du Dpt Info','permanent',192,2013),(1,81,705,'Boudard Mélanie',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,706,'Cogliati Benoit-Michel',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,707,'Hugounenq Cyril',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,708,'Huynh (mon) Kim Tam',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,710,'Ouksili Hanane',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,712,'Rodier Lise',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,713,'Vial Prado Francisco José',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,714,'Huynh (ater) Kim Tam',17,'ATER-6mois','ATER 6 mois','contractuel',96,2013),(1,81,715,'Dufaud Thomas',23,'EC-IUT-Vel','Enseignant-Chercheur IUT-Vélizy','permanent',0,2013),(1,81,716,'Guerard Guillaume',5,'DOCT-Ens','Contrat doct. avec act. d\'ens.','contractuel',64,2013),(1,81,717,'Benamara Lamia',17,'ATER-6mois','ATER 6 mois','contractuel',96,2013),(1,81,718,'MENOUER Tarek',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,719,'ISTY 2 Vacataire',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013),(1,81,720,'BERRADA Karima',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,721,'PETIT Eric',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,722,'YE Fan',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,723,'BESNARD Jean-Baptiste',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,724,'BENDIFALLAH  Zakaria ',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,725,'DANG Florian',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,726,'IZRI Nora',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,727,'BOUCHEMAL Naila',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,728,'COUCHOT Alain',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,729,'CHRISTOFIS Maria',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,873,'PERACHE Marc',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,874,'CARRIBAULT Patrick',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,875,'HEURES MUTUALISEES',27,'Heures','Heures','vacataire',0,2013),(1,81,876,'ISTY CPI',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013),(1,81,877,'BROCHARD Luigi',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,878,'PETITON Serge',6,'VAC-Pub','Vacataire du Public','vacataire',0,2013),(1,81,879,'ISTY 1 Vacataire',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013),(1,81,880,'ISTY 3 Vacataire',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013),(1,81,881,'NOUDOHOUENOU José',22,'EC-ISTY','Enseignant-Chercheur ISTY','permanent',0,2013);
/*!40000 ALTER TABLE `DIM_ENS_DPT` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-30 14:27:00
